const _                     = require('lodash');
const moment                = require('moment');
const UserSVC               = require('../../utils/user-service');
const NotifSVC              = require('../../utils/notif-service');
const Utils                 = require('../../utils/utils');
const { URL }               = require('../../utils/helpers/enum');
const UserModel             = require('../models/user.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const { COUNTRY_CODES, LANG }   = require('../../utils/helpers/localization');
const LOG                   = 'RegisterController';

const RegisterController = {}

RegisterController.register = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : register`);
        
        let { name, email, country_code, password } = req.body;

        let cc          = country_code == COUNTRY_CODES.Indonesia ? COUNTRY_CODES.Indonesia : 'EN';
        let result      = { registered: false, sendEmail: false };
        let message     = LANG[cc].REGISTER.FAILED;
        let acknowledge = false;
        let sendverification    = false;

        // check user on db
        let conditionDB = [{ jointer: 'AND', key: 'email', op: '=', value: email }];
        let userDB      = await UserModel.getBy(conditionDB, [], [], 1);

        // check user-service
        let { acknowledge: userAck, result: userService } = await UserSVC.getUserByEmail(null, { email });
        
        if (userAck == true) {
            let saveToDB    = false;

            if (_.isEmpty(userService) && _.isEmpty(userDB)) {
                //create data di user service
                let userFirstname = (name).split(' ').slice(0, -1).join(' ');
                let userLastname  = (name).split(' ').slice(-1).join(' ');
                    userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
                    userLastname  = (userFirstname ===  userLastname) ? '' : userLastname;
                let userAddress     = { 
                    home        : '', 
                    clinic      : '',
                    country_code
                }

                let userData      = {
                    firstname       : userFirstname,
                    lastname        : userLastname,
                    display_name    : name,
                    email           : email,
                    type            : 'doctor',
                    password        : password,
                    address         : JSON.stringify(userAddress),
                }
                
                let userInsert  = await UserSVC.saveUser(null, userData);
                
                if (userInsert.acknowledge == true && !_.isEmpty(userInsert.result) && userInsert.result.affectedRows > 0) {
                    let getUser         = await UserSVC.getUserByEmail(null, { email });
                        userService     = getUser.result;  

                    saveToDB            = true;
                    sendverification    = true;
                    acknowledge         = true;
                } else {
                    message     = LANG[cc].REGISTER.FAILED;
                }
            } else if (!_.isEmpty(userService) && _.isEmpty(userDB)) {
                saveToDB    = true;
                message     = LANG[cc].REGISTER.ACCOUNT_EXISTS;
            } else if (!_.isEmpty(userService) && !_.isEmpty(userDB)) {
                message     = LANG[cc].REGISTER.ACCOUNT_EXISTS;
            }

            if (saveToDB == true) {
                //set default born date to 1 january 1970
                let born_date = _.toLower(moment(new Date('1970-01-01')).format('YYYY-MM-DD'));
                //create data di DB d2d
                let data    = [
                    { key: 'uid' , value: userService.uid }, 
                    { key: 'name' , value: name }, 
                    { key: 'email' , value: email },
                    { key: 'born_date' , value: born_date },
                    { key: 'country_code' , value: country_code } 
                ];

                let resInsertUser = await UserModel.save(data);
                userDB = await UserModel.getBy(conditionDB, [], [], 1);
            }

            if(_.isEmpty(userDB) == false){
                result.registered = true;
                message           = LANG[cc].REGISTER.SUCCESS;
            }else{
                result.registered = false;
            }

            if (!_.isEmpty(userService) && !_.isEmpty(userDB)) {
                if (userService.emailVerified == false) {
                    sendverification = true;

                    if (saveToDB == false) {
                        message = LANG[cc].REGISTER.VERIFY_ACCOUNT;
                    }
                }

            }
            
            if (sendverification == true) {
                // sending verification
                let randString  = Utils.createRandomString(32);
                let sendEmail   = await NotifSVC.sendEmail(userService.uid, userService.displayName, email, 'ACTIVATION', { link: `${URL.ACTIVATION}/${userService.uid}/${randString}` });
                let updateHash  = await UserSVC.setVerification(userService.uid, { email, verification: randString });
                
                message         = sendEmail.result == true ? LANG[COUNTRY_CODES.Indonesia].REGISTER.VERIFY_ACCOUNT : LANG[COUNTRY_CODES.Indonesia].REGISTER.SEND_EMAIL_FAILED;
                result.sendEmail = sendEmail.result;    
            }
        }
        
        console.log(`└─ ${LOG} : register -> Success`);
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : register -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

// regis with device id
// for d2d phi requirement can't save phone number and email of users
RegisterController.registerByDeviceId = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : registerByDeviceId`);
        
        let { name, device_unique_id, country_code } = req.body;

        let cc          = country_code == COUNTRY_CODES.Indonesia ? COUNTRY_CODES.Indonesia : 'EN';
        let result      = { registered: false };
        let message     = LANG[cc].REGISTER.FAILED;
        let acknowledge = false;
        let password    = device_unique_id;

        // add email format to make it can be save on firebase
        device_unique_id = device_unique_id + `@d2d${country_code}.com`

        // check user on db
        let conditionDB = [{ jointer: 'AND', key: 'email', op: '=', value: device_unique_id }];
        let userDB      = await UserModel.getBy(conditionDB, [], [], 1);

        // check user-service
        let { acknowledge: userAck, result: userService } = await UserSVC.getUserByEmail(null, { email: device_unique_id });
        
        if (userAck == true) {
            conditionDB     = [];
            let existUid = null;

            //create data di user service
            let userFirstname = (name).split(' ').slice(0, -1).join(' ');
            let userLastname  = (name).split(' ').slice(-1).join(' ');
                userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
                userLastname  = (userFirstname ===  userLastname) ? '' : userLastname;
            let userAddress     = { 
                home        : '', 
                clinic      : '',
                country_code
            }

            let userData      = {
                firstname       : userFirstname,
                lastname        : userLastname,
                display_name    : name,
                email           : device_unique_id,
                type            : 'doctor',
                password        : password,
                email_verified  : "true",
                address         : JSON.stringify(userAddress),
            }

            //update if user service not empty by set uid
            if (_.isEmpty(userService) == false){
                userData.uid = userService.uid;
                existUid = userService.uid;
            }
            
            let userInsert  = await UserSVC.saveUser(existUid, userData);
            //update user db in d2d db
            if (userInsert.acknowledge == true && !_.isEmpty(userInsert.result) && userInsert.result.affectedRows > 0) {
                let getUser         = await UserSVC.getUserByEmail(null, { email: device_unique_id });
                    userService     = getUser.result;  
                acknowledge         = true;

                let data    = [
                    { key: 'uid' , value: userService.uid }, 
                    { key: 'name' , value: name }, 
                    { key: 'email' , value: device_unique_id },
                    { key: 'country_code' , value: country_code } 
                ];

                if(_.isEmpty(userDB) == false){
                    conditionDB = [{ jointer: 'AND', key: 'id', op: '=', value: `${userDB.id}` }];
                }

                let resInsertUser = await UserModel.save(data,conditionDB);

                conditionDB = [{ jointer: 'AND', key: 'email', op: '=', value: device_unique_id }];
                userDB      = await UserModel.getBy(conditionDB, [], [], 1);
                result.data = userDB;

                message     = LANG[cc].REGISTER.SUCCESS;
            } else {
                message     = LANG[cc].REGISTER.FAILED;
            }

            result.registered = _.isEmpty(userDB) ? false : true;
            
        }
        
        console.log(`└─ ${LOG} : registerByDeviceId -> Success`);
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : registerByDeviceId -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegisterController.resendVerification = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : resendVerification`);
        
        let { email }   = req.body;
        let result      = false;
        let message     = LANG[COUNTRY_CODES.Indonesia].GENERAL.DATA_NOT_FOUND;
        let acknowledge = false;

        let { acknowledge: ackUser, result: userService } = await UserSVC.getUserByEmail(null, { email });
        if (ackUser == true) {
            if (!_.isEmpty(userService)) {
                let randString  = Utils.createRandomString(32);
                let sendEmail   = await NotifSVC.sendEmail(userService.uid, userService.displayName, email, 'ACTIVATION', { link: `${URL.ACTIVATION}/${userService.uid}/${randString}` });
                let updateHash  = await UserSVC.setVerification(userService.uid, { email, verification: randString });
                
                result      = sendEmail.result;
                message     = sendEmail.result == true ? LANG[COUNTRY_CODES.Indonesia].REGISTER.VERIFY_ACCOUNT : LANG[COUNTRY_CODES.Indonesia].REGISTER.SEND_EMAIL_FAILED;
                acknowledge = sendEmail.acknowledge;

                console.log(`└─ ${LOG} : resendVerification -> Success`);
            }
        }

        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(error)
        console.log(`└─ ${LOG} : resendVerification -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegisterController.verification = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : verification`);
        
        let { uid, code } = req.params;
        let { acknowledge, result, message } = await UserSVC.verification(uid, { verification: code });
        console.log(result)
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(error)
        console.log(`└─ ${LOG} : verification -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = RegisterController;