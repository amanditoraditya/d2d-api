const _                 = require('lodash');
const admin         = require('firebase-admin')
const parseResponse     = require('../../utils/helpers/parse-response');
const TestingController = {}

TestingController.get = async (req, res, next) => {
    parseResponse(res, 200, 'result', 'Message Get');
}

TestingController.getPaging = async (req, res, next) => {
    parseResponse(res, 200, 'result', 'Message GetPagging');
}

TestingController.delete = async (req, res, next) => {
    parseResponse(res, 200, 'result', 'Message Delete');
}

TestingController.save = async (req, res, next) => {
    parseResponse(res, 200, 'result', 'Message save');
}

TestingController.generateToken = async (req, res, next) => {
    let { uid } = req.query;
    let result = await admin.auth().createCustomToken(uid)
    res.status(200)
    .send({
        token: result,
    })
}


module.exports = TestingController;