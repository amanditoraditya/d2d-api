const _                     = require('lodash');
const UserModel             = require('../models/user.model');
const CompetenceModel       = require('../models/competence.model');
const AttendeeModel         = require('../models/event_attendee.model');
const utils                 = require('../../utils/utils');
const GUESVC                = require('../../utils/gue-service');
const UserSVC               = require('../../utils/user-service');
const ContentSVC            = require('../../utils/content-service');
const NotifSVC              = require('../../utils/notif-service');
const Utils                 = require('../../utils/utils');
const parseResponse         = require('../../utils/helpers/parse-response');
const {COUNTRY_CODES, LANG} = require('../../utils/helpers/localization');
const {COMPETENCE_TYPES, URL}   = require('../../utils/helpers/enum');
const LOG                   = 'LoginController';

const LoginController = {}

LoginController.doLogin = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : doLogin`);

        let { email, country_code }   = req.body;
        let result      = {};
        let cc          = COUNTRY_CODES.Indonesia;
        let message     = LANG[cc].LOGIN.SUCCESS;

        // check user on db
        let conditionDB = [{ jointer: 'AND', key: 'email', op: '=', value: email }];
        let userDB      = await UserModel.getBy(conditionDB, [], [], 1);

        // check user-service
        let { acknowledge, result: userService } = await UserSVC.getUserByEmail(null, { email });

        if (acknowledge == true) {

            if (!_.isEmpty(userService) && _.isEmpty(userDB)) {
                //insert data di db d2d sesuai dengan data dari user-service
                let data    = [
                    { key: 'uid' , value: userService.uid }, 
                    { key: 'name' , value: userService.displayName }, 
                    { key: 'email' , value: userService.email },
                    { key: 'enabled' , value: !userService.disabled },
                    { key: 'country_code' , value: country_code },
                ];

                let resInsertUser = await UserModel.save(data);
            } else if (_.isEmpty(userService) && _.isEmpty(userDB)) {
                message = LANG[cc].LOGIN.USER_NOT_FOUND
            } else if (_.isEmpty(userService) && !_.isEmpty(userDB)) {
                message = LANG[cc].LOGIN.USER_INACTIVE
            }
            
            if (!_.isEmpty(userService)) {
                uid = userService.uid
                //get data profile
                resultDB     = await UserModel.getBy([{ jointer: 'AND', key: 'uid', value: uid, op: '=' }], [], [], 1)
                
                if (!_.isEmpty(resultDB)) {
                    result  = resultDB
                    
                    result.pns = result.pns == "true" ? true : result.pns == "false" ? false : null; 
                    cc      = result.country_code == COUNTRY_CODES.Indonesia ? COUNTRY_CODES.Indonesia : 'EN';
                
                    //get specialization by uid
                    let resCompetence   = await CompetenceModel.getSpecializationByType(uid, COMPETENCE_TYPES.SPESIALIZATION)
                    let resultArr       = []
                    if (!_.isEmpty(resCompetence)) {
                        await utils.asyncForEach(resCompetence, async (row) => {
                            //get subspecialization per row
                            let subSpecialistList   = [];
                            subSpecialistList       = await CompetenceModel.getSubSpecialization(row.id,uid);
                            row.subSpecialistList   = subSpecialistList;
                            resultArr.push(row)
                        })
                    }
                    result.spesialization   = resultArr;
                
                    //get subscription / interest
                    result.subscription     = await CompetenceModel.getSpecializationByType(uid, COMPETENCE_TYPES.SUBSCRIPTION);
                
                    //get sum activity
                    let filter  = [
                        { key: 'activity', value: 'bookmark' },
                        { key: 'activity', value: 'download' }
                    ]
                    let sum             = await ContentSVC.useractivity.getSum(uid, filter);
                    if (!_.isEmpty(sum.docs)) {
                        sum = sum.docs
                    }
                    result.counting     = sum;
                
                    //get attendee
                    let resAttendee     = await AttendeeModel.getAttendeeByUser(uid);
                    let stringAttendee  = [];
                    let arrAttendee     = [];
                    let resWebinar      = [];
                    
                    if (!_.isEmpty(resAttendee.event_attendee)) {    
                        stringAttendee  = resAttendee.event_attendee;
                        arrAttendee     = stringAttendee.split(',');
                        resWebinar      = await ContentSVC.webinar.getEvent(uid, { stringAttendee });
                    
                        if (!_.isEmpty(resWebinar.webinar)) {
                            resWebinar  = resWebinar.webinar.split(',');
                        }
                    }
                    result.webinar          = resWebinar
                    result.event_attendee   = arrAttendee
                
                    result.photo            = !_.isEmpty(result.photo) ? `https://static.d2d.co.id/image/profile/${result.photo}` : ''
                } else {
                    message = LANG[cc].GENERAL.USER_NOT_FOUND;
                }
            }
        }
        console.log(`└─ ${LOG} : doLogin -> Success`);        
        parseResponse(res, 200, result, message);
    } catch (error) {
        console.log(`└─ ${LOG} : doLogin -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = LoginController;