const _                     = require('lodash');
const ContentSVC            = require('../../utils/content-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'UseractivityController';

const UseractivityController = {}

UseractivityController.getPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getPaging`);

        let { uid }     = req.headers;
        let { page, limit, app, activity } = req.query;
        
        let filter  = [];

        if (!_.isEmpty(app)) {
            filter.push({ key: 'app', value: app });
        }

        if (!_.isEmpty(activity)) {
            activity  = _.isArray(activity) ? activity : [activity];

            activity.forEach(element => {
                filter.push({ key: 'activity', value: element });
            });
        }

        let record  = await ContentSVC.useractivity.getAll(uid, filter, page, limit);

        console.log(`└─ ${LOG} : getPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

UseractivityController.getSum = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getSum`);

        let { uid }     = req.headers;
        let { app, activity } = req.query;
        
        let filter  = [];

        if (!_.isEmpty(app)) {
            filter.push({ key: 'app', value: app });
        }

        if (!_.isEmpty(activity)) {
            activity  = _.isArray(activity) ? activity : [activity];
            activity.forEach(element => {
                filter.push({ key: 'activity', value: element });
            });
        }
        console.log(activity)

        let record  = await ContentSVC.useractivity.getSum(uid, filter);

        console.log(`└─ ${LOG} : getSum -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getSum -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

UseractivityController.save = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : save`);
        
        let { uid }  = req.headers;
        let { app, activity, content, content_id } = req.body;

        let payload = {
            app        : app,
            uid        : uid,
            activity   : activity,
            content    : content,
            content_id : content_id
        }
        
        let record  = await ContentSVC.useractivity.save(uid, payload);

        console.log(`└─ ${LOG} : save -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : save -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = UseractivityController;