const _                     = require('lodash');

const utils                 = require('../../utils/utils');
const parseResponse         = require('../../utils/helpers/parse-response');
const redisService          = require('../../utils/helpers/RedisService');

const RedisController     = {}
const LOG                   = 'Redis Controller';

RedisController.getValue = async (req, res, next) => {

    let { key } = req.params;

    var value = '';
    if(_.isEmpty(key) == false){
        value = await redisService.getValue(key);
    }

    parseResponse(res, 200, value, _.isEmpty(value)? 'not found' : 'found', true);
}

RedisController.setValue = async (req, res, next) => {

    const { rediskey, redisvalue } = req.body;

    let jsonValue = JSON.stringify(redisvalue);

    let insertSuccess = false;
    if(_.isEmpty(rediskey) == false){
        insertSuccess = await redisService.insert(rediskey,jsonValue);
    }

    parseResponse(res, 200, jsonValue, insertSuccess == 'true'? 'success' : 'failed', true);
}

RedisController.delete = async (req, res, next) => {

    let { key } = req.params;

    let deleteSuccess = false;
    if(_.isEmpty(key) == false){
        deleteSuccess = await redisService.delete(key);
    }

    parseResponse(res, 200, key, deleteSuccess == 'true'? 'success' : 'failed', true);
}

module.exports = RedisController;