const _                 = require('lodash');
const parseResponse     = require('../../utils/helpers/parse-response');
const jwt               = require('jsonwebtoken');
const UserSVC           = require('../../utils/user-service');
const PatientController     = {};
const LOG               = 'PatientController';

PatientController.getAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getAll`);

        // let decode  = jwt.decode(req.headers.authorization);
        let { uid }                           = req.headers;
        let { page, limit, keyword, sort }    = req.query;

        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        sort                    = _.isEmpty(sort) || _.isUndefined(sort) ? 'pair_id ASC' : sort;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        
        let params = [
            { key: 'page', value: page },
            { key: 'limit', value: limit },
            { key: 'sort', value: sort },
            { key: 'keyword', value: keyword },
            { key: 'uid', value: uid },
        ];

        let record          = await UserSVC.getPatients(uid, params);

        console.log(`└─ ${LOG} : getAll -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getAll -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

PatientController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);
        
        let { uid: patientUID }     = req.params;
        // let decode      = jwt.decode(req.headers.authorization);
        let { uid } = req.headers
        console.log('patient uid', patientUID)
        console.log(' uid', uid)
        
        let { status, message, result, acknowledge }      = await UserSVC.getUser(patientUID);
        console.log("result usersvc getuser: ", result);
        
        if (acknowledge == true && !_.isEmpty(result)) {
            console.log(`├── ${LOG} :: get :: success`);
            // remove unused object
            delete result.password_code;
            delete result.confirmation_hash;
            delete result.verified;
            delete result.disabled;
            delete result.deleted;
            
            let pmm         = await UserSVC.getPatientConnected(uid, patientUID);
            if (pmm.acknowledge == true && pmm.result.length > 0) {
                let record = pmm.result;
                let status = null;
            
                record.forEach((row, i) => {
                    status = row.status;
                    if (row.status == 'U' && uid == row.uid) {
                        let lastData        = JSON.parse(row.last_data);
                        
                        result.birthday         = lastData.birthday;
                        result.gender           = (lastData.gender === 'F') ? 'Perempuan' : 'Laki-laki';
                        result.other_info       = JSON.stringify(lastData.other_info);
                        result.updated          = row.updated;
                        result.created          = row.created;
                    }
                })
                result.gender  = (result.gender === 'F') ? 'Perempuan' : 'Laki-laki';
                _.assign(result, {status: status});           
            }
        }
        console.log(`┌─ ${LOG} : get -> Success`);
        parseResponse(res, 200, result, _.isEmpty(result) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = PatientController;