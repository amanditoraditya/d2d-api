const _                     = require('lodash');
const JournalModel          = require('../models/journal.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const UserSVC               = require('../../utils/user-service');
const ContentSVC            = require('../../utils/content-service');
const utils                 = require('../../utils/utils');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const JournalController     = {}
const LOG                   = 'Journal Controller';

JournalController.request = async (req, res, next) => {
    
    console.log(`┌─ ${LOG} : journal`);
        
    let { countrycode, uid }  = req.headers;
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc      =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

    try {
        let { 
            fullname,
            competence_id,
            competence_name,
            phone,
            email,
            city,
            journal_title,
            address 
        } = req.body;
        
        let payload = [
            { key:'uid',             value: uid },
            { key:'fullname',        value: fullname },
            { key:'competence_id',   value: competence_id },
            { key:'competence_name', value: competence_name },
            { key:'phone',           value: phone },
            { key:'email',           value: email },
            { key:'city',            value: city },
            { key:'journal_title',   value: journal_title },
            { key:'address',         value: address },
            { key:'country_code',    value: countrycode }
        ]
        
        // set language
        let record      = []
        
        let result      = {};
        let message     = LANG[cc].GENERAL.SUCCESS;
        let acknowledge = false;
        
        let insertJournal   = await JournalModel.save(payload)

        if (insertJournal.insertId && (insertJournal.insertId != null || insertJournal.insertId != 0)) {
            let user            = await UserSVC.getUser(uid);

            // sending email to doctor
            let subjectDoctor   = LANG[cc].JOURNAL.FULL_JOURNAL_REQUEST;
            let payloadString   = [
                user.result.display_name, 
                journal_title
            ]
            let contentDoctor   = utils.stringReplace(LANG[cc].JOURNAL.CONTENT_DOCTOR, payloadString)
            let form = {
                toEmail: email, subject: subjectDoctor, emailContent: contentDoctor,
                apps: 'd2d'
            }
            let emailDoctor     = await ContentSVC.email.send(uid, form);

            // sending email to cs
            let subjectCs       = `REQJ${insertJournal.insertId}`;
            let payloadStringCs = [
                user.result.display_name, 
                journal_title,
                fullname,
                address,
                city,
                countrycode,
                competence_name,
                phone,
                email
            ]
            let contentCs       = utils.stringReplace(LANG[cc].JOURNAL.CONTENT_CS, payloadStringCs)
            let formCs          = {
                toEmail:`info@d2d.co.id`, subject: subjectCs, emailContent: contentCs,
                apps: 'd2d'
            }
            let emailCs         = await ContentSVC.email.send(uid, formCs);
            
            // set result & message
            result.email_doctor = emailDoctor.acknowledge == true ? emailDoctor.result : false;
            result.email_admin  = emailCs.acknowledge == true ? emailCs.result : false;
            result.request_id   = subjectCs;
            message             = LANG[cc].JOURNAL.SUCCESS_SUBMITED;
            acknowledge         = true;
        } else {
            message             = LANG[cc].JOURNAL.FAILED_SUBMITED;
        }

        console.log(`└─ ${LOG} : journal -> Success`);
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(`│  ├── ${LOG} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = JournalController;