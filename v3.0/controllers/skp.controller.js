const _                 = require('lodash');
const parseResponse     = require('../../utils/helpers/parse-response');
const SkpModel          = require('../models/skp.model');
const SkpController     = {};
const LOG               = 'SkpController';
const TYPES             = {
    all         : 'all',
    cme         : 'cme',
    event       : 'event',
    webinar     : 'webinar',
    offline     : 'offline',
}

SkpController.getHistory = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getHistory`);

        let { page, limit, keyword, status }    = req.query;
        let { type }            = req.params;
        let { uid }             = req.headers;
        
        let condition           = [];

        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        
        // set condition by type
        if (!_.isEmpty(type) && !_.isUndefined(type)) {
            // offline certificate
            if (_.toLower(type) == TYPES.offline) {
                condition.push({ key: `quiz_id IS NULL` });
            }

            // CME certificate
            else if (_.toLower(type) == TYPES.cme) {
                condition.push({ jointer: 'AND', key: `quiz_id IS NOT NULL` });
            }

            // Event & Webinar certificate
            else if (_.toLower(type) == TYPES.event || _.toLower(type) == TYPES.webinar) {
                condition.push({ jointer: 'AND', key: `type`, op: '=', value: `${_.toLower(type)}` });
            } 
            
            // Exclude offline
            else {
                condition.push({ jointer: 'AND', key: `type`, op: '<>', value: `offline` });
            }
        } else {
            // Exclude offline
            condition.push({ jointer: 'AND', key: `type`, op: '<>', value: `offline` });
        }

        if (!_.isEmpty(status) && !_.isUndefined(status)) {
            if (_.includes(['valid', 'invalid', 'rejected'], status)) {
                status = status;
            } else {
                status = '';
            }
        }

        let docs            = await SkpModel.getHistory(uid, condition, page, limit, keyword, status);
        let total           = await SkpModel.getHistoryCount(uid, condition, page, limit, keyword, status);

        let response        = { page, limit, total, docs }

        console.log(`└─ ${LOG} : getHistory -> Success`);
        parseResponse(res, 200, response, _.isEmpty(docs) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getHistory -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

SkpController.getDetail = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getDetail`);

        let { content_id, certificate }   = req.body;
        let { type }        = req.params;
        let { uid }         = req.headers;
        let condition       = [];

        condition.push({ jointer: 'AND', key: `type`, op: '=', value: `${_.toLower(type)}` });

        if (!_.isUndefined(content_id) && !_.isEmpty(`${content_id}`) && _.includes(['cme', 'event', 'webinar'], _.toLower(type))) {
            condition.push({ jointer: 'AND', key: `quiz_id`, op: '=', value: `${content_id}` });
        }

        if (!_.isUndefined(certificate) && !_.isEmpty(`${certificate}`) && _.toLower(type) == 'offline') {
            condition.push({ jointer: 'AND', key: `certificate`, op: '=', value: `${certificate}` });
        }

        let docs            = await SkpModel.getBy(uid, condition);

        console.log(`┌─ ${LOG} : getDetail -> Success`);
        parseResponse(res, 200, docs, _.isEmpty(docs) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getDetail -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = SkpController;