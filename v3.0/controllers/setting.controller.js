const _                     = require('lodash');
const  moment               = require('moment');
const SettingModel             = require('../models/setting.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'SettingController';

const SettingController = {}

SettingController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { code }      = req.query;
        let join        = []

        let conditions  = [];
        let valid       = true
        let message     = 'Success'
        let record      = []

        // set condition by type
        if (!_.isEmpty(code) && !_.isUndefined(code)) {
            code  = _.isArray(code) ? code : [code];
            
            conditions.push({ key: `settings.code IN ('${code.join("', '")}')`});
        }
        else {
            valid   = false
            message = 'Code cannot be empty'
        }

        if (valid) {
            record = await SettingModel.getAll(conditions);
        }
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

SettingController.save = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : save`);
        
        let { code }            = req.params;
        let { type, content }   = req.body;
        let conditions          = [];
        let valid               = true
        let message             = 'Success'
        let typeSelection       = ['text','html']
        let record              = []
        let data                = []

        //check type
        if (!_.isEmpty(type) && !_.isUndefined(type)) {

            //check type
            if(typeSelection.includes(type)) {
                data.push({ key: 'type', value: type })
            } 
            else {
                valid   = false
                message = 'Type incorrect'
            }
        }
        else {
            valid   = false
            message = 'Type cannot be empty'
        }

        //check content
        if (!_.isEmpty(content) && !_.isUndefined(content)) {
            data.push({ key: 'content', value: content })
        }
        else {
            valid   = false
            message = 'Content cannot be empty'
        }
        
        conditions.push({ key: `code`, op: '=', value: code });

        if (valid) {
            record  = await SettingModel.save(data, conditions);
        }
        console.log(`└─ ${LOG} : save -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : save -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = SettingController;