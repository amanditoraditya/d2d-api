const _                     = require('lodash');
const UserSVC               = require('../../utils/user-service');
const UserModel             = require('../models/user.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'DkonsulController';

const DkonsulController = {}

// DkonsulController.validate = async (req, res, next) => {
//     try {
//         console.log(`┌─ ${LOG} : validate`);

//         let { uid }     = req.headers;

//         let record  = await UserSVC.dKonsul.validate(uid);
        
//         if (record.acknowledge == false || record.message == 'No data found!') {
//             console.log('Error: ', record);
//             record = []
//         } else {
//             _.assign(record, { url: 'https://staging-konsultasi.d2d.co.id/login' });
//         }
//         console.log(`└─ ${LOG} : validate -> Success`);
//         parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
//     } catch (error) {
//         console.log(`└─ ${LOG} : validate -> Error`);
//         let err         = new Error(error.message);
//             err.code    = 500;
//         next(err);
//     }
// }

DkonsulController.login = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : login`);

        let { uid }     = req.headers;
        let { email }   = req.body;
        let record = {}
        let message = 'Success'
        let acknowledge = false;
        let condition = [
            { key: 'email', op: '=', value: email },
            // { key: 'uid', op: '=', value: uid }
        ]
        let tableuser = []
        let serviceuser = []

        //get user from table
        tableuser = await UserModel.getAll(condition);
        
        //get user from user service
        serviceuser = await UserSVC.getUser(uid);
        
        // console.log(uid)
        // console.log((tableuser))
        // console.log((serviceuser.result))
        // console.log(!_.isEmpty(tableuser) && !_.isEmpty(serviceuser.result))
        
        if (!_.isEmpty(tableuser) && !_.isEmpty(serviceuser.result)) {
            let data = serviceuser.result
            
            if(data.verified == 1 && (data.disabled == null || data.disabled == false)) {
                //get user in dkonsul table
                let dkonsulUser  = await UserSVC.dKonsul.get(uid);

                if (!_.isEmpty(dkonsulUser)) {
                    let payload     = { email: email, status: 'A' };

                    await UserSVC.dKonsul.save(uid, payload);

                    dkonsulUser.status = 'A'
                    // record = serviceuser.result
                    // console.log(dkonsulUser)
                    // console.log(serviceuser.result)
                    record = {
                        uid: serviceuser.result.uid,
                        email: serviceuser.result.email,
                        name: serviceuser.result.display_name
                    }
                    
                    // _.assign(record, { serviceuser: serviceuser.result });
                    // _.assign(record, { dkonsulUser });

                    message = 'Login Success'
                    acknowledge = true
                } else {
                    message = 'Login Failed -> User Unavailable in DKonsul'
                }         
            } else {
                message = 'Login Failed -> User is Disabled or is not verified'
            }  
        } else {
            message = 'Login Failed -> User Unavailable'
        }  


        console.log(`└─ ${LOG} : login -> Success`);
        parseResponse(res, 200, record, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : login -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

// DkonsulController.getEula = async (req, res, next) => {
//     try {
//         console.log(`┌─ ${LOG} : getEula`);

//         let { uid }     = req.headers;

//         let record  = await UserSVC.dKonsul.get(uid);

//         console.log(`└─ ${LOG} : getEula -> Success`);
//         parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
//     } catch (error) {
//         console.log(`└─ ${LOG} : getEula -> Error`);
//         let err         = new Error(error.message);
//             err.code    = 500;
//         next(err);
//     }
// }

DkonsulController.save = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : save`);

        let { uid } = req.headers;
        let { 
            id, email, status
        } = req.body;

        id      = _.isEmpty(id) || _.isUndefined(id) ? '' : id;
        email   = _.isEmpty(email) || _.isUndefined(email) ? '' : email;
        status  = _.isEmpty(status) || _.isUndefined(status) ? '' : status;

        let statusSelect = ['A','D']
        let valid        = true
        let message      = 'Success'
        let record       = {}
        let payload      = {};

        if (id != '' ) {
            if (!isNaN(id)){
                _.assign(payload, { id });
            } else {
                valid   = false
                message = 'id must be a number'
            }
        } 

        if (email != '') {
            _.assign(payload, { email });
        } else {
            valid   = false
            message = 'Email cannot be empty'
        }

        //check status
        if (!_.isEmpty(status) && !_.isUndefined(status)) {
        
            //check status
            if(statusSelect.includes(status)) {
                
                _.assign(payload, { status });
            } 
            else {
                valid   = false
                message = 'Status incorrect'
            }
        }
        else {
            valid   = false
            message = 'Status cannot be empty'
        }

        console.log(payload)
        if (valid) {
            record  = await UserSVC.dKonsul.save(uid, payload);
        }
        console.log(`└─ ${LOG} : save -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : save -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}
module.exports = DkonsulController