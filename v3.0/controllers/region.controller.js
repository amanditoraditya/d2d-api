const _                     = require('lodash');
const UserSVC               = require('../../utils/user-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'RegionController';

const RegionController = {}

RegionController.getCountryPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getCountryPaging`);

        let { uid }     = req.headers;
        let { page, limit, keyword } = req.query;
        let { countryid } = req.params;

        let filter  = [];
        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        countryid               = _.isEmpty(countryid) || _.isUndefined(countryid) ? '' : countryid;

        filter.push({ key: 'page', value: page });
        filter.push({ key: 'limit', value: limit });
        filter.push({ key: 'keyword', value: keyword });
            
        let record  = await UserSVC.regionCountry.getAll(uid, filter, countryid)

        console.log(`└─ ${LOG} : getCountryPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getCountryPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getCountry = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getCountry`);

        let { uid }     = req.headers;
        let { id }      = req.params;
            
        let record  = await UserSVC.regionCountry.get(uid, id)

        console.log(`└─ ${LOG} : getCountry -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getCountry -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getDistrictPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getDistrictPaging`);

        let { uid }     = req.headers;
        let { page, limit, keyword } = req.query;
        let { districtid } = req.params;
        
        let filter  = [];
        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        districtid              = _.isEmpty(districtid) || _.isUndefined(districtid) ? '' : districtid;

        filter.push({ key: 'page', value: page });
        filter.push({ key: 'limit', value: limit });
        filter.push({ key: 'keyword', value: keyword });
            
        let record  = await UserSVC.regionDistrict.getAll(uid, filter, districtid)

        console.log(`└─ ${LOG} : getDistrictPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getDistrictPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getDistrict = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getDistrict`);

        let { uid }     = req.headers;
        let { id }      = req.params;
            
        let record  = await UserSVC.regionDistrict.get(uid, id)

        console.log(`└─ ${LOG} : getDistrict -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getDistrict -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getProvincePaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getProvincePaging`);

        let { uid }     = req.headers;
        let { page, limit, keyword } = req.query;
        let { provinceid } = req.params;
        
        let filter  = [];
        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        provinceid              = _.isEmpty(provinceid) || _.isUndefined(provinceid) ? '' : provinceid;

        filter.push({ key: 'page', value: page });
        filter.push({ key: 'limit', value: limit });
        filter.push({ key: 'keyword', value: keyword });
            
        let record  = await UserSVC.regionProvince.getAll(uid, filter, provinceid)

        console.log(`└─ ${LOG} : getProvincePaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getProvincePaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getProvince = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getProvince`);

        let { uid }     = req.headers;
        let { id }      = req.params;
        
        let record  = await UserSVC.regionProvince.get(uid, id)

        console.log(`└─ ${LOG} : getProvince -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getProvince -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getRegencyPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getRegencyPaging`);

        let { uid }     = req.headers;
        let { page, limit, keyword } = req.query;
        let { regencyid } = req.params;
        
        let filter  = [];
        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        regencyid               = _.isEmpty(regencyid) || _.isUndefined(regencyid) ? '' : regencyid;

        filter.push({ key: 'page', value: page });
        filter.push({ key: 'limit', value: limit });
        filter.push({ key: 'keyword', value: keyword });
            
        let record  = await UserSVC.regionRegency.getAll(uid, filter, regencyid)

        console.log(`└─ ${LOG} : getRegencyPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getRegencyPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getRegency = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getRegency`);

        let { uid }     = req.headers;
        let { id }      = req.params;

        let record  = await UserSVC.regionRegency.get(uid, id)

        console.log(`└─ ${LOG} : getRegency -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getRegency -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getVillagePaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getVillage`);

        let { uid }     = req.headers;
        let { page, limit, keyword } = req.query;
        let { villageid } = req.params;
        
        let filter  = [];
        page                    = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limit                   = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keyword                 = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        villageid               = _.isEmpty(villageid) || _.isUndefined(villageid) ? '' : villageid;

        filter.push({ key: 'page', value: page });
        filter.push({ key: 'limit', value: limit });
        filter.push({ key: 'keyword', value: keyword });
            
        let record  = await UserSVC.regionVillage.getAll(uid, filter, villageid)

        console.log(`└─ ${LOG} : getVillagePaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getVillagePaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

RegionController.getVillage = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getVillage`);

        let { uid }     = req.headers;
        let { id }      = req.params;
            
        let record  = await UserSVC.regionVillage.get(uid, id)

        console.log(`└─ ${LOG} : getVillage -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getVillage -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = RegionController;