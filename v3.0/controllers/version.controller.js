const _                     = require('lodash');
const  moment               = require('moment');
const VersionModel          = require('../models/version.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const utils                 = require('../../utils/utils');
const LOG                   = 'VersionController';

const VersionController = {}

VersionController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { platform }                = req.headers;
        let { version, build_number }   = req.body;
        
        let message     = 'Success';
        let acknowledge = false;
        let record      = {};
        let vrsDB       = [];
        platform        = _.toLower(platform);

        // get version from database
        vrsDB = await VersionModel.getBy([
            { jointer: 'AND', key: 'status', value: 'A', op: '=' },
            { jointer: 'AND', key: 'platform', value: `${platform}`, op: '=' },
        ],[],[],1,['version DESC, build_number DESC']);
        if (!_.isEmpty(vrsDB)) {            
            let force_update    = false;
            let show            = false;
            
            let fe_version               = utils.convertToDecimalVersion(version);
            let db_compatibility_version = utils.convertToDecimalVersion(vrsDB.compatibility_version);
            let db_current_version       = utils.convertToDecimalVersion(vrsDB.version);
            let fe_build_number          = Number(build_number);
            let db_compatibility_number  = Number(vrsDB.compatibility_number);
            let db_current_number        = Number(vrsDB.build_number);

            vrsDB.force = vrsDB.force == 1 ? true : false;
            if (fe_version < db_compatibility_version) { 
                force_update = true;
                show = true;
            }else if (fe_version == db_compatibility_version && fe_build_number < db_compatibility_number){
                force_update = true;
                show = true;
            }else if (fe_version < db_current_version || (fe_version == db_current_version && fe_build_number < db_current_number)) {
                force_update = vrsDB.force;
                show = true;
            }  
        
            acknowledge = true;
            record      = {
                version      : vrsDB.version,
                build_number : vrsDB.build_number,
                force_update : force_update,
                show         : show
            };
            
        } else {
            message     = 'No active versions currently';
            acknowledge = false;
        }
        
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = VersionController;