const _                     = require('lodash');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'NotificationController';
const NotificationModel          = require('../models/notification.model');
const NotificationController     = {};

NotificationController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { uid }     = req.headers;
        let { id }      = req.params;
        let join        = []

        let conditions  = [
            { jointer: 'AND', key: 'id',  value: id,  op: '=' },
            { jointer: 'AND', key: 'uid', value: uid, op: '=' }
        ];

        let record = await NotificationModel.getBy(conditions, join, [], 1);

        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}


NotificationController.getAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getAll`);

        let { page, limit, sort, keyword, type, exclude } = req.query;
        let { uid }     = req.headers;
        
        let condition   = [];
        let join        = [];
        let group       = [];

        page            = _.isEmpty(page) || _.isUndefined(page) ? 1 : page;
        limitpage       = _.isEmpty(limit) || _.isUndefined(limit) ? 10 : limit;
        keywordpage     = _.isEmpty(keyword) || _.isUndefined(keyword) ? '' : keyword;
        
        // set condition by uid
        condition.push({ key: `uid`, op: '=', value: uid });

        // searching by title
        if (!_.isEmpty(keyword)) {
            /* double kutip untuk search */
            keyword = keyword.replace(/'/g,"''")

            condition.push({ key: `title LIKE '%${keyword}%'` });
        }

        // set condition by type
        if (!_.isEmpty(type) && !_.isUndefined(type)) {
            type  = _.isArray(type) ? type : [type];
            
            condition.push({ key: `user_notification.type IN ('${type.join("', '")}')`});
        }

        //exclude condition by type
        if (!_.isEmpty(exclude) && !_.isUndefined(exclude)) {
            exclude  = _.isArray(exclude) ? exclude : [exclude];
            
            condition.push({ key: `user_notification.type NOT IN ('${exclude.join("', '")}')`});
        }

        if (!_.isUndefined(sort)) {
            sort        = _.isArray(sort) ? sort : [sort];
        }
        
        let docs        = await NotificationModel.getAll(condition, sort, page, limit);
        let total       = await NotificationModel.getCount(condition, sort);

        let response    = { page, limit, total, docs }

        console.log(`└─ ${LOG} : getAll -> Success`);
        parseResponse(res, 200, response, _.isEmpty(docs) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getAll -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

NotificationController.delete = async (req, res, next) => {
    try {

        let { uid }     = req.headers;
        let { id }      = req.params;

        let conditions  = [
            { jointer: 'AND', key: 'id',  value: id,  op: '=' },
            { jointer: 'AND', key: 'uid', value: uid, op: '=' }
        ];

        const record = await NotificationModel.delete(conditions);

        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        let err = new Error(error.message);
        err.code = 500;
        next(err);
    }
}

NotificationController.updateStatus = async (req, res, next) => {
    try {
        const statusSelection = [
            'read',
            'unread',
        ]

        let { uid }     = req.headers;
        let { status }  = req.params;
        let { id }      = req.body;
        let data        = [];
        let conditions  = [];
        let record      = {}
        let valid       = true
        let message     = 'Success'

        if (!_.isEmpty(status) && !_.isUndefined(status)) {
            
            //check statusnya sesuai atau tidak
            if(statusSelection.includes(status)) {
                data.push({ key: 'status', value: status })
            } 
            else {
                valid   = false
                message = 'Status incorrect'
            }
        } 
        else {
            valid   = false
            message = 'Status cannot be empty'
        }

        //default condition by uid
        conditions.push({ key: 'uid', value: uid });

        //check kalau id tidak kosong
        if (id != '' && id != undefined) {

            let existing_cond = [
                { jointer: 'AND', key: 'id', value: `${id}`, op: '=' }
            ];

            let existing = await NotificationModel.getBy(existing_cond, [], [], 1);

            if (!_.isEmpty(existing)) {
                conditions.push({ key: 'id', value: `${existing.id}` });
            }
            else {
                valid   = false
                message = 'ID not found'
            }
        }

        if (valid) {
            record = await NotificationModel.save(data, conditions);
        }

        parseResponse(res, 200, record, message);
    } catch (error) {
        let err = new Error(error.message);
        err.code = 500;
        next(err);
    }
}

module.exports = NotificationController;