const _                     = require('lodash');
const ContentSVC            = require('../../utils/content-service');
const UserCompetenceModel   = require('../models/user_competence.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const LOG                   = 'SpecialistController';

const SpecialistController = {}

SpecialistController.getPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getPaging`);

        let { uid, countrycode }     = req.headers;
        let { page, limit, keyword, type, nesting } = req.query;

        //set default country code
        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        
        let filter  = [];

        if (!_.isEmpty(keyword)) {
            filter.push({ key: 'keyword', value: keyword });
        }

        if (!_.isEmpty(type)) {
            filter.push({ key: 'type', value: type });
        }

        if (!_.isEmpty(nesting)) {
            filter.push({ key: 'nesting', value: nesting });
        }

        let record  = await ContentSVC.specialist.getAll(uid, filter, page, limit, countrycode);
        
        // get counter subscription
        if (type == 'subscription' && !_.isEmpty(record.docs)) {
            let data = record.docs;
            for(let row of data) {
                row.subscribed = await UserCompetenceModel.getSubscriber(row.id, countrycode);
            }
        }
        
        console.log(`└─ ${LOG} : getPaging -> Success`);
        parseResponse(res, !_.isUndefined(record.status) ? record.status : 200, !_.isUndefined(record.status) ? [] : record, !_.isUndefined(record.status) ? `${record.message} service` : 'Success', !_.isUndefined(record.status) ? record.acknowledge : true);
    } catch (error) {
        console.log(`└─ ${LOG} : getPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

SpecialistController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { uid, countrycode }     = req.headers;
        let { id }      = req.params;

        //set default country code
        countrycode = _.isEmpty(countrycode) ? 'ID' : _.toUpper(countrycode);

        let record  = await ContentSVC.specialist.get(uid, id, countrycode);

        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, _.isEmpty(record) ? 404 : 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = SpecialistController;