const _                     = require('lodash');
const  moment               = require('moment');
const ContentSVC            = require('../../utils/content-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const { BUCKET }            = require('../../utils/helpers/enum');
const LOG                   = 'EventController';
const EventAttendModel      = require('../models/event_attendee.model');
const EventCertificateModel = require('../models/user_certificate.model');
const EventModel            = require('../models/event.model');
const PaymentModel          = require('../../payment/models/payment.model');
const datetime              = require('node-datetime');
const {STATUS, PAYMENTS}    = require('../../utils/midtrans-service');

const utils                 = require('../../utils/utils');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const EventController = {}

EventController.get = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : get`);

        let { uid, countrycode }    = req.headers;
        let { id, hash }            = req.params;

        let fields = [];
        let join = [];
        let conditions = [];

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            apps_id: 1,
            countrycode: countrycode,
            platform: 'android' 
        }
        let record = [];

        if(_.isEmpty(id) == false){
            record = await ContentSVC.event.get(id, uid, headers);
        }
        else if(_.isEmpty(hash) == false){
            record = await ContentSVC.event.getByHash(hash, uid, headers);
        }

        // get payment order
        let paymentInfo = await PaymentModel.getBy([
            { jointer: 'AND', key: 'ref_id', value: `${record.id}`, op: '=' },       
            { jointer: 'AND', key: 'type', value: 'event', op: '=' },       
            { jointer: 'AND', key: 'uid', value: uid, op: '=' },       
            // { jointer: 'AND', key: `status not in ('${STATUS.ACCEPT_STATUS.CANCEL}', '${STATUS.ACCEPT_STATUS.FAILURE}')`, grouping: 'start'},    
            // { jointer: 'OR', key: 'status IS NULL', grouping: 'end'}    
        ],[],[],1,2,['created desc']);

        // nambahin current_time
        let dt          = datetime.create();
        let formatted   = dt.format('Y-m-d H:M:S');
        record.current_time = formatted;

        if(_.isEmpty(paymentInfo) == false){
            paymentInfo = _.isArray(paymentInfo) ? paymentInfo : [paymentInfo];

            let excludeStatus = [STATUS.ACCEPT_STATUS.FAILURE,STATUS.ACCEPT_STATUS.CANCEL];
            
            let pi = {}
            // pi = paymentInfo[0];
            // filter by exclude status array
            let excludeResult = paymentInfo.filter(function (row){
                return excludeStatus.includes(row.status) == false;
            })[0];

            //check if there any success payment
            let successResult = paymentInfo.filter(function (row){
                return row.status == STATUS.ACCEPT_STATUS.SUCCESS
            })[0];

            if(_.isEmpty(successResult) == false){
                pi = successResult;
            }else{
                pi = excludeResult
            }

            if(_.isEmpty(pi) == false){
                // make expired into null for FE
                pi.status = pi.status == STATUS.ACCEPT_STATUS.EXPIRED ? null : pi.status;

                // to handle if pi status is null because first index of payment info(transaction) is cancel or failure, and get old transanction
                if(pi.status == paymentInfo[0].status || pi.status != null){
                    let order = {
                        order_id: pi.order_code,
                        status: pi.status,
                        fraud_status: pi.fraud_status,
                        expired: pi.expired,
                        token: pi.token,
                        redirect_url: pi.redirect_url,
                        receipt: _.isEmpty(pi.receipt) ? null : `${BUCKET.PAYMENT_RECEIPT}${pi.receipt}`
                    }
                    
                    record.order = order;
                }
            }
            
        }

        // get is verified
        let verified = await EventAttendModel.getVerifiedByUser(uid, record.slug_hash);
        record.is_verified = verified == 1 ? true : false;

        // get user certificate
        fields = [];
        join = [];
        conditions = [
            { jointer: 'AND', key: 'event_id', value: `${record.id}`, op: '=' },
            { jointer: 'AND', key: 'status', value: 'A', op: '=' },
            { jointer: 'AND', key: 'uid', value: uid, op: '=' },
        ];
        let certificate = await EventCertificateModel.getAll(conditions,[],[],[],1,1);

        if (_.isEmpty(certificate) == false) {
            record.certificate = certificate[0];
        }

        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

EventController.getPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getPaging`);

        let { uid, countrycode, email } = req.headers;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English);  

        let { page, limit, competence, keyword }    = req.query;
        let headers = { 
            email: email,
            countrycode: countrycode,
            platform: 'android' 
        }

        let filter  = [];

        if (!_.isEmpty(keyword)) {
            filter.push({ key: 'keyword', value: keyword });
        }

        if (!_.isEmpty(competence)) {
            competence  = _.isArray(competence) ? competence : [competence];
            filter.push({ key: 'competence', value: competence.join(',') });
        }

        let record  = await ContentSVC.event.getAll(uid, filter, page, limit, headers);

        console.log(`└─ ${LOG} : getPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : getPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = EventController;