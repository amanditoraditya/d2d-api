const _                     = require('lodash');
const  moment               = require('moment');
const MedicineSVC            = require('../../utils/medicine-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'MedicineController';

const MedicineController = {}

MedicineController.getPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getPaging`);

        let { page, keyword, limit, prefix } = req.query;
        let { uid }     = req.headers
        let join        = []

        let filter      = [];
        let record      = []

        //defult apps key d2d
        filter.push({ key: 'apps', value: 'd2d' });

        // set condition by keyword
        if (!_.isEmpty(keyword) && !_.isUndefined(keyword)) {
            filter.push({ key: 'keyword', value: keyword });
        }
        
        // set condition by prefix
        if (!_.isEmpty(prefix) && !_.isUndefined(prefix)) {
            filter.push({ key: 'prefix', value: prefix });
        }
        
        record = await MedicineSVC.getAll(uid, filter, page, limit);
            
        console.log(`└─ ${LOG} : getPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

MedicineController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { id }      = req.params;
        let { uid }     = req.headers

        let filter      = [];
        let record      = []

        //defult apps key d2d
        filter.push({ key: 'apps', value: 'd2d' });
        filter.push({ key: 'obat_id', value: id });

        record = await MedicineSVC.getBy(uid, filter);
        
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

MedicineController.save = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : save`);
        
        let { code }            = req.params;
        let { type, content }   = req.body;
        let conditions          = [];
        let valid               = true
        let message             = 'Success'
        let typeSelection       = ['text','html']
        let record              = []
        let data                = []

        //check type
        if (!_.isEmpty(type) && !_.isUndefined(type)) {

            //check type
            if(typeSelection.includes(type)) {
                data.push({ key: 'type', value: type })
            } 
            else {
                valid   = false
                message = 'Type incorrect'
            }
        }
        else {
            valid   = false
            message = 'Type cannot be empty'
        }

        //check content
        if (!_.isEmpty(content) && !_.isUndefined(content)) {
            data.push({ key: 'content', value: content })
        }
        else {
            valid   = false
            message = 'Content cannot be empty'
        }
        
        conditions.push({ key: `code`, op: '=', value: code });

        if (valid) {
            record  = await SettingModel.save(data, conditions);
        }
        console.log(`└─ ${LOG} : save -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : save -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = MedicineController;