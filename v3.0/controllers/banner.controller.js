const _                     = require('lodash');
const  moment               = require('moment');
const BannerModel           = require('../models/banner.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'BannerController';

const BannerController = {}

BannerController.getAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        // let { id }      = req.params;
        // let { uid }     = req.headers

        let condition      = [];
        let record      = []

        //defult apps key d2d
        condition.push({ key: 'publish', value: '1' });
        condition.push({ key: 'apps', value: 'D2D' });

        record = await BannerModel.getAll(condition);

        //add Url images
        if (!_.isEmpty(record)) {
            record.forEach(row => {
                if (row.type == 'image') {
                    row.url = `https://static.d2d.co.id/banner/images/${row.url}`
                }
                return row
            });
        }
        
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = BannerController;