const _                     = require('lodash');
const  moment               = require('moment');
const UserModel             = require('../models/user.model');
const EventAttendModel      = require('../models/event_attendee.model');
const UserCertificateModel  = require('../models/user_certificate.model');
const UserSkpTmpModel       = require('../models/user_skp_tmp.model');
const AgreementModel        = require('../models/agreement.model');
const ContentSVC            = require('../../utils/content-service');
const ContentCMS            = require('../../utils/content-cms');
const NotifService          = require('../../utils/notif-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const { BUCKET }            = require('../../utils/helpers/enum');
const LOG                   = 'WebinarController';
const querystring           = require("querystring");
const datetime              = require('node-datetime');

const utils                 = require('../../utils/utils');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const { Utils }             = require('sequelize');
const { stringReplace }     = require('../../utils/utils');

const WebinarController = {}

// ----------------- connect to content api v.2 -------------------------
WebinarController.get = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : get`);

        let { uid, email, countrycode }     = req.headers;
        let { id, hash }      = req.params;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc      =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let webinarParam = '';
        let message      = LANG[cc].GENERAL.SUCCESS;
        let aknowledge   = true;
        let getBy        = '';
        let headers      = { 
            country_code: countrycode,
            platform: 'android' 
        }

        if(_.isEmpty(id) == false){
            getBy = 'id';
            webinarParam = id;
        }
        else if(_.isEmpty(hash) == false){
            getBy = 'hash';
            webinarParam = hash;
        }

        let record = await ContentSVC.webinar.get(uid, getBy, webinarParam, email, headers);

        let pass = true;

        // if(countrycode == COUNTRY_CODES.Indonesia && (record.exclusive == 1 || record.paid == 1)){
        //     // get is verified
        //     let checkAttendee = await EventAttendModel.getBy([
        //         { jointer: 'AND', key: 'email', value: email, op: '=' },
        //         { jointer: 'AND', key: 'event_slug_hash', value: record.event_slug_hash, op: '=' },
        //     ])
        //     pass = _.isEmpty(checkAttendee) == false ? true : false;
        // }

        if (!_.isEmpty(record) && pass == true) {
            // Check if user already aggree with tnc and check if this webinar has tnc
            let isAgree         = true;
            if(record.has_tnc == true){
                isAgree             = false;
                let condition       = [
                    { key: 'uid', value: uid },
                    { key: 'webinar_id', value: id }
                ]
                let agreement_log   = await AgreementModel.log.getBy(condition);
                // record.showTnC = null
                // agreement_log = {}
                // record.showTnC  = (record.showTnC === false) ? false : (_.isEmpty(agreement_log) ? true : false)
                // if ((record.showTnC == true && !_.isEmpty(agreement_log)) || record.showTnC == null) {
                //         record.showTnC = false
                // }
                
                record.user_submit  = null;
                if (_.isEmpty(agreement_log) == false) {
                    if (agreement_log.isAgree == 1 ) {
                        record.user_submit  =  'accept';
                        isAgree             =  true
                    } else {
                        record.user_submit  =  'decline';
                        isAgree             =  false
                    }
                    
                    //make pretest posttest flag false if user decline webinar tnc
                    if (record.user_submit == 'decline') {
                        record.pretest                = 0;
                        record.posttest               = 0;
                        record.quisioner_link         = ''
                        record.posttest_max_attempt   = 0;
                        record.posttest_count_attempt = 0;
                    }
                }
            }
            
            record.certificate = null
            if (isAgree) {
                let conditions      = [
                    { jointer: 'AND', key: 'user_certificate.uid', value: uid, op: '=' },
                    { jointer: 'AND', key: 'user_certificate.event_id', value: id, op: '=' },
                    { jointer: 'AND', key: 'user_certificate.type', value: 'webinar', op: '=' },
                    { jointer: 'AND', key: 'user_certificate.status', value: 'A', op: '=' }
                ];
                let userCertificate = await UserCertificateModel.getBy(conditions, [], [], 1);

                record.certificate  = !_.isEmpty(record) && !_.isEmpty(userCertificate) ? `${BUCKET.CERTIFICATE}${uid}/${userCertificate.filename}` : null;
            }

            //add current server time
            let dt          = datetime.create();
            let formatted   = dt.format('Y-m-d H:M:S');
            record.server_date = formatted;

            message     = LANG[cc].GENERAL.SUCCESS;
            aknowledge  = true;
        }else if(_.isEmpty(record) == false && pass == false){
            record      = [];
            message     = LANG[cc].GENERAL.ACCESS_DENIED;
            aknowledge  = false;
        }else{
            message     = LANG[cc].GENERAL.DATA_NOT_FOUND;
            aknowledge  = false;
        }
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, message,);
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.getPaging = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getPaging`);

        let { uid, countrycode } = req.headers;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let { page, limit, skp, specialist, keyword }    = req.query;
        let headers = { 
            appid: 1,
            platform: 'android' 
        }

        let filter  = [];

        if (!_.isEmpty(countrycode)) {
            filter.push({ key: 'country_code', value: countrycode });
        }

        if (!_.isEmpty(skp)) {
            filter.push({ key: 'skp', value: skp });
        }

        if (!_.isEmpty(keyword)) {
            filter.push({ key: 'keyword', value: keyword });
        }

        if (!_.isEmpty(specialist)) {
            specialist  = _.isArray(specialist) ? specialist : [specialist];
        
            filter.push({ key: 'specialist', value: specialist.join(',') });
        }

        let record  = await ContentSVC.webinar.getAll(uid, filter, page, limit, headers);

        //add current server time
        let dt          = datetime.create();
        let formatted   = dt.format('Y-m-d H:M:S');
        record.server_date = formatted;

        console.log(`└─ ${LOG} : getPaging -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : getPaging -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.getPretest = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : getPretest`);

        let { uid, email, countrycode }    = req.headers;
        let { id }                          = req.params

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            country_code: countrycode,
            platform: 'android' 
        }

        let payload = {
            uid     : uid,
            email   : email
        }

        let record  = await ContentSVC.webinar.getPretest(uid, id, payload,headers);

        console.log(`└─ ${LOG} : getPretest -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : getPretest -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.getPosttest = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : getPosttest`);
        let { uid, email, countrycode }     = req.headers;
        let { id }      = req.params

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            country_code: countrycode,
            platform: 'android' 
        }

        let payload = {
            uid     : uid,
            email   : email
        }

        let record  = await ContentSVC.webinar.getPosttest(uid, id, payload, headers);

        console.log(`└─ ${LOG} : getPosttest -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : getPosttest -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.submitPretest = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : submitPretest`);
        let { id } = req.params;
        let { uid, email, countrycode } = req.headers;
        let { member_id, id_attempt, answers, time } = req.body;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            country_code: countrycode,
            platform: 'android' 
        }

        let payload = {
            uid         : uid,
            email       : email,
            member_id   : member_id,
            id_attempt  : id_attempt, 
            answers     : answers,
            time        : time
        }
        
        req.headers['content-type'] = 'application/json'
        
        let record  = await ContentSVC.webinar.submitPretest(uid, id, payload, headers);

        console.log(`└─ ${LOG} : submitPretest -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : submitPretest -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.submitPosttest = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : submitPosttest`);
        
        let { id } = req.params;
        let { uid, email, countrycode } = req.headers;
        let { member_id, id_attempt, answers, time } = req.body;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            country_code: countrycode,
            platform: 'android' 
        }

        let payload = {
            uid         : uid,
            email       : email,
            member_id   : member_id,
            id_attempt  : id_attempt, 
            answers     : answers,
            time        : time
        }

        req.headers['content-type'] = 'application/json'
        
        let record  = await ContentSVC.webinar.submitPosttest(uid, id, payload, headers);

        console.log(`└─ ${LOG} : submitPosttest -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : submitPosttest -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

WebinarController.getAttendeFromContent = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : getAttendeFromContent`);
        
        let { uid, countrycode } = req.headers;
        let { stringAttendee } = req.body;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let headers = { 
            country_code: countrycode,
            platform: 'android' 
        }

        let payload = {
            stringAttendee: stringAttendee
        }

        req.headers['content-type'] = 'application/json'
        
        let record  = await ContentSVC.webinar.getAttende(uid, payload, headers);

        console.log(`└─ ${LOG} : getAttendeFromContent -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].GENERAL.SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : getAttendeFromContent -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

// ----------- connect to db --------------

WebinarController.submiteCertificate = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : submiteCertificate`);
        
        let { uid, countrycode } = req.headers;
        let { webinar_id, fullname } = req.body;
        console.log(req.headers);
        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
        let result      = {};
        let acknowledge = false;
        let message     = LANG[cc].GENERAL.DATA_NOT_FOUND;
        let now         = moment().format('YYYY-MM-DD h:mm:ss');

        let user = await UserModel.getBy([{ key: 'uid', op: '=', value: uid }]);

        if (!_.isEmpty(user)) {
            let conditions      = [
                { jointer: 'AND', key: 'user_certificate.uid', value: uid, op: '=' },
                { jointer: 'AND', key: 'user_certificate.event_id', value: webinar_id, op: '=' },
                { jointer: 'AND', key: 'user_certificate.type', value: 'webinar', op: '=' },
                { jointer: 'AND', key: 'user_certificate.status', value: 'A', op: '=' }
            ];
            let userCertificate = await UserCertificateModel.getBy(conditions, [], [], 1);

            // checking user certificate
            if (!_.isEmpty(userCertificate)) {
                message = LANG[cc].WEBINAR.CERTIFICATE.ALREADY_HAVE;
            } else {
                let data = {
                    event_id    : webinar_id,
                    fullname    : fullname,
                    uid         : uid
                }
                let payload = querystring.stringify(data);
                
                let certificate = await ContentCMS.createCertificate(uid, payload, req.headers);
                    certificate = certificate ? certificate : {};

                if (!_.isEmpty(certificate) && (!_.isUndefined(certificate.data) && !_.isEmpty(certificate.data))) {
                    let event = {
                        skp         : certificate.data.skp,
                        title       : certificate.data.event_title,
                        certificate : certificate.data.filename,
                    }
    
                    // insert user certificate
                    let userCertificate = [
                        { key: 'uid', value: uid }, 
                        { key: 'event_id', value: webinar_id }, 
                        { key: 'event_title', value: event.title }, 
                        { key: 'fullname', value: fullname }, 
                        { key: 'filename', value: event.certificate }, 
                        { key: 'type', value: 'webinar' }, 
                        { key: 'skp', value: event.skp }, 
                        { key: 'status', value: 'A' }, 
                    ];
                    await UserCertificateModel.save(userCertificate);
                    
                    // check existing skp
                    let condCheckSKP    = [{ key: 'uid', op: '=', value: uid }, { key: 'quiz_id', op: '=', value: webinar_id }];
                    let existingSKP     = await UserSkpTmpModel.getBy(condCheckSKP);
    
                    // update/insert new skp
                    let condUpsertSKP    = [];
                    if (!_.isEmpty(existingSKP)) {
                        condUpsertSKP.push({ key: 'uid', op: '=', value: uid });
                        condUpsertSKP.push({ key: 'quiz_id', op: '=', value: webinar_id });
                    }
                    
                    let userSkpTmp      = [
                        { key: 'uid', value: uid }, 
                        { key: 'skp', value: event.skp }, 
                        { key: 'title', value: event.title }, 
                        { key: 'source', value: 'D2D' }, 
                        { key: 'taken_on', value: now }, 
                        { key: 'certificate', value: event.certificate }, 
                        { key: 'quiz_id', value: webinar_id }, 
                        { key: 'type', value: 'webinar' }, 
                        { key: 'status', value: 'valid' }, 
                        { key: 'create_date', value: now }, 
                    ];
                    await UserSkpTmpModel.save(userSkpTmp, condUpsertSKP);

                    let notifTitle = utils.stringReplace(LANG[cc].WEBINAR.CERTIFICATE.GET_CERTIFICATE,[event.skp,event.title]);
                    notifTitle = notifTitle.length > 100 ? notifTitle.substring(0,96) + '...' : notifTitle;

                    // sending notification
                    let notif   = {
                        title           : notifTitle,
                        body            : LANG[cc].WEBINAR.CERTIFICATE.ACCESS_NOTIFY,
                        type            : 'certificate_webinar',
                        content_id      : `${webinar_id}`
                    };
                    console.log(notif)
                    let data    = {
                        'type'          : notif.type,
                        'content_id'    : notif.content_id,
                        'certificate'   : ''
                    };
                    let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);

                    // inser full url certificate
                    certificate.data.filename = `${BUCKET.CERTIFICATE}${uid}/${certificate.data.filename}`;

                    acknowledge = true;
                    message     = LANG[cc].WEBINAR.CERTIFICATE.SUCCESS_CREATED;
                    result      = certificate.data;
                } else {
                    message     = certificate.message;
                    acknowledge = false;
                }   
            }
        }

        console.log(`└─ ${LOG} : submiteCertificate -> Success`);
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : submiteCertificate -> Error`);
        console.log(error);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}


WebinarController.emaileCertificate = async (req, res, next) => {

    try {
        console.log(`┌─ ${LOG} : emaileCertificate`);
        
        let { uid, countrycode } = req.headers;
        let { certificate, title, displayName, email } = req.body;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        certificate   = _.isEmpty(certificate) || _.isUndefined(certificate) ? '' : certificate;
        title         = _.isEmpty(title) || _.isUndefined(title) ? '' : title;
        displayName   = _.isEmpty(displayName) || _.isUndefined(displayName) ? '' : displayName;
        email         = _.isEmpty(email) || _.isUndefined(email) ? '' : email;
        let valid     = false
        let message   = ''

        if (certificate == '') {
            message = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.CERTIFICATE_ISEMPTY
        } else if (title == '') {
            message = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.TITLE_ISEMPTY
        } else if (displayName == '') {
            message = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.DISPLAY_NAME_ISEMPTY
        } else if (email == '') {
            message = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.EMAIL_ISEMPTY
        } else {
            valid = true
        }

        if (valid){
            let subject         = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.GET_CERTIFICATE;
            let emailContent    = `<p>
                                   Dear ${displayName},<br/><br/>
                                   ${utils.stringReplace(LANG[cc].WEBINAR.EMAIL_CERTIFICATE.GET_CERTIFICATE_DETAIL,[title])}<br/><br/>
                                   Best wishes,<br/>
                                   D2D<br/><br/>
                                   </p>
                                   <center>
                                       <a href="${certificate}" target="_blank">
                                           <button class="activation">DOWNLOAD CERTIFICATE</button>
                                       </a>
                                   </center>`;
            let form            = { toEmail: email, subject, emailContent, apps: 'd2d' };

            let resemail = await ContentSVC.email.send(uid, form)
            message = LANG[cc].WEBINAR.EMAIL_CERTIFICATE.SEND_CERTIFICATE
            console.log(resemail)
        }

        console.log(`└─ ${LOG} : emaileCertificate -> Success`);
        parseResponse(res, 200, {}, message);
    } catch (error) {
        console.log(`└─ ${LOG} : emaileCertificate -> Error`);
        console.log(error);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = WebinarController;