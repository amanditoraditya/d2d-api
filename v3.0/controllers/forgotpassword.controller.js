const _                     = require('lodash');
const UserModel             = require('../models/user.model');
const UserSVC               = require('../../utils/user-service');
const NotifSVC              = require('../../utils/notif-service');
const Utils                 = require('../../utils/utils');
const parseResponse         = require('../../utils/helpers/parse-response');
const {COUNTRY_CODES, LANG} = require('../../utils/helpers/localization');
const { URL}                = require('../../utils/helpers/enum');
const LOG                   = 'ForgotPasswordController';

const ForgotPasswordController = {}

ForgotPasswordController.forgotPassword = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : forgotPassword`);

        let { email }   = req.body;

        let cc          = COUNTRY_CODES.Indonesia;
        let message     = LANG[cc].FORGOT_PASSWORD.SUCCESS;
        let result      = {};
        let acknowledge = false;

        // check user-service
        let { acknowledge: ackUser, result: userService } = await UserSVC.getUserByEmail(null, { email });

        if (ackUser == true) {
            if (_.isEmpty(userService)) {
                // user is doesn't exist
                message         = LANG[cc].FORGOT_PASSWORD.EMAIL_NOT_EXIST;
            } else {
                if (userService.emailVerified == true) {
                    // check user on db
                    let conditionDB = [{ jointer: 'AND', key: 'email', op: '=', value: email }];
                    let userDB      = await UserModel.getBy(conditionDB, [], [], 1);

                    //insert data di db d2d sesuai dengan data dari user-service
                    if (_.isEmpty(userDB)) {
                        let data    = [
                            { key: 'uid' , value: userService.uid }, 
                            { key: 'name' , value: userService.displayName }, 
                            { key: 'email' , value: userService.email },
                            { key: 'enabled' , value: !userService.disabled },
                        ];
                        let resInsertUser = await UserModel.save(data);
                    }

                    // sending email
                    let randString  = Utils.createRandomString(6);
                    let updateCode  = await UserSVC.setPassCode(userService.uid, { email, passcode: randString });
                    let sendEmail   = await NotifSVC.sendEmail(userService.uid, userService.displayName, email, 'FORGOT_PASSWORD', { link: `${URL.FORGOT_PASSWORD}/${userService.uid}/${randString}` });
                    
                    acknowledge     = true;
                    message         = sendEmail.result == true ? LANG[cc].FORGOT_PASSWORD.SEND_EMAIL_SUCCESS : LANG[cc].FORGOT_PASSWORD.SEND_EMAIL_FAILED;
                    result          = {
                        email       : userService.email,
                        name        : userService.displayName,
                        sendEmail   : sendEmail.result
                    };
                } else {
                    message         = LANG[cc].REGISTER.VERIFY_ACCOUNT;
                }
            }

        } else {
            message     = LANG[cc].FORGOT_PASSWORD.EMAIL_NOT_EXIST;
        }

        console.log(`└─ ${LOG} : forgotPassword -> Success`);
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(`└─ ${LOG} : forgotPassword -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

ForgotPasswordController.resendForgotPassword = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : resendForgotPassword`);
        
        let { email }   = req.body;

        let cc          = COUNTRY_CODES.Indonesia;
        let result      = false;
        let message     = LANG[cc].GENERAL.DATA_NOT_FOUND;
        let acknowledge = false;

        let { acknowledge: ackUser, result: userService } = await UserSVC.getUserByEmail(null, { email });
        if (ackUser == true) {
            if (!_.isEmpty(userService)) {
                if (userService.emailVerified == true) {
                    let randString  = Utils.createRandomString(6);
                    let updateCode  = await UserSVC.setPassCode(userService.uid, { email, passcode: randString });
                    let sendEmail   = await NotifSVC.sendEmail(userService.uid, userService.displayName, email, 'FORGOT_PASSWORD', { link: `${URL.FORGOT_PASSWORD}/${userService.uid}/${randString}` });

                    acknowledge     = sendEmail.acknowledge;
                    message         = sendEmail.result == true ? LANG[cc].FORGOT_PASSWORD.SEND_EMAIL_SUCCESS : LANG[cc].FORGOT_PASSWORD.SEND_EMAIL_FAILED;
                    result          = {
                        email       : userService.email,
                        name        : userService.displayName,
                        sendEmail   : sendEmail.result
                    };

                    console.log(`└─ ${LOG} : resendForgotPassword -> Success`);
                } else {
                    message         = LANG[cc].REGISTER.VERIFY_ACCOUNT;
                }
            }
        }

        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(error)
        console.log(`└─ ${LOG} : resendForgotPassword -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

ForgotPasswordController.changePassword = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : changePassword`);

        let { uid, passcode, password }   = req.body;

        let cc          = COUNTRY_CODES.Indonesia;
        let result      = false;
        let message     = LANG[cc].FORGOT_PASSWORD.CHANGE_PASSWORD_FAILED;
        let acknowledge = false;

        let { acknowledge: ackUser, result: userService } = await UserSVC.getUser(uid);
        if (ackUser == true) {
            if (!_.isEmpty(userService)) {
                let changePassword  = await UserSVC.changePassword(userService.uid, { email: userService.email, passcode, password });

                acknowledge     = changePassword.acknowledge;
                message         = changePassword.acknowledge == true ? LANG[cc].FORGOT_PASSWORD.CHANGE_PASSWORD_SUCCESS : changePassword.message;
                result          = changePassword.result;

                console.log(`└─ ${LOG} : changePassword -> Success`);
            }
        }
        
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        console.log(error)
        console.log(`└─ ${LOG} : changePassword -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = ForgotPasswordController;