const _                     = require('lodash');
const moment                = require('moment');
const CmeModel              = require('../models/cme.model');
const QuizQuestionModel     = require('../models/quiz_questions.model');
const QuizModel             = require('../models/quiz.model');
const UserSkpModel          = require('../models/user_skp.model');
const QuizTakenModel        = require('../models/quiz_taken.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const utils                 = require('../../utils/utils');
const co                    = require('co');
const fs                    = require('fs');
const oss                   = require('ali-oss');
const LOG                   = 'CmeController';
const datetime          = require('node-datetime');
const path                  = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';
const ContentSVC            = require('../../utils/content-service');
const D2DSVC                = require('../../utils/d2d-service');
const NotifService          = require('../../utils/notif-service');
const UserSkpTmpModel       = require('../models/user_skp_tmp.model');
const D2DCMSSVC             = require('../../utils/d2d-cms-service');
const jwt                   = require('jsonwebtoken');
const querystring           = require("querystring");
const { sort } = require('../../utils/utils');
const { create } = require('lodash');

const CmeController = {}

CmeController.quizList = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : quizList`);

        let { type }      = req.params
        let { uid }       = req.headers
        let { page, limit }      = req.query

        let condition      = []
        let record         = []
        let agreement      = []
        let join           = []
        let fields         = []

        if (type == 'offline') {
            record = await CmeModel.getOffline(uid, condition, page, limit)
            total = await CmeModel.getOfflineTotal(uid, condition)
        } else if (type == 'all' || type == 'available' || type == 'done') {
            if (type == 'all') {
                condition.push({ key:`(DATE_FORMAT(NOW(), '%Y-%m-%d') >= quiz.available_start AND DATE_FORMAT(NOW(), '%Y-%m-%d') <= quiz.available_end)` });
            } else if (type == 'available') {
                condition.push({ key:`cert.uid IS NULL` });

            } else if (type == 'done') {
                condition.push({ key:`cert.uid IS NOT NULL` });
            }
            record = await CmeModel.getAll(uid, condition, page, limit)
            total  = await CmeModel.getAllTotal(uid, condition)
        }
        //get skp
        let skp = await CmeModel.getSkp(uid)
        
        let result = {
            quiz: record,
            skp:  skp[0]
        }

        console.log(`└─ ${LOG} : quizList -> Success`);
        parseResponse(res, 200, result, _.isEmpty(result.quiz) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : quizList -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.quizDetail = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : quizDetail`);

        let { quizid }     = req.params;
        let { uid }         = req.headers

        let condition       = []
        let record          = []
        let valid           = true
        let message         = 'Success'
        let data            = []

        //condition default
        if (!_.isEmpty(quizid)) {
            condition.push({ jointer: 'AND', key: `quiz_id`, value: quizid, op: '=' });
        } else {
            valid = false
            message = 'Quiz ID Invalid'
        }
        //table yang diselect join agreements dan user_agreements
        if (valid){
            data            = await QuizQuestionModel.getAll(condition)
        }
        //data handling
        let arr = []
        for(let i = 0; i < data.length; i++) {
            data[i].answer   = unescape(data[i].answer)
            let choices                 = JSON.parse(data[i].choices)
            let pilihan                 = choices.map(unescape)
            data[i].choices  = JSON.stringify(pilihan)
            //data[i].dataValues.choices  = choices.map(unescape)

            arr.push(data[i])
        }
            
        record   = await utils.randomArray(arr)

        console.log(`└─ ${LOG} : quizDetail -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : quizDetail -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.submitOffline = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : submitOffline`);

        let { uid }     = req.headers
        let {
            skp,
            title,
            source,
            taken_on
        } = req.body
        let type        = `certificate`;
        
        let data = [
            { key: 'uid',      value: uid },
            { key: 'type',     value: 'offline' },
            { key: 'skp',      value: skp },
            { key: 'title',    value: title },
            { key: 'source',   value: source },
            { key: 'taken_on', value: taken_on }
        ];

        let condition       = []
        let record          = []
        let agreement_log   = []

        //table yang diselect user_agreement_logs
        record = await UserSkpTmpModel.save(data)
        
        let rowId       = record.insertId;
            const client    = new oss({
                region: 'oss-ap-southeast-5',
                accessKeyId: 'LTAINk4kOx5TLJT6',
                accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
                bucket: 'd2doss'
            })

            console.log(`start uploading...`);

            co(function* () {
                console.log(`mulai`);
                let arr = req.file.originalname.split('.');
                let ext = arr[(arr.length-1)];
                console.log(`${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);
                let result = yield client.put(`${path}${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);

                fs.unlink(`../../fileupload/${req.file.filename}`, function(err) {
                    if(!err) {
                        let dataUpdate = [
                            { key: 'certificate', value: `${req.file.filename}.${ext}` }
                        ]
                        let conditionUpdate = [
                            { jointer: 'AND', key: `id`, value: rowId, op: '=' }
                        ]
                        UserSkpTmpModel.save(dataUpdate, conditionUpdate)
                        
                        let dataRes =  {certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${req.file.filename}.${ext}`}
                        console.log(`└─ ${LOG} : submitOffline -> Success`);
                        parseResponse(res, 200, dataRes, _.isEmpty(dataRes) ? 'No data found!' : 'Success');                
                    } else {
                        console.log(error);
                        let err = new Error(error)
                        err.code = 500;
                        next(err);
                    }
                })
            })
    } catch (error) {
        console.log(`└─ ${LOG} : submitOffline -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.resetSkp = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : resetSkp`);

        let { uid }     = req.headers

        let condition       = []
        let record          = []
        //table yang diselect user_agreement_logs
        record = await UserSkpTmpModel.getAll([{key:`uid = '${uid}'`}])
        record.forEach(async row => {
            
            let data = [
                { key: 'uid',           value: row.uid },
                { key: 'skp',           value: row.skp },
                { key: 'title',         value: row.title },
                { key: 'source',        value: row.source },
                { key: 'taken_on',      value: row.taken_on },
                { key: 'certificate',   value: row.certificate },
                { key: 'quiz_id',       value: row.quiz_id },
                { key: 'type',          value: row.type },
                { key: 'status',        value: row.status },
                { key: 'create_date',   value: row.create_date }
            ];
            let ressave = await UserSkpModel.save(data)
        });
        //delete data dari skp
        let resdelete = await UserSkpTmpModel.delete([{key:`uid = '${uid}'`}])

        console.log(`└─ ${LOG} : resetSkp -> Success`);
        parseResponse(res, 200, resdelete, _.isEmpty(resdelete) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : resetSkp -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.emailCertificate = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : resetSkp`);

        let { uid }     = req.headers
        let { email, displayName, certificate, title } = req.body
        let subject         = `Your CME Certificate is here!`
        let emailContent    = `<p>Dear ${displayName},<br/><br/>Here is your certificate for quiz ${title}.<br/><br/>Best wishes,<br/>D2D<br/><br/></p><center><a href="${certificate}" target="_blank"><button class="activation">DOWNLOAD CERTIFICATE</button></a></center>`;
        let body            = {
            toEmail: email,
            subject,
            emailContent,
            apps: 'd2d'
        }

        let condition       = []
        let record          = []
        
        record = await ContentSVC.email.send(uid, body)

        console.log(`└─ ${LOG} : resetSkp -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : resetSkp -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.submitQuiz = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : submitQuiz`);

        let { uid, authorization }     = req.headers
        let { quizid, skp, source, answers } = req.body
        let ansTos      = answers;
        let answer      = ansTos.replace(/\"\[/g, '[').replace(/\]\"/g, ']');
            answer      = answer.replace(/\\"/g, '"');
            answers     = JSON.parse(answer); //answers
        
        let condition       = []
        let record          = []
        let message         = ''
        
        //get quiz detail row
        let quiz = await QuizModel.getBy([{key:'id', value: quizid}])

        let getDb = await D2DSVC.cme.getQuizDetail(uid, quizid, authorization)
        // console.log(getDb)
        let sortedAnswers   = await utils.sort(answers);
        let dbAnswers       = await utils.sort(getDb);
        let score           = await utils.compare(sortedAnswers, dbAnswers);
        let result          = await (score >= 60) ? `Y` : `N`; //result
        
        //loop escape answer dan choicesnya
        let jawaban = answers.map(obj => {
            let jawab = {
                id:         obj.id,
                question:   obj.question,
                answer:     obj.answer,
                choices:    obj.choices
            }

            jawab.answer = escape(obj.answer)
            let pilihan = obj.choices
            jawab.choices = JSON.stringify(pilihan.map(escape))

            return jawab
        })

        //save quiz taken
        let dataQuizTake = [
            {key: 'quiz_id',    value: quizid},
            {key: 'uid',        value: uid},
            {key: 'answers',    value: JSON.stringify(jawaban)},
            {key: 'result',     value: result}
        ]
        // console.log(dataQuizTake)
        let insertRes = await QuizTakenModel.save(dataQuizTake)

        if (result == 'Y') {
            const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
            const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 360});
            let form        = {uid, quiz: quizid}

            let createCertRes = await D2DCMSSVC.certificate.create(uid, querystring.stringify(form), token)
            
            if (!_.isEmpty(createCertRes.data)) {
                let certificate = createCertRes.data.split('/');
                let dt          = datetime.create();
                let formatted   = dt.format('Y-m-d H:M:S');

                let dataSkpTmp = [
                    { key: 'uid', value: uid },
                    { key: 'skp', value: skp },
                    { key: 'source', value: source },
                    { key: 'taken_on', value: formatted },
                    { key: 'certificate', value: certificate[1] },
                    { key: 'quiz_id', value: quizid },
                    { key: 'type', value: 'cme' },
                    { key: 'status', value: 'valid' },
                ]
                let createUserSkpTmpRes = await UserSkpTmpModel.save(dataSkpTmp)

                // sending notification
                let notif   = {
                    title           : quiz.skp <= 0 ? 'Selamat Anda Mendapatkan Sertifikat CME' : `Selamat Anda Mendapatkan Sertifikat CME dan ${quiz.skp} SKP`,
                    body            : `Klik disini untuk akses sertifikat Anda`,
                    type            : 'certificate_cme',
                    content_id      : `${quizid}`
                };
                
                let data    = {
                    'type'          : notif.type,
                    'content_id'    : notif.content_id,
                    'certificate'   : ''
                };

                // sending notification
                let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);
                record = { certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${certificate[1]}` }
                message = `You have successfully taken this quiz and get ${skp} SKP`
            }

            console.log(createCertRes)
        }

        console.log(`└─ ${LOG} : submitQuiz -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : submitQuiz -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

CmeController.generateCertificate = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : generate Certificate`);
        let { uid }     = req.headers
        let { quizid } = req.body
        let message     = 'success genereate'
        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 360});
        let form        = {uid, quiz: quizid}

        let createCertRes = await D2DCMSSVC.certificate.create(uid, querystring.stringify(form), token)
        console.log(`└─ ${LOG} : generate Certificate -> Success`);
        parseResponse(res, 200, createCertRes, message);
    } catch (error) {
        console.log(`└─ ${LOG} : generate Certificate -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = CmeController;