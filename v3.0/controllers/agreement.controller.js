const _                   = require('lodash');
const  moment             = require('moment');
const AgreementModel      = require('../models/agreement.model');
const parseResponse       = require('../../utils/helpers/parse-response');
const LOG                 = 'AgreementController';
const { COUNTRY_CODES }   = require('../../utils/helpers/localization');
const ContentSVC          = require('../../utils/content-service');

const AgreementController = {}

AgreementController.getAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getAll`);

        // let { id }      = req.params;
        let { uid }        = req.headers

        let condition      = []
        let record         = []
        let agreement      = []
        let join           = []
        let fields         = []
        let message        = 'Success'

        //defult conditions untuk select
        condition.push({ key: 'agreements.status', value: 'A' })
        // condition.push({ key: 'user_agreements.uid', value: uid })
        condition.push({ key: 'user_agreements.uid IS NULL' })
        condition.push({ key: 'agreements.type', value: 'popup_tnc' })

        join.push(`LEFT JOIN user_agreements ON user_agreements.agreement_id = agreements.id AND user_agreements.uid = '${uid}'`)
        
        fields = [
            'agreements.*',
            'user_agreements.uid'
        ]

        //table yang diselect join agreements dan user_agreements
        agreement = await AgreementModel.agreement.getAll(condition, join, fields)
        
        if (agreement.length > 0) {
            
            let agreement_logs = await AgreementModel.log.getBy([{ jointer: 'AND', key: 'uid', value: uid, op: '=' }])
            
            if (_.isEmpty(agreement_logs)) {
                let data = [
                    { key: 'uid',           value: uid },
                    { key: 'agreement_id',  value: agreement[0].id }
                ];
                record = await AgreementModel.log.save(data)
            }
            else {
                let show = agreement_logs.show
                let total_show = agreement[0].total_show
                
                if (show <= total_show) {
                    record = agreement
                } else {
                    message = 'Data tidak ditampilkan'
                }
            }
        } else {
            message = 'No Data Found'
        }
        record = !_.isEmpty(record) && record.length > 1 ? record[0] : record

        console.log(`└─ ${LOG} : getAll -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : getAll -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

AgreementController.submit = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : submit`);

        // let { id }      = req.params;
        let { uid }                  = req.headers
        let { agreement_id, status } = req.body

        let condition       = []
        let record          = []
        let user_agreement  = []
        let message         = ''
        let statusEnum      = ['A', 'D']

        //defult conditions untuk select
        condition.push({ key: 'agreement_id', value: agreement_id })
        condition.push({ key: 'uid', value: uid })

        //table yang diselect join agreements dan user_agreements
        user_agreement = await AgreementModel.userAgreement.getAll(condition)

        if (user_agreement.length > 0) {
            let valid = true
            let data  = [{ key: 'uid', value: uid }]

            //check agreement_id
            if (!_.isEmpty(agreement_id) && !_.isUndefined(agreement_id)) {
                data.push({ key: 'agreement_id',  value: agreement_id })
            } else {
                message = 'Submit Failed, agreement_id cannot be empty'
                valid   = false
            }

            //check status
            if (!_.isEmpty(status) && !_.isUndefined(status)) {
                
                if(statusEnum.includes(status)) {
                    data.push({ key: 'status',  value: status })
                } 
                else {
                    valid   = false
                    message = 'Submit Failed, status Invalid'
                }
            } else {
                message = 'Submit Failed, status cannot be empty'
                valid   = false
            }
            
            if (valid) {
                record  = await AgreementModel.userAgreement.save(data)
                message = 'Anda berhasil submit'
            } 
        }
        else {
            message = 'Anda telah menyetujui'
        }
        console.log(`└─ ${LOG} : submit -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : submit -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

AgreementController.cancel = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : cancel`);

        let { uid }         = req.headers

        let condition       = []
        let record          = []
        let agreement_log   = []
        let message         = ''

        //defult conditions untuk select
        condition.push({ key: 'uid', value: uid })

        //table yang diselect user_agreement_logs
        agreement_log = await AgreementModel.log.getBy(condition)
        
        if (agreement_log != undefined) {

            let show = agreement_log.show + 1
            let data = [{ key: 'show', value: String(show) }]

            record = await AgreementModel.log.save(data, condition)
            message = 'Added Show popup'
        } else {
            message = 'Data tidak ditemukan'
        }

        console.log(`└─ ${LOG} : cancel -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : cancel -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}


AgreementController.webinarTNCgetAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : webinarTNCgetAll`);

        let { uid }          = req.headers
        let { 
            country_code, webinar_id
        } = req.query

        let condition       = []
        let record          = []
        let join            = []
        let fields          = []
        
        country_code        = _.isEmpty(country_code) || _.isUndefined(country_code) ? COUNTRY_CODES.Indonesia : _.toUpper(country_code);

        //check this webinar is paid webinar or free webianr
        let headers      = { 
            country_code: country_code,
            platform: 'android' 
        };
        let webinar = await ContentSVC.webinar.get(uid, 'id', webinar_id, '', headers);
        let webinarType = '';
        if(_.isEmpty(webinar) == false){
            webinarType = webinar.paid == 1? "webinar_tnc_paid"  : "webinar_tnc_free" ;
        }
        
        //defult conditions untuk select
        condition = [];
        condition.push({ key: 'agreements.status', value: 'A' })
        condition.push({ key: 'agreements_locale.country_code', value: country_code })
        condition.push({ key: 'agreements.type', value: `${webinarType}` })

        join.push('LEFT JOIN agreements_locale ON agreements_locale.agreement_id = agreements.id');

        fields = [
            'agreements_locale.*',
            'agreements_locale.id as locale_id',
            'agreements_locale.agreement_id as id',
            'agreements.total_show',
            'agreements.status',
            'agreements.type',
            'agreements.company',
            'agreements_locale.country_code',
        ];

        //table yang diselect join agreements dan user_agreements_logs
        record = await AgreementModel.agreement.getAll(condition, join, fields, [], [], '1', '1')
        
        record = _.omit(record[0], ['status', 'created', 'updated', 'total_show', 'company']);
        let agreement_log = await AgreementModel.log.getBy([
            { key: 'uid', value: uid },
            { key: 'webinar_id', value: webinar_id }
        ]);
        
        record.user_submit = null;
        if (_.isEmpty(agreement_log) == false) {
            record.user_submit = agreement_log.isAgree == null ? null : (agreement_log.isAgree == 1? 'accept' : 'decline');
        }
        
        console.log(`└─ ${LOG} : webinarTNCgetAll -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : webinarTNCgetAll -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

AgreementController.webinarTNCSubmit = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : webinarTNCSubmit`);

        let { uid }         = req.headers
        let { 
            isAgree, 
            agreement_id, 
            webinar_id 
        } = req.body

        let valid           = false
        let condition       = [
            { key: 'uid', value: uid },
            { key: 'agreement_id', value: agreement_id },
            { key: 'webinar_id', value: webinar_id }
        ]
        let conditionUpdate = []
        let record          = []
        let agreement_log   = []
        let isAgreeSel      = ['0', '1']
        let message         = 'Success'

        //check isAgree
        if (_.isEmpty(isAgree) && _.isUndefined(isAgree)) {
            message = 'isAgree cannot be empty'
        } else if (!isAgreeSel.includes(isAgree)) {
            message = 'isAgree must invalid (must be 1 or 0)'
        } else {
            valid = true
        }
        
        if (valid) {
            let data            = [
                { key: 'isAgree', value: isAgree }, 
                { key: 'agreement_id', value: agreement_id }, 
                { key: 'webinar_id', value: webinar_id }, 
            ]

            //table yang diselect user_agreement_logs
            agreement_log = await AgreementModel.log.getBy(condition)
            
            if (!_.isEmpty(agreement_log)) {
                conditionUpdate = condition
            } else {
                data.push({ key: 'uid', value: uid })
            }
            
            record = await AgreementModel.log.save(data, conditionUpdate)
        }

        console.log(`└─ ${LOG} : webinarTNCSubmit -> Success`);
        parseResponse(res, 200, record, message, valid);
    } catch (error) {
        console.log(`└─ ${LOG} : webinarTNCSubmit -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = AgreementController;