const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const request               = require('request');
const RchatSvc              = require('../../utils/RchatSvc');

const RchatController    = {}
const log                   = 'Activity controller';

RchatController.testing = async (req, res, next) => {
    // try {
        console.log('testing');
    // } catch (error) {
    //     console.log(`│  ├── ${log} :: insert :: error`);
    //     console.log(error)

    //     let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
    //     let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
    //     let err         = new Error(msg)

    //     err.code        = code
    //     err.database    = code == 488 ? { message: msg } : null;
    //     next(err);
    // }
}

/**
 * Login to chat
 * hit API post => rchat/auth/login
 */
RchatController.joinwebinar = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.joinwebinar(req);
        let resp        = responseCustom.v21(200, true, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Send Message To Room OR DM user
 * hit API post => rchat/messages/send/:email
 */
RchatController.sendMessage = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.sendMessage(req);
        let resp        = responseCustom.v21(200, true, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Uplaod Avatar User
 * hit API post => rchat/profile/avatar
 */
RchatController.uploadAvatar = async (req, res, next) => {

}

/**
 * Get Avatar User
 * hit API get => rchat/profile/avatar/:username
 */
RchatController.getAvatar = async (req, res, next) => {
    try {
        await RchatSvc.getAvatar(req, res);
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Create Group Chat
 * hit API post => rchat/groups/create
 */
RchatController.createGroup = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.createGroup(req);
        let resp        = responseCustom.v21(200, true, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = RchatController;