const _                     = require('lodash');
const UserSVC            = require('../../utils/user-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'EulaController';

const EulaController = {}

EulaController.getBy = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getBy`);

        let { uid }  = req.headers;
        let { type } = req.params;

        let record  = await UserSVC.eula.getBy(uid, type)

        console.log(`└─ ${LOG} : getBy -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getBy -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

EulaController.getByAndCheck = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getByAndCheck`);

        let { uid }  = req.headers;
        let { type } = req.params;
        
        let record  = await UserSVC.eula.getByAndCheck(uid, type)

        console.log(`└─ ${LOG} : getByAndCheck -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : getByAndCheck -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = EulaController;