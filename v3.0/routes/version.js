const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { VersionController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    getVersion: [
        body('version')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('build_number')
            .isNumeric().withMessage('Invalid format ?, must be numeric')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
};

router
    // .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/',  Validation.getVersion, AuthMiddleware.validation, VersionController.get)

module.exports = router;