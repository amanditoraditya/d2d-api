const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { PatientController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', PatientController.getAll)
    .get('/:uid', PatientController.get)

module.exports = router;