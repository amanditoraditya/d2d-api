const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { DkonsulController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/login', DkonsulController.login)
    // .get('/eula', DkonsulController.getEula)
    .post('/submit', DkonsulController.save)

module.exports = router;