const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { JournalController }     = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/request', JournalController.request)

module.exports = router;