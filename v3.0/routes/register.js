const router                    = require('express').Router();
const { body }                  = require('express-validator/check')
const { RegisterController }    = require('../controllers');
const { AuthMiddleware }        = require('../middlewares');

const Validation = {
    save: [
        body('name')
            .isLength({ min: 3 }).withMessage('? min.3 characters')
            .custom((value, {req, loc, path}) => {
                let re = /^(?!\s)[A-Za-z\.\'\’\s]+$/;
                if (re.test(value)) {
                    return value;
                } else {
                    throw new Error("Invalid format ?");
                }
            })
            .not().isEmpty().withMessage('Invalid param ?'),
        body('email')
            .isEmail().withMessage('Invalid email format')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('country_code')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('password')
            .isLength({ min: 6 }).withMessage('? min.6 characters')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('confirm_password')
            .custom((value,{req, loc, path}) => {
                if (value !== req.body.password) {
                    throw new Error("Passwords don't match");
                } else {
                    return value;
                }
            }).withMessage('Password must match.')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
    saveByDevice: [
        body('name')
            .isLength({ min: 3 }).withMessage('? min.3 characters')
            .custom((value, {req, loc, path}) => {
                let re = /^(?!\s)[A-Za-z\.\'\’\s]+$/;
                if (re.test(value)) {
                    return value;
                } else {
                    throw new Error("Invalid format ?");
                }
            })
            .not().isEmpty().withMessage('Invalid param ?'),
        body('country_code')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('device_unique_id')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
}

router
    .post('/', Validation.save, AuthMiddleware.validation, RegisterController.register)
    .post('/by_device', Validation.saveByDevice, AuthMiddleware.validation, RegisterController.registerByDeviceId)
    .post('/resend_verification', RegisterController.resendVerification)
    .get('/verify/:uid/:code', RegisterController.verification)

module.exports = router;