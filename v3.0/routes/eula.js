const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { EulaController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/:type', EulaController.getBy)
    .get('/check/:type', EulaController.getByAndCheck)

module.exports = router;