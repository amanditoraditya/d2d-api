const router = require('express').Router();
const { header, body } = require('express-validator/check')
const { WebinarController } = require('../controllers');
const { AuthMiddleware }    = require('../middlewares');

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    submitPrePostTest: [
        body('id_attempt')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('answers')
            .not().isEmpty().withMessage('Invalid param ?'),
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', WebinarController.getPaging)
    .get('/:id', WebinarController.get)
    .get('/hash/:hash', WebinarController.get)
    .post('/attendee',WebinarController.getAttendeFromContent)
    .post('/ecertificate', WebinarController.submiteCertificate)
    .post('/email-certificate', WebinarController.emaileCertificate)
    .get('/posttest/:id', WebinarController.getPosttest)
    .get('/pretest/:id', WebinarController.getPretest)
    .post('/posttest/:id/answers', Validation.submitPrePostTest, AuthMiddleware.validation, WebinarController.submitPosttest)
    .post('/pretest/:id/answers', Validation.submitPrePostTest, AuthMiddleware.validation, WebinarController.submitPretest)

module.exports = router;