const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { LoginController }   = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    
    login: [
        body('email')
            .isEmail().withMessage('Invalid email format')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/', Validation.login, AuthMiddleware.validation, LoginController.doLogin)

module.exports = router;