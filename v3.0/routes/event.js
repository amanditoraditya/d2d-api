const router = require('express').Router();
const { header, body } = require('express-validator/check')
const { EventController } = require('../controllers');
const { AuthMiddleware }    = require('../middlewares');

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', EventController.getPaging)
    .get('/:id', EventController.get)
    .get('/hash/:hash', EventController.get)

module.exports = router;