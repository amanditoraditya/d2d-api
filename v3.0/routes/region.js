const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { RegionController }  = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/country/', RegionController.getCountryPaging)
    .get('/country/:id', RegionController.getCountry)

    .get('/district/', RegionController.getDistrictPaging)
    .get('/district/r:districtid', RegionController.getDistrictPaging)
    .get('/district/:id', RegionController.getDistrict)

    .get('/province/', RegionController.getProvincePaging)
    .get('/province/c:provinceid', RegionController.getProvincePaging)
    .get('/province/:id', RegionController.getProvince)

    .get('/regency/', RegionController.getRegencyPaging)
    .get('/regency/p:regencyid', RegionController.getRegencyPaging)
    .get('/regency/:id', RegionController.getRegency)

    .get('/village/', RegionController.getVillagePaging)
    .get('/village/d:villageid', RegionController.getVillagePaging)
    .get('/village/:id', RegionController.getVillage)

module.exports = router;