const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { NotificationController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/:id', NotificationController.get)
    .get('/', NotificationController.getAll)
    .delete('/:id', NotificationController.delete)
    .post('/:status', NotificationController.updateStatus)
    // .post('/', NotificationController.save)

module.exports = router;