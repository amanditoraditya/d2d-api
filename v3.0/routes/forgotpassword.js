const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { ForgotpasswordController }   = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    forgot: [
        body('email')
            .isEmail().withMessage('Invalid email format')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    changePassword: [
        body('uid')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('passcode')
            .not().isEmpty().withMessage('Invalid param ?'),
        // body('password')
        //     .isLength({ min: 6 }).withMessage('? min. 6 characters'),
    ],
};

router
    .post('/', Validation.forgot, AuthMiddleware.validation, ForgotpasswordController.forgotPassword)
    .post('/resend', Validation.forgot, AuthMiddleware.validation, ForgotpasswordController.resendForgotPassword)
    .post('/change', Validation.changePassword, AuthMiddleware.validation, ForgotpasswordController.changePassword)

module.exports = router;