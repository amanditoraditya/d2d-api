const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { AgreementController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/active', AgreementController.getAll)
    .post('/cancel', AgreementController.cancel)
    // .get('/:id', BannerController.get)
    .post('/', AgreementController.submit)
    .get('/webinartnc', AgreementController.webinarTNCgetAll)
    .post('/webinartnc', AgreementController.webinarTNCSubmit)

module.exports = router;