const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { SettingController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', SettingController.get)
    .post('/:code', SettingController.save)

module.exports = router;