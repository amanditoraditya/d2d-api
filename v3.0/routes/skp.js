const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { SkpController }     = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', SkpController.getHistory)
    .get('/:type', SkpController.getHistory)
    .post('/:type', SkpController.getDetail)

module.exports = router;