const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { CmeController } = require('../controllers');
const { header, body }      = require('express-validator/check')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/quiz-list/:type', CmeController.quizList)
    .get('/quiz-detail/:quizid', CmeController.quizDetail)
    .post('/submit-offline', upload.single('filename'), CmeController.submitOffline)
    .post('/reset-skp', CmeController.resetSkp)
    .post('/email-certificate', CmeController.emailCertificate)
    .post('/submit-quiz', CmeController.submitQuiz)

module.exports = router;