const router                = require('express').Router();
const { RedisController }     = require('../controllers');

router
    .get('/:key',  RedisController.getValue)
    .post('/',  RedisController.setValue)
    .delete('/:key',  RedisController.delete)

module.exports = router;