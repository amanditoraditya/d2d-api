const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { TestingController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation)
    .get('/', TestingController.getPaging)
    .get('/detail/:id', TestingController.get)
    .post('/', TestingController.save)
    .delete('/delete/:id', TestingController.delete)
    .get('/generateToken', TestingController.generateToken)

module.exports = router;