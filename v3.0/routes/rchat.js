const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { RchatController }     = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .get('/', AuthMiddleware.authentication, RchatController.testing)
    .post('/joinwebinar', AuthMiddleware.authentication, RchatController.joinwebinar)
    .post('/messages/send/:email', AuthMiddleware.authentication, RchatController.sendMessage)
    .post('/profile/avatar', AuthMiddleware.authentication, RchatController.uploadAvatar)
    .get('/profile/avatar/:username', AuthMiddleware.authentication, RchatController.getAvatar)
    .post('/groups/create', AuthMiddleware.authentication, RchatController.createGroup)

module.exports = router;