const _         = require('lodash');
const { BUCKET }    = require('../../utils/helpers/enum');
const CoreDB    = require('../../utils/CoreDB');
const SkpModel  = {};

SkpModel.getBy = async (uid, conditions = []) => {
    let condition       = [];
    let conditionTmp    = [];
    let values          = [uid];
    let valuesTmp       = [uid];

    let limit = `LIMIT 1`;

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} user_skp.${record.key} ${op} ? ${groupEnd}`);
                conditionTmp.push(`${jointer} ${groupStart} user_skp_tmp.${record.key} ${op} ? ${groupEnd}`);

                values.push(record.value);
                valuesTmp.push(record.value);
            } else {
                condition.push(`${jointer} ${groupStart} user_skp.${record.key} ${groupEnd}`);
                conditionTmp.push(`${jointer} ${groupStart} user_skp_tmp.${record.key} ${groupEnd}`);
            }
        })
    }
    condition       = condition.join(' ');
    conditionTmp    = conditionTmp.join(' ');

    let query = `SELECT 
            user_skp_tmp.uid, user_skp_tmp.quiz_id, 
            COALESCE(user_skp_tmp.title, quiz.title, 'Deleted Quiz')  AS title, user_skp_tmp.skp, user_skp_tmp.source, 
            (CASE WHEN source_cme.logo IS NULL THEN NULL ELSE CONCAT('${BUCKET.SOURCE_CME}', source_cme.logo) END) AS logo, user_skp_tmp.taken_on, 
            (CASE WHEN user_skp_tmp.certificate IS NULL THEN NULL ELSE CONCAT('${BUCKET.CERTIFICATE}', user_skp_tmp.uid, '/', user_skp_tmp.certificate) END) AS certificate, 
            type, user_skp_tmp.status, user_skp_tmp.reason, user_skp_tmp.create_date
        FROM user_skp_tmp  
            LEFT JOIN quiz ON quiz.id = user_skp_tmp.quiz_id AND type = 'cme'
            LEFT JOIN source_cme ON source_cme.id = quiz.source_id
        WHERE 1
            AND uid = ? ${conditionTmp}

        UNION

        SELECT 
            user_skp.uid, user_skp.quiz_id, 
            COALESCE(user_skp.title, quiz.title, 'Deleted Quiz')  AS title, user_skp.skp, user_skp.source, 
            (CASE WHEN source_cme.logo IS NULL THEN NULL ELSE CONCAT('${BUCKET.SOURCE_CME}', source_cme.logo) END) AS logo, user_skp.taken_on, 
            (CASE WHEN user_skp.certificate IS NULL THEN NULL ELSE CONCAT('${BUCKET.CERTIFICATE}', user_skp.uid, '/', user_skp.certificate) END) AS certificate, 
            type, user_skp.status, 'reason', user_skp.create_date
        FROM user_skp 
            LEFT JOIN quiz ON quiz.id = user_skp.quiz_id AND type = 'cme'
            LEFT JOIN source_cme ON source_cme.id = quiz.source_id
        WHERE 1 
            AND  uid = ? ${condition}
        ORDER BY create_date DESC
        ${limit}`;
    
    let result  = await CoreDB.query(query, _.concat(values, valuesTmp));
        result  = !_.isEmpty(result) ? result[0] : [];

    return result;
}

SkpModel.getHistory = async (uid, conditions = [], _page = null, _limit = null, keyword = null, status = null) => {
    let offset      = (_.isEmpty(_page) || _page == 1) ? 0 : (_page - 1) * _limit;
    let limit       = _limit !== null && _page > 0 ? `LIMIT ${offset}, ${_limit}` : '';
    let condition   = [];
    let values      = [uid];
    let valuesTmp   = [uid];

    limit = _limit == 1 ? `LIMIT 1` : limit;

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);

                values.push(record.value);
                valuesTmp.push(record.value);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    let search      = !_.isEmpty(keyword) ? `AND (user_skp.title LIKE '%${keyword}%' OR quiz.title LIKE '%${keyword}%')` : ``;
    let searchTmp   = !_.isEmpty(keyword) ? `AND (user_skp_tmp.title LIKE '%${keyword}%' OR quiz.title LIKE '%${keyword}%')` : ``;
    let Fstatus     = !_.isEmpty(status) ? `AND user_skp.status = '${status}'` : ``;
    let FstatusTmp  = !_.isEmpty(status) ? `AND user_skp_tmp.status = '${status}'` : ``;

    condition       = condition.join(' ');

    let query = `SELECT 
            user_skp_tmp.uid, user_skp_tmp.quiz_id, 
            COALESCE(user_skp_tmp.title, quiz.title, 'Deleted Quiz')  AS title, user_skp_tmp.skp, user_skp_tmp.source, 
            (CASE WHEN source_cme.logo IS NULL THEN NULL ELSE CONCAT('${BUCKET.SOURCE_CME}', source_cme.logo) END) AS logo, user_skp_tmp.taken_on, 
            (CASE WHEN user_skp_tmp.certificate IS NULL THEN NULL ELSE CONCAT('${BUCKET.CERTIFICATE}', user_skp_tmp.uid, '/', user_skp_tmp.certificate) END) AS certificate, 
            type, user_skp_tmp.status, user_skp_tmp.reason, user_skp_tmp.create_date
        FROM user_skp_tmp  
            LEFT JOIN quiz ON quiz.id = user_skp_tmp.quiz_id AND type = 'cme'
            LEFT JOIN source_cme ON source_cme.id = quiz.source_id
        WHERE 1
            AND uid = ? ${searchTmp} ${FstatusTmp} ${condition}

        UNION

        SELECT 
            user_skp.uid, user_skp.quiz_id, 
            COALESCE(user_skp.title, quiz.title, 'Deleted Quiz')  AS title, user_skp.skp, user_skp.source, 
            (CASE WHEN source_cme.logo IS NULL THEN NULL ELSE CONCAT('${BUCKET.SOURCE_CME}', source_cme.logo) END) AS logo, user_skp.taken_on, 
            (CASE WHEN user_skp.certificate IS NULL THEN NULL ELSE CONCAT('${BUCKET.CERTIFICATE}', user_skp.uid, '/', user_skp.certificate) END) AS certificate, 
            type, user_skp.status, 'reason', user_skp.create_date
        FROM user_skp 
            LEFT JOIN quiz ON quiz.id = user_skp.quiz_id AND type = 'cme'
            LEFT JOIN source_cme ON source_cme.id = quiz.source_id
        WHERE 1 
            AND  uid = ? ${search} ${Fstatus} ${condition}
        ORDER BY create_date DESC
        ${limit}`;
    
    return await CoreDB.query(query, _.concat(values, valuesTmp));
}

SkpModel.getHistoryCount = async (uid, conditions = [], _page = null, _limit = null, keyword = null, status = null) => {
    let offset      = (_.isEmpty(_page) || _page == 1) ? 0 : (_page - 1) * _limit;
    let limit       = _limit !== null && _page > 0 ? `LIMIT ${offset}, ${_limit}` : '';
    let condition   = [];
    let values      = [uid];
    let valuesTmp   = [uid];

    limit = _limit == 1 ? `LIMIT 1` : limit;

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);
                values.push(record.value);
                valuesTmp.push(record.value);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    let search      = !_.isEmpty(keyword) ? `AND (user_skp.title LIKE '%${keyword}%' OR quiz.title LIKE '%${keyword}%')` : ``;
    let searchTmp   = !_.isEmpty(keyword) ? `AND (user_skp_tmp.title LIKE '%${keyword}%' OR quiz.title LIKE '%${keyword}%')` : ``;
    let Fstatus     = !_.isEmpty(status) ? `AND user_skp.status = '${status}'` : ``;
    let FstatusTmp  = !_.isEmpty(status) ? `AND user_skp_tmp.status = '${status}'` : ``;
    condition       =  condition.join(' ');

    let query = `SELECT user_skp_tmp.id FROM user_skp_tmp  
            LEFT JOIN quiz ON quiz.id = quiz_id
        WHERE 1 
            AND uid = ?
            ${searchTmp}
            ${FstatusTmp}
            ${condition}

        UNION

        SELECT user_skp.id FROM user_skp 
            LEFT JOIN quiz ON quiz.id = quiz_id
        WHERE 1 
            AND  uid = ?
            ${search}
            ${Fstatus}
            ${condition}`;
    
    let total = await CoreDB.query(query, _.concat(values, valuesTmp));
    total = _.isEmpty(total) ? 0 : total.length;

    return total;
}

module.exports = SkpModel;