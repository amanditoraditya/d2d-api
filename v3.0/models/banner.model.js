const _         = require('lodash');
const moment    = require('moment');
const CoreDB    = require('../../utils/CoreDB');
const BannerModel = {}

BannerModel.save = async (data, condition = []) => {
    if (condition.length > 0) {
        CoreDB.update('banner');

        data.push({ key: 'updated', value: moment().format('YYYY-MM-DD kk:mm:ss') });

        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    } else {
        CoreDB.insert('banner')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

BannerModel.delete = async (condition) => {
    CoreDB.delete('banner');

    condition.forEach((record, index) => {
        let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

        if (!_.isEmpty(record.value)) {
            let op = !_.isEmpty(record.op) ? record.op : '=';

            CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
        } else {
            CoreDB.setWhere(`${jointer} ${record.key}`);
        }
    })

    return await CoreDB.execute();
}

BannerModel.getBy = async (condition = [], join = [], group = [], limit = null) => {
    CoreDB.select('banner');

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    CoreDB.setLimit(limit);
    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

BannerModel.getAll = async (condition = [], join = [], group = [], sort = [], page = null, limit = null) => {
    const now = new Date();
    CoreDB.select('banner');
    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';
            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';
                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

BannerModel.getCount = async (condition = [], join = [], group = [], sort = []) => {
    CoreDB.select('banner');
    CoreDB.setFields(['COUNT(banner.id) AS total']);

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    let total = await CoreDB.execute();
    total = _.isEmpty(total) ? 0 : total[0].total;

    return total;
}

module.exports = BannerModel;