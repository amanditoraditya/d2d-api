const _             = require('lodash');
const moment        = require('moment');
const CoreDB        = require('../../utils/CoreDB');
const UserCompetenceModel  = {}

UserCompetenceModel.save = async (data, condition = []) => {
    if (condition.length > 0) {
        CoreDB.update('user_competence');

        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    } else {
        CoreDB.insert('user_competence')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

UserCompetenceModel.delete = async (condition) => {
    CoreDB.delete('user_competence');
    
    condition.forEach((record, index) => {
        let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
        let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
        let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
        let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

        if (!_.isEmpty(record.value)) {
            let op      = !_.isEmpty(record.op) ? record.op : '=';
            
            CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
        } else {
            CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
        }
    })

    return await CoreDB.execute();
}

UserCompetenceModel.getBy = async (condition = [], join = [], group = [], limit = null, sort = []) => {
    CoreDB.select('user_competence');

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }
    
    CoreDB.setLimit(limit);
    let result =  await CoreDB.execute();
    
    return result.length == 1 ? result[0] : result;
}

UserCompetenceModel.getAll = async (condition = [], join = [], group = [], sort = [], page = null, limit = null) => {
    CoreDB.select('user_competence');

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

UserCompetenceModel.insertEmptyComp = async (uid) => {
    let data =  [
        { key: 'uid', value: uid },
        { key: 'type', value: 'subscription' },
    ]
    CoreDB.insert('user_competence')
    CoreDB.setData(data);
    let resSub = await CoreDB.execute()

    data =  [
        { key: 'uid', value: uid },
        { key: 'type', value: 'spesialization' },
    ]
    CoreDB.insert('user_competence')
    CoreDB.setData(data);
    let resSpec = await CoreDB.execute()

    return {resSpec, resSub};
}

UserCompetenceModel.getSubscriber = async (competence_id, countrycode) => {
    let result  = await CoreDB.query(`SELECT competence_id, COUNT(user.uid) as subscriber FROM user
        inner join user_competence uc on user.uid = uc.uid  
        WHERE 1 AND uc.competence_id = ? AND user.country_code = '${countrycode}'
        GROUP BY uc.competence_id`, [competence_id]);
    
    return _.isEmpty(result) ? 0 : result[0].subscriber;
}

module.exports  = UserCompetenceModel;