const _         = require('lodash');
const moment    = require('moment');
const CoreDB    = require('../../utils/CoreDB');
const JournalModel = {}

JournalModel.save = async (data, condition = []) => {
    if (condition.length > 0) {
        CoreDB.update('v_profile_list');

        data.push({ key: 'updated', value: moment().format('YYYY-MM-DD kk:mm:ss') });

        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    } else {
        CoreDB.insert('v_profile_list')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

JournalModel.delete = async (condition) => {
    CoreDB.delete('v_profile_list');

    condition.forEach((record, index) => {
        let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

        if (!_.isEmpty(record.value)) {
            let op = !_.isEmpty(record.op) ? record.op : '=';

            CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
        } else {
            CoreDB.setWhere(`${jointer} ${record.key}`);
        }
    })

    return await CoreDB.execute();
}

JournalModel.getBy = async (condition = [], join = [], group = [], limit = null) => {
    CoreDB.select('v_profile_list');

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    CoreDB.setLimit(limit);
    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

JournalModel.getAll = async (uid) => {
    let query = `
                SELECT 
                    u.uid, u.name, u.email, u.npa_idi, u.phone, u.born_date, 
                    u.education_1, u.education_2, u.education_3, u.pns, u.home_location, u.photo,
                    u.clinic_location_1, u.clinic_location_2, u.clinic_location_3,
                    sbs.subscription_id, sbs.subscription_name, sbs.subscription_description,
                    sps.spesialization_id, sps.spesialization_name, sps.spesialization_description,
                    sbsps.subspesialization_id, sbsps.subspesialization_name, sbsps.subspesialization_description,
                    sbsps.subspesialization_parent_id, ea.event_attendee
                FROM user AS u
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(competence_id) AS subscription_id,
                            GROUP_CONCAT(competence_name) AS subscription_name,
                            GROUP_CONCAT(competence_description) AS subscription_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'subscription'
                    ) AS sbs ON sbs.uid = u.uid
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(competence_id) AS spesialization_id,
                            GROUP_CONCAT(competence_name) AS spesialization_name,
                            GROUP_CONCAT(competence_description) AS spesialization_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'spesialization'
                    ) AS sps ON sps.uid = u.uid
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(parent_id) AS subspesialization_parent_id,
                            GROUP_CONCAT(competence_id) AS subspesialization_id,
                            GROUP_CONCAT(competence_name) AS subspesialization_name,
                            GROUP_CONCAT(competence_description) AS subspesialization_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'subspesialization'
                    ) AS sbsps ON sbsps.uid = u.uid
                    LEFT JOIN (
                        SELECT GROUP_CONCAT(DISTINCT event_slug_hash) AS event_attendee, u.uid FROM event_attendee AS ea
                            LEFT JOIN user as u ON u.email = ea.email OR u.npa_idi = ea.npa_idi
                        WHERE u.uid = '${npaoremailoruid}' AND ea.type = 'materi'
                    ) AS ea ON ea.uid = u.uid
                WHERE u.uid = '${npaoremailoruid}'
        `;
        
    return await CoreDB.query(query, _.concat(values, valuesTmp));
}

JournalModel.getCount = async (condition = [], join = [], group = [], sort = []) => {
    CoreDB.select('v_profile_list');
    CoreDB.setFields(['COUNT(v_profile_list.id) AS total']);

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    let total = await CoreDB.execute();
    total = _.isEmpty(total) ? 0 : total[0].total;

    return total;
}

module.exports = JournalModel;