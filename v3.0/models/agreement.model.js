const _         = require('lodash');
const moment    = require('moment');
const CoreDB    = require('../../utils/CoreDB');
const AgreementModel = {}

AgreementModel.agreement = {

    getAll: async (condition = [], join = [], fields = ['*'], group = [], sort = [], page = null, limit = null) => {        

        CoreDB.select('agreements');
        CoreDB.setFields(fields)

        if (condition.length > 0) {
            condition.forEach((record, index) => {
                let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';
                if (!_.isEmpty(record.value)) {
                    let op = !_.isEmpty(record.op) ? record.op : '=';
                    CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${record.key}`);
                }
            })
        }
    
        if (join.length > 0) {
            join.forEach((record, index) => {
                CoreDB.setJoin(record);
            })
        }
    
        if (group.length > 0) {
            group.forEach((record, index) => {
                CoreDB.setJoin(record);
            })
        }
    
        if (sort.length > 0) {
            sort.forEach((record, index) => {
                CoreDB.setOrder(record);
            })
        }
        
        if (!_.isEmpty(page) || !_.isEmpty(limit)) {
            CoreDB.setPage(page);
            CoreDB.setLimit(limit);
        }
    
        return await CoreDB.execute();
    }
}

AgreementModel.userAgreement = {
    save: async (data, condition = []) => {
        if (condition.length > 0) {
            CoreDB.update('user_agreements');

            data.push({ key: 'updated', value: moment().format('YYYY-MM-DD kk:mm:ss') });

            CoreDB.setData(data);
            condition.forEach((record, index) => {
                let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

                if (!_.isEmpty(record.value)) {
                    let op = !_.isEmpty(record.op) ? record.op : '=';

                    CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${record.key}`);
                }
            })
        } else {
            CoreDB.insert('user_agreements')
            CoreDB.setData(data);
        }

        return await CoreDB.execute();
    },

    getAll: async (condition = [], join = [], fields = ['*'], group = [], sort = [], page = null, limit = null) => {        

        CoreDB.select('user_agreements');
        CoreDB.setFields(fields)

        if (condition.length > 0) {
            condition.forEach((record, index) => {
                let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';
                if (!_.isEmpty(record.value)) {
                    let op = !_.isEmpty(record.op) ? record.op : '=';
                    CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${record.key}`);
                }
            })
        }
    
        if (join.length > 0) {
            join.forEach((record, index) => {
                CoreDB.setJoin(record);
            })
        }
    
        if (group.length > 0) {
            group.forEach((record, index) => {
                CoreDB.setJoin(record);
            })
        }
    
        if (sort.length > 0) {
            sort.forEach((record, index) => {
                CoreDB.setOrder(record);
            })
        }
    
        if (!_.isEmpty(page) || !_.isEmpty(limit)) {
            CoreDB.setPage(page);
            CoreDB.setLimit(limit);
        }
    
        return await CoreDB.execute();
    }
}


AgreementModel.log = {
    save: async (data, condition = []) => {
        if (condition.length > 0) {
            CoreDB.update('user_agreement_logs');
            CoreDB.setData(data);
            condition.forEach((record, index) => {
                let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

                if (!_.isEmpty(record.value)) {
                    let op = !_.isEmpty(record.op) ? record.op : '=';

                    CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${record.key}`);
                }
            })
        } else {
            CoreDB.insert('user_agreement_logs')
            CoreDB.setData(data);
        }

        return await CoreDB.execute();
    },

    getBy: async (condition = [], join = [], group = [], limit = null) => {
        CoreDB.select('user_agreement_logs');
    
        if (condition.length > 0) {
            condition.forEach((record, index) => {
                let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';
    
                if (!_.isEmpty(record.value)) {
                    let op = !_.isEmpty(record.op) ? record.op : '=';
    
                    CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${record.key}`);
                }
            })
        }
    
        CoreDB.setLimit(limit);
        let result = await CoreDB.execute();
    
        return result.length == 1 ? result[0] : result;
    }
}

module.exports = AgreementModel;