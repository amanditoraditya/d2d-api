module.exports = {
  apps : [{
    name: 'd2d-api',
    script: 'app.js',

    output: '/opt/logs/nginx/d2d-api.log',
    error: '/opt/logs/nginx/d2d-api_error.log',
    merge_logs: false
  }]
};
