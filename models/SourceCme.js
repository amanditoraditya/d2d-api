const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('source_cme', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'source': Sequelize.STRING(255),
    'description': Sequelize.TEXT,
    'logo': Sequelize.STRING(128),
    'certificate': Sequelize.STRING(128),
    'certificate_template': Sequelize.STRING(64),
    'create_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'source_cme'
})