const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('event_attendee', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'event_slug_hash': Sequelize.STRING(50),
    'event_name': Sequelize.STRING(255),
    'npa_idi': Sequelize.STRING(32),
    'name': Sequelize.STRING(64),
    'email': Sequelize.STRING(255),
    'verified': Sequelize.BOOLEAN,
    'need_verified': Sequelize.BOOLEAN,
    'type': {
        type: Sequelize.ENUM,
        values: ['materi', 'absent']
    }
}, {
    timestamps: true,
    freezeTableName: true,
    tableName: 'event_attendee'
})