const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_competence', {
    'id': {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(50),
    'competence_id': Sequelize.INTEGER(11),
    'competence_name': Sequelize.STRING(255),
    'type': Sequelize.ENUM('subscription','spesialization', 'subspesialization')
}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_competence'
})