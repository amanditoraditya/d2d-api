const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_skp', {
    'id': {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(50),
    'skp': Sequelize.INTEGER(11),
    'title': Sequelize.STRING(255),
    'source': Sequelize.STRING(255),
    'taken_on': Sequelize.STRING(20),
    'certificate': Sequelize.STRING(255),
    'quiz_id': Sequelize.INTEGER(11),
    'create_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'type': {
        type: Sequelize.ENUM,
        values: ['cme', 'offline', 'event', 'webinar'],
        allowNull: true
    },
    'status': Sequelize.STRING(255)
}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_skp'
})