const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('quiz_taken', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'quiz_id': Sequelize.INTEGER(11).UNSIGNED,
    'uid': Sequelize.STRING(100),
    'answers': Sequelize.TEXT,
    'result': Sequelize.ENUM('Y','N'),
    'taken_on': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'quiz_taken'
})