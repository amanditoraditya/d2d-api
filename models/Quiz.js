const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('quiz', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'source_id': Sequelize.STRING(255),
    'title': Sequelize.STRING(255),
    'description': Sequelize.TEXT,
    'filename': Sequelize.STRING(255),
    'skp': Sequelize.INTEGER(11),
    'no_cme': Sequelize.STRING(64),
    'create_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'create_by': Sequelize.INTEGER(11),
    'last_modify_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'last_modify_by': Sequelize.INTEGER(11),
    'available_start': Sequelize.STRING(15),
    'available_end': Sequelize.STRING(15)
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'quiz'
})