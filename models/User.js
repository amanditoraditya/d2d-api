const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(255),
    'name': Sequelize.STRING(255),
    'email': Sequelize.STRING(255),
    'npa_idi': Sequelize.STRING(100),
    'phone': Sequelize.STRING(20),
    'born_date': Sequelize.STRING(20),
    'education_1': Sequelize.STRING(255),
    'education_2': Sequelize.STRING(255),
    'education_3': Sequelize.STRING(255),
    'pns': {
        type: Sequelize.ENUM,
        values: ['true', 'false'],
        allowNull: true
    },
    'home_location': Sequelize.TEXT,
    'clinic_location': Sequelize.TEXT,
    'clinic_location_1': Sequelize.STRING(255),
    'clinic_location_2': Sequelize.STRING(255),
    'clinic_location_3': Sequelize.STRING(255),
    'photo': Sequelize.STRING(255),
    'registered_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'enabled': Sequelize.ENUM('true', 'false'),
    'token': Sequelize.STRING
}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'user'
})