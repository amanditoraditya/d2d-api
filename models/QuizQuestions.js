const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('quiz_questions', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'quiz_id': Sequelize.INTEGER(11).UNSIGNED,
    'question': Sequelize.TEXT,
    'answer': Sequelize.STRING(32),
    'choices': Sequelize.TEXT
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'quiz_questions'
})