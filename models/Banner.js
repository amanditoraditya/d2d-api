const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('banner', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
	'apps': {
        type:   Sequelize.ENUM,
        allowNull: false,
        values: ['D2D','TD','TB','GS','IF'],
        defaultValue: 'D2D',
    },
	'title': Sequelize.STRING(255),
    'url': Sequelize.TEXT,
    'link': Sequelize.TEXT,
	'publish': {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
	'type': {
        type:   Sequelize.ENUM,
        allowNull: false,
        values: ['image','video','link'],
        defaultValue: 'image',
    },
	'created_at': {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
    },
	'updated_at': {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
    },
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'banner'
})