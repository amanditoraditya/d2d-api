const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('settings', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'code': Sequelize.STRING(64),
    'type': Sequelize.ENUM('text','html'),
    'content': Sequelize.TEXT,
    'content_en': Sequelize.TEXT
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'settings'
})