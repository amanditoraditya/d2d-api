const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_agreements', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
	'uid': Sequelize.STRING(128),
    'agreement_id': Sequelize.INTEGER(11),
    'status': {
        type:   Sequelize.ENUM,
        allowNull: false,
        values: ['A','D'],
        defaultValue: 'D',
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_agreements'
})