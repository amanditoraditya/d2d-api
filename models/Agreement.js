const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('agreements', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
	'company': Sequelize.STRING(128),
    'title': Sequelize.STRING(128),
    'description': Sequelize.STRING(255),
    'agreement': Sequelize.TEXT,
    'confirmation': Sequelize.STRING(128),
    'total_show': Sequelize.INTEGER(11),
    'status': {
        type:   Sequelize.ENUM,
        allowNull: false,
        values: ['A','D'],
        defaultValue: 'D',
    },
	'created': {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
    },
	'updated': {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
    },
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'agreements'
})