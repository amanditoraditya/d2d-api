const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('journal_request', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(50),
    'fullname': Sequelize.STRING(255),
    'competence_id': Sequelize.STRING(255),
    'competence_name': Sequelize.STRING(255),
    'phone': Sequelize.STRING(255),
    'email': Sequelize.STRING(255),
    'city': Sequelize.STRING(255),
    'address': Sequelize.TEXT,
    'journal_title': Sequelize.TEXT,
    'create_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'paid': {
        type: Sequelize.ENUM,
        values: ['Y','N'],
        defaultValue: 'N'
    },
    'status': {
        type: Sequelize.ENUM,
        values: ['waiting','rejected','sent'],
        defaultValue: 'waiting'
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'journal_request'
})