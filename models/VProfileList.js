const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('v_profile_list', {
    'id': {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING,
    'name': Sequelize.STRING,
    'email': Sequelize.STRING,
    'npa_idi': Sequelize.STRING,
    'phone': Sequelize.STRING,
    'born_date': Sequelize.STRING,
    'education_1': Sequelize.STRING,
    'education_2': Sequelize.STRING,
    'education_3': Sequelize.STRING,
    'pns': Sequelize.ENUM('true', 'false'),
    'home_location': Sequelize.STRING,
    'clinic_location_1': Sequelize.STRING,
    'clinic_location_2': Sequelize.STRING,
    'clinic_location_3': Sequelize.STRING,
    'photo': Sequelize.STRING,
    'subscription_id': Sequelize.STRING,
    'subscription_name': Sequelize.STRING,
    'subscription_description': Sequelize.STRING,
    'spesialization_id': Sequelize.STRING,
    'spesialization_name': Sequelize.STRING,
    'spesialization_description': Sequelize.STRING,
    'subspesialization_id': Sequelize.STRING,
    'subspesialization_name': Sequelize.STRING,
    'subspesialization_description': Sequelize.STRING,
    'subspesialization_parent_id': Sequelize.STRING,
    'event_attendee': Sequelize.STRING
}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'v_profile_list'
})