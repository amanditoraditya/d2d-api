const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('event_reservation', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(50),
    'event_id': Sequelize.INTEGER(11),
    'event_name': Sequelize.TEXT,
    'registered_date': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'event_reservation'
})