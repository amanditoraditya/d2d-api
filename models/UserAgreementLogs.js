const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_agreement_logs', {
    'id': {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
	'uid': Sequelize.STRING(128),
    'agreement_id': Sequelize.INTEGER(11),
    'show': Sequelize.INTEGER(11),
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_agreement_logs'
})