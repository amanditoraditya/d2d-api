const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_notification', {
    'id': {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(128),
    'title': Sequelize.STRING(100),
    'body': Sequelize.STRING(200),
    'type': Sequelize.STRING(128),
    'data': Sequelize.TEXT,
    'slug': Sequelize.STRING(255),          // will be removed
    'slug_hash': Sequelize.STRING(255),     // will be removed
    'status': {
        type: Sequelize.ENUM,
        values: ['read','unread'],
        defaultValue: 'unread'
    },
    'created': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    'updated': {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_notification'
})