const Sequelize = require('sequelize');
const sequelize = SEQUELIZE_INIT;

module.exports = sequelize.define('user_device', {
    'id': {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    'uid': Sequelize.STRING(255),
    'device_token': Sequelize.STRING(255),

}, {
    //prevent sequelize transform table name into plural
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_device'
})