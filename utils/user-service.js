const _             = require('lodash');
const generateToken = require('../utils/helpers/generate-token');
const API_REQUEST   = require('../utils/api-request');
const BASE_URL      = `${process.env.USERSVC_URL}/${process.env.USERSVC_VERSION}`;
const SERVICE       = 'user';

const UserService   = {};

UserService.saveUser = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}

UserService.getUser = async (uid) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/${uid}`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);
        
        result      = data;
    } catch (error) {
        result      = error.response.data;
    }

    return result;
}

UserService.getUserByEmail = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/check`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result      = data;
    } catch (error) {
        result      = error.response.data;
    }

    return result;
}

UserService.getPatients = async (uid, params) => {
    let result      = null;
    try {
        let query       = [];
        if (params.length > 0) {
            params.forEach((row) => {
                query.push(`${row.key}=${row.value}`);
            });
        }
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `pmm?${query.join('&')}`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);
        
        result      = data;
    } catch (error) {
        result      = error.response.data;
    }

    return result;
}

UserService.getPatientConnected = async (duid, puid) => {
    let result      = null;
    try {
        const TOKEN = generateToken(duid, SERVICE);
        const URL   = `pmm/${duid}/${puid}`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);
        
        result      = data;
    } catch (error) {
        result      = error.response.data;
    }

    return result;
}

UserService.regionCountry = {
    getAll : async (uid, params, id) => {
        let result      = null;
        try {
            let query       = [];
            const TOKEN = generateToken(uid, SERVICE);
            if (params.length > 0) {
                params.forEach((row) => {
                    query.push(`${row.key}=${row.value}`);
                });
            }

            if (id != '') {
                id = `c${id}`
            }

            const URL   = `country/${id}?${query.join('&')}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },

    get : async (uid, id) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `country/${id}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
    
}


UserService.regionDistrict = {
    getAll : async (uid, params, id) => {
        let result      = null;
        try {
            let query       = [];
            const TOKEN = generateToken(uid, SERVICE);
            if (params.length > 0) {
                params.forEach((row) => {
                    query.push(`${row.key}=${row.value}`);
                });
            }

            if (id != '') {
                id = `r${id}`
            }

            const URL   = `district/${id}?${query.join('&')}`;
            
            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },

    get : async (uid, id) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `district/${id}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
}

UserService.regionProvince = {
    getAll : async (uid, params, id) => {
        let result      = null;
        try {
            let query       = [];
            const TOKEN = generateToken(uid, SERVICE);
            if (params.length > 0) {
                params.forEach((row) => {
                    query.push(`${row.key}=${row.value}`);
                });
            }          
            
            if (id != '') {
                id = `c${id}`
            }  
            const URL   = `province/${id}?${query.join('&')}`;
            console.log(URL)
            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },

    get : async (uid, id) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `province/${id}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
}

UserService.regionRegency = {
    getAll : async (uid, params, id) => {
        let result      = null;
        try {
            let query       = [];
            const TOKEN = generateToken(uid, SERVICE);
            if (params.length > 0) {
                params.forEach((row) => {
                    query.push(`${row.key}=${row.value}`);
                });
            }
            
            if (id != '') {
                id = `p${id}`
            }
            const URL   = `regency/${id}?${query.join('&')}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },

    get : async (uid, id) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `regency/${id}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
}

UserService.regionVillage = {
    getAll : async (uid, params, id) => {
        let result      = null;
        try {
            let query       = [];
            const TOKEN = generateToken(uid, SERVICE);
            if (params.length > 0) {
                params.forEach((row) => {
                    query.push(`${row.key}=${row.value}`);
                });
            }
            
            if (id != '') {
                id = `d${id}`
            }
            const URL   = `village/${id}?${query.join('&')}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },
    
    get : async (uid, id) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `village/${id}`;

            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
}

UserService.dKonsul = {
    // validate: async (uid) => {
    //     let result      = [];

    //     try {
    //         const TOKEN = await generateToken(uid, SERVICE);

    //         const URL   = `Dkonsul/check/${uid}`;
    //         console.log(` base = ${BASE_URL}/${URL}`)
    //         const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

    //         result      = data;
    //     } catch (error) {
    //         result      = error.response.data;
    //     }
    
    //     return result;
    // }, 

    get: async (uid) => {
        let result      = [];

        try {
            const TOKEN = await generateToken(uid, SERVICE);
            const URL   = `dkonsul/${uid}`;

            let record  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }

            result      = record.data.message == 'No data found!' ? [] : record.data.result;

        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },

    save: async (uid, payload) => {
        let result      = [];
        try {
            const TOKEN = await generateToken(uid, SERVICE);
            const URL   = `dkonsul`;
            
            let record  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
            
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
            result      = error.message;
        }
        return result;
    }
}

UserService.eula = {
    getBy : async (uid, type) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `eula/${type}`;

            const record  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    },
    
    getByAndCheck : async (uid, type) => {
        let result      = null;
        try {
            const TOKEN = generateToken(uid, SERVICE);
            const URL   = `eula/check/${type}`;
            
            const { data }  = await API_REQUEST(BASE_URL, TOKEN).get(URL);

            result      = data
        } catch (error) {
            result      = error.response.data;
        }
    
        return result;
    }
}

UserService.setVerification = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/setverification`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}
UserService.verification = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/verification`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, _.assign(payload, {uid}));
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}

UserService.setPassCode = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/setpasscode`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}
UserService.changePassword = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `user/changepassword`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, _.assign(payload, {uid}));
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}

module.exports = UserService;