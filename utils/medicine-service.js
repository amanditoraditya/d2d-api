const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL      = `${CONFIG.URL_GS}`;
// const BASE_URL_V3   = `${process.env.CONTENTSVC_URL}/${process.env.CONTENTSVC_VERSION}`;
const SERVICE       = 'user';

const MedicineService    = {};

/* GET ALL MEDICINE
 ** uid          : String from firebase
 ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
 ** page         : Integer
 ** limit        : Integer
 ** headers      : Array object headers
 */
MedicineService.getAll = async (uid, filter = [], page = 1, limit = 10, headers = { platform: 'android' }) => {
    let result      = [];

    try {
        let TOKEN = generateToken(uid, 'content', headers.platform);
        let URL   = `obat/filter?page=${page}&limit=${limit}`;

        if (filter.length > 0) {
            for(let val of filter) {
                URL += `&${val.key}=${val.value}`;
            }
        }

        _.assign(headers, { uid });
        
        let record = await API_REQUEST(BASE_URL, TOKEN, headers).get(URL);
        
        if (record.data.acknowledge == false) {
            console.log('Error: ', record);
        }
        if (record.data.message == 'No data found!') {
            console.log('Error: ', record);
        }
    
        result      = record.data.message == 'No data found!' ? [] : record.data.data;
    } catch (error) {
       result  = error.response.data;
   }

    return result;
},

/* GET ALL MEDICINE
 ** uid          : String from firebase
 ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
 ** page         : Integer
 ** limit        : Integer
 ** headers      : Array object headers
 */
MedicineService.getBy = async (uid, filter = [], headers = { platform: 'android' }) => {
    let result      = [];

    try {
        let TOKEN = generateToken(uid, 'content', headers.platform);
        let URL   = `obat/detail?`;

        if (filter.length > 0) {
            for(let val of filter) {
                URL += `&${val.key}=${val.value}`;
            }
        }

        _.assign(headers, { uid });
        
        let record = await API_REQUEST(BASE_URL, TOKEN, headers).get(URL);
        
        if (record.data.acknowledge == false) {
            console.log('Error: ', record);
        }
        if (record.data.message == 'No data found!') {
            console.log('Error: ', record);
        }
    
        result      = record.data.message == 'No data found!' ? [] : record.data.data;
    } catch (error) {
       result  = error.response.data;
   }

    return result;
}

module.exports = MedicineService;