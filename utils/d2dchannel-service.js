const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const moment        = require('moment');
const generateToken = require('../utils/helpers/generate-token');

const BASE_URL      = `${CONFIG.URL_D2D_CHANNEL}`;

const D2dChannelService    = {};

D2dChannelService.members = {
    triggerUpdate: async (token) => {
        let result      = [];
        let data        = [];
        let header      = {
            "X-DCHANNEL-KEY" : `${CONFIG.XDCHANNEL}`
        }
        try {
            token = `Bearer ${token}`;

            let URL   = `open-api/v1/user/trigger-update`;
            let result = await API_REQUEST(BASE_URL, token, header).get(URL);

            if (_.toLower(result.header.message) == "success") {
                data = {
                    triggerUpdate: "success",
                    result: result
                }
            }else{
                console.log('Error: ', result);
            }
        
            result = data;
        } catch (error) {
            console.log(error)
            result = [];
        }

        return result;
    },
}

module.exports = D2dChannelService;
