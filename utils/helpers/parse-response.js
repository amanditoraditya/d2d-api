module.exports = function parseResponse(response, status, result, message, acknowledge = true) {
    response.status(status)
        .send({
            status      : status,
            message     : message,
            acknowledge : acknowledge,
            result      : result,
        })
}