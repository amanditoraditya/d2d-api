const fs    = require('fs')
const jwt   = require('jsonwebtoken')

const generateToken = (uid = null, serviceName = null, platform = 'android') => {
    let result = null
    
    try {
        if (serviceName == 'content') {
            const keyIos      = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJmNTUzZWFlMmNhZDk5MmNjNTAxNzIzYjM5MTQzZDk2ZSIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuaW9zIiwicGxhdGZvcm0iOiJpb3MiLCJpYXQiOjE1MjIwNjgzOTR9.5EDnPOK-HB7H0TCyA3AcztC8aZc_VpkXr7dRxlL0IwQ';
            const keyAndroid  = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJkNmM4OGUwZGE3MWFjNzFiNjU3MTg5NTI4MTZlZTMwMCIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuYW5kcm9pZCIsInBsYXRmb3JtIjoiYW5kcm9pZCIsImlhdCI6MTUyNDExMTQ3MX0.59_NSziZw1zm_QiqgpokdLIK8pOeOGz13OcRO9NQLxQ';

            result  = platform == 'android' ? keyAndroid : keyIos;
        } else if (serviceName == 'd2d') {
            const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
            const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', { expiresIn: 360 });

            result  = token;
        } else {
            const currdir       = process.cwd()
            const accessToken   = jwt.sign({ uid: uid, aid: 'dd' }, fs.readFileSync(currdir + `/keys/${serviceName}-service.pem`), {
                expiresIn: 30, // expires in 30 seconds
                algorithm: 'RS256'
            })
        
            result  = accessToken
        }
    } catch (error) {
        console.error('Get Token Error', error.message)
    }
  
    return result
}

module.exports = generateToken;
