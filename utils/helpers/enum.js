const STATIC = `https://static.d2d.co.id/`;
const ENV    = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';

const BUCKET = {
    SOURCE_CME  : `${STATIC}${ENV}image/logo/`,
    CERTIFICATE : `${STATIC}${ENV}certificate/`,
    PAYMENT_RECEIPT : `${STATIC}${ENV}payment/`,
}

const URL  = {
    ACTIVATION          : CONFIG.API_ENVIRONMENT == 'production' ? `https://d2d.co.id/verify`           : `https://staging.d2d.co.id/verify`,
    FORGOT_PASSWORD     : CONFIG.API_ENVIRONMENT == 'production' ? `https://d2d.co.id/changepassword`   : `https://staging.d2d.co.id/changepassword`,
    PAYMENT_FINISH      : process.env.MIDTRANS_ENVIRONMENT == 'production' ? `https://staging.d2d.co.id/payment/finish`   : `https://staging.d2d.co.id/payment/finish`,
    PAYMENT_UNFINISH    : process.env.MIDTRANS_ENVIRONMENT == 'production' ? `https://staging.d2d.co.id/payment/unfinish` : `https://staging.d2d.co.id/payment/unfinish`,
    PAYMENT_ERROR       : process.env.MIDTRANS_ENVIRONMENT == 'production' ? `https://staging.d2d.co.id/payment/error`    : `https://staging.d2d.co.id/payment/error`,
}

const TYPES_NOTIFICATION = [
    "pdf_journal", "pdf_guideline", "pdf_knowledge", "pdf_literature", "pdf_product_group", "pdf_brochure", "pdf_materi",
    "video_materi", "video_learning", "video_knowledge", "video_product_group", "video_brochure", "video_general", 
    "event", "event_list", "webinar", "cme_certificate", "profile_notification", "cme", "req_journal", "journal",
    'pmm_patient', 
    'certificate_cme', 'certificate_offline', 'certificate_event', 'certificate_webinar'
];

const COMPETENCE_TYPES = {
    SUBSCRIPTION        : 'subscription',
    SPESIALIZATION      : 'spesialization',
    NONSPESIALIZATION   : 'nonspesialization',
    SUBSPESIALIZATION   : 'subspesialization',
}

// can see on https://docs.midtrans.com/en/after-payment/http-notification => Sample Notification section
const MidtransPaymentTypes ={
    BriEpay: 'bri_epay',
    Akulaku: 'akulaku',
    Cstore: 'cstore', //toko seperti alfa, indomaret
    DanamonOnlineBanking: 'danamon_online',
    CimbClicks: 'cimb_clicks',
    MandiriClickPay: 'mandiri_clickpay',
    KlikBca: 'bca_klikbca',
    BcaKlikPay: 'bca_klikpay',
    BankTransfer: 'bank_transfer', // VA account
    MandiriBill: 'echannel',
    Gopay: 'gopay',
    CreditCard: 'credit_card',
}

const MidtransTransactionStatus ={
    challenge: 'challenge',
    success: 'success',
    failure: 'failure', 
    pending: 'pending',
}

const RedisKey ={
    profile: 'profile_', // profile_ + uid
    event_atendee: 'eventAttendee_', // eventAttendee_ + uid
}

module.exports = { STATIC, ENV, BUCKET, URL, TYPES_NOTIFICATION, COMPETENCE_TYPES, MidtransPaymentTypes, MidtransTransactionStatus, RedisKey}
