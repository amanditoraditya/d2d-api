const COUNTRY_CODES = {
    Indonesia   : 'ID',
    Philippines : 'PH',
    Myanmar     : 'MM',
    Cambodia    : 'KH'
}

const lANGUAGE_TYPE = {
    Indonesia   : 'ID',
    English     : 'EN'
}

const LANG = {
    ID: {
        GENERAL: {
            SUCCESS: 'Success',
            DATA_NOT_FOUND: 'Data Not Found!',
            SOMETHING_WENT_WRONG: 'Something went wrong!',
            USER_NOT_FOUND: 'User ID Not Found!',
            ACCESS_DENIED: 'Cannot Access this content or feature',
        },
        REGISTER: {
            SUCCESS: 'Akun berhasil didaftarkan.',
            FAILED: 'Register failed, please try again.',
            ACCOUNT_EXISTS: `This account already exists. Please login.`,
            VERIFY_ACCOUNT: 'Please check your email for activate your account.',
            SEND_EMAIL_FAILED: 'Email failed to sent.',
            FULLNAME_LENGTH: 'Masukkan Fullname minimal 3 karakter',
            FULLNAME_FORMAT: 'Format fullname yang Anda masukkan tidak sesuai',
        },
        PROFILE: {
            //success handling
            UPDATE_SUCCESS: 'Profile berhasil diupdate.',
            // error handling
            USER_NOT_FOUND: 'User ID Not Found!',
            MEDICALID_HAS_BEEN_USED: 'NPA IDI anda sudah digunakan di aplikasi ini.',
            MEDICALID_NOT_FOUND: 'NPA IDI dan nama yang anda masukkan tidak ditemukan.',
            MEDICALID_NOT_MATCH: 'NPA IDI yang anda masukkan tidak sesuai.',
            SPECIALIST_UPDATED: 'Specialist berhasil ter-update',
            SUBCRIPTION_UPDATED: 'Subscription berhasil ter-update',
        },
        LOGIN:{
            //success handling
            SUCCESS: 'Login Success',
            // error handling
            USER_NOT_FOUND: `This account doesn't exist. Please register`,
            USER_INACTIVE: 'Your account is no longer active',
            NOT_REGISTERED:'Silahkan register dahulu.',
        },
        FORGOT_PASSWORD:{
            EMAIL_NOT_EXIST:`Account doesn't exist, please register.`,
            EMAIL_NOT_VERTIFIED:'Please verified your email first!',
            EMAIL_NOT_VALID:'Email tidak valid.',
            INVALID_CODE:'Invalid Code.',
            SEND_EMAIL_SUCCESS: `Reset Password has been sent.`,
            SEND_EMAIL_FAILED: `Email failed to sent.`,
            CHANGE_PASSWORD_SUCCESS: 'Your password has been changed.',
            CHANGE_PASSWORD_FAILED: 'Change password failed, please try again!'
        },
        AGREEMENTS:{
            DATA_NOT_SHOW: 'Data tidak ditampilkan',
            ACCEPT_AGREEMENT:'Anda telah menyetujui',
            SUCCESS_SUBMIT: 'Anda berhasil submit',
        },
        EVENT:{
            NOT_FOUND: 'EVENT NOT FOUND.',
            SIGN_IN:{
                SUCCESS:'Anda berhasil absen dalam event ini!',
                ALREADY_IN:'Anda sudah absen dalam event ini!',
                NOT_REGISTERED:'Anda harus daftar terlebih dahulu!',
                NOT_START:'Event ini belum dimulai!',
                HAS_END:'Event ini telah berakhir!',
            },
            CERTIFICATE:{
                GET_CERTIFICATE:'Selamat Anda Mendapatkan Sertifikat Event',
                GET_SKP_CERTIFICATE:'Selamat Anda Mendapatkan Sertifikat Event dan {0} SKP',// {0} => skp
                ACCESS_NOTIFY:'Klik disini untuk akses sertifikat Anda',
                ACCESS_DENIED:'Your user not have an access!',
            },
            EMAIL_CERTIFICATE:{
                GET_CERTIFICATE:'Your e-Certificate for {0} is here!', // {0} => title
                THANKS_FOR_PARTICIPATE:'Thank you for your participation in {0} .', // {0} => title
                SEND_CERTIFICATE:'Certificate has been sent to your registered email',
            }
        },
        JOURNAL:{
            FULL_JOURNAL_REQUEST:'Full Journal Request',
            JOURNAL_DEAR: 'Dear Admin D2D',
            SUCCESS_SUBMITED: 'Your request was successfully submitted for processing.',
            FAILED_SUBMITED: 'Your request is failed, please try again.',
            THANKS_FOR_INTEREST: 'Terima kasih untuk ketertarikan anda.',            
            HAVE_REQUEST: 'Anda memiliki permintaan download full jurnal',
            CONTENT_DOCTOR :
                `<p style="color:white;">Dear {0}<br><br>

                Terima kasih untuk ketertarikan anda.<br>
                Anda baru saja mengirimkan permintaan untuk mengunduh full version jurnal {1}.<br>
                Permintaan Anda akan kami proses terlebih dahulu, untuk informasi lebih lanjut terkait proses ini akan
                kami informasikan melalui telepon atau email yang terdaftar. Mohon kesediaannya untuk menunggu
                proses permintaan.<br>
                Demikian informasi ini kami sampaikan, jika ada pertanyaan lebih lanjut, Anda dapat mengirimkan
                email ke <a href="mailto:info@d2d.co.id" target="_top">info@d2d.co.id</a>.<br><br>
          
                Terima kasih telah menggunakan aplikasi D2D sebagai pilihan sarana informasi dan pembelajaran untuk
                anda.<br><br>
          
                Salam,<br>
                Tim Admin D2D</p>`,
            CONTENT_CS :
                `<p style="color:white;">Dear {0}<br><br>
            
                Anda memiliki permintaan download full jurnal {1} untuk User dengan data sebagai berikut di
                bawah:<br><br>

                Nama: {2}<br>
                Doctor Practice: {3}<br>
                City: {4}<br>
                Country Code: {5}<br>
                Specialist: {6}<br>
                Phone Number: {7}<br>
                Email: <a href="mailto:{8}" target="_top">{8}</a><br><br>

                Mohon untuk dapat diproses lebih lanjut.<br><br>
                Terima Kasih.</p>`
        },
        WEBINAR:{
            CERTIFICATE:{
                ALREADY_HAVE:`Anda sudah memiliki sertifikat webinar ini.`,
                GET_CERTIFICATE:`Selamat Anda mendapatkan sertifikat {0} SKP dari Webinar "{1}"`, //{0} => event.skp , {1} => event.title
                ACCESS_NOTIFY:`Klik disini untuk akses sertifikat Anda`,
                SUCCESS_CREATED:`certificate created`,
            },
            EMAIL_CERTIFICATE:{
                CERTIFICATE_ISEMPTY:`certificate cannot be empty`,
                TITLE_ISEMPTY:`title cannot be empty`,
                DISPLAY_NAME_ISEMPTY:`displayName cannot be empty`,
                EMAIL_ISEMPTY:`email cannot be empty`,
                GET_CERTIFICATE:`Your Webinar Certificate is here!`,
                GET_CERTIFICATE_DETAIL:`Here is your certificate for Webinar "{0}".`, // {0} => title
                SEND_CERTIFICATE:`Certificate has been sent to your registered email`,
            }
        },
        CME:{
            SKP:{
                SUCCESS_RESET:`SKP point successfully reset`,
            },
            CERTIFICATE:{
                GET_CERTIFICATE:`Your CME Certificate is here!`,
                GET_CERTIFICATE_DETAIL:`Here is your certificate for quiz {0}.`, // {0} => title
                SEND_CERTIFICATE:`Certificate has been sent to your registered email`,
            },
            EMAIL_CERTIFICATE:{
                GET_CERTIFICATE:`Selamat Anda Mendapatkan Sertifikat CME`,
                GET_CERTIFICATE_DETAIL:`Selamat Anda Mendapatkan Sertifikat CME dan {0} SKP`, // {0} => quiz.SKP
                ACCESS_NOTIFY:`Klik disini untuk akses sertifikat Anda`, 
                SUCCESS_TAKE_QUIZ:`You have successfully taken this quiz and get {0} SKP`, // {0} => skp
                FAILED_CREATED:`Failed create certificate`,
                TRY_AGAIN:`You may try again next time`,
            },
        },
    },
    EN: {
        GENERAL: {
            SUCCESS: 'Success',
            DATA_NOT_FOUND: 'Data Not Found!',
            SOMETHING_WENT_WRONG: 'Something went wrong!',
            ACCESS_DENIED: 'Cannot Access this content or feature',
        },
        REGISTER: {
            SUCCESS: 'Account Successfully Registered.',
            FAILED: 'Register failed, please try again.',
            ACCOUNT_EXISTS: `This account already exists. Please login.`,
            VERIFY_ACCOUNT: 'Please check your email for activate your account.',
            SEND_EMAIL_FAILED: 'Email failed to sent.',
            FULLNAME_LENGTH: 'Please input fullname min 3 character',
            FULLNAME_FORMAT: 'Format Fullname does not match!',
        },
        PROFILE: {
            //success handling
            UPDATE_SUCCESS: 'Profile has been updated.',
            // error handling
            USER_NOT_FOUND: 'User ID Not Found',
            MEDICALID_HAS_BEEN_USED: 'Your Medical ID has been used in this application.',
            MEDICALID_NOT_FOUND: 'The Medical ID and name you entered were not found.',
            MEDICALID_NOT_MATCH: `The Medical ID doesn't match.`,
            SPECIALIST_UPDATED: 'Your Specialist is successfully updated',
            SUBCRIPTION_UPDATED: 'Your Subscription is successfully updated',
        },
        LOGIN:{
            //success handling
            SUCCESS: 'Login Success',
            // error handling
            USER_NOT_FOUND: `This account doesn't exist. Please register`,
            USER_INACTIVE: 'Your account is no longer active',
            NOT_REGISTERED:'You Have to Register First!',
        },
        FORGOT_PASSWORD:{
            EMAIL_NOT_EXIST:`Account doesn't exist, please register.`,
            EMAIL_NOT_VERTIFIED:'Please verified your email first!',
            EMAIL_NOT_VALID:'Invalid email address.',
            INVALID_CODE:'Invalid Code.',
            SEND_EMAIL_SUCCESS: `Reset Password has been sent.`,
            SEND_EMAIL_FAILED: `Email failed to sent.`,
            CHANGE_PASSWORD_SUCCESS: 'Your password has been changed.',
            CHANGE_PASSWORD_FAILED: 'Change password failed, please try again!'
        },
        AGREEMENTS:{
            DATA_NOT_SHOW: 'Data Not Displayed',
            ACCEPT_AGREEMENT: 'You are agree for this Term & Conditions',
            SUCCESS_SUBMIT: 'You has successfully submit the Term & Conditions',
        },
        EVENT:{
            NOT_FOUND: 'EVENT NOT FOUND.',
            SIGN_IN:{
                SUCCESS:'You are successfully absent for this event!',
                ALREADY_IN:'You are already absent for this event!',
                NOT_REGISTERED:'You have to register first for joining this event!',
                NOT_START:'This Event session has not started yet!',
                HAS_END:'This Event session has already ended!',
            },
            CERTIFICATE:{
                GET_CERTIFICATE:'Congratulations! You got the e-certificate from this event!',
                GET_SKP_CERTIFICATE_OLD:'Congratulations! You got the e-certificate and {0} SKP from this event!', // {0} => skp
                GET_SKP_CERTIFICATE:'Congratulations! You have received {0} CME point from {1}', // {0} => skp , {1} => event_title
                ACCESS_NOTIFY:'Please click here to access your e-certificate',
                ACCESS_DENIED:'Your user not have an access!',
            },
            EMAIL_CERTIFICATE:{
                GET_CERTIFICATE:'Your e-Certificate for {0} is here!', // {0} => title
                THANKS_FOR_PARTICIPATE:'Thank you for your participation in {0} .', // {0} => title
                SEND_CERTIFICATE:'Certificate has been sent to your registered email',
            }
        },
        JOURNAL:{
            FULL_JOURNAL_REQUEST:'Full Journal Request',
            JOURNAL_DEAR: 'Dear Admin D2D',            
            THANKS_FOR_INTEREST: 'Thank you for your interest!',
            HAVE_REQUEST:'You have a request to download the full journal of ',
            SUCCESS_SUBMITED: 'Your request was successfully submitted for processing.',
            FAILED_SUBMITED: 'Your request is failed, please try again.',
            CONTENT_DOCTOR :
                `<p style="color: white;">Dear {0}<br><br>

                Thank you for your interest.<br>
                You have just submitted a request to download the full version of {1}.<br>
                We will process your request first, for further information regarding this process we will
                inform you via phone or registered email. Please wait for the request to be processed.<br>
                Hopefully you find this information useful, if you have further question please send an
                email to <a href="mailto:info@d2d.co.id" target="_top">info@d2d.co.id</a>.<br><br>
          
                Thank you for using D2D application as your preferred information and learning tool
                platform.<br><br>
          
                Sincerely,<br>
                D2D Admin Team</p>`,
            CONTENT_CS :
                `<p style="color:white;">Dear {0}<br><br>
            
                You have received a new full version journal download request for the title {1}, user request credentials are as below::<br><br>

                Name: {2}<br>
                Doctor Practice: {3}<br>
                City: {4}<br>
                Country Code: {5}<br>
                Specialist: {6}<br>
                Phone Number: {7}<br>
                Email: <a href="mailto:{8}" target="_top">{8}</a><br><br>

                Please process the request further.<br><br>
                Thank you.</p>`
        },
        WEBINAR:{
            CERTIFICATE:{
                ALREADY_HAVE:`You already has the e-certificate for this Webinar`,
                GET_CERTIFICATE_OLD:`Congratulations you have received the e-certificate from this Webinar`,
                GET_CERTIFICATE:`Congratulations! You have received {0} CME point from {1}`, //{0} => event.skp , {1} => event.title
                ACCESS_NOTIFY:`Click here to access your e-certificate`,
                SUCCESS_CREATED:`certificate created`,
            },
            EMAIL_CERTIFICATE:{
                CERTIFICATE_ISEMPTY:`certificate cannot be empty`,
                TITLE_ISEMPTY:`title cannot be empty`,
                DISPLAY_NAME_ISEMPTY:`displayName cannot be empty`,
                EMAIL_ISEMPTY:`email cannot be empty`,
                GET_CERTIFICATE:`Your Webinar Certificate is here!`,
                GET_CERTIFICATE_DETAIL:`Here is your certificate for Webinar "{0}".`, // {0} => title
                SEND_CERTIFICATE:`Certificate has been sent to your registered email`,
            }
        },
        CME:{
            SKP:{
                SUCCESS_RESET:`SKP point successfully reset`,
            },
            CERTIFICATE:{
                GET_CERTIFICATE:`Your CME Certificate is here!`,
                GET_CERTIFICATE_DETAIL:`Here is your certificate for quiz {0}.`, // {0} => title
                SEND_CERTIFICATE:`Certificate has been sent to your registered email`,
            },
            EMAIL_CERTIFICATE:{
                GET_CERTIFICATE:`Congratulations! You have received e-certificate from CME`,
                GET_CERTIFICATE_DETAIL:`Congratulations! You have received e-certificate from CME and SKP Quizz`,
                ACCESS_NOTIFY:`Click here to access your e-certificate`,
                SUCCESS_TAKE_QUIZ:`You have successfully taken this quiz and get {0} SKP`, // {0} => SKP
                FAILED_CREATED:`Failed create certificate`,
                TRY_AGAIN:`You may try again next time`,
            },
            MEDICALID_NOT_MATCH: `The Medical ID doesn't match.`
        },
    }
}

module.exports = { COUNTRY_CODES, LANG, lANGUAGE_TYPE };