const redis         = require('redis');

const client        = redis.createClient(CONFIG.REDIS_PORT,CONFIG.REDIS_HOST_PRIVATE, {db: CONFIG.REDIS_DB_NUMBER,auth_pass: CONFIG.REDIS_AUTH_PASS});

client.on('connect' , () => console.log('connected'));
client.on("error"   , (err) => console.log("error =>", err));

const RedisService  = {};
const serviceName   = 'Redis';

 RedisService.getValue = async (key) => {
    return new Promise ((resolve, reject) => { 
        client.get(key, async (err, result) => {
            if(result) {
                resolve(JSON.parse(result));
            }
            else{
                console.log(`${serviceName} [error] => `, err);
                reject(null);
            }
        });
    })
}

// key => redis key
// value => saved value (must string) 
// duration => duration of key (in minute) default 1 minute
RedisService.insert = async (key, value, duration = 1) => {
    return new Promise ((resolve, reject) => { 
        client.set(key, value, async (err, result) => {
            if(err) {
                console.log(`${serviceName} [error] => `, err);
                reject('false');
            }
            else{
                if(duration > 0){
                    client.expire(key,(duration*60));
                }
                resolve('true');
            }
        });
    });
}

RedisService.keyExist = async (key) => {
    return new Promise ((resolve, reject) => { 
        client.exists(key, async (err, result) => {
            if(err){
                console.log(`${serviceName} [error] => `, err);
                reject('false');
            }
            else if (result === 1) {
                resolve('true');
            } else {
                resolve('false');
            }
        });
    });
}

RedisService.delete = async (key) => {
    return new Promise ((resolve, reject) => { 
        client.del(key, async (err, result) => {
            if(err) reject('false');
            else{
                resolve('true');
            }
        });
    });
}

module.exports = RedisService;