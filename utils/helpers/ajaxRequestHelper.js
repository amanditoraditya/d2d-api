/* eslint-disable quote-props */

const _         = require('lodash');
const axios     = require('axios');

const API_REQUEST = (baseURL, idToken = null, header_params = null) => {
    try {
        let payload = {
            baseURL: `${baseURL}/`,
        };

        let headers = { 'Authorization': idToken };
        
        if (!_.isEmpty(header_params)) {
            _.assign(headers, header_params);
        }

        if (!_.isEmpty(idToken)) {
            _.assign(payload, { headers });
        }
        
        return axios.create(payload)
    } catch (err) {
        console.error('Axios error')
    }
};

module.exports = API_REQUEST;
