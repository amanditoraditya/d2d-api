const fs                = require('fs');
const _                 = require('lodash');
const jwt               = require('jsonwebtoken');
const axios             = require('axios');
const request               = require('request');
const SVCCHAT_URL         = process.env.SVCCHAT_SERVICE_URL;
const SVCCHAT_VERSION     = process.env.SVCCHAT_SERVICE_VERSION;
const BASE_SVCCHAT_URL    = `${SVCCHAT_URL}/${SVCCHAT_VERSION}`;
const RchatService      = {};

let authorization

/**
 * function axios get from url
 * url              = link 
 * aid              = aplikasi id ('dd', 'td', 'if, 'gs', 'tb'), 
 * XAuthToken       = Token from rocket chat 
 * XUserId          = userid from login
 * additionalparam  = { key: value }
 * responseType     = 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
 */
RchatService.setgetURL = async function (url, authorization = null, additionalparam = {}, responseType = null, contenttype = null) {
  try {
      let cType = (!_.isEmpty(contenttype)) ? {'content-type': contenttype} : '' ;
      let headers = {
          aid: 'dd',
          Authorization: authorization,
      }
      headers = _.merge(headers, cType)
      let connect = await axios.get(`${BASE_SVCCHAT_URL}${url}`, {
          responseType: responseType,
          params: additionalparam,
          headers : headers,
      });
  
      return connect;
  } catch (error) {
      console.log(error);
      return false;
  }
}

/**
* function axios Post
* url              = link 
* data             = { key: value }
* aid              = aplikasi id ('dd', 'td', 'if, 'gs', 'tb'), 
* XAuthToken       = Token from rocket chat 
* XUserId          = userid from login
* additionalparam  = { key: value }
* responseType     = 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
*/
RchatService.setpostURL = async function (url = null, data = {}, authorization = null, additionalparam = {}, responseType = null, contenttype = null) {
  try{
      let cType = (!_.isEmpty(contenttype)) ? {'content-type': contenttype} : '' ;
      let headers = {
          aid: 'dd',
          Authorization: authorization,
      }
      headers = _.merge(headers, cType)
      let connect = await axios.post(`${BASE_SVCCHAT_URL}${url}`, data,
          {
              responseType: responseType,
              params: additionalparam,
              headers : headers,
          });
          
      return connect;
  } catch (error) {
      console.log(error);
      return false;
  }
}

/**
 * 
 * @param {*} uid 
 * Generate Token based UID
 */
const generateToken = (uid = null) => {
    let result = null
    
    try {
      const currdir       = process.cwd()
      const accessToken   = jwt.sign({uid: uid, aid: 'dd'}, fs.readFileSync(currdir + '/keys/chat-service.pem'), {
          expiresIn: 30, // expires in 30 seconds
          algorithm: 'RS256'
      })
  
      result = accessToken
    } catch (error) {
      console.error('Get Token Error', error.message)
    }
  
    return result
}

/**
 * Testing chat
 */
RchatService.messagesTesting = async (req) => {
    let { uid }   = req.headers;
    let authorization = generateToken(uid);
    let result = await RchatService.setgetURL(`/testing/testRchat`, authorization, req.query);
    // console.log(`hit to chat services ${BASE_SVCCHAT_URL}/testing/testRchat`);
    // console.log(result);
}

/**
 * Login to chat
 */
RchatService.joinwebinar = async (req) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let authorization = generateToken(uid);
        result = await RchatService.setpostURL(`/auth/loginwebinar`, req.body, authorization);
    } catch (error) {
        console.log('RchatSvc Login Error : ', error);
    }
    return result;
}

/**
 * Login to chat for old apk
 */
RchatService.login = async (req) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let authorization = generateToken(uid);
        result = await RchatService.setpostURL(`/auth/login`, req.body, authorization);
    } catch (error) {
        console.log('RchatSvc Login Error : ', error);
    }
    return result;
}

/**
 * Message to room or Team
 */
RchatService.sendMessage = async (req) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let { email }   = req.params;
        let authorization = generateToken(uid);
        result = await RchatService.setpostURL(`/messages/send/${email}`, req.body, authorization);
    } catch (error) {
        console.log('RchatSvc send Messages Error : ', error);
    }
    return result;
}

/**
 * Upload Avatar
 */
RchatService.uploadAvatar = async (req) => {
  
}

/**
 * Upload Avatar
 */
RchatService.getAvatar = async (req, res) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let { username }   = req.params;
        let authorization = generateToken(uid);
        let url = `${BASE_SVCCHAT_URL}/profile/avatar/${username}?authorization=${authorization}&aid=dd`;
        result = request.get(url).pipe(res);
    } catch (error) {
        console.log('RchatSvc send Messages Error : ', error);
    }
    return result; 
}

/**
 * Create Group Chat
 */
RchatService.createGroup = async (req) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let authorization = generateToken(uid);
        result = await RchatService.setpostURL(`/groups/create`, req.body, authorization);
    } catch (error) {
        console.log('RchatSvc Vreate Group Error : ', error);
    }
    return result;
}

/**
 * Update Profile
 */
RchatService.updateName = async (req) => {
    let result = null;
    try {
        let { uid }   = req.headers;
        let authorization = generateToken(uid);
        result = await RchatService.setpostURL(`/profile`, req.body, authorization);
    } catch (error) {
        console.log('RchatSvc Vreate Group Error : ', error);
    }
    return result;
}

module.exports = RchatService;