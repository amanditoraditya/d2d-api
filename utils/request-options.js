const fs    = require('fs')
const path  = require('path')
const jwt   = require('jsonwebtoken')

module.exports = requestOptions = (head, uid, url, body) => {
    //set SSL path
    let ssl_path = '../../../../../conf/ssl/2018/';

    // get SSL files
    let certFile    = path.resolve(__dirname, `${ssl_path}ssl_certificate.crt`);
    let keyFile     = path.resolve(__dirname, `${ssl_path}wildcard.d2d.co.id.key`);
    let caFile      = path.resolve(__dirname, `${ssl_path}dhparam2048.d2d.co.id.pem`);

    let platform = head.platform
    let authorization = head.authorization
    let key = (platform == 'android') ? 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJkNmM4OGUwZGE3MWFjNzFiNjU3MTg5NTI4MTZlZTMwMCIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuYW5kcm9pZCIsInBsYXRmb3JtIjoiYW5kcm9pZCIsImlhdCI6MTUyNDExMTQ3MX0.59_NSziZw1zm_QiqgpokdLIK8pOeOGz13OcRO9NQLxQ' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJmNTUzZWFlMmNhZDk5MmNjNTAxNzIzYjM5MTQzZDk2ZSIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuaW9zIiwicGxhdGZvcm0iOiJpb3MiLCJpYXQiOjE1MjIwNjgzOTR9.5EDnPOK-HB7H0TCyA3AcztC8aZc_VpkXr7dRxlL0IwQ'

    // generate JWT token
    // let token = jwt.sign({
    //     uid,
    //     platform,
    //     app_name: 'D2D',
    //     app_num: 1
    // }, fs.readFileSync(keyFile), {algorithm:'HS384', expiresIn: '5m'});
    let headers = null

    if(url.includes(CONFIG.URL_CMS_D2D)) {
        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 60});
        headers = {authorization: token}
    }

    if(url.includes(CONFIG.URL_CONTENT)) {
        headers = {key, uid, apps_id: 1}
    }

    if(url.includes(CONFIG.URL_D2D)) {
        headers = {authorization, platform}
    }

    if(url.includes(CONFIG.URL_GS)) {
        headers = {
            DeviceID: '1234567890',
            PushToken: 'AAAAAAAAAAAA',
            Authorization: 'Basic JDJhJDExJHZ4RlhBOGJTVG90dUpFMzEuek9zZE9zbUY4Q2NnS05mQ1dESHUzNHRvSmJadi9JOUxhL0ZXOldXS3hoNlNqaFlHV3MxU2NiSkl4RzdQazRqRXVaN1VGMkRDaHJGVXMzODV6YWN3b0Vv'
        }
    }

    let ssl_content = {
        url,
        headers,
        // headers:    {authorization: `Bearer ${token}`},
        form:       body
        // cert:       fs.readFileSync(certFile),
        // key:        fs.readFileSync(keyFile),
        // ca:         fs.readFileSync(caFile)
    };

    console.log(url)

    return ssl_content;
}