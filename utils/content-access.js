const request           = require('request');
const requestOptions    = require('./request-options');
const Utils             = require('./utils');

const ContentAccess = {};

ContentAccess.doctor = (req, uid, any) => {
    let isNumber = Utils.cekNumber(any)

    if(isNumber) {
        // Search doctor by NPA IDI
        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/doctor/read/npa_idi/${any}`, null);
        request.get(options, function(error, response, body) {
            console.log(body)
            return body;
        })
    } else {
        // Search doctor by Name
        console.log('campur / bukan nomor')
    }
}

ContentAccess.CmsContentRequest = (req, path) => {
    let url = `${CONFIG.URL_CMS_CONTENT}/api/v1/${path}`;

    // if (CONFIG.API_ENVIRONMENT == 'production') {
    //     url = `${CONFIG.URL_CMS_CONTENT}/api/v1/${path}`;
    // } else if (CONFIG.API_ENVIRONMENT == 'staging') {
    //     url = `${CONFIG.URL_CMS_CONTENT}/api/v1/${path}`;
    // } else {
    //     url = `${CONFIG.URL_CMS_CONTENT}/api/v1/${path}`;
    // }

    return url;
}

module.exports = ContentAccess;