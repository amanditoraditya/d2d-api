const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const { paginate }  = require('../utils/utils');
const moment        = require('moment');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL      = `${CONFIG.URL_GS}`;
const SERVICE       = 'GUE_SEHAT';

const GueSehatService    = {};

GueSehatService.obatAtoZ = {
    /* POST CREATE OBAT A TO Z
     ** uid          : String from firebase
     ** TOKEN        : Authorization from headers
     ** headers      : Array object headers
     */
    getAll: async (uid, TOKEN, filter = [], page= 1, limit = '', headers = { platform: 'android' }) => {
        let result      = [];
        // let result      = {
        //     acknowledge:true,
        //     data:[]  
        // };

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            // let URL   = `obat/filter?page=${page}&limit=200&apps=d2d`;
            let URL   = `obat/filter?page=${page}&limit=${limit}&apps=d2d`;
            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).get(URL);

            let data = record.data.data.data

            if (!_.isEmpty(data)) {
                //sort by create date
                // let sorted = data.sort(function(a, b){
                //     if(a.meta_title < b.meta_title) { return 1; }
                //     if(a.meta_title > b.meta_title) { return -1; }
                //     return 0;
                // })
                //paginate
                // data = sorted
                // data = paginate(data, page, limit)
                data = data
            } else {
                data = []
            }
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result = data;
        } catch (error) {
            console.log(error)
            result = [];
        }

        return result;
    },
    
    get: async (TOKEN, obat_id, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            // let URL   = `obat/filter?page=${page}&limit=200&apps=d2d`;
            let URL   = `obat/detail?obat_id=${obat_id}&apps=d2d`;
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).get(URL);

            let data = record.data.data.data

            if (!_.isEmpty(data)) {
                //sort by create date
                // let sorted = data.sort(function(a, b){
                //     if(a.meta_title < b.meta_title) { return 1; }
                //     if(a.meta_title > b.meta_title) { return -1; }
                //     return 0;
                // })
                //paginate
                // data = sorted
                // data = paginate(data, page, limit)
                data = data
            } else {
                data = []
            }
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result = data;
        } catch (error) {
            console.log(error)
           result  = error;
        }

        return result;
    },
}

module.exports = GueSehatService;
