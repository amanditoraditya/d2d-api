const { QueryTypes }    = require('sequelize');
const sequelize         = SEQUELIZE_INIT;
const Utils             = {};

Utils.cekEmail = async (email) => {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

Utils.cekNumber = async (number) => {
    let re = /^[0-9]*$/;
    return re.test(number);
}

Utils.cekNama = async (text) => {
    let re = /^(?!\s)[A-Za-z\.\'\’\s]+$/;
    let hasil = await re.test(text)
    return hasil;
}

Utils.cekFirebase = async (any) => {
    const admin = require('firebase-admin');
    isEmail     = Utils.cekEmail(any);
    isNumber    = Utils.cekNumber(any);
    
    if(isEmail) {
        admin.auth().getUserByEmail(any)
        .then(function(userRecord) {
            return userRecord;
        })
        .catch(function(error) {
            return error;
        });
    } else if(isNumber) {
        admin.auth().getUserByPhoneNumber(any)
        .then(function(userRecord) {
            return userRecord.toJSON();
        })
        .catch(function(error) {
            return error;
        });
    } else {
        admin.auth().getUser(any)
        .then(function(userRecord) {
            return userRecord.toJSON();
        })
        .catch(function(error) {
            return error;
        });
    }
}

Utils.getProfile = async (obj) => {
    const VProfileList      = require('../models/VProfileList');
    const requestOptions    = require('./request-options');
    const responseCustom    = require('./response-custom');
    const request           = require('request');

    await VProfileList.findOne({ where: {'uid': obj.uid} }).then(async profile => {
        if(profile != '' || profile != null ) {
            let sb_id           = (profile.subscription_id == null) ?  '' : profile.subscription_id.split(',');
            let sb_name         = (profile.subscription_name == null) ?  '' : profile.subscription_name.split(',');
            let sb_description  = (profile.subscription_description == null) ?  '' : profile.subscription_description.split(',');
            let sp_id           = (profile.spesialization_id == null) ?  '' : profile.spesialization_id.split(',');
            let sp_name         = (profile.spesialization_name == null) ?  '' : profile.spesialization_name.split(',');
            let sp_description  = (profile.spesialization_description == null) ?  '' : profile.spesialization_description.split(',');
            let spp_id          = (profile.subspesialization_id == null) ?  '' : profile.subspesialization_id.split(',');
            let spp_name        = (profile.subspesialization_name == null) ?  '' : profile.subspesialization_name.split(',');
            let spp_description = (profile.subspesialization_description == null) ?  '' : profile.subspesialization_description.split(',');
            let spp_parent_id   = (profile.subspesialization_parent_id == null) ?  '' : profile.subspesialization_parent_id.split(',');
            let ea              = (profile.event_attendee == null) ?  [] : profile.event_attendee.split(',');
            let sb              = [];
            let sp              = [];

            for(let i = 0; i < sb_id.length; i++) {
                sb.push({
                    id: sb_id[i],
                    title: sb_name[i],
                    description: sb_description[i]
                });
            }

            for(let i = 0; i < sp_id.length; i++) {
                let sublist = [];
                for(let j = 0; j < spp_id.length; j++) {
                    if(spp_parent_id[j] == sp_id[i]) {
                        sublist.push({
                            id: spp_id[j],
                            title: spp_name[j],
                            description: spp_description[j]
                        })
                    }
                }
                sp.push({
                    id: sp_id[i],
                    title: sp_name[i],
                    description: sp_description[i],
                    subSpecialistList: sublist
                });
            }

            let pns = (profile.pns == 'true') ? true : (profile.pns == 'false') ? false : null;

            let hasil = {
                id: profile.id,
                uid: profile.uid,
                name: profile.name,
                email: profile.email,
                npa_idi: profile.npa_idi,
                phone: profile.phone,
                born_date: profile.born_date,
                education_1: profile.education_1,
                education_2: profile.education_2,
                education_3: profile.education_3,
                pns,
                home_location: profile.home_location,
                clinic_location_1: profile.clinic_location_1,
                clinic_location_2: profile.clinic_location_2,
                clinic_location_3: profile.clinic_location_3,
                photo: profile.photo,
                spesialization: sp,
                subscription: sb,
                event_attendee: ea,
                counting: null
            };

            let options = requestOptions(obj.platform, obj.uid, `${CONFIG.URL_CONTENT}/user-activity?get=bookmark,download`, null)
            request.get(options, async function(error, response, body) {
                let result      = JSON.parse(body)
                hasil.counting  = result.result.data[0]
                let resp        = responseCustom.v1(200, true, null, [hasil]);
                return resp;
            })
        } else if(profile == null) {
            let resp = await responseCustom.v1(404, false, 'User ID Not Found', []);
            return resp;
        }
    });
}

Utils.randomArray = async (arr) => {
    let acak = arr.sort(function(a,b){
        return 0.5 - Math.random();
    });

    return acak;
}

Utils.asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

Utils.compare = async (sortedAnswers, dbAnswers) => {
    let score = 0;
    for(let i = 0; i < dbAnswers.length; i++) {
        if (typeof sortedAnswers[i] != 'undefined') {
            if(dbAnswers[i].answer == sortedAnswers[i].answer) await score++;
        }
    }

    let percentage = (score / dbAnswers.length) * 100;

    return percentage;
}

Utils.sort = async (answers) => {
    return answers.sort(function(a, b){
        var keyA = a.id,
            keyB = b.id;
        if(keyA < keyB) return -1;
        if(keyA > keyB) return 1;
        return 0;
    });
}

Utils.paging = async (page) => {
    let hasil = await (page * 10) - 10
    return hasil
}

Utils.getNestedSpecialist = async (uid, specialistId = 0) => {
    let type        = specialistId == 0 || specialistId == null || specialistId == 'null' ? `AND type = 'spesialization'` : `AND type = 'subspesialization'`;
    let parent_id   = specialistId == 0 || specialistId == null || specialistId == 'null' ? 'AND parent_id IS NULL' : `AND parent_id = '${specialistId}'`;
    let query = `SELECT 
                    competence_id AS id, competence_name AS name, competence_description AS description
                FROM user_competence 
                WHERE 1 AND uid = '${uid}' ${parent_id} ${type}`;

    let result  = await sequelize.query(query, { raw: false, type: QueryTypes.SELECT });

    return result;
}

Utils.getUserSpecialist = async (uid, specialistId = 0) => {
    let result      = []
    let specialist  = await Utils.getNestedSpecialist(uid, specialistId);
    
    if (specialist.length > 0) {
        for (let index in specialist) {
            let subspecialist   = await Utils.getNestedSpecialist(uid, specialist[index].id);
            let data            = {
                id: specialist[index].id, 
                name: specialist[index].name,
                description: specialist[index].description,
                subspecialist: subspecialist
            }
            result.push(data)
        }
    }
    
    return result;
}

Utils.getUser = async (uid) => {
    let query = `SELECT uid, email FROM user WHERE 1 AND uid = '${uid}'`;

    let result  = await sequelize.query(query, { raw: false, type: QueryTypes.SELECT });

    return result;
}

//change version string 1.1.1 into decimal version 10101
Utils.convertToDecimalVersion = (version) => {
    // version = `${String(version)}.${buildNumber}`;
    version = String(version);
    let stringArray = version.split(".");
    //skip first number
    for(let i = 1; i < stringArray.length ; i++){
        //if element version below 10 add 0 to front, example : 9 => 09
        stringArray[i] = stringArray[i].length < 2 ? `0${stringArray[i]}` : stringArray[i];
    }
    let versionDecimal = stringArray.join("");

    return Number(versionDecimal);
}

Utils.stringReplace = (text, array) => {
    let result = text.replace(/{([^{}]*)}/g,
        function(original, index) {
            return array[index];
        }
    );

    return result;
}

Utils.createRandomString = (length) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

Utils.paginate = (array, page_number,page_size,) => {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}
  
module.exports = Utils;