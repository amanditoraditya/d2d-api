const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL      = `${CONFIG.URL_GUE_SERVICE}`;
const SERVICE       = 'GUE';

const GueService    = {};

GueService.user = {
    /* POST CREATE GUE USER
     ** uid          : String from firebase
     ** payload      : Object
     ** TOKEN        : Authorization from headers
     ** headers      : Array object headers
     */
    createGue: async (data, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `user/create/gue_user`;

            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, data);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    },

    /* POST CREATE USER
     ** uid          : String from firebase
     ** payload      : Object
     ** TOKEN        : Authorization from headers
     ** headers      : Array object headers
     */
    create: async (data, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `user/create`;
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, data);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
            console.log(error.message)
           result  = error.message;
        }

        return result;
    },

    /* GET USER EMAIL
     ** uid          : String from firebase
     ** email        : String
     ** TOKEN        : Authorization from headers
     ** headers      : Array object headers
     */
    getEmail: async (email, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `user/detail/email/${email}`;
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).get(URL);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data;
        } catch (error) {
           result  = error.message;
        }

        return result;
    },

    resendVerification: async (payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(null, 'content', headers.platform);
            let URL   = `user/create/resend_verification`;
            
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.result;
        } catch (error) {
           result  = error.message;
        }

        return result;
    },

    forgotPassword: async (payload, uid, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `user/forgot_password`;
            
            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.result;
        } catch (error) {
           result  = error.message;
        }

        return result;
    },

    forgotPasswordConfirm: async (payload, uid, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `user/forgot_password/confirm`;
            
            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.result;
        } catch (error) {
           result  = error.message;
        }

        return result;
    },
}

module.exports = GueService;
