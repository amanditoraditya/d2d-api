const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL_V2   = `${process.env.D2DSVC_URL}/${process.env.D2DSVC_VERSION}`;
const BASE_URL      = `${CONFIG.URL_D2D}`;
const SERVICE       = 'D2D';

const D2DService    = {};

D2DService.cme = {
    /* GET Quiz Detail
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** headers      : Array object headers
     */
    getQuizDetail: async (uid, quizId, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `cme/quiz-detail/${quizId}`;

            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    },
}


D2DService.profile = {
    /* GET Quiz Detail
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** headers      : Array object headers
     */
    get: async (uid, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `profile`;

            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    },
}

D2DService.dkonsul = {
    /* GET Quiz Detail
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** headers      : Array object headers
     */
    login: async (uid, payload, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `dkonsul/login`;
            console.log(`${BASE_URL_V2}/${URL}`)
            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    },
}

module.exports = D2DService;