const _                 = require('lodash');
const generateToken     = require('./helpers/generate-token');
const API_REQUEST       = require('./api-request');
const BASE_URL          = `${CONFIG.URL_CMS_CONTENT}/api/v1`;
const SERVICE           = 'content-cms';

const ContentCms   = {};

ContentCms.createCertificate = async (uid, payload, headers = {}) => {
    let result      = null;
    try {
        const TOKEN = '';
        const URL   = `event/certificate`;
        console.log(BASE_URL, '/', URL)
        const { data }  = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
        console.log(data)
        result  = data;
    } catch (error) {
        console.log(error);
        result  = error.response.data;
    }

    return result;
}

module.exports = ContentCms;