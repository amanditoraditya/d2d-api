const parseMessageCode = require('./helpers/parse-messagecode');
const path                  = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';
const ResponseCustom = {};

ResponseCustom.v1 = (status, acknowledge, message, data) => {
    const messageCode = parseMessageCode(status);
    
    const res = {
        status,
        message: messageCode,
        result: {
            acknowledge,
            message,
            data
        }
    }

    return res;
}
/**
 * new result APi
 */
ResponseCustom.v21 = (status, acknowledge, data) => {
    const messageCode = parseMessageCode(status);
    
    const res = {
        status,
        message: messageCode,
        acknowledge,
        result: data
    }

    return res;
}
/**
 * Edit Value Object
 */
ResponseCustom.addUrlimages = (result) => {
    let obj = [];
    for(let i = 0; i < result.length; i++) {
        if(result[i].type=='image'){
            result[i].url = `https://static.d2d.co.id/${path}banner/images/${result[i].url}`;
        }
        obj.push(result[i]);
    }
    return obj;
}

module.exports = ResponseCustom;