const _                 = require('lodash');
const generateToken     = require('./helpers/generate-token');
const API_REQUEST       = require('./api-request');
const UserDevice        = require('../models/UserDevice');
const UserNotification  = require('../models/UserNotification');
const { TYPES_NOTIFICATION }    = require('./helpers/enum');
const BASE_URL          = `${process.env.NOTIFSVC_URL}/${process.env.NOTIFSVC_VERSION}`;
const SERVICE           = 'notification';

const NotifService   = {};

NotifService.sendNotification = async (uid, payload) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `notifications`;
        
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}

NotifService.addNotification = async (uid, type, title, body, data) => {
    let result = {}
    try {
        if (_.includes(TYPES_NOTIFICATION, type)) {
            let userNotif   = { title, body, type, data: JSON.stringify(data), status: 'unread', uid } 
            let saveNotif   = await UserNotification.build(userNotif).save();
                result      = saveNotif.dataValues;

            // get FCM token
            let userDevices = await UserDevice.findAll({ where: { uid: uid } });
            let devices     = [];
            userDevices.forEach((val, i) => {
                if (!_.isEmpty(val.dataValues.device_token)) {
                    devices.push(val.dataValues.device_token);
                }
            });

            if (!_.isEmpty(devices)) {
                let target  = { type: "tokens", "value": devices };
                let payload = {
                    priority    : 10,
                    title       : title,
                    body        : body,
                    data        : JSON.stringify(data),
                    target      : JSON.stringify(target)
                };
                // sending notification
                let notif = await NotifService.sendNotification(uid, payload);
                
                console.log(`├─ addNotification`);
            }
        }
    } catch (error) {
        console.log(`error`, error);
    }

    return result;
}

NotifService.sendEmail = async (uid, name, email, type, other = {}) => {
    let result      = null;
    try {
        const TOKEN = generateToken(uid, SERVICE);
        const URL   = `email`;

        let payload = _.assign({ name, email, type}, other);
        const { data }  = await API_REQUEST(BASE_URL, TOKEN).post(URL, payload);
        
        result  = data;
    } catch (error) {
        result  = error.response.data;
    }

    return result;
}

module.exports = NotifService;