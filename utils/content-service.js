const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL      = `${CONFIG.URL_CONTENT}`;
const BASE_URL_V2   = `${process.env.CONTENTSVC_URL}/${process.env.CONTENTSVC_VERSION}`;
const SERVICE       = 'user';

const ContentService    = {};

ContentService.checkIdi = async (name, npa_idi, options = null) => {
    let result      = null;
    // try {
        let TOKEN = '';
        let URL   = `doctor/${npa_idi}`;
        let names = name.split(' ');

        console.log(`- Check NPA IDI: ${BASE_URL}/${URL}`);
        let { data: doctor }  = await API_REQUEST(BASE_URL, TOKEN, options).get(URL);
        let valid   = false;

        if (typeof doctor.result !== 'undefined' || !_.isEmpty(doctor.result)) {
            // get result from data doctor
            console.log(`- Check NPA IDI: get result from data doctor`);

            let doctors = doctor.result;
            doctors.forEach((val, i) => {
                names.forEach((rec, j) => {
                    if (_.includes(_.lowerCase(val.name), _.lowerCase(rec)) && valid === false) {
                        console.log(`- Check NPA IDI: valid from data doctor 2`);
                        valid   = true;
                        result  = val;
                    }
                })
            })
        }

        if (valid === false) {
            URL   = `scrape/${npa_idi}`;

            console.log(`- Check NPA IDI: ${BASE_URL}/${URL}`);
            let { data: doctor }  = await API_REQUEST(BASE_URL, TOKEN, options).get(URL);

            if (typeof doctor.result !== 'undefined' || !_.isEmpty(doctor.result)) {
                // get result from idi online
                console.log(`- Check NPA IDI: get result from IDI Online`);

                let doctors = doctor.result;
                doctors.forEach((val, i) => {
                    names.forEach((rec, j) => {
                        if (_.includes(_.lowerCase(val.name), _.lowerCase(rec)) && npa_idi == val.npa_idi && valid === false) {
                            console.log(`- Check NPA IDI: valid from IDI Online`);
                            valid   = true;
                            result  = val;
                        }
                    })
                })
            }
        }
    // } catch (error) {
    //     result = error.message;
    // }

    return result;
}

ContentService.useractivity = {
    /* GET ALL WEBINAR
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (uid, filter = [], page = 1, limit = 10, headers = { platform: 'android' }) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `useractivity?page=${page}&limit=${limit}`;

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
       }

        return result;
    },

    /* GET SUM WEBINAR
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** headers      : Array object headers
     */
    getSum: async (uid, filter = [], headers = { platform: 'android' }) => {
        let result      = [];

        try {
            let TOKEN = await generateToken(uid, 'content', headers.platform);
            let URL   = `useractivity/sum?`;

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }
            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
       }

        return result;
    },

    /* SAVE USERACTIVITY
     ** uid          : String from firebase
     ** app          : app,
     ** activity     : Enum('like','bookmark','share','view','download','comment','reserve','join','quisioner'),
     ** content      : String,
     ** content_id   : Integer,
     ** platform     : Enum('android','ios') from headers
     */
    save: async (uid, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `useractivity`;
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    }
}

ContentService.event = {
    /* GET ALL EVENT
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'competence', value: '1,32'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (uid, filter = [], page = 1, limit = 10, headers) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `event?page=${page}&limit=${limit}`;

            _.assign(headers, { uid });

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
       }

        return result;
    },
    /* GET EVENT by id
     ** uid         : String from firebase
     ** Event Id    : Integer
     ** headers     : Array object headers
     */
    get: async (eventId, uid, headers = { apps_id: 1, platform: 'android' }) =>{
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `event/${eventId}`;

            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },
    /* GET EVENT by hash
     ** uid         : String from firebase
     ** Event hash  : string
     ** headers     : Array object headers
     */
    getByHash: async (eventHash, uid, headers = { apps_id: 1, platform: 'android' }) =>{
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `event/hash/${eventHash}`;

            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },
}

ContentService.webinar = {
    /* GET WEBINAR
     ** uid          : String from firebase
     ** getBy        => set get webinar using id or slug hash : string
     ** webinarParam => webinar id or webinar slug hash: Integer / string
     ** headers      : Array object headers
     */
    get: async (uid, getBy = 'id', webinarParam, email, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = ``;
            if(_.toLower(getBy) == 'hash'){
                URL   = `webinar/hash/${webinarParam}`;
            }
            else {
                URL   = `webinar/${webinarParam}`;
            }

            _.assign(headers, { uid });
            _.assign(headers, { email });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* GET ALL WEBINAR
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (uid, filter = [], page = 1, limit = 10, headers = { platform: 'android' }) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar?page=${page}&limit=${limit}`;

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result = error.message;
       }

        return result;
    },

    /* GET WEBINAR PRETEST
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** payload      : Object with format --> { member_id: optional, email: required }
     ** headres      : Array object headers
     */
    getPretest: async (uid, webinarId, payload, headers = { platform: 'android' }) => {
        let result      = {};
        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/pretest/${webinarId}`;
            _.assign(payload, { uid });

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result  = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* SUBMIT WEBINAR PRETEST
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** payload      : Object with format --> { member_id: optional, email: required, id_attempt: required, answers: required }
     ** headres      : Array object headers
     */
    submitPretest: async (uid, webinarId, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/pretest/${webinarId}/answers`;

            _.assign(payload, { uid });

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* GET WEBINAR POSTTEST
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** payload      : Object with format --> { member_id: optional, email: required }
     ** headres      : Array object headers
     */
    getPosttest: async (uid, webinarId, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/posttest/${webinarId}`;

            _.assign(payload, { uid });

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* SUBMIT WEBINAR POSTTEST
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** payload      : Object with format --> { member_id: optional, email: required, id_attempt: required, answers: required }
     ** headres      : Array object headers
     */
    submitPosttest: async (uid, webinarId, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/posttest/${webinarId}/answers`;

            _.assign(payload, { uid });
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* SAVE QUISIONER
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** payload      : Object with format --> { member_id: optional, email: required, id_attempt: required, answers: required }
     ** headres      : Array object headers
     */
    saveQuisioner: async (uid, webinar_id, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/quisioner/${webinar_id}`;
            console.log(payload)
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result = error.message;
        }

        return result;
    },

    /* GET EVENT WEBINAR
     ** uid          : String from firebase
     ** payload      : Object with format --> { member_id: optional, email: required, id_attempt: required, answers: required }
     ** headres      : Array object headers
     */
    getEvent: async (uid, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/event`;
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result  = error.message;
        }

        return result;
    },

    /* GET Attende Webinar
     ** uid          : String from firebase
     ** payload      : Object with format --> { stringAttendee: required }
     ** headres      : Array object headers
     */
    getAttende: async (uid, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `webinar/event`;
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);

            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
        
            result      = record.data.acknowledge == false ? {} : record.data.result;
        } catch (error) {
           result  = error.message;
        }

        return result;
    }

}


ContentService.email = {

    /*  SEND EMAIL
     ** uid          : String from firebase
     ** app          : app,
     ** activity     : Enum('like','bookmark','share','view','download','comment','reserve','join','quisioner'),
     ** content      : String,
     ** content_id   : Integer,
     ** platform     : Enum('android','ios') from headers
     */
    send: async (uid, payload, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `email/send`;
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            
            if (record.data.rejected.length > 0 || record.data.accepted.length == 0) {
                console.log('Error: ', record);
            }
            
            result      = record.data.response == 'No data found!' ? [] : record.data;
        } catch (error) {
           result = error.message;
        }

        return result;
    }
}

ContentService.doctor = {
    /* GET ALL doctor
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (uid, filter = [], page = 1, limit = 10, headers = { platform: 'android' }) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `doctor?page=${page}&limit=${limit}`;
            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }
            _.assign(headers, { uid });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.message;
       }

        return result;
    },

    /* GET doctor
     ** uid          : String from firebase
     ** npaidi       : Integer
     ** names        : Array
     ** headers      : Array object headers
     */
    get: async (uid, npaidi, names, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `doctor/${npaidi}`;

            _.assign(headers, { uid });
            
            let {data} = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (data.acknowledge == true) {
                let doctors = data.result;

                if (doctors.length > 0) {
                    doctors.forEach((val, i) => {
                        names.forEach((rec, j) => {
                            if (_.includes(_.lowerCase(val.name), _.lowerCase(rec)) && npaidi == val.npa_idi) {
                                result  = val;
                            }
                        })
                    })
                }
            }
        } catch (error) {
            console.log('content-service: check npa idi', error.message);
        }

        return result;
    },
}


ContentService.specialist = {
    /* GET ALL SPECIALIST
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (uid, filter = [], page = 1, limit = 10, countrycode = 'ID', headers = { platform: 'android'}) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `specialist?page=${page}&limit=${limit}`;

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            _.assign(headers, { uid });
            _.assign(headers, { countrycode });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
       }

        return result;
    },

    /* GET SPECIALIST
     ** uid          : String from firebase
     ** id           : String from req.params
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** headers      : Array object headers
     */
    get: async (uid, id, countrycode = 'ID', headers = { platform: 'android' }) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `specialist/${id}`;
            
            _.assign(headers, { uid });
            _.assign(headers, { countrycode });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
       }

        return result;
    },

    /* GET DETAIL LIST SPECIALIST
     */
    getDetailList: async (uid, payload = [], countrycode = 'ID', headers = { platform: 'android'}) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `specialist/detail-list`;

            _.assign(headers, { uid });
            _.assign(headers, { countrycode });

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).post(URL, payload);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
            result  = error.response.data;
        }

        return result;
    },
}

ContentService.search = {
    getAll: async (uid, filter = [], page = 1, limit = 3, countrycode = 'ID', headers = { platform: 'android'}) => {
        let result      = [];

        try {
            let TOKEN = generateToken(uid, 'content', headers.platform);
            let URL   = `search?page=${page}&limit=${limit}`;

            if (filter.length > 0) {
                for(let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            _.assign(headers, { uid });
            _.assign(headers, { countrycode });
            
            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
           result  = error.response.data;
       }

        return result;
    },
}

module.exports = ContentService;
