const _             = require('lodash');
const API_REQUEST   = require('../utils/api-request');
const generateToken = require('../utils/helpers/generate-token');
const BASE_URL      = `${CONFIG.URL_CMS_D2D}/api/v1`;
const SERVICE       = 'D2D';

const D2DCMSService    = {};

D2DCMSService.certificate = {
    /* GET Quiz Detail
     ** uid          : String from firebase
     ** webinarId    : Integer
     ** headers      : Array object headers
     */
    create: async (uid, payload, TOKEN, headers = { platform: 'android' }) => {
        let result      = {};

        try {
            // let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `certificate/create`;
            
            console.log(payload)
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            console.log(record)
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    },
}

D2DCMSService.payment = {
    receipt: async (uid, payload, headers = { platform: 'android' }) => {
        let result      = {};
        try {
            let TOKEN = generateToken(uid, 'd2d', headers.platform);
            let URL   = `payment/receipt`;
            
            let record = await API_REQUEST(BASE_URL, TOKEN, headers).post(URL, payload);
            
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }
        
            result      = record.data.message == 'No data found!' ? [] : record.data;
        } catch (error) {
           result  = error.response.data;
        }

        return result;
    }
}

module.exports = D2DCMSService;