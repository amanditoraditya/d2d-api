const midtransClient            = require('midtrans-client');
const _                         = require('lodash');
const API_REQUEST               = require('./api-request');
const {MidtransPaymentTypes}    = require('../utils/helpers/enum');

const MidTransService   = {};
const LOG               = "MidTransService"

const SNAP = new midtransClient.Snap({
    isProduction    : process.env.MIDTRANS_ENVIRONMENT == 'production',
    serverKey       : process.env.MIDTRANS_SERVER_KEY,
    clientKey       : process.env.MIDTRANS_CLIENT_KEY
});

const CoreApi = new midtransClient.CoreApi({
    isProduction    : process.env.MIDTRANS_ENVIRONMENT == 'production',
    serverKey       : process.env.MIDTRANS_SERVER_KEY,
    clientKey       : process.env.MIDTRANS_CLIENT_KEY
});

const CONTENT_TYPES ={
    EVENT : "event"
}

const STATUS    = {
    TRANSACTION: {
        CAPTURE     : 'capture',
        SETTLEMENT  : 'settlement',
        CANCEL      : 'cancel',
        DENY        : 'deny',
        EXPIRE      : 'expire',
        PENDING     : 'pending',
        REFUND      : 'refund'
    },
    FRAUD: {
        CHALLENGE   : 'challenge',
        ACCEPT      : 'accept',
        DENY        : 'deny'
    },
    ACCEPT_STATUS: {
        CHALLENGE   : 'challenge',
        PENDING     : 'pending',
        SUCCESS     : 'success',
        CANCEL      : 'cancel',
        FAILURE     : 'failure',
        EXPIRED     : 'expired',
    }
}

const PAYMENTS = {
    bri_epay                : { key: 'bri_epay', value: 'BRI Epay' },
    akulaku                 : { key: 'akulaku', value: 'Akulaku' },
    cstore                  : { key: 'cstore', value: 'CStore' }, //toko seperti alfa, indomaret
    danamon_online          : { key: 'danamon_online', value: 'Danamon Online' },
    cimb_clicks             : { key: 'cimb_clicks', value: 'CIMB Clicks' },
    echannel                : { key: 'echannel', value: 'Mandiri Bill' },
    mandiri_clickpay        : { key: 'mandiri_clickpay', value: 'Mandiri ClickPay' },
    bca_klikbca             : { key: 'bca_klikbca', value: 'KlikBCA' },
    bca_klikpay             : { key: 'bca_klikpay', value: 'BCA Klikpay' },
    bank_transfer           : { key: 'bank_transfer', value: 'Transfer' }, // VA account
    gopay                   : { key: 'gopay', value: 'GoPay' },
    credit_card             : { key: 'credit_card', value: 'Credit Card' },
}

//get payment info for get detail of notification to store it into other info
MidTransService.getPaymentInfo = (data) =>{
    console.log(`|- ${LOG} - getPaymentInfo`);
    try{  
        let paymentInfo = {};
        if(_.isEmpty(data) == false){
            paymentInfo = {
                payment_type: data.payment_type,
                card_type: "",
                bank: "", // bank/store
                account_number: "", //masked_card/va_number/
                signature_key: data.signature_key,
                approval_code: "" ,
                transaction_time : "",
                bill_key: "", //for mandiri bill key
            }
    
            if(data.payment_type == MidtransPaymentTypes.CreditCard){
                paymentInfo.bank = data.bank;
                paymentInfo.account_number = data.masked_card;
                paymentInfo.card_type = data.card_type;
            }
            else if(data.payment_type == MidtransPaymentTypes.Cstore){
                paymentInfo.bank = data.store
            }
            else if(data.payment_type == MidtransPaymentTypes.BankTransfer){
                if(_.isUndefined(data.permata_va_number) == false){
                    paymentInfo.bank = "Permata" 
                    paymentInfo.account_number = data.permata_va_number
                }
                else if(_.isUndefined(data.va_numbers) == false){
                    paymentInfo.bank = data.va_numbers[0].bank;
                    paymentInfo.account_number = data.va_numbers[0].va_number;
                    if(_.isUndefined(data.transaction_time) == false){
                        paymentInfo.transaction_time = data.transaction_time;
                    }
                }
            }
            else if(data.payment_type == MidtransPaymentTypes.MandiriBill){
                paymentInfo.bank = "Mandiri Bill";
                paymentInfo.account_number = data.biller_code;
                paymentInfo.bill_key = data.bill_key;
                paymentInfo.approval_code = data.approval_code;
            }
            else if(data.payment_type == MidtransPaymentTypes.Akulaku){
                paymentInfo.transaction_time = data.transaction_time;
            }
            else {
                paymentInfo.approval_code = data.approval_code;
                if(_.isUndefined(data.transaction_time) == false){
                    paymentInfo.transaction_time = data.transaction_time;
                }
            }

            return JSON.stringify(paymentInfo);
        }
    }catch(error){
        let err     = new Error(error);
        err.code    = 500;
        console.log(`|- ${LOG} - ERROR : ${err}`);
        return "";
    }
}

module.exports = { MidTransService, SNAP, CoreApi, STATUS, PAYMENTS, CONTENT_TYPES } ;