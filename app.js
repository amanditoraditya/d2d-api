require('dotenv').config()

const express       = require('express')
const bodyParser    = require('body-parser')
const _             = require('lodash')
const app           = express()
const useragent     = require('express-useragent')
const config        = process.env
const Sequelize     = require('sequelize')
const admin         = require('firebase-admin')
const cors          = require('cors')
const UserSVC       = require('./utils/user-service');


let serviceAccount  = require(`./gue-login-prod.json`)

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://gue-login.firebaseio.com'
})

const parseMessageCode = require('./utils/helpers/parse-messagecode')

global.CONFIG = {
    API_VERSION: config.API_VERSION,
    API_MAINTENANCE: config.API_MAINTENANCE,
    API_ENVIRONMENT: config.API_ENVIRONMENT,

    PORT: (config.API_ENVIRONMENT == 'development' ? config.PORT_DEV : (config.API_ENVIRONMENT == 'staging' ? config.PORT_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.PORT_DEV : config.PORT_PROD))),
    XDCHANNEL: ((config.API_ENVIRONMENT == 'development' || config.API_ENVIRONMENT == 'localhost') ? config.XDCHANNEL_KEY : (config.API_ENVIRONMENT == 'staging' ? config.XDCHANNEL_KEY : config.XDCHANNEL_KEY)),

    URL_CONTENT: (config.API_ENVIRONMENT == 'development' ? config.URL_CONTENT_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_CONTENT_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_CONTENT_LOCAL : config.URL_CONTENT_PROD))),
    URL_D2D_CHANNEL: ((config.API_ENVIRONMENT == 'development' || config.API_ENVIRONMENT == 'localhost') ? config.D2DCHANNEL_URL_LOCAL : (config.API_ENVIRONMENT == 'staging' ? config.D2DCHANNEL_URL_STAG : config.D2DCHANNEL_URL_PROD)),
    URL_D2D: (config.API_ENVIRONMENT == 'development' ? config.URL_D2D_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_D2D_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_D2D_LOCAL : config.URL_D2D_PROD))),
    URL_GUE_SERVICE: (config.API_ENVIRONMENT == 'development' ? config.URL_GUE_SERVICE_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_GUE_SERVICE_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_GUE_SERVICE_LOCAL : config.URL_GUE_SERVICE_PROD))),
    URL_CMS_D2D: (config.API_ENVIRONMENT == 'development' ? config.URL_CMS_D2D_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_CMS_D2D_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_CMS_D2D_LOCAL : config.URL_CMS_D2D_PROD))),
    URL_CMS_CONTENT: (config.API_ENVIRONMENT == 'development' ? config.URL_CMS_CONTENT_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_CMS_CONTENT_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_CMS_CONTENT_LOCAL : config.URL_CMS_CONTENT_PROD))),
    URL_TD: (config.API_ENVIRONMENT == 'development' ? config.URL_TD_STAG : (config.API_ENVIRONMENT == 'staging' ? config.URL_TD_STAG : (config.API_ENVIRONMENT == 'localhost' ? config.URL_TD_STAG : config.URL_TD_PROD))),
    URL_GS: (config.API_ENVIRONMENT == 'development' ? config.URL_GUE_SEHAT_DEV : (config.API_ENVIRONMENT == 'staging' ? config.URL_GUE_SEHAT_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.URL_GUE_SEHAT_STAGING : config.URL_GUE_SEHAT_PROD))),

    DB_HOST: (config.API_ENVIRONMENT == 'production' ? config.DB_HOST : config.DB_HOST_DEV),
    DB_USER_CONTENT: config.DB_USER_CONTENT,
    DB_USER_D2D: config.DB_USER_D2D,
    DB_PASS_CONTENT: config.DB_PASS_CONTENT,
    DB_PASS_D2D: (config.API_ENVIRONMENT == 'production'  ? config.DB_PASS_D2D : config.DB_PASS_D2D_DEV),
    DB_D2D: (config.API_ENVIRONMENT == 'development'  ? config.DB_D2D_DEV : (config.API_ENVIRONMENT == 'staging' ? config.DB_D2D_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.DB_D2D_DEV : config.DB_D2D_PROD))),
    DB_CONTENT: (config.API_ENVIRONMENT == 'development'  ? config.DB_CONTENT_DEV : (config.API_ENVIRONMENT == 'staging' ? config.DB_CONTENT_STAGING : (config.API_ENVIRONMENT == 'localhost' ? config.DB_CONTENT_DEV : config.DB_CONTENT_PROD))),
    
    CONNECTION_LIMIT: config.CONNECTION_LIMIT,
    FIREBASE_SDK: (config.API_ENVIRONMENT == 'production' ? config.FIREBASE_SDK_PROD : config.FIREBASE_SDK_DEV),

    REDIS_PORT: config.REDIS_PORT,
    REDIS_HOST_PRIVATE: config.REDIS_IP_PRIVATE,
    REDIS_DB_NUMBER: config.REDIS_DB_NUMBER,
    REDIS_AUTH_PASS: config.REDIS_AUTH_PASS,
    
    SSL_PATH: config.SSL_PATH
}

global.SEQUELIZE = {
    host: CONFIG.DB_HOST,
    dialect: 'mysql',
    logging: false,
    // logging: function (str) { console.log(str) },
    pool: {
        max: CONFIG.CONNECTION_LIMIT,
        min: 0,
        idle: 300
    },
   // dialectOptions: {
       // useUTC: false //for reading from database
    //},
    timezone: '+07:00' //for writing to database
}

global.SEQUELIZE_INIT = new Sequelize(CONFIG.DB_D2D, CONFIG.DB_USER_D2D, CONFIG.DB_PASS_D2D, SEQUELIZE);

app.use(cors());
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({
    verify: (req, res, buf) => {
        req.rawBody = buf
    }
}));
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.raw());
app.use(useragent.express())

require('./payment/routes')(app)
require('./v2.1/routes')(app)
require('./v3.0/routes')(app)
require('./v3.1/routes')(app)

// for verfiy account
app.use('/verify/:uid/:code', async function (req, res, next) {
    let { uid, code } = req.params;
    
    let { acknowledge, result } = await UserSVC.verification(uid, { verification: code });

    if (acknowledge == true && !_.isEmpty(result)) {
        res.redirect('https://d2d.co.id?verification=success');
    } else {
        res.redirect('https://d2d.co.id');
    }
})

// Service is under maintenance && Catching error and forward to error handler
app.use(function (req, res, next) {
    if (CONFIG.API_MAINTENANCE === true) {
        let err     = new Error('The server is under maintenance');
        err.code    = 503
        next(err)
    } else {
        let err     = new Error('The server can not find your request');
        err.code    = 404;
        next(err)
    }
});

// Error handler
app.use(function (err, req, res, next) {
    let message     = null;
    let validation  = typeof err.validation != 'undefined' ? err.validation.result : null;
    
    if (typeof err.validation != 'undefined' && typeof err.validation.messages == 'string') {
        message = err.validation.messages;
    } else {
        message = parseMessageCode(err.code, err.message)
    }
    
    res.status(err.code || 400)
        .send({
            status      : err.code,
            message     : message,
            acknowledge : false,
            result      : {},
            errors      : { validation }
        })
    //Error
    console.log(`└─ ${err.code} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
});

process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', reason.stack || reason)
})

const server    = app.listen(global.CONFIG.PORT || 3000, "0.0.0.0", function () {
    let now = new Date()
    let month = (now.getMonth()+1 < 10) ? `0${now.getMonth()+1}` : now.getMonth()
    let day = (now.getDay() < 10) ? `0${now.getDay()}` : now.getDay()
    let hours = (now.getHours() < 10) ? `0${now.getHours()}` : now.getHours()
    let minutes = (now.getMinutes() < 10) ? `0${now.getMinutes()}` : now.getMinutes()
    let seconds = (now.getSeconds() < 10) ? `0${now.getSeconds()}` : now.getSeconds()
    let format = `${now.getFullYear()}-${month}-${day} ${hours}:${minutes}:${seconds}`
    console.log('======================================')
    console.log('       App listening on port %s', server.address().port)
    console.log('       Press Ctrl+C to quit.')
    console.log('======================================')
})
