const _                 = require('lodash');
const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const utils             = require('../../utils/utils');
const Quiz              = require('../../models/Quiz');
const QuizTaken         = require('../../models/QuizTaken');
const NotifService      = require('../../utils/notif-service');

const jwt               = require('jsonwebtoken');
const datetime          = require('node-datetime');
const request           = require('request');

const parseResponse     = require('../../utils/helpers/parse-response');
const CmeController     = {}
const log               = 'Cme controller';
const path              = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const UserSkpTmpModel    = require('../models/user_skp_tmp.model');
const D2DCMSSVC          = require('../../utils/d2d-cms-service');
const querystring        = require("querystring");

CmeController.submitQuiz = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Quiz`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let auth            = req.headers.authorization
        let plat            = req.headers.platform

        // asyncrequest        = asyncrequest.defaults({headers: {auth, plat}});
        let quizid          = req.body.quizid; //quiz id
        let uid             = req.body.uid; //uid
        let skp             = req.body.skp; //skp
        let source          = req.body.source; //source
        let ansTos          = req.body.answers.toString();
        let answer          = ansTos.replace(/\"\[/g, '[').replace(/\]\"/g, ']');
            answer          = answer.replace(/\\"/g, '"');

        // let answer          = ansTos.replace(/"\[/g, '[').replace(/\]"/g, ']');
        // answer              = answer.replace(/'\[/g, '[').replace(/\]'/g, ']');
        // answer              = answer.replace(/\\"/g, '"');
        let answers         = JSON.parse(answer); //answers
        // let score           = await scoring(quizid, answers); //score
        // let result          = await (score >= 60) ? `Y` : `N`; //result

        // async function scoring(quizid, answers) {

            // get detail quiz
            let quiz = await Quiz.findOne({ where: { id: quizid }, raw: true });

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/cme/quiz-detail/${quizid}`, null)
            request.get(options, async function(error, response, body) {
                let sortedAnswers   = await utils.sort(answers);
                let getDb           = body;
                let parsedDb        = JSON.parse(getDb);
                let dbAnswers       = await utils.sort(parsedDb.result.data);
                let score           = await utils.compare(sortedAnswers, dbAnswers);
                let result          = await (score >= 60) ? `Y` : `N`; //result

                // return await score;
                // console.log({quiz_id: quizid, uid, answers, score, result})
                let jawaban = answers.map(obj => {
                    let jawab = {
                                    id: obj.id,
                                    question: obj.question,
                                    answer: obj.answer,
                                    choices: obj.choices
                                }

                    jawab.answer = escape(obj.answer)
                    let pilihan = obj.choices
                    jawab.choices = JSON.stringify(pilihan.map(escape))

                    return jawab
                })

                QuizTaken.build(
                    {quiz_id: quizid, uid, answers: `${JSON.stringify(jawaban)}`, result}
                ).save().then( async () => {
                    if(result == 'Y') {
                        let conditions = [
                            { jointer: 'AND', key: `quiz_id`, value: quizid, op: '=' },
                            { jointer: 'AND', key: `uid`, value: uid, op: '=' }
                        ]
                        let certifExist = await UserSkpTmpModel.getBy(['*'],conditions,[],[],2,[])
                        //update data if certificate exist, and insert if not exist.
                        let isInsert = _.isEmpty(certifExist);

                        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
                        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 360});
                        let form        = {uid, quiz: quizid}

                        let createCertResult = await D2DCMSSVC.certificate.create(uid, querystring.stringify(form), token)
                        // console.log("certif create",createCertResult,uid, querystring.stringify(form),token);
                        // let result   = JSON.parse( await response.body);
                        let certificate = await createCertResult.data.split('/');
                        let dt          = datetime.create();
                        let formatted   = dt.format('Y-m-d H:M:S');

                        let dataSkpTmp = [
                            { key: 'uid', value: uid },
                            { key: 'skp', value: skp },
                            { key: 'source', value: source },
                            { key: 'taken_on', value: formatted },
                            { key: 'certificate', value: certificate[1] },
                            { key: 'quiz_id', value: quizid },
                            { key: 'type', value: 'cme' },
                            { key: 'status', value: 'valid' },
                        ];
                        if(isInsert == true) conditions = [];
                        let createUserSkpTmpRes = await UserSkpTmpModel.save(dataSkpTmp,conditions)

                        if(isInsert == true){
                            let notif   = {
                                title           : quiz.skp <= 0 ? 
                                    LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE : 
                                    utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE_DETAIL,[quiz.skp]),
                                body            : LANG[cc].CME.EMAIL_CERTIFICATE.ACCESS_NOTIFY,
                                type            : 'certificate_cme',
                                content_id      : `${quizid}`
                            };
                            
                            let data    = {
                                'type'          : notif.type,
                                'content_id'    : notif.content_id,
                                'certificate'   : ''
                            };

                            // sending notification
                            let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);
                            console.log(`├─ ${log} :: ecertificate :: send notification`);
                            message  = utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.SUCCESS_TAKE_QUIZ,[skp]);
                        }
                        
                        let record = {certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${certificate[1]}`}
                        parseResponse(res, 200, record, message, true);
                    } else {
                        message  = LANG[cc].CME.EMAIL_CERTIFICATE.TRY_AGAIN;
                        parseResponse(res, 200, null, message, false);
                    }
                }).catch(error => {
                    console.log(error);
                    let err = new Error(error)
                        err.code = 488;
                    next(err);
                })
            })
        // }
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Quiz :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = CmeController;