const _                     = require('lodash');
const  moment               = require('moment');
const BannerModel           = require('../models/banner.model');
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'BannerController';
const path                  = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';

const BannerController = {}

BannerController.getAll = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        // let { id }      = req.params;
        // let { uid }     = req.headers

        let condition   = [];
        let record      = []

        //defult apps key d2d
        condition.push({ key: 'publish', value: '1' });
        condition.push({ key: 'apps', value: 'D2D' });


        record = await BannerModel.getAll(condition);
        
        //add Url images
        if (!_.isEmpty(record)) {
            record.forEach(row => {
                if (row.type == 'image') {
                    row.url = `https://static.d2d.co.id/${path}banner/images/${row.url}`
                }
                return row
            });
        }
        
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

BannerController.get = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { name }            = req.params;
        let { countrycode }     = req.headers
        
        let condition   = [];
        let join        = [];
        let record      = [];
        let result      = [];
        let fields      = [
            'banner.id',
            'banner.apps',
            'banner.url',
            'banner.title',
            'banner.type',
            'banner.link',
            'sizes.name',
            'banner_locale.country_code'
        ];

        //defult apps key d2d
        condition.push({ key: 'publish', value: '1' });
        condition.push({ key: 'apps', value: 'D2D' });
        condition.push({ key: 'sizes.type', value: 'banner' });
        condition.push({ key: 'sizes.name', value: name });
        condition.push({ key: 'banner_locale.country_code', value: countrycode });

        join.push(`LEFT JOIN sizes ON banner.size_id = sizes.id `)
        join.push(`LEFT JOIN banner_locale ON banner_locale.banner_id = banner.id `)

        record = await BannerModel.getAll(condition, fields, join);

        //add Url images
        if (!_.isEmpty(record)) {
            for (row of record) {
                let newrow = {}

                if (row.type == 'image') {
                    row.url = `https://static.d2d.co.id/${path}banner/images/${row.url}`
                }
                newrow = {
                    id: row.id,
                    apps: row.apps,
                    url: row.url,
                    title: row.title,
                    link: row.link,
                    screen_name: row.name,
                    country_code: row.country_code
                }
                result.push(newrow)
            }
        }
        
        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, result, _.isEmpty(result) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = BannerController;