const _                     = require('lodash');
const moment                = require('moment');
const utils                 = require('../../utils/utils');
const fs                    = require('fs');
const oss                   = require('ali-oss'); 

const redisService          = require('../../utils/helpers/RedisService');
const UserSVC               = require('../../utils/user-service');
const ContentSVC            = require('../../utils/content-service');
const RchatSvc              = require('../../utils/RchatSvc');
const d2dChannelSVC         = require('../../utils/d2dchannel-service');

const CompetenceModel       = require('../models/competence.model');
const AttendeeModel         = require('../models/event_attendee.model');
const UserModel             = require('../models/user.model');
const UserSKPModel          = require('../models/user_skp_tmp.model');

const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const {COMPETENCE_TYPES, RedisKey}    = require('../../utils/helpers/enum');                 
const parseResponse         = require('../../utils/helpers/parse-response');
const LOG                   = 'ProfileController';

const ProfileController = {}

ProfileController.getProfileSummary = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getProfileSummary`);
        
        let { uid, authorization} = req.headers;
        let record              = {};
        let cc                  = lANGUAGE_TYPE.Indonesia;
        let message             = LANG[cc].GENERAL.SUCCESS;
        let key                 = RedisKey.profile+uid;

        let successRedisGet = false; // for if redis crash or error get data manually
        // try{
        //     results = await redisService.getValue(key);
        //     if(_.isEmpty(results) == false){
        //         successRedisGet = true;
        //     }
        // }catch(error){
        //     console.log(`|─ ${LOG} : getProfile -> Redis Get Error`, error);
        //     successRedisGet = false;
        // }
        
        if(successRedisGet == false){
            let userField           = ['name','email','npa_idi','born_date','education_1 as education','photo'];
            results                 = await UserModel.getAll(userField,[{ jointer: 'AND', key: `uid = '${uid}'`}],[],[],[],null,null)
            if (!_.isEmpty(results)) {
                record  = results[0]
                
                //if country code null give default country indo
                record.country_code = (_.isEmpty(record.country_code) || _.toLower(record.country_code) == 'null' ) ? COUNTRY_CODES.Indonesia : record.country_code;

                record.born_date    = _.toLower(moment(new Date(record.born_date)).format('YYYY-MM-DD'));
                record.born_date    = record.born_date == "invalid date" || record.born_date == 'null' ? "" : record.born_date;
                record.photo        = _.isEmpty(record.photo) == false ? `https://static.d2d.co.id/image/profile/${record.photo}` : '';

                cc          = record.country_code == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English;
                message     = LANG[cc].GENERAL.SUCCESS;
                
                //get total skp point
                let totalSkp = UserSKPModel.getTotalSkp(uid);
                record.skp_point = _.isEmpty(totalSkp) == false ? totalSkp.skp : 0;

                // if redis empty, save it to redis
                // try{
                //     let jsonValue = JSON.stringify(record);
                //     await redisService.insert(key,jsonValue);
                // }catch(error){
                //     console.log(`|─ ${LOG} : getProfile -> Redis Save Error`, error);
                // }

            } else {
                message = LANG[cc].GENERAL.USER_NOT_FOUND;
            }
        }else {
            record.name         = results.name;
            record.email        = results.email;
            record.npa_idi      = results.npa_idi;
            record.born_date    = results.born_date;
            record.education    = results.education;
            record.photo        = results.photo;
            record.skp_point    = results.skp_point;
        }
        
        console.log(`└─ ${LOG} : getProfileSummary -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : getProfile -> Error`, error);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

ProfileController.getProfileComplete = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : getProfile`);
        
        let { uid }             = req.headers;
        let record              = {};
        let cc                  = lANGUAGE_TYPE.Indonesia;
        let message             = LANG[cc].GENERAL.SUCCESS;
        let key                 = RedisKey.profile+uid;

        let successRedisGet = false; // for if redis crash or error get data manually
        // try{
        //     results = await redisService.getValue(key);
        //     if(_.isEmpty(results) == false){
        //         successRedisGet = true;
        //         record = results;
        //     }
        // }catch(error){
        //     console.log(`|─ ${LOG} : getProfile -> Redis Get Error`, error);
        //     successRedisGet = false;
        // }
        
        if(successRedisGet == false){
            let userField  = ['*'];
            results        = await UserModel.getAll(userField,[{ jointer: 'AND', key: `uid = '${uid}'`}])
            if (!_.isEmpty(results)) {
                record  = results[0];
                
                //if country code null give default country indo
                record.country_code = (_.isEmpty(record.country_code) || record.country_code == 'null' ) ? COUNTRY_CODES.Indonesia : record.country_code;

                record.pns          = record.pns == "true" ? true : record.pns == "false" ? false : null; 
                record.born_date    = _.toLower(moment(new Date(record.born_date)).format('YYYY-MM-DD'));
                record.born_date    = record.born_date == "invalid date" || record.born_date == 'null' ? "" : record.born_date;

                //get total skp point
                let totalSkp     = await UserSKPModel.getTotalSkp(uid);
                record.skp_point = _.isEmpty(totalSkp) == false ? totalSkp[0].skp : 0;

                record.photo     = !_.isEmpty(record.photo) ? `https://static.d2d.co.id/image/profile/${record.photo}` : ''

                cc          = record.country_code == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English;
                message     = LANG[cc].GENERAL.SUCCESS;
                
                //get non-specialiszation by uid
                let resNonSpecialist        = await CompetenceModel.getSpecializationByType(uid, COMPETENCE_TYPES.NONSPESIALIZATION);
                record.nonspesialization    = resNonSpecialist;
                //get specialization by uid
                let resCompetence           = await CompetenceModel.getSpecializationByType(uid, COMPETENCE_TYPES.SPESIALIZATION)
                
                let resultArr               = [];
                if (!_.isEmpty(resCompetence)) {
                    await utils.asyncForEach(resCompetence, async (row) => {
                        //get subspecialization per row
                        let subSpecialistList   = [];
                        subSpecialistList       = await CompetenceModel.getSubSpecialization(row.id,uid);
                        row.subspecialist       = subSpecialistList;
                        resultArr.push(row)
                    })
                }
                record.spesialization   = resultArr;

                //get attendee
                let resAttendee     = await AttendeeModel.getAttendeeByUser(uid);
                let stringAttendee  = [];
                let arrAttendee     = [];
                let resWebinar      = [];
                if (!_.isEmpty(resAttendee.event_attendee)) {    
                    stringAttendee  = resAttendee.event_attendee;
                    arrAttendee     = stringAttendee.split(',');
                    resWebinar      = await ContentSVC.webinar.getEvent(uid, { stringAttendee });

                    if (!_.isEmpty(resWebinar.webinar)) {
                        resWebinar  = resWebinar.webinar.split(',');
                    } else {
                        resWebinar  = []
                    }
                }
                record.webinar          = resWebinar
                record.event_attendee   = arrAttendee

                //get subscription / interest
                record.subscription    = await CompetenceModel.getSpecializationByType(uid, COMPETENCE_TYPES.SUBSCRIPTION);

                // if redis empty, save it to redis
                // try{
                //     let jsonValue = JSON.stringify(record);
                //     await redisService.insert(key,jsonValue,30);
                // }catch(error){
                //     console.log(`|─ ${LOG} : getProfile -> Redis Save Error`, error);
                // }
            } else {
                message = LANG[cc].GENERAL.USER_NOT_FOUND;
            }
        }
        
        console.log(`└─ ${LOG} : getProfile -> Success`);
        parseResponse(res, 200, record, message);
    } catch (error) {
        console.log(`└─ ${LOG} : getProfile -> Error`, error);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

ProfileController.updateProfile = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : updateProfile`);

        let { uid , authorization}     = req.headers;
        let {
            // profile data
            name, npa_idi, email, phone, home_location, country_code, pns, born_date,
            education_1, education_2, education_3, 
            clinic_location_1, clinic_location_2, clinic_location_3,
            clinic_type_1,clinic_type_2,clinic_type_3,
            // for device info
            app_version, device_unique_id, fcm_token, device_platform, system_version, os_build_version, device_brand
        } = req.body;
        let key         = RedisKey.profile+uid;

        // check if invalid born date
        born_date       = _.toLower(born_date);
        born_date       = born_date == "invalid date" || born_date == 'null' || _.isEmpty(born_date) ? null : born_date;
        born_date       = moment(new Date(born_date)).format('YYYY-MM-DD');
        name            = name.replace("’", "'");
        country_code    = _.isUndefined(country_code) || _.isEmpty(country_code) ? lANGUAGE_TYPE.Indonesia : country_code;

        let record      = {};
        let cc          = country_code == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English;
        let message     = LANG[cc].PROFILE.UPDATE_SUCCESS;

        // get user data
        let userCond    = [{ jointer: 'AND', key: 'uid', value: uid, op: '=' }]
        let user        = await UserModel.getBy(['*'],userCond, [], [], 1, []);
        
        // update specialist into new country (if change country)
        if(country_code != user.country_code){
            
            //get specialization by uid
            let competenceList   = await CompetenceModel.getAllDynamic(
                ['GROUP_CONCAT(user_competence.competence_id) as competenceList'],
                [{ jointer: 'AND', key: 'uid', value: uid, op: '=' }]
                );

            if (_.isEmpty(competenceList) == false) {
                
                let userCompetence   = await CompetenceModel.getAllDynamic(
                    ['user_competence.id as pk_id','user_competence.competence_id as id','user_competence.parent_id', 'user_competence.type'],
                    [{ jointer: 'AND', key: 'uid', value: uid, op: '=' }]
                );

                if(_.isEmpty(userCompetence) == false){

                    let payload = {
                        competence_list : `[${competenceList[0].competenceList}]`
                    };
                    
                    let competenceDetailList = await ContentSVC.specialist.getDetailList(uid,payload,country_code);
                    competenceDetailList = competenceDetailList.docs;

                    await utils.asyncForEach(userCompetence, async (row) => {
                        let type = null;
                        let parentId = null;
                        let currentCompetence = null;
                        if(row.type == COMPETENCE_TYPES.SPESIALIZATION){
                            currentCompetence = competenceDetailList.filter(function (cdl) { 
                                return cdl.id == row.id && cdl.show_specialist == 'Y' 
                            })[0];
                            if(row.parent_id != null){
                                parentId = row.parent_id;
                                type = COMPETENCE_TYPES.SUBSPESIALIZATION;
                            }
                            else{
                                type = COMPETENCE_TYPES.SPESIALIZATION;
                            }
                        }
                        else if(row.type == COMPETENCE_TYPES.SUBSCRIPTION){
                            currentCompetence = competenceDetailList.filter(function (cdl) { 
                                return cdl.id == row.id && cdl.show_interest == 'Y' 
                            })[0];
                            type = COMPETENCE_TYPES.SUBSCRIPTION;
                        }else if(row.type == COMPETENCE_TYPES.NONSPESIALIZATION){
                            currentCompetence = competenceDetailList.filter(function (cdl) { 
                                return cdl.id == row.id && cdl.show_non_specialist == 'Y' 
                            })[0];
                            type = COMPETENCE_TYPES.NONSPESIALIZATION;
                        }

                        if(_.isEmpty(currentCompetence)== false){
                            let specialistData = [
                                { key: 'uid', value: uid }, 
                                { key: 'competence_id', value: currentCompetence.id }, 
                                { key: 'competence_name', value: currentCompetence.title }, 
                                { key: 'competence_description', value: currentCompetence.description }, 
                                { key: 'parent_id', value: parentId }, 
                                { key: 'type', value: type }, 
                            ];

                            let saveSpecialist = await CompetenceModel.save(specialistData,
                                [
                                    { jointer: 'AND', key: 'id', value: `${row.pk_id}`, op: '=' },
                                    { jointer: 'AND', key: 'uid', value: uid, op: '=' },
                                ]
                            )
                        }
                    });
                }
            }

        }

        let emptyNpa    = (npa_idi == null || npa_idi == 'null' || _.isEmpty(npa_idi));
        let onlyNumber  = false;
        //validate that npa idi only number
        if (emptyNpa == false) {
            onlyNumber  = npa_idi.match(/\d/g).length == npa_idi.length;
        }
        
        // checking medical id
        if (!_.isEmpty(user) && user.npa_idi !== npa_idi && emptyNpa !== true && onlyNumber === true) {
            // checking npa idi is already used
            let existUser = await UserModel.getAll(['*'],[
                { jointer: 'AND', key: 'npa_idi', value: npa_idi, op: '=' },
                { jointer: 'AND', key: 'country_code', value: country_code, op: '=' }
            ])

            if (existUser.length > 0) {
                record  = existUser[0];
                message = LANG[cc].PROFILE.MEDICALID_HAS_BEEN_USED;
                npa_idi = null;
            } else {
                // if region code -> ID (indonesian)
                // checking medical id
                if(country_code == COUNTRY_CODES.Indonesia){
                    let names   = name.split(' ');
                    let doctor  = await ContentSVC.doctor.get(uid, npa_idi, names);
                    if (_.isEmpty(doctor)) {
                        message = LANG[cc].PROFILE.MEDICALID_NOT_FOUND;
                        npa_idi = null;
                    } else {
                        name    = doctor.name;
                    } 
                }
            }
        }

        let data = [
            { key: 'uid', value: uid }, 
            { key: 'name', value: name }, 
            { key: 'email', value: email }, 
            { key: 'phone', value: phone }, 
            { key: 'home_location', value: home_location }, 
            { key: 'country_code', value: country_code }, 
            { key: 'born_date', value: born_date },
            { key: 'education_1', value: education_1 }, 
            { key: 'education_2', value: education_2 }, 
            { key: 'education_3', value: education_3 },
            { key: 'clinic_location_1', value: clinic_location_1 }, 
            { key: 'clinic_location_2', value: clinic_location_2 }, 
            { key: 'clinic_location_3', value: clinic_location_3 },
            { key: 'clinic_type_1', value: clinic_type_1 }, 
            { key: 'clinic_type_2', value: clinic_type_2 }, 
            { key: 'clinic_type_3', value: clinic_type_3 }
        ]
        
        // checking pns is not set
        if (!_.isEmpty(pns) && pns !== 'null') {
            data.push({ key: 'pns', value: pns })
        }
        // checking npa idi not set
        if (!_.isEmpty(npa_idi) && onlyNumber !== false) {
            data.push({ key: 'npa_idi', value: npa_idi })
        } else if (country_code != COUNTRY_CODES.Indonesia) {
            data.push({ key: 'npa_idi', value: null })
        }
        
        // -------------- update profile ----------------------
        let update = await UserModel.save(data, userCond);
        
        // update last profile with new profile data.      
        if (!_.isEmpty(user)) {
            // get user from user service
            let { result: dataUserInfo, acknowledge } = await UserSVC.getUser(uid);

            user.name                = name 
            user.email               = email 
            user.phone               = phone
            user.home_location       = home_location 
            user.country_code        = country_code 
            user.born_date           = born_date 
            user.education_1         = education_1 
            user.education_2         = education_2 
            user.education_3         = education_3
            user.clinic_location_1   = clinic_location_1 
            user.clinic_location_2   = clinic_location_2 
            user.clinic_location_3   = clinic_location_3 
            user.clinic_type_1       = clinic_type_1 
            user.clinic_type_2       = clinic_type_2 
            user.clinic_type_3       = clinic_type_3 

            if (!_.isEmpty(npa_idi) && onlyNumber !== false) {
                user.npa_idi = npa_idi;
            }

            if (!_.isEmpty(pns) && pns !== 'null') {
                user.pns = pns == 'true' ? true : false;
            }
            
            record  = user;
            //trigger data user update in d2d channel api
            // masih belum dibuat generate token yang sesuai / dipakai buat d2dchannel
            await d2dChannelSVC.members.triggerUpdate(authorization);

            //delete profile data in redis
            // await redisService.delete(key);

            // emptyNpa = (user.npa_idi == null || user.npa_idi == 'null' || _.isEmpty(user.npa_idi));
            // //validate that npa idi only number
            // if (emptyNpa == false) {
            //     onlyNumber  = user.npa_idi.match(/\d/g).length == user.npa_idi.length;
            // }

            if (onlyNumber === false) {
                if (emptyNpa === false) {
                    message = onlyNumber == false ? LANG[cc].PROFILE.MEDICALID_NOT_MATCH : '';
                }
            }

            // --------------- save device info ------------------------
            let clinic          = [];
            let education       = [];

            if (!_.isEmpty(clinic_location_1)) {
                clinic.push(clinic_location_1);
            }
            if (!_.isEmpty(clinic_location_2)) {
                clinic.push(clinic_location_2);
            }
            if (!_.isEmpty(clinic_location_3)) {
                clinic.push(clinic_location_3);
            }

            if (!_.isEmpty(education_1)) {
                education.push(education_1);
            }
            if (!_.isEmpty(education_2)) {
                education.push(education_2);
            }
            if (!_.isEmpty(education_3)) {
                education.push(education_3);
            }

            let resCompetence = await CompetenceModel.getSpecializationByType(uid, 'spesialization');
            let resultArr = []
            if (!_.isEmpty(resCompetence)) {

                await utils.asyncForEach(resCompetence, async (row) => {
                    //get subspecialization per row
                    let subSpecialistList = [];
                    subSpecialistList = await CompetenceModel.getSubSpecialization(row.id,uid);
                    row.subSpecialistList = subSpecialistList;
                    resultArr.push(row)
                })
            }
            let specialist = resultArr

            let userAddress     = {};
            let userOtherInfo   = {};

            if (acknowledge == true && !_.isEmpty(dataUserInfo)) {
                if (!_.isEmpty(dataUserInfo.other_info)) {
                    userOtherInfo   = JSON.parse(dataUserInfo.other_info);
                    userOtherInfo['pns']        = pns ? pns : user.pns;
                    userOtherInfo['npa_idi']    = npa_idi ? npa_idi : user.npa_idi;
                    userOtherInfo['str_no']     = '';
                    userOtherInfo['education']  = education;
                    userOtherInfo['specialist'] = specialist;
                }

                if (!_.isEmpty(dataUserInfo.address)) {
                    userAddress                 = JSON.parse(dataUserInfo.address);
                    userAddress['home']         = home_location;
                    userAddress['clinic']       = clinic;
                }
            }

            let doctorFullName  = user.name;
            let userFirstname   = (user.name).split(' ').slice(0, -1).join(' ');
            let userLastname    = (user.name).split(' ').slice(-1).join(' ');
                userFirstname   = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
                userLastname    = (userFirstname ===  userLastname) ? '' : userLastname;
            
            let userDevice      = {
                uid             : user.uid,
                // nik              : '',
                firstname       : userFirstname,
                lastname        : userLastname,
                display_name    : doctorFullName,
                email           : user.email,
                phone_number    : user.phone,
                photo           : !_.isEmpty(user.photo) ? `https://static.d2d.co.id/image/profile/${user.photo}` : '',
                birthday        : born_date,
                // gender           : 'M',
                // religion         : 'Islam',
                address         : JSON.stringify(userAddress),
                other_info      : JSON.stringify(userOtherInfo), 
                type            : 'doctor',
                //---------------------------------------------- for device -----------------------------------------------
                app_version     : app_version,
                device_unique_id: device_unique_id,
                fcm_token       : fcm_token,
                device_platform : device_platform,
                system_version  : system_version,
                os_build_version: os_build_version,
                device_brand    : device_brand
            }
            
            let userInfo = await UserSVC.saveUser(user.uid, userDevice);

            // Update Chat services
            // await RchatSvc.updateName(req);
        }
        
        console.log(`└─ ${LOG} : updateProfile -> Success`);
        message = _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : message
        record.npa_idi  = _.isEmpty(record.npa_idi) ? '' : record.npa_idi;
        parseResponse(res, 200, record, message);

    } catch (error) {
        console.log(`└─ ${LOG} : updateProfile -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

ProfileController.picture = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : picture`);

        let { uid, country_code, authorization}     = req.headers;
        let { name, medical_id }    = req.body;
        let record      = [];
        country_code    = _.isUndefined(country_code) || _.isEmpty(country_code) ? lANGUAGE_TYPE.Indonesia : country_code;
        let cc          = country_code == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English;

        let key         = RedisKey.profile+uid;
        let result      = [];
        let data        = [];

        console.log(`├─ ${LOG} : request picture : file`);
        console.log(req.file);

        if(_.isEmpty(req.file) == false){
            
            let type = `image`;
            let content = `profile`;

            const client = new oss({
                region: 'oss-ap-southeast-5',
                accessKeyId: 'LTAINk4kOx5TLJT6',
                accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
                bucket: 'd2doss'
            });

            let arr = req.file.originalname.split('.');
            let ext = arr[(arr.length-1)];
            
            result = await client.put(`${type}/${content}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);

            fs.unlinkSync(`../../fileupload/${req.file.filename}`)

            data.push({ key: 'photo', value: `${req.file.filename}.${ext}` });
        }

        data.push(
            { key: 'name', value: name },
            { key: 'npa_idi', value: medical_id }
        );
        let condition = [
            { jointer: 'AND', key: 'uid', value: uid, op: '=' }
        ]
        
        let resSave = await UserModel.save(data, condition)
        // console.log(`├─ ${LOG} : picture -> Saving New Picture Success`, resSave);

        //trigger data user update in d2d channel api
        // masih belum dibuat generate token yang sesuai / dipakai buat d2dchannel
        await d2dChannelSVC.members.triggerUpdate(authorization);

        //delete profile data in redis
        // await redisService.delete(key);

        record = {
            picture: result,
            name: name,
            medical_id: medical_id,
        };

        console.log(`└─ ${LOG} : picture -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? LANG[cc].GENERAL.DATA_NOT_FOUND : LANG[cc].PROFILE.UPDATE_SUCCESS);
    } catch (error) {
        console.log(`└─ ${LOG} : picture -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = ProfileController;
