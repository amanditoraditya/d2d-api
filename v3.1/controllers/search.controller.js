const _                     = require('lodash');
const  moment               = require('moment');
const datetime              = require('node-datetime');

const ContentSVC            = require('../../utils/content-service');
const redisService          = require('../../utils/helpers/RedisService');
const GUESehatSVC           = require('../../utils/guesehat-service');
const parseResponse         = require('../../utils/helpers/parse-response');
const {ENV, RedisKey}       = require('../../utils/helpers/enum')

const settingsModel         = require('../models/setting.model');
const CmeModel              = require('../models/cme.model');
const EventAttendeeModel    = require('../models/event_attendee.model');

const LOG                   = 'SearchController';

const filterTypeSetting = 'search_Filter_types';
let searchRedisKey = RedisKey.event_atendee;
const SearchController = {}

SearchController.search = async (req, res, next) => {
    try {
        console.log(`┌─ ${LOG} : get`);

        let { keyword, page, limit, upcomingWebinar, liveWebinar, webinar, event, journal, guideline, cme, job, obatatoz } = req.query;
        let { uid, countrycode, authorization } = req.headers

        let filter          = [];
        let record          = {
            page: page,
            limit: limit,
            result : {}
        };
        let contentRecord = {
            result: {
                live_webinar:[],
                upcoming_webinar:[],
                webinar:[],
                event:[],
                journal:[],
                guideline:[],
            }
        };
        let searchContent   = false;
        let isSearchWebinar = false;
        searchRedisKey = searchRedisKey + uid;

        //defult apps key d2d
        if(!_.isEmpty(keyword)){
            filter.push({ key: 'keyword', value: keyword });
        }
        if(!_.isEmpty(upcomingWebinar)){
            filter.push({ key: 'searchUpcomingWebinar', value: upcomingWebinar });
            searchContent = true;
            isSearchWebinar = true;
        }
        if(!_.isEmpty(liveWebinar)){
            filter.push({ key: 'searchLiveWebinar', value: liveWebinar });
            searchContent = true;
            isSearchWebinar = true;
        }
        if(!_.isEmpty(webinar)){
            filter.push({ key: 'searchWebinar', value: webinar });
            searchContent = true;
            isSearchWebinar = true;
        }
        if(!_.isEmpty(event)){
            filter.push({ key: 'searchEvent', value: event });
            searchContent = true;
        }
        if(!_.isEmpty(journal)){
            filter.push({ key: 'searchJournal', value: journal });
            searchContent = true;
        }
        if(!_.isEmpty(guideline)){
            filter.push({ key: 'searchGuideline', value: guideline });
            searchContent = true;
        }

        //get all webinar exlusive list and save it to redis
        if(isSearchWebinar == true){
            let atendeeList = await EventAttendeeModel.getAttendeeByUser(uid);
            // console.log("atendeeList",atendeeList)
            if(_.isEmpty(atendeeList.event_attendee) == false){
                atendeeList = atendeeList.event_attendee.split(',');
            }else{
                atendeeList = [];
            }
            // console.log("atendeeList",atendeeList)
            let successRedisGet = false;

            try{
                let results = await redisService.getValue(searchRedisKey);
                if(_.isEmpty(results) == false){
                    successRedisGet = true;
                }
            }catch(error){
                console.log(`|─ ${LOG} : search -> Redis Get Error`, error);
                successRedisGet = false;
            }

            if(successRedisGet == false){
                // if redis empty, save it to redis
                try{
                    let jsonValue = JSON.stringify(atendeeList);
                    await redisService.insert(searchRedisKey,jsonValue,60);
                }catch(error){
                    console.log(`|─ ${LOG} : search -> Redis Save Error`, error);
                }
            }
            
        }
        
        if(searchContent == true){
            contentRecord = await ContentSVC.search.getAll(uid,filter,page,limit,countrycode);
        }

        if(cme == 1){
            let path = ENV;
            let condition=[
                { key:`(DATE_FORMAT(NOW(), '%Y-%m-%d') >= quiz.available_start AND DATE_FORMAT(NOW(), '%Y-%m-%d') <= quiz.available_end)` }
            ];

            if(!_.isEmpty(keyword)){
                condition.push({ jointer: 'AND', key: `quiz.title like '%${keyword}%'`});
            }

            let cmeResult = await CmeModel.getAll(uid, condition, page, limit, path);
            contentRecord.result.cme = cmeResult;
        }else{
            contentRecord.result.cme = [];
        }
        
        if(job == 1){
            contentRecord.result.job = [];
        }else{
            contentRecord.result.job = [];
        }

        if(obatatoz == 1){
            let obatatozResult        = await GUESehatSVC.obatAtoZ.getAll(uid, authorization, filter, page, limit)
        
            contentRecord.result.obatatoz  = !_.isEmpty(obatatozResult) ? obatatozResult  : []
        }else{
            contentRecord.result.obatatoz = [];
        }

        let result = {
            live_webinar        : contentRecord.result.live_webinar,
            upcoming_webinar    : contentRecord.result.upcoming_webinar,
            webinar             : contentRecord.result.webinar,
            cme                 : contentRecord.result.cme,
            journal             : contentRecord.result.journal,
            guideline           : contentRecord.result.guideline,
            event               : contentRecord.result.event,
            obatatoz            : contentRecord.result.obatatoz,
            job                 : contentRecord.result.job,
        }
        record.result = result;

        let filterSetting = await settingsModel.getAll(['content'],[{ jointer: 'AND', key: `code = '${filterTypeSetting}'`}]);
    
        record.filter    = !_.isEmpty(filterSetting[0].content) ? JSON.parse(filterSetting[0].content) : [];

        //add server date
        let dt              = datetime.create();
        let formatted       = dt.format('Y-m-d H:M:S');
        record.server_date  = formatted;

        console.log(`└─ ${LOG} : get -> Success`);
        parseResponse(res, 200, record, _.isEmpty(record) ? 'No data found!' : 'Success');
    } catch (error) {
        console.log(`└─ ${LOG} : get -> Error`);
        let err         = new Error(error.message);
            err.code    = 500;
        next(err);
    }
}

module.exports = SearchController;