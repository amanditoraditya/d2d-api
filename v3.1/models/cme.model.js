const _         = require('lodash');
const moment    = require('moment');
const CoreDB    = require('../../utils/CoreDB');
const CmeModel = {}

CmeModel.getOffline = async (uid, conditions = [], _page = null, _limit = null, path = '') => { 

    let offset      = (_.isEmpty(_page) || _page == 1) ? 0 : (_page - 1) * _limit;
    let limit       = _limit !== null && _page > 0 ? `LIMIT ${offset}, ${_limit}` : '';
    let condition   = [];
    let values      = [uid];
    let valuesTmp   = [uid];

    limit = _limit == 1 ? `LIMIT 1` : limit;

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);
                values.push(record.value);
                valuesTmp.push(record.value);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }
    
    condition       = condition.join(' ');

    query = `SELECT
            user_skp_tmp.uid, user_skp_tmp.skp, user_skp_tmp.title, user_skp_tmp.source, user_skp_tmp.taken_on,
            CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
            user_skp_tmp.status
            FROM user_skp_tmp
            WHERE 1 
            AND quiz_id IS NULL AND title IS NOT NULL AND uid = ?
                    
            UNION
    
            SELECT
            user_skp.uid, user_skp.skp, user_skp.title, user_skp.source, user_skp.taken_on,
            CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp.certificate) as certificate,
            user_skp.status
            FROM user_skp
            WHERE 1 
            AND quiz_id IS NULL AND title IS NOT NULL AND uid = ?

            ORDER BY taken_on DESC
            ${limit}`;

    return await CoreDB.query(query, _.concat(values, valuesTmp));
}

CmeModel.getOfflineTotal = async (uid, conditions = []) => { 

    let condition   = []
    let values      = [uid];
    let valuesTmp   = [uid];

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);
                values.push(record.value);
                valuesTmp.push(record.value);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }
    
    condition       = condition.join(' ');

    query = `SELECT
            user_skp_tmp.id
            FROM user_skp_tmp
            WHERE 1 
            AND quiz_id IS NULL AND title IS NOT NULL AND uid = ?
                    
            UNION
    
            SELECT
            user_skp.id
            FROM user_skp
            WHERE 1 
            AND quiz_id IS NULL AND title IS NOT NULL AND uid = ?
            `;
    
    let total = await CoreDB.query(query, _.concat(values, valuesTmp));
    total = _.isEmpty(total) ? 0 : total.length;
    
    return total
}

CmeModel.getAll = async (uid, conditions = [], _page = null, _limit = null, path = '') => { 

    let offset      = (_.isEmpty(_page) || _page == 1) ? 0 : (_page - 1) * _limit;
    let limit       = _limit !== null && _page > 0 ? `LIMIT ${offset}, ${_limit}` : '';
    let condition   = [];
    let values      = [uid];
    let valuesTmp   = [uid];

    limit = _limit == 1 ? `LIMIT 1` : limit;

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }
    
    condition       = condition.join(' ');

    query = `SELECT
                quiz.id,
                quiz.source_id,
                quiz.title,
                quiz.description,
                CONCAT('https://static.d2d.co.id/${path}pdf/quiz/', quiz.filename) as filename,
                quiz.skp,
                quiz.available_start,
                quiz.available_end,
                source_cme.source,
                source_cme.description as source_description,
                CONCAT('https://static.d2d.co.id/${path}image/logo/', source_cme.logo) as logo,
                cert.certificate,
                cert.status,
                cert.type,
                IF(cert.uid IS NOT NULL, true, false) as is_done
            FROM quiz
            INNER JOIN source_cme ON quiz.source_id = source_cme.id
            LEFT JOIN
                (
                    SELECT
                        CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                        user_skp_tmp.uid,
                        user_skp_tmp.status,
                        user_skp_tmp.skp,
                        user_skp_tmp.type,
                        user_skp_tmp.title,
                        user_skp_tmp.quiz_id
                    FROM user_skp_tmp
                    WHERE 1 
                        AND title IS NULL
                        AND status = 'valid'
                        AND uid = '${uid}'
                                
                    UNION
                    
                    SELECT
                        CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp.certificate) as certificate,
                        user_skp.uid,
                        user_skp.status,
                        user_skp.skp,
                        user_skp.title,
                        user_skp.quiz_id,
                        user_skp.type
                    FROM user_skp
                    WHERE 1 
                        AND title IS NULL
                        AND status = 'valid'
                        AND uid = '${uid}'
                ) as cert ON cert.quiz_id = quiz.id
            WHERE 1 ${condition}
            ORDER BY quiz.available_end, cert.uid IS NOT NULL, quiz.last_modify_date DESC
            ${limit}`;

    return await CoreDB.query(query, _.concat(values, valuesTmp));
}

CmeModel.getAllTotal = async (uid, conditions = [], path = '') => { 

    let condition   = [];

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                condition.push(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`);
            } else {
                condition.push(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }
    
    condition       = condition.join(' ');

    query = `SELECT
                quiz.id
            FROM quiz
            INNER JOIN source_cme ON quiz.source_id = source_cme.id
            LEFT JOIN
                (
                    SELECT
                        user_skp_tmp.id,
                        user_skp_tmp.quiz_id
                    FROM user_skp_tmp
                    WHERE 1 
                        AND title IS NULL
                        AND status = 'valid'
                        AND uid = '${uid}'
                                
                    UNION
                    
                    SELECT
                        user_skp.id,
                        user_skp.quiz_id
                    FROM user_skp
                    WHERE 1 
                        AND title IS NULL
                        AND status = 'valid'
                        AND uid = '${uid}'
                ) as cert ON cert.quiz_id = quiz.id
            WHERE 1 ${condition}
            `;

        let total = await CoreDB.query(query, []);
        total = _.isEmpty(total) ? 0 : total.length;
        
        return total
}

CmeModel.getSkp = async (uid) => { 
    
    let query = `SELECT
                uid,
                SUM(skp) as skp
                FROM
                user_skp_tmp
                WHERE
                uid = '${uid}' AND
                status = 'valid'
                GROUP BY
                uid`;

    return await CoreDB.query(query, []);
}

module.exports = CmeModel;