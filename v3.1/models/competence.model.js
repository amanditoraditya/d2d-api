const _             = require('lodash');
const moment        = require('moment');
const CoreDB        = require('../../utils/CoreDB');
const CompetenceModel  = {}

CompetenceModel.save = async (data, conditions = []) => {
    if (conditions.length > 0) {
        CoreDB.update('user_competence');

        CoreDB.setData(data);
        if (conditions.length > 0) {
            conditions.forEach((record, index) => {
                let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
                let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
                let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
                let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';
    
                if (!_.isEmpty(record.value)) {
                    let op      = !_.isEmpty(record.op) ? record.op : '=';
                    
                    CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
                }
            })
        }
    } else {
        CoreDB.insert('competence')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

CompetenceModel.delete = async (conditions) => {
    CoreDB.delete('user_competence');

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    return await CoreDB.execute();
}

CompetenceModel.getBy = async (conditions = [], join = [], group = [], limit = null) => {
    CoreDB.select('user_competence');
    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setGroup(record);
        })
    }

    CoreDB.setLimit(limit);
    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

CompetenceModel.getAll = async (conditions = [], join = [], group = [], sort = [], page = null, limit = null) => {    
    let fields  = [
        'user_competence.competence_id as id', 
        'user_competence.competence_name as title', 
        'user_competence.competence_description AS description', 
    ];
    
    CoreDB.select('user_competence');
    CoreDB.setFields(fields);

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }
    
    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setGroup(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

CompetenceModel.getAllDynamic = async (fields = ['*'],conditions = [], join = [], group = [], sort = [], page = null, limit = null) => {    
    CoreDB.select('user_competence');
    CoreDB.setFields(fields);

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }
    
    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setGroup(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

CompetenceModel.getCount = async (conditions = [], join = [], group = [], sort = []) => {
    CoreDB.select('user_competence');
    CoreDB.setFields(['COUNT(competence.id) AS total']);
     
    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    let total = await CoreDB.execute();
    total = _.isEmpty(total) ? 0 : total[0].total;

    return total;
}

CompetenceModel.getSpecializationByType = async (uid, type, sort = [], page = 1 , pageSize = 10) => {
    let fields  = [
        'user_competence.competence_id as id', 
        'user_competence.competence_name as title', 
        'user_competence.competence_description AS description', 
    ];
    
    CoreDB.select('user_competence');
    CoreDB.setFields(fields);
    CoreDB.setWhere(`AND uid = '${uid}'` );
    CoreDB.setWhere(`AND type = '${type}'`, );
    CoreDB.setWhere(`AND competence_id IS NOT NULL`);

    let result = await CoreDB.execute();

    return result.length == 1 ? [result[0]] : result;
}

CompetenceModel.getSubSpecialization = async (parentId, uid) => {

    let fields  = [
        'user_competence.competence_id as id', 
        'user_competence.competence_name as title', 
        'user_competence.competence_description AS description', 
    ];
    
    CoreDB.select('user_competence');
    CoreDB.setFields(fields);
    CoreDB.setWhere(`AND uid = '${uid}'` );
    CoreDB.setWhere(`AND parent_id = '${parentId}'` );
    CoreDB.setWhere(`AND type = 'subspesialization'`);

    let result = await CoreDB.execute();

    return result.length == 1 ? [result[0]] : result;
}

module.exports = CompetenceModel;