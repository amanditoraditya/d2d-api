const _         = require('lodash');
const moment    = require('moment');
const CoreDB    = require('../../utils/CoreDB');
const EventAttendeeModel = {}

EventAttendeeModel.save = async (data, condition = []) => {
    if (condition.length > 0) {
        CoreDB.update('event_attendee');

        data.push({ key: 'updatedAt', value: moment().format('YYYY-MM-DD kk:mm:ss') });

        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    } else {
        CoreDB.insert('event_attendee')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

EventAttendeeModel.delete = async (condition) => {
    CoreDB.delete('event_attendee');

    condition.forEach((record, index) => {
        let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

        if (!_.isEmpty(record.value)) {
            let op = !_.isEmpty(record.op) ? record.op : '=';

            CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
        } else {
            CoreDB.setWhere(`${jointer} ${record.key}`);
        }
    })

    return await CoreDB.execute();
}

EventAttendeeModel.getBy = async (condition = [], join = [], group = [], limit = null, sort = []) => {
    CoreDB.select('event_attendee');

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }
    
    CoreDB.setLimit(limit);
    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

EventAttendeeModel.getAll = async (condition = [], join = [],fields = ['*'], group = [], sort = [], page = null, limit = null) => {
    const now = new Date();
    CoreDB.select('event_attendee');
    CoreDB.setFields(fields);
    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';
            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';
                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

EventAttendeeModel.getCount = async (condition = [], join = [], group = [], sort = []) => {
    CoreDB.select('event_attendee');
    CoreDB.setFields(['COUNT(event_attendee.id) AS total']);

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer = !_.isEmpty(record.jointer) ? record.jointer : 'AND';

            if (!_.isEmpty(record.value)) {
                let op = !_.isEmpty(record.op) ? record.op : '=';

                CoreDB.setWhere(`${jointer} ${record.key} ${op} ?`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${record.key}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    let total = await CoreDB.execute();
    total = _.isEmpty(total) ? 0 : total[0].total;

    return total;
}

EventAttendeeModel.getAttendeeByUser = async (uid) => {
    CoreDB.select('event_attendee');

    CoreDB.setFields(['GROUP_CONCAT(DISTINCT event_slug_hash) AS event_attendee']);
    CoreDB.setJoin('LEFT JOIN user ON user.email = event_attendee.email OR user.npa_idi = event_attendee.npa_idi');
    CoreDB.setWhere(`AND user.uid = '${uid}'`);
    CoreDB.setWhere(`AND event_attendee.type = 'materi'`);

    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

EventAttendeeModel.getAttendeeArrayByUser = async (uid) => {
    CoreDB.select('event_attendee');

    CoreDB.setFields(['DISTINCT event_attendee.event_slug_hash']);
    CoreDB.setJoin('LEFT JOIN user ON user.email = event_attendee.email');
    CoreDB.setWhere(`AND user.uid = '${uid}'`);
    CoreDB.setWhere(`AND event_attendee.type = 'materi'`);

    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

module.exports = EventAttendeeModel;