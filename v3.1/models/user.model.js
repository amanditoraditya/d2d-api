const _             = require('lodash');
const moment        = require('moment');
const CoreDB        = require('../../utils/CoreDB');
const UserModel  = {}

UserModel.save = async (data, condition = []) => {
    if (condition.length > 0) {
        CoreDB.update('user');

        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    } else {
        CoreDB.insert('user')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

UserModel.delete = async (condition) => {
    CoreDB.delete('user');
    
    condition.forEach((record, index) => {
        let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
        let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
        let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
        let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

        if (!_.isEmpty(record.value)) {
            let op      = !_.isEmpty(record.op) ? record.op : '=';
            
            CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
        } else {
            CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
        }
    })

    return await CoreDB.execute();
}

UserModel.getBy = async (fields = [], condition = [], join = [], group = [], limit = null, sort = []) => {
    CoreDB.select('user');

    if(fields.length > 0){
        CoreDB.setFields(fields);
    }

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }
    
    CoreDB.setLimit(limit);
    let result =  await CoreDB.execute();
    
    return result.length == 1 ? result[0] : result;
}

UserModel.getAll = async (fields = [], condition = [], join = [], group = [], sort = [], page = null, limit = null) => {
    CoreDB.select('user');

    if(fields.length > 0){
        CoreDB.setFields(fields);
    }

    if (condition.length > 0) {
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    if (!_.isEmpty(page) || !_.isEmpty(limit)) {
        CoreDB.setPage(page);
        CoreDB.setLimit(limit);
    }

    return await CoreDB.execute();
}

module.exports  = UserModel;