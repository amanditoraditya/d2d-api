const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { SearchController } = require('../controllers');
const { header, body }      = require('express-validator/check')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/',  SearchController.search)

module.exports = router;