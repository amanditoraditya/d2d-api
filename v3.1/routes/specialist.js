const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { SpecialistController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', SpecialistController.getPaging)
    .get('/:id', SpecialistController.get)
    .delete('/type/:type', SpecialistController.deleteAllByType)

module.exports = router;