const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { BannerController } = require('../controllers');
const { header, body }      = require('express-validator/check')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ]
};

router
    .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .get('/', BannerController.getAll)
    .get('/:name', BannerController.get)
    // .post('/', BannerController.save)

module.exports = router;