const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { CmeController }     = require('../controllers')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .all('*', AuthMiddleware.authentication)
    .post('/submit-quiz', CmeController.submitQuiz)

module.exports = router;