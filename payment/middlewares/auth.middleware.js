const _                 = require('lodash');
const jwt               = require('jsonwebtoken');
const admin             = require('firebase-admin');
const utils             = require('../../utils/utils');
const { validationResult } = require('express-validator/check');
const parseValidator    = require('../../utils/helpers/parse-validator');
const AuthMiddleware    = {}



AuthMiddleware.validation = (req, res, next) => {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
        console.log(`── validation:error`);

        let validator = parseValidator(errors.array());
        let message = typeof validator.messages == 'string' ? validator.messages : 'Something went wrong';
        let result = typeof validator.result != 'undefined' ? validator.result : 'Something went wrong';

        res.status(200).send({
            message: message,
            acknowledge: false,
            result: {},
            errors: {
                validation: result
            }
        });
    }
    else {
        next();
    }
}

AuthMiddleware.authentication = async (req, res, next) => {
    try {
        console.log(`┌─ url            : ${req.originalUrl}`);
        let { authorization } = req.headers;

        // // for debug
        // req.headers.uid      = '9KNqx8FG85bh1O7jfg9hvoWDemc2';
        // req.headers.email    = 'rahmat1929@gmail.com';
        // next();

        let access = false
        let decode = jwt.decode(authorization);
        if (!_.isUndefined(decode.user_id)) {
            req.headers.uid     = decode.user_id;
            req.headers.email   = decode.email;

            let token   = await admin.auth().verifyIdToken(authorization);
                access  = token ? true : false;
        } else {
            let uid     = !_.isUndefined(decode.user_id) ? decode.user_id : decode.uid;
            let user    = await utils.getUser(uid);
            req.headers.uid     = uid;
            req.headers.email   = user[0].email;

            access = (user.length > 0) ? true : false;
        }

        if (access === true) {
            next();
        }
    } catch (error) {
        let err = new Error(error.message);
        err.code = 403

        next(err);
    }
}

module.exports = AuthMiddleware;