const _             = require('lodash');
const moment        = require('moment');
const CoreDB        = require('../../utils/CoreDB');
const paymentModel  = {}

paymentModel.getBy = async (conditions = [], join = [], group = [], page = null,  limit = null,  sort = []) => {
    CoreDB.select('transaction');
    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setGroup(record);
        })
    }
    if (sort.length > 0) {
        sort.forEach((record, index) => {
            CoreDB.setOrder(record);
        })
    }

    CoreDB.setPage(page);
    CoreDB.setLimit(limit);

    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

paymentModel.save = async (data, condition = []) => {

    if (condition.length > 0) {
        CoreDB.update('transaction');
        CoreDB.setData(data);
        condition.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    } else {
        CoreDB.insert('transaction')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

module.exports = paymentModel;