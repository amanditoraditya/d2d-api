const _             = require('lodash');
const moment        = require('moment');
const CoreDB        = require('../../utils/CoreDB');
const paymentNotificationModel  = {}

paymentNotificationModel.getBy = async (conditions = [], join = [], group = [], limit = null) => {
    CoreDB.select('transaction_notification');
    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    if (join.length > 0) {
        join.forEach((record, index) => {
            CoreDB.setJoin(record);
        })
    }

    if (group.length > 0) {
        group.forEach((record, index) => {
            CoreDB.setGroup(record);
        })
    }

    CoreDB.setLimit(limit);
    let result = await CoreDB.execute();

    return result.length == 1 ? result[0] : result;
}

paymentNotificationModel.delete = async (conditions) => {
    CoreDB.delete('transaction_notification');

    if (conditions.length > 0) {
        conditions.forEach((record, index) => {
            let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
            let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
            let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
            let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';

            if (!_.isEmpty(record.value)) {
                let op      = !_.isEmpty(record.op) ? record.op : '=';
                
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
            } else {
                CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
            }
        })
    }

    return await CoreDB.execute();
}

paymentNotificationModel.save = async (data, conditions = []) => {
    if (conditions.length > 0) {
        CoreDB.update('transaction_notification');

        CoreDB.setData(data);
        if (conditions.length > 0) {
            conditions.forEach((record, index) => {
                let jointer     = !_.isEmpty(record.jointer) ? record.jointer :'AND';
                let grouping    = !_.isEmpty(record.grouping) ? record.grouping :'';
                let groupStart  = grouping.toLowerCase() === 'start' ? '(' : '';
                let groupEnd    = grouping.toLowerCase() === 'end' ? ')' : '';
    
                if (!_.isEmpty(record.value)) {
                    let op      = !_.isEmpty(record.op) ? record.op : '=';
                    
                    CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${op} ? ${groupEnd}`, record.value);
                } else {
                    CoreDB.setWhere(`${jointer} ${groupStart} ${record.key} ${groupEnd}`);
                }
            })
        }
    } else {
        CoreDB.insert('transaction_notification')
        CoreDB.setData(data);
    }

    return await CoreDB.execute();
}

module.exports = paymentNotificationModel;
