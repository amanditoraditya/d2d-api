const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares');
const { PaymentController } = require('../controllers')

const Validation = {
    all: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    start: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('id')
            .isNumeric().withMessage('Invalid format ?, must be numeric')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('type')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
    order_id: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('order_id')
            .not().isEmpty().withMessage('Invalid param ?')
    ],
    refund: [
        header('authorization')
            .not().isEmpty().withMessage('Invalid param ?'), 
        body('refund_key')
            .not().isEmpty().withMessage('Invalid param ?'),
        body('amount')
            .not().isEmpty().withMessage('Invalid param ?')
            .custom((value, {req, loc, path}) => {
                if (Number(value) > 0) {
                    return value;
                } else {
                    throw new Error("Invalid format ?, must be larger than 0");
                }
            }),
        body('reason')
            .not().isEmpty().withMessage('Invalid param ?'),
    ],
};

router
    // .all('*', Validation.all, AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/start', AuthMiddleware.authentication, Validation.start, AuthMiddleware.validation, PaymentController.start)
    .post('/approve', AuthMiddleware.authentication, Validation.order_id, AuthMiddleware.validation, PaymentController.approve)
    .post('/deny', AuthMiddleware.authentication, Validation.order_id, AuthMiddleware.validation, PaymentController.deny)
    .post('/cancel', AuthMiddleware.authentication, Validation.order_id, AuthMiddleware.validation, PaymentController.cancel)
    .post('/refund', AuthMiddleware.authentication, Validation.refund, AuthMiddleware.validation, PaymentController.refund)
    .get('/finish', PaymentController.finish)
    .get('/unfinish', PaymentController.unfinish)
    .get('/error', PaymentController.error)

module.exports = router;