const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares');
const { NotificationController } = require('../controllers')

const Validation = {
    all: []
};

router
    // .all('*', AuthMiddleware.validation, AuthMiddleware.authentication)
    .post('/handling', NotificationController.handling)

module.exports = router;