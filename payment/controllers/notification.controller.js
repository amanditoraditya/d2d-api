const _                                             = require('lodash');
const moment                                        = require('moment');
const querystring                                   = require("querystring");
const paymentModel                                  = require('../models/payment.model');
const paymentNotificationModel                      = require('../models/paymentNotif.model');
const EventAttendeModel                             = require('../models/event_attendee.model');
const SettingModel                                  = require('../models/setting.model');
const { MidTransService, SNAP, STATUS, PAYMENTS, CONTENT_TYPES }    = require('../../utils/midtrans-service');
const NotifService                                  = require('../../utils/notif-service');
const ContentSVC                                    = require('../../utils/content-service');
const D2DCMSSVC                                     = require('../../utils/d2d-cms-service');
const NotifSVC                                      = require("../../utils/notif-service");
const { useractivity }                              = require('../../utils/content-service');
const parseResponse                                 = require('../../utils/helpers/parse-response');

const NotificationController            = {}
const LOG                               = 'Midtrans:Notification:Controller';

NotificationController.handling = async (req, res, next) => {
    console.log(`|- ${LOG} - start`);
    let trxResponse         = req.body;

    try {
        let status_code         = trxResponse.status_code;
        let status_message      = trxResponse.status_message;

        console.log(`|- ${LOG} - trxResponse :`, JSON.stringify(trxResponse));
        //is allow to check that transaction status code is acceptable
        let isAccept = _.isUndefined(status_code) ? false : (status_code == 200 || status_code == 201  || status_code == 202);
        if(_.isEmpty(trxResponse) == false && isAccept == true) {
            let content         = {};
            let trx             = trxResponse;
            let trx_db          = await paymentModel.getBy([{ jointer: 'AND', key: 'order_code', op: '=', value: trx.order_id }], [], [], null, 1)

            // check transaction on database is exist?
            if (!_.isEmpty(trx_db)) {
                // check latest status
                if (!_.isEmpty(trx_db.status)) {
                    trx = await SNAP.transaction.status(trx.order_id);
                }

                // get content
                if (trx_db.type == CONTENT_TYPES.EVENT) {
                    content         = await ContentSVC.event.get(trx_db.ref_id, trx_db.uid);
                }

                // define variables
                let trx_status          = trx.transaction_status;
                let fraud_status        = trx.fraud_status;
                let status              = '';
                let email               = {
                    title           : '',
                    description     : '',
                    fullname        : (`${trx_db.first_name} ${trx_db.last_name}`).trim(),
                    content         : {
                        title       : content.title,
                        date        : moment(content.start_date).format('YYYY-MM-DD'),
                        link        : content.url.app_link,
                    },
                    order_id        : trx.order_id,
                    transaction_time: moment(trx.transaction_time).format('D MMM YYYY - HH:mm'),
                    gross_amount    : 'Rp'+Number(trx.gross_amount).toLocaleString().replace(/,/g,'.') + ',00',
                    payment_type    : PAYMENTS[trx.payment_type].value,
                    strikethrough   : false,
                    success         : false
                };
                let notification        = {
                    title   : '',
                    body    : '',
                    type    : trx_db.type
                };
                let notification_data   = {
                    'type'          : notification.type,
                    'content_id'    : `${trx_db.ref_id}`,
                    'order_code'    : trx.order_id,
                    'status'        : ''
                };
                let sendNotification    = false;
                let sendEmail           = false;

                // save transaction notification
                let data_trxNotif       = [
                    { key: 'order_id', value: trx.order_id },
                    { key: 'payment_type', value: trx.payment_type },
                    { key: 'transaction_id', value: trx.transaction_id },
                    { key: 'transaction_status', value: trx.transaction_status },
                    { key: 'transaction_time', value: trx.transaction_time },
                    { key: 'fraud_status', value: trx.fraud_status },
                    { key: 'gross_amount', value: trx.gross_amount },
                    { key: 'signature_key', value: trx.signature_key },
                    { key: 'approval_code', value: trx.approval_code },
                    { key: 'response', value: JSON.stringify(trx) }
                ];
                let trxNotif    = await paymentNotificationModel.save(data_trxNotif);

                // check transaction status
                if (trx_status == STATUS.TRANSACTION.CAPTURE) {
                    if (fraud_status == STATUS.FRAUD.CHALLENGE) {
                        // TODO set transaction status on your database to 'challenge'
                        status  =  STATUS.ACCEPT_STATUS.CANCEL;

                        sendEmail           = true;
                        sendNotification    = false;
                    } else if (fraud_status == STATUS.FRAUD.ACCEPT) {
                        // TODO set transaction status on your database to 'success'
                        status  =  STATUS.ACCEPT_STATUS.SUCCESS;

                        sendEmail           = true;
                        sendNotification    = true;
                    }
                } else if (trx_status == STATUS.TRANSACTION.SETTLEMENT) {
                    // TODO set transaction status on your database to 'success'
                    status  =  STATUS.ACCEPT_STATUS.SUCCESS;

                    sendEmail           = true;
                    sendNotification    = true;
                } else if (trx_status == STATUS.TRANSACTION.DENY) {
                    // TODO set transaction status on your database to 'failure'
                    status      = STATUS.ACCEPT_STATUS.FAILURE;                  
                } else if (trx_status == STATUS.TRANSACTION.EXPIRE) {
                    // TODO set transaction status on your database to 'expired'
                    status              = STATUS.ACCEPT_STATUS.EXPIRED; 
                    sendEmail           = true;              
                    sendNotification    = false;   
                } else if (trx_status == STATUS.TRANSACTION.CANCEL) {
                    status              =  STATUS.ACCEPT_STATUS.CANCEL;    
                    sendEmail           = true;
                    sendNotification    = false;  
                } else if (trx_status == STATUS.TRANSACTION.PENDING) {
                    // TODO set transaction status on your database to 'pending' / waiting payment

                    // get payment settings
                    let settingPayments     = await SettingModel.getBy([{ jointer: 'AND', key: 'code', op: '=', value: 'payment_channel' }], [], [], 1);
                    let all_payments        = !_.isEmpty(settingPayments) ? JSON.parse(settingPayments.content) : JSON.parse(`{"payment":"bank_transfer","show":true,"cancel_on_pending":false}`);
                        all_payments        = all_payments.filter((d) => { return d.show == true });
                    let payments            = [];
                    
                    for(let row of all_payments) {
                        if (row.cancel_on_pending == true) {
                            payments.push(row.payment);
                        }
                    }

                    status  = _.includes(payments, trx.payment_type) ? STATUS.ACCEPT_STATUS.CANCEL : STATUS.ACCEPT_STATUS.PENDING;

                    //to ensure that cancel cannot sent notif
                    if(status == STATUS.ACCEPT_STATUS.CANCEL){
                        sendEmail           = false;
                        sendNotification    = false;
                    }else{
                        sendEmail           = true;
                        sendNotification    = true;
                    }
                }

                //update transaction if status or fraud status don't same as trx db
                if(trx_db.status != status){

                    // define transaction data
                    let otherInfo   = MidTransService.getPaymentInfo(trx);
                    let trxData     = [
                        { key: 'payment_type', value: trx.payment_type },
                        { key: 'updated', value: moment().format('YYYY-MM-DD h:mm:ss') },
                        { key: 'other_info', value: otherInfo }, 
                        { key: 'fraud_status', value: trx.fraud_status }, 
                    ];
                    
                    // set status
                    trxData.push({ key: 'status', value: status });

                    // payment success
                    if (status == STATUS.ACCEPT_STATUS.SUCCESS) {
                        if (_.isEmpty(trx_db.status)) {
                            let saveTrx     = await paymentModel.save(trxData, [{jointer: 'AND', key: 'order_code', value: trx.order_id }]);
                        }
                        // create receipt
                        let form            = { uid: trx_db.uid, order_code: trx.order_id }
                        let createReceipt   = await D2DCMSSVC.payment.receipt(trx_db.uid, querystring.stringify(form));
                        
                        if (!_.isEmpty(createReceipt) && createReceipt.status_code == 200) {
                            trxData.push({ key: 'receipt', value: createReceipt.data });
                        }

                        // TODO registered user to event
                        let attendee_cond   = [];
                        let existAttendance = await EventAttendeModel.getBy([
                            { jointer: 'AND', key: 'email', op: '=', value: trx_db.email },
                            { jointer: 'AND', key: 'event_slug_hash', op: '=', value: content.slug_hash },
                            { jointer: 'AND', key: 'type', op: '=', value: 'materi' },
                        ],[],[],1);

                        let attendee = [
                            { key: 'event_slug_hash' , value: content.slug_hash }, 
                            { key: 'event_name' , value: content.title}, 
                            { key: 'name' , value: `${trx_db.first_name} ${trx_db.last_name}` }, 
                            { key: 'email' , value: trx_db.email }, 
                            { key: 'type' , value: 'materi' }, 
                            { key: 'need_verified' , value: content.is_close},
                        ];
                        if (!_.isEmpty(existAttendance)){
                            attendee = [
                                { key: 'event_name' , value: content.title}, 
                                { key: 'name' , value: `${trx_db.first_name} ${trx_db.last_name}` }, 
                                { key: 'email' , value: trx_db.email }, 
                                { key: 'need_verified' , value: content.is_close}, 
                            ];
                            attendee_cond.push({ jointer: 'AND', key: 'id', op: '=', value: `${existAttendance.id}` });
                        }
                        
                        let saveAttendance = await EventAttendeModel.save(attendee, attendee_cond);
                        // TODO join activity user
                        let data = {
                            app         : '1',
                            uid         : trx_db.uid,
                            activity    : 'join',
                            content     : trx_db.type,
                            content_id  : content.id,
                            platform    : 'android',
                        };
                        let saveActivity = await useractivity.save(trx_db.uid, data);
                        
                        
                        // set push notification
                        notification.title  = `Pembayaran Berhasil!`;
                        notification.body   = `Pembayaran Teman Sejawat untuk Event ${content.title} telah berhasil! Klik disini untuk melihat Event Detail lebih lanjut!`;
                        notification_data.status = STATUS.ACCEPT_STATUS.SUCCESS;
                        
                        // set email content
                        email.title             = `Payment Success`;
                        email.description       = `Pembayaran Anda telah berhasil di konfirmasi pada tanggal ${moment(trx.transaction_time).format('D MMM YYYY')}. Selamat bertemu di Event nanti ya!`;
                        email.strikethrough     = false;
                        email.success           = true;
                    } else {
                        let payment_info = [];

                        if (trx.payment_type == 'bank_transfer') {
                            let payment_method = {}, bank_account = {};

                            payment_method.label    = 'Payment Method';
                            bank_account.label      = 'No. Rekening';
                            if (!_.isEmpty(trx.va_numbers)){
                                // bni & bca bentuknya sama
                                payment_method.value    = (trx.va_numbers[0].bank).toUpperCase();
                                bank_account.value      = trx.va_numbers[0].va_number;
                            } else {
                                // permata gajelas beda sendiri
                                payment_method.value    = 'PERMATA';
                                bank_account.value      = trx.permata_va_number;
                            }
        
                            payment_info.push(payment_method);
                            payment_info.push(bank_account);
        
                        } else if (trx.payment_type == 'cstore') {
                            payment_info.push({ label: 'Payment Method', value: `${PAYMENTS[trx.payment_type].value} : ${_.toUpper(trx.store)}` })
                            payment_info.push({ label: 'Payment Code', value: trx.payment_code })
                        } else if (trx.payment_type == 'echannel') {
                            payment_info.push({ label: 'Payment Method', value: `${PAYMENTS[trx.payment_type].value}` })
                            payment_info.push({ label: 'Company Code', value: trx.biller_code })
                            payment_info.push({ label: 'Payment Code', value: trx.bill_key })
                        } else {
                            payment_info.push({ label: 'Payment Method', value: `${PAYMENTS[trx.payment_type].value}` })
                            if (!_.isEmpty(trx.payment_code)) {
                                payment_info.push({ label: 'Payment Code', value: trx.payment_code })
                            }
                        }

                        payment_info.push({ label: 'Price', value: 'Rp'+Number(trx.gross_amount).toLocaleString().replace(/,/g,'.') + ',00' })
                        _.assign(email, { payment_info });

                        // payment pending
                        if (status == STATUS.ACCEPT_STATUS.PENDING) {
                            // set push notification
                            notification.title  = `Menunggu Pembayaran`;
                            notification.body   = `Teman Sejawat, jangan lupa lakukan Pembayaran untuk Event ${content.title} ya! Klik disini untuk melihat cara pembayaran!`;
                            notification_data.status = STATUS.ACCEPT_STATUS.PENDING;

                            // set email contents
                            email.title             = `Waiting for Payment`;
                            email.description       = `Anda telah memilih untuk membayar menggunakan ${PAYMENTS[trx.payment_type].value}. Mohon selesaikan pembayaran sebelum ${moment(trx_db.expired).format('D MMM YYYY LTS')}`;
                            email.strikethrough     = false;
                            email.success           = false;
                        }
                        // payment cancel atau expired
                        else if (status == STATUS.ACCEPT_STATUS.CANCEL || status == STATUS.ACCEPT_STATUS.EXPIRED) {
                            // set email content
                            email.title             = `Payment Canceled`;
                            email.description       = `Pembayaran tagihan Anda melalui ${PAYMENTS[trx.payment_type].value} telah kami batalkan. Anda tidak perlu membayar tagihan ini.`;
                            email.strikethrough     = true;
                            email.success           = false;
                        }
                    }
                    
                    let saveTrx         = await paymentModel.save(trxData, [{jointer: 'AND', key: 'order_code', value: trx.order_id }]);
                    if(sendEmail == true){
                        let sending_email   = await NotifService.sendEmail(trx_db.uid, trx_db.first_name.trim(), trx_db.email, 'PAYMENT', email);
                    }
                    if (sendNotification == true) {
                        let push_notif      = await NotifSVC.addNotification(trx_db.uid, notification.type, notification.title, notification.body, notification_data);
                    }
                } 
            }
        }

        parseResponse(res, status_code, trxResponse, status_message, status_code == 200);
    } catch (error) {
        if (!_.isEmpty(error.ApiResponse)) {
            let { status_code, status_message } = error.ApiResponse;
            let data   = { response: JSON.stringify(trxResponse) };
            let record = paymentNotificationModel.save(data);
            parseResponse(res, status_code, {}, status_message, false);
        } else {
            let err     = new Error(error);
            err.code    = 500;
            console.log(`|- ${LOG} - ERROR : ${err}`);
            next(err);
        }
    }
}

module.exports = NotificationController;

