const _                                 = require('lodash');
const UserModel                         = require('../models/user.model');
const PaymentModel                      = require('../models/payment.model');
const SettingModel                      = require('../models/setting.model');
const utils                             = require('../../utils/utils');
const ENUM                              = require('../../utils/helpers/enum');
const parseResponse                     = require('../../utils/helpers/parse-response');
const ContentSVC                        = require('../../utils/content-service');
const { SNAP, CoreApi, CONTENT_TYPES }  = require('../../utils/midtrans-service');
const moment                            = require('moment');
const { URL } = require('../../utils/helpers/enum');
const PaymentController                 = {}
const LOG                               = 'Midtrans:Payment:Controller';

PaymentController.start = async (req, res, next) => {
    console.log(`|- ${LOG} - start`);
    try {
        let { id, type } = req.body;
        let { uid } = req.headers;

        let message     = 'Failed';
        let acknowledge = false;
        let trx         = {};
        let content     = {};
        // to check that is allow to create transaction or not
        let contentPass = false;
        type            = _.toLower(type);

        //get user data
        let user = await UserModel.getBy([{ jointer: 'AND', key: 'uid', op: '=', value: uid }], [], [], 1);

        //get content 
        if(type == CONTENT_TYPES.EVENT){
            content = await ContentSVC.event.get(id, uid);

            if (_.isEmpty(content) == false) {
                let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                contentPass     = currentTime < content.end_date || true;
            }
            else {
                message     = 'Failed, event has finished';
                contentPass = false;
            }
        }

        if (_.isEmpty(user) == false && contentPass == true){
            let nameArray   = (user.name).split(' ');
            let firstName   = nameArray.slice(0, -1).join(' ');
            let lastName    = nameArray.slice(-1).join(' ');

            let settingExpired      = await SettingModel.getBy([{ jointer: 'AND', key: 'code', op: '=', value: 'payment_expiry_time' }], [], [], 1);
            let settingPayments     = await SettingModel.getBy([{ jointer: 'AND', key: 'code', op: '=', value: 'payment_channel' }], [], [], 1);
            let expired             = !_.isEmpty(settingExpired) ? settingExpired.content : 24;
            let all_payments        = !_.isEmpty(settingPayments) ? JSON.parse(settingPayments.content) : JSON.parse(`{"payment":"bank_transfer","show":true,"cancel_on_pending":false}`);
                all_payments        = all_payments.filter((d) => { return d.show == true });
            let payments            = [];
            
            for(let row of all_payments) {
                if (row.show == true) {
                    payments.push(row.payment);
                }
            }

            let orderID     = `d2d-${type}-${id}-${utils.createRandomString(6)}`;
            //validate is null or string null
            let amount      = content.nominal == null || content.nominal < 1 || content.nominal == '' ? 0 : content.nominal;

            let parameter   = {
                "transaction_details": {
                    "order_id"      : orderID,
                    "gross_amount"  : amount
                }, 
                "credit_card":{
                    "secure"        : true
                },
                "expiry": {
                    "start_time"    : moment().format('YYYY-MM-DD H:mm:ss') + ' +0700',
                    "unit"          : "hour",
                    "duration"      : Number(expired)
                },
                "enabled_payments": payments,
                "gopay": {
                    "enable_callback"   : true,
                    "callback_url"      : ENUM.URL.PAYMENT_FINISH
                },
                "customer_details": {
                    "first_name"    : firstName,
                    "last_name"     : lastName,
                    "email"         : user.email,
                    "phone"         : user.phone
                }
            };
            console.log(parameter)
            //sent pramtere to midtrans to create transaction
            trx         = await SNAP.createTransaction(parameter);

            //add order id 
            _.assign(trx, {order_id: orderID});
            trx.redirect_url = `${trx.redirect_url}`;

            let ref_info = JSON.stringify({
                title: _.isEmpty(content.title) ? "" : content.title,
                start_date: _.isEmpty(content.start_date) ? null : content.start_date,
                end_date: _.isEmpty(content.end_date) ? null : content.end_date,
                type: type
            });

            // check expired on db
            expired     = moment().add(Number(expired),'hours').format('YYYY-MM-DD kk:mm:ss');
            // save transaction
            let data    = [
                { key: 'order_code', value: orderID }, 
                { key: 'ref_id', value: id }, 
                { key: 'ref_info', value: ref_info }, 
                { key: 'uid', value: uid }, 
                { key: 'type', value: type }, 
                { key: 'amount', value: amount }, 
                { key: 'first_name', value: firstName }, 
                { key: 'last_name', value: lastName },
                { key: 'email', value: user.email }, 
                { key: 'phone', value: user.phone },
                { key: 'token', value: trx.token }, 
                { key: 'redirect_url', value: trx.redirect_url }, 
                { key: 'expired', value: expired }, 
            ]
            let saveTrans   = await PaymentModel.save(data);

            message         = 'Success';
            acknowledge     = true;
        }else{
            message = 'Failed, event / user not found';
            acknowledge = false;
        }
        
        parseResponse(res, 200, trx, message, acknowledge);
    } catch (error) {
        if (error.name == 'MidtransError') {
            parseResponse(res, error.httpStatusCode, {}, error.ApiResponse.error_messages[0], false);
        } else {
            let err         = new Error(error);
                err.code    = 500;
        }
    }
}

PaymentController.finish = async (req, res, next) => {
    try {
        let { order_id, status_code, transaction_status } = req.query;
        
        let baseURL = ENUM.URL.PAYMENT_FINISH
        let url   = `${baseURL}?status=finish&order_id=${order_id}&status_code=${status_code}&transaction_status=${transaction_status}`;
        res.redirect(url);
    } catch (error) {
        console.log(error);

        res.redirect(ENUM.URL.PAYMENT_ERROR);
    }
}

PaymentController.unfinish = (req, res, next) => {
    try {
        let { order_id, status_code, transaction_status } = req.query;
        
        let baseURL = ENUM.URL.PAYMENT_UNFINISH
        let url     = `${baseURL}?status=unfinish&order_id=${order_id}&status_code=${status_code}&transaction_status=${transaction_status}`
        
        res.redirect(url);
    } catch (error) {
        console.log(error);

        res.redirect(ENUM.URL.PAYMENT_ERROR);
    }
}

PaymentController.error = async (req, res, next) => {
    try {
        let { order_id, status_code, transaction_status } = req.query;

        let baseURL = ENUM.URL.PAYMENT_ERROR
        let url   = `${baseURL}?status=finish&order_id=${order_id}&status_code=${status_code}&transaction_status=${transaction_status}`
        
        res.redirect(url);
    } catch (error) {
        console.log(error);

        res.redirect(ENUM.URL.PAYMENT_ERROR);
    }
}

PaymentController.approve = async (req, res, next) => {
    try {        
        let { order_id } = req.body;
        // let { uid } = req.headers;

        let message     = 'Failed';
        let acknowledge = false;

        let result      = {}
        let trx         = {}
        let trxDB       = {}

        // check transaction from db
        trxDB = await PaymentModel.getBy([{ jointer: 'AND', key: 'order_code', op: '=', value: order_id }])
        if (!_.isEmpty(trxDB) && !_.isEmpty(trxDB.status)) {
            // check status from midtrans 
            trx = await SNAP.transaction.status(order_id);

            let fraud_status = trx.fraud_status
            
            if (fraud_status == 'challenge') {
                result      = await SNAP.transaction.approve(order_id);
                message    = 'Success'
                acknowledge = true
            } else {
                message = `Failed, fraud_status not 'challenge'`
            }
        } else {
            message = 'Failed, transaction not found'
        }

        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        let err         = new Error(error);
            err.code    = 500;
    }
}

PaymentController.deny = async (req, res, next) => {
    try {        
        let { order_id } = req.body;
        // let { uid } = req.headers;

        let message     = 'Failed';
        let acknowledge = false;

        let result      = {}
        let trx         = {}

        // check transaction from db
        trxDB = await PaymentModel.getBy([{ jointer: 'AND', key: 'order_code', op: '=', value: order_id }])
        if (!_.isEmpty(trxDB) && !_.isEmpty(trxDB.status)) {
            // check status from midtrans 
            trx = await SNAP.transaction.status(order_id);

            let fraud_status = trx.fraud_status
            
            if (fraud_status == 'challenge') {
                result      = await SNAP.transaction.deny(order_id);
                message    = 'Success'
                acknowledge = true
            } else {
                message = `Failed, fraud_status not 'challenge'`
            }
        } else {
            message = 'Failed, transaction not found'
        }

        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        let err         = new Error(error);
            err.code    = 500;
    }
}

PaymentController.cancel = async (req, res, next) => {
    try {        
        let { order_id } = req.body;
        // let { uid } = req.headers;

        let message     = 'Failed';
        let acknowledge = false;

        let result      = {}
        let trx         = {}
        let trxDB       = {}

        
        // check transaction from db
        trxDB = await PaymentModel.getBy([{ jointer: 'AND', key: 'order_code', op: '=', value: order_id }])
        
        if (!_.isEmpty(trxDB) && !_.isEmpty(trxDB.status)) {
            // check status from midtrans 
            trx = await SNAP.transaction.status(order_id);

            // Cancelling transaction can only be done for CARD transaction with CAPTURE / SUCCESS status, before the settlement time (D+1 at 16.00 PM).
            if (!_.isEmpty(trx)) {

                let flagCancel = false
                
                if (trx.payment_type == 'credit_card') {
                    if (trx.transaction_status == 'capture' || trx.transaction_status == 'success') {
                        flagCancel = true
                    } else {
                        message = 'Failed, transaction must be done for card transaction with CAPTURE / SUCCESS status'
                    }
                } else {
                    if (trx.transaction_status == 'pending') {
                        flagCancel = true
                    } else {
                        message = 'Failed, transaction must be a transaction with PENDING status'
                    }
                }

                if (flagCancel) {
                    result      = await SNAP.transaction.cancel(order_id);
                    message     = 'Success'
                    acknowledge = true
                }
            }
        } else {
            message = 'Failed, transaction not found'
        }
        
        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        let err         = new Error(error);
            err.code    = 500;
    }
}

PaymentController.refund = async (req, res, next) => {
    try {        
        let { refund_key, amount, reason } = req.body;
        // let { uid } = req.headers;

        let message     = 'Failed';
        let acknowledge = false;

        let result      = {}

        let parameter = {
            refund_key,
            amount,
            reason
        }

        // refund ke midtrans 
        result              = await SNAP.transaction.refund(parameter);
        message            = 'Success'
        acknowledge         = true

        parseResponse(res, 200, result, message, acknowledge);
    } catch (error) {
        let err         = new Error(error);
            err.code    = 500;
    }
}

module.exports = PaymentController;