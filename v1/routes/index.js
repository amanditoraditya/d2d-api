const fs    = require('fs')
const path  = require('path')

module.exports = (app) => {
    const currDir = process.cwd()

    fs.readdirSync(currDir + '/v1/routes').forEach((file) => {
        let extname     = path.extname(file)
        let basename    = path.basename(file, extname)
        
        if (~file.indexOf('.js') && basename !== 'index') {
            app.use('/v1/' + basename, require(currDir + '/v1/routes/' + file))
        }
    })
}
