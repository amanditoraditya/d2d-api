const parseMessageCode = require('../../utils/helpers/parse-messagecode');
const requestOptions = require('../../utils/request-options');
const responseCustom = require('../../utils/response-custom');
const JournalRequest = require('../../models/JournalRequest');
const request = require('request');

const JournalController        = {}
const log                   = 'Journal controller';

JournalController.request  = async (req, res, next) => {
    console.log(`├── ${log} :: journal`);

    try {
        let uid = req.body.uid;
        let fullname = req.body.fullname;
        let competence_id = req.body.comid;
        let competence_name = req.body.comna;
        let phone = req.body.phone;
        let email = req.body.email;
        let city = req.body.city;
        let journal_title = req.body.journal_title;
        let address = req.body.address;

        JournalRequest.build(
            {uid, fullname, competence_id, competence_name, phone, email, city, address, journal_title}
        ).save().then(data => {
            let idRequest   = data.id
            let options     = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${uid}`)
            request.get(options, function(error, response, body) {
                let result = JSON.parse(body)
                let subjectDoctor = `Full Journal Request`;
                let contentDoctor = `<p>Dear ${result.result.data.name}<br><br>

                            Terima kasih untuk ketertarikan anda.<br>
                            Anda baru saja mengirimkan permintaan untuk mengunduh full version jurnal ${journal_title}.<br>
                            Permintaan Anda akan kami proses terlebih dahulu, untuk informasi lebih lanjut terkait proses ini akan
                            kami informasikan melalui telepon atau email yang terdaftar. Mohon kesediaannya untuk menunggu
                            proses permintaan.<br>
                            Demikian informasi ini kami sampaikan, jika ada pertanyaan lebih lanjut, Anda dapat mengirimkan
                            email ke <a href="mailto:info@d2d.co.id" target="_top">info@d2d.co.id</a>.<br><br>
                        
                            Terima kasih telah menggunakan aplikasi D2D sebagai pilihan sarana informasi dan pembelajaran untuk
                            anda.<br><br>
                        
                            Salam,<br>
                            Tim Admin D2D</p>`;

                let subjectCs = `REQJ${idRequest}`;
                let contentCs = `<p>Dear Admin D2D,<br><br>

                                Anda memiliki permintaan download full jurnal ${journal_title} untuk User dengan data sebagai berikut di
                                bawah:<br><br>

                                Nama: ${fullname}<br>
                                Doctor Practice: ${address}<br>
                                City: ${city}<br>
                                Specialist: ${competence_name}<br>
                                Phone Number: ${phone}<br>
                                Email: <a href="mailto:${email}" target="_top">${email}</a><br><br>
                        
                                Mohon untuk dapat diproses lebih lanjut.<br><br>
                                Terima Kasih.</p>`;

                sendEmail(email, subjectDoctor, contentDoctor)
                sendEmail(`info@d2d.co.id`, subjectCs, contentCs)

                async function sendEmail(toEmail, subject, emailContent) {
                    let form = {
                        toEmail,
                        subject,
                        emailContent,
                        apps: 'd2d'
                    }
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/email/send`, form)
                    request.post(options, function(error, response, body) {
                        if(!error) {
                            let result = JSON.parse(body)
                            console.log(result)
                        } else {
                            throw error
                        }
                    })
                }

                let resp = responseCustom.v1(200, true, `Thank you, your request should be processed in 48 working hours`, data)
                res.status(resp.status).send(resp)
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = JournalController;