const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const responseCustom    = require('../../utils/response-custom');
const requestOptions    = require('../../utils/request-options');
const UserCompetence    = require('../../models/UserCompetence');
const request           = require('request');
const sequelize         = SEQUELIZE_INIT;

const CompetenceController        = {}
const log                   = 'Competence controller';

CompetenceController.getCompetence  = async (req, res, next) => {
    console.log(`├── ${log} :: getCompetence`);

    try {
        let uid         = req.headers.uid;
        let page        = (req.query.page == null || req.query.page == '') ? 'page=' : `page=${req.query.page}`;
        let search      = (req.query.search == null || req.query.search == '') ? '&search=' : `&search=${req.query.search}`;
        let options     = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/competence?${page}${search}`, null)
        
        request.get(options, function(error, response, body) {
            if(!error) {
                let result = JSON.parse(body)
                let data = result.result.data;
                let competence = [];
                let obj = { id: null, title: null, meta_title: null, sub: null };
                let sub_obj = { id: null, title: null, meta_title: null };

                for(let i = 0; i < data.length; i++) {
                    if(data[i].main_id > 0) {
                        let sub = [];
                        
                        if(data[i].sub_id != null) {
                            let sub_id = data[i].sub_id.split(',');
                            let sub_title = data[i].sub_title.split(',');
                            let sub_meta_title = data[i].sub_meta_title.split(',');
            
                            for(let j = 0; j < sub_meta_title.length; j++) {
                                sub[j] = {
                                    id: sub_id[j],
                                    title: sub_title[j],
                                    description: sub_meta_title[j]
                                };
                            }
                        }
            
                        competence[i] = {
                            id: data[i].main_id,
                            title: data[i].main_title,
                            description: data[i].main_meta_title,
                            subSpecialistList: sub
                        };
                    } else {
                        competence[i] = {
                            id: data[i].main_id,
                            title: data[i].main_title,
                            description: data[i].main_meta_title,
                            subSpecialistList: null
                        };
                    }
                }
        
                result.result.data = competence;
        
                res.status(result.status).send(result);
            } else {
                throw error
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: getCompetence :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CompetenceController.count  = async (req, res, next) => {
    console.log(`├── ${log} :: count`);

    try {
        let uid         = req.headers.uid;
        
        sequelize.query(`SELECT competence_id, COUNT(uid) as subscribe FROM user_competence
                            WHERE competence_id IS NOT NULL
                            GROUP BY competence_id`).spread((results, metadata) => {
                                let resp = responseCustom.v1(200, true, null, results)
                                res.status(resp.status).send(resp)
                            }).catch(error => {
                                throw error
                            })
    } catch(error) {
        console.log(`│  ├── ${log} :: count :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CompetenceController.subscribe  = async (req, res, next) => {
    console.log(`├── ${log} :: subscribe`);

    try {
        let uid                         = req.body.uid;
        let competence_id               = req.body.competence_id.split(',');
        let competence_name             = req.body.competence_name.split(',');
        let competence_description      = req.body.competence_description.split(',');
        let subcompetence_id            = (req.body.subcompetence_id == null || req.body.subcompetence_id == '') ? null : req.body.subcompetence_id.split(',');
        let subcompetence_name          = (req.body.subcompetence_name == null || req.body.subcompetence_name == '') ? null : req.body.subcompetence_name.split(',');
        let subcompetence_description   = (req.body.subcompetence_description == null || req.body.subcompetence_description == '') ? null : req.body.subcompetence_description.split(',');
        let parent_id                   = (req.body.parent_id == null || req.body.parent_id == '') ? null : req.body.parent_id.split(',');
        let type                        = req.params.type;
        let values                      = [];
        let subvalues                   = [];

        for(let i = 0; i < competence_id.length; i++) {
            values.push(`('${uid}', ${competence_id[i]}, '${competence_name[i]}', '${competence_description[i]}', null, '${type}')`);
        }

        if(subcompetence_id != null) {
            for(let i = 0; i < subcompetence_id.length; i++) {
                subvalues.push(`('${uid}', ${subcompetence_id[i]}, '${subcompetence_name[i]}', '${subcompetence_description[i]}', '${parent_id[i]}', 'subspesialization')`);
            }
        }
        
        let query = (type == 'spesialization') ? `DELETE FROM user_competence WHERE uid = '${uid}' AND type IN ('spesialization','subspesialization')` : `DELETE FROM user_competence WHERE uid = '${uid}' AND type = '${type}'`;

        sequelize.query(query).then(() => {
            query = `INSERT INTO user_competence (uid, competence_id, competence_name, competence_description, parent_id, type) VALUES ${values.join(',')}${(subvalues.length > 0) ? ','+subvalues.join(',') : ''}`;
            sequelize.query(query).then(data => {
                let resp = responseCustom.v1(200, true, `${type} berhasil ter-update`, data)
                res.status(resp.status).send(resp)
            }).catch(error => {
                throw error
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: subscribe :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = CompetenceController;