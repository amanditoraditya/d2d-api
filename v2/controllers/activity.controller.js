const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const request               = require('request');

const ActivityController    = {}
const log                   = 'Activity controller';

ActivityController.insert = async (req, res, next) => {
    console.log(`├── ${log} :: insert`);

    try {
        let uid = req.headers.uid;

        let form = {
            act: req.body.act,
            content: req.body.content,
            contentId: req.body.contentId
        }

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form);

        request.post(options, function(error, response, body) {
            if(!error) {
                const hasil = JSON.parse(body)
                res.status(hasil.status).send(hasil)
            } else {
                throw error
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ActivityController;