const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const User                  = require('../../models/User');
const UserCompetence        = require('../../models/UserCompetence');
const request               = require('request');

const LoginController   = {}
const log               = 'Login controller';

LoginController.doLogin = async (req, res, next) => {
    console.log(`├── ${log} :: Login`);

    try {
        let uid         = req.body.uid;
        let email       = req.body.email;
        let name        = req.body.name;

        User.findOne(
            {where: {email}}
        ).then(async data => {
            let userExist = (data == "" || data == null) ? false : true

            if(userExist) {
                if(data.uid == '' || data.uid == null) {
                    User.update(
                        {uid},
                        {where: {email}}
                    ).then(() => {
                        UserCompetence.bulkCreate([
                            {uid, type: 'subscription'},
                            {uid, type: 'spesialization'}
                        ]).then(() => {
                            getProfile(uid)
                        }).catch(error => {
                            throw error
                        })
                    }).catch(error => {
                        throw error
                    })
                } else {
                    getProfile(data.uid)
                }
            } else {
                if(name == null || name == 'null' || name == '') {
                    let resp = responseCustom.v1(200, true, `Silahkan register dahulu`, {npa_idi: null})
                    res.status(resp.status).send(resp)
                } else {
                    let uid         = req.body.uid;
                    let email       = req.body.email;
                    let name        = req.body.name;
                    
                    User.build(
                        {uid, email, name}
                    )
                    .save()
                    .then(async () => {
                        let form = {
                            email,
                            uid,
                            displayName: name
                        }

                        let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/create/gue_user`, form);
                        request.post(options, function(error, response, body) {
                            if(!error) {
                                UserCompetence
                                .bulkCreate([
                                    {uid, type: 'subscription'},
                                    {uid, type: 'spesialization'}
                                ]).then(() => {
                                    return UserCompetence.findAll(
                                        {where: {uid}}
                                    )
                                }).then(data => {
                                    getProfile(uid)
                                })
                                .catch(error => {
                                    throw error;
                                })
                            } else {
                                throw error
                            }
                        });
                    }).catch(error => {
                        throw error
                    })
                }
            }
        }).catch(error => {
            throw error
        })

        async function getProfile(uid) {
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${uid}`, null)
            request.get(options, function(error, response, body) {
                if(!error) {
                    let resp = JSON.parse(body);
                    res.status(resp.status).send(resp)
                } else {
                    throw error
                }
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: doLogin :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

LoginController.forgotPassword = async (req, res, next) => {
    console.log(`├── ${log} :: forgotPassword`);

    try {
        let email = req.body.email;
        let code = (req.body.code == null || req.body.code == '') ? null : req.body.code;
        let new_password = (req.body.new_password == null || req.body.new_password == '') ? null : req.body.new_password;
        let stage = (code == null && new_password == null) ? false : true;
        let name = ``;

        if(!stage) {
            let options = requestOptions(req.headers, null, `${CONFIG.URL_GUE_SERVICE}/user/detail/email/${email}`)
            request.get(options, function(error, response, body) {
                let result = JSON.parse(body);

                if(result.status == 500) {
                    let resp = responseCustom.v1(200, false, `Email tidak terdaftar`, [])
                    res.status(resp.status).send(resp)
                } else {
                    name = result.result[0].displayName;
                    let uid = result.result[0].uid;
                    form = {
                        email,
                        emailContent: `<p>Dear ${name},<br/><br/>Please enter this code in order to completely reset your password<br/><br/>Best wishes,<br/>D2D</p>`,
                        subject: `D2D Password Change Confirmation`,
                        apps: 'd2d'
                    }
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/forgot_password/`, form)
                    request.post(options, function(error, response, body) {
                        let result = JSON.parse(body);

                        res.json(result);
                    })
                }
            })
        } else {
            form = {
                email,
                code,
                password: new_password
            }
            let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_GUE_SERVICE}/user/forgot_password/confirm`, form)
            request.post(options, function(error, response, body) {
                let result = JSON.parse(body);
                res.json(result);
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: forgotPassword :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = LoginController;