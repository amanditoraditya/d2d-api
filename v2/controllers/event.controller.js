const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const responseCustom    = require('../../utils/response-custom');
const requestOptions    = require('../../utils/request-options');
const EventReservation  = require('../../models/EventReservation');
const EventAttendee     = require('../../models/EventAttendee');
const request           = require('request');
const sequelize         = SEQUELIZE_INIT;

const EventController        = {}
const log                   = 'Event controller';

EventController.event  = async (req, res, next) => {
    console.log(`├── ${log} :: event`);

    try {
        let bookmark    = (req.query.bookmark == null || req.query.bookmark == ``) ? `` : req.query.bookmark;
        let reserve     = (req.query.reserve == null || req.query.reserve == ``) ? `` : req.query.reserve;
        let page        = (req.query.page == null) ? '1' : req.query.page;
        let search      = (req.query.search == null || req.query.search == ``) ? `` : req.query.search;

        let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_CONTENT}/event?page=${page}&bookmark=${bookmark}&reserve=${reserve}&search=${search}`, null)
        request.get(options, function(error, response, body) {
            if(!error) {
                let hasil = JSON.parse(body)
                res.status(hasil.status).send(hasil)
            } else {
                throw error
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: event :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.slug  = async (req, res, next) => {
    console.log(`├── ${log} :: slug`);

    try {
        let slug        = req.params.slug;
        let slug_hash   = req.params.slug_hash;
        let uid         = req.params.uid;

        let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_CONTENT}/event/${slug}/${slug_hash}/${uid}`, null)
        request.get(options, function(error, response, body) {
            if(!error) {
                let hasil = JSON.parse(body)
                res.status(haisl.status).send(hasil)
            } else {
                throw error
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: slug :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.reserve  = async (req, res, next) => {
    console.log(`├── ${log} :: reserve`);

    try {
        let uid         = req.body.uid;
        let event_id    = req.body.event_id;
        let event_name  = req.body.event_name;
        let type        = req.params.type;

        if(type == 'add') {
            EventReservation.build(
                {uid, event_id, event_name}
            ).save().then(data => {
                aktifitas(data)
            }).catch(error => {
                throw error
            })
        } else if(type == 'delete') {
            EventReservation.destory(
                {where: {uid}}
            ).then(data => {
                aktifitas(data)
            }).catch(error => {
                throw error
            })
        }

        async function aktifitas(data) {
            let act = (type == `add`) ? `reserve`: `unreserve`;
            let form    = {act, content: 'event', contentId: event_id}
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form)
            request.post(options, function(error, response, body) {
                let mess    = (type == `add`) ? `Reservasi berhasil` : `Reservasi telah dihapus`;
                let resp    = responseCustom.v1(200, true, mess, data)
                res.status(resp.status).send(resp)
            })
        }        
    } catch(error) {
        console.log(`│  ├── ${log} :: reserve :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.signin  = async (req, res, next) => {
    console.log(`├── ${log} :: signin`);

    try {
        let slug_hash   = req.body.slug_hash;
        let uid         = req.params.uid;
        let email       = req.body.email;

        EventAttendee.findAll(
            {where: {event_slug_hash: slug_hash, email, type: 'materi'}}
        ).then(data => {
            if(data != null || data != '') {
                let event_name  = ``;
                let name        = ``;
                let options     = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/event/${slug_hash}/${uid}`, null)
                request.get(options, function(error, response, body){
                    console.log(body)
                    let result              = JSON.parse(body)
                    let event_start_date    = result.result.data[0].start_date;
                    let event_end_date      = result.result.data[0].end_date;
                    let tgl                 = datetime.create().format('Y-m-d');
                    let available           = (tgl >= event_start_date && tgl <= event_end_date) ? true : false;
                    let errMsg              = (tgl > event_end_date) ? `Event telah berakhir!,toast` : (tgl < event_start_date) ? `Event belum mulai!,toast` : ``;
                    
                    event_name = result.result.data[0].title;

                    if(available) {
                        let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${npaoremail}`, null)
                        request.get(options, function(error, response, body) {
                            let hasil   = JSON.parse(body)
                            let email   = hasil.result[0].email
                            name        = hasil.result[0].name
                            
                            EventAttendee.build(
                                {event_slug_hash, event_name, name, email, type: 'absent'}
                            ).then(data => {
                                let form    = {act: 'bookmark', content: 'event', contentId: result.result.data[0].id}
                                let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form)
                                request.post(options, function(error, response, body) {
                                    if(!error) {
                                        let resp = responseCustom.v1(200, true, `Anda berhasil absen dalam event ini!`, result.result.data)
                                        res.status(resp.status).send(resp)
                                    } else  {
                                        throw error
                                    }
                                })
                            }).catch(error => {
                                throw error
                            })
                        })
                    } else {
                        let resp = responseCustom.v1(200, true, errMsg, result.result.data)
                        res.status(resp.status).send(resp)
                    }
                })
            } else {
                let resp = responseCustom.v1(200, false, `Anda tidak terdaftar dalam event ini!,popup`, null)
                res.status(resp.status).send(resp)
            }
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: signin :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = EventController;