const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const QuizQuestions     = require('../../models/QuizQuestions');
const UserSkpTmp        = require('../../models/UserSkpTmp');
const UserSkp           = require('../../models/UserSkp');
const QuizTaken           = require('../../models/QuizTaken');
const asyncrequest      = require('async-request');
const co                = require('co');
const fs                = require('fs');
const oss               = require('ali-oss');
const jwt               = require('jsonwebtoken');
const datetime          = require('node-datetime');
const request           = require('request');
const sequelize         = SEQUELIZE_INIT;
const CmeController     = {}
const log               = 'Cme controller';

CmeController.quizList = async (req, res, next) => {
    console.log(`├── ${log} :: Quiz-List`);

    try {
        let uid         = req.params.uid;
        let page        = (req.query.page == null) ? await utils.paging(1) : await utils.paging(req.query.page);
        let type        = req.params.type //all / done / available

        let done = (type == 'done') ? 'true' : (type == 'available') ? 'false' : '';
        let whereQ = '';
        let query = '';

        if(type == 'offline') {
            query = `SELECT
                    user_skp_tmp.uid,
                    user_skp_tmp.skp,
                    user_skp_tmp.title,
                    user_skp_tmp.source,
                    user_skp_tmp.taken_on,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                    user_skp_tmp.status
                    FROM
                    user_skp_tmp
                    WHERE
                    title IS NOT NULL AND
                    uid = '${uid}'
                    
                    UNION
                    
                    SELECT
                    user_skp.uid,
                    user_skp.skp,
                    user_skp.title,
                    user_skp.source,
                    user_skp.taken_on,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp.certificate) as certificate,
                    user_skp.status
                    FROM
                    user_skp
                    WHERE
                    title IS NOT NULL AND
                    uid = '${uid}'

                    ORDER BY
                    taken_on DESC
                    
                    LIMIT ${page},10`;
        } else if(type == 'all' || type == 'done' || type == 'available') {
            query = `SELECT X.*
                    
                    FROM

                    (SELECT
                    A.*,
                    B.certificate,
                    IF(B.uid IS NOT NULL, 'true', 'false') done
                    
                    FROM
                    
                    (SELECT
                    quiz.id,
                    quiz.source_id,
                    quiz.title,
                    quiz.description,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/pdf/quiz/', quiz.filename) as filename,
                    quiz.skp,
                    quiz.available_start,
                    quiz.available_end,
                    source_cme.source,
                    source_cme.description as source_description,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/image/logo/', source_cme.logo) as logo
                    FROM
                    quiz
                    INNER JOIN source_cme ON quiz.source_id = source_cme.id) as A
                    
                    LEFT JOIN
                    
                    (SELECT
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                    user_skp_tmp.uid,
                    user_skp_tmp.status,
                    user_skp_tmp.skp,
                    user_skp_tmp.title,
                    user_skp_tmp.quiz_id
                    FROM
                    user_skp_tmp
                    WHERE
                    title IS NULL AND
                    status = 'valid' AND
                    uid = '${uid}'
                    
                    UNION
                    
                    SELECT
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp.certificate) as certificate,
                    user_skp.uid,
                    user_skp.status,
                    user_skp.skp,
                    user_skp.title,
                    user_skp.quiz_id
                    FROM
                    user_skp
                    WHERE
                    title IS NULL AND
                    status = 'valid' AND
                    uid = '${uid}') as B
                    
                    ON A.id = B.quiz_id
                    
                    WHERE
                    DATE_FORMAT(NOW(), '%Y-%m-%d') >= A.available_start AND
                    DATE_FORMAT(NOW(), '%Y-%m-%d') <= A.available_end) as X

                    ${(type == "done") ? "WHERE X.done = 'true'" : (type == "available") ? "WHERE X.done = 'false'" : ""}
                    
                    ORDER BY
                    X.done,
                    X.available_end

                    LIMIT ${page},10`;
        }

        sequelize.query(query).spread((results, metadata) => {
            let quiz = results

            let query = `SELECT
                uid,
                SUM(skp) as skp
                FROM
                user_skp_tmp
                WHERE
                uid = '${uid}' AND
                status = 'valid'
                GROUP BY
                uid`;

            sequelize.query(query).spread((results, metadata) => {
                let skp = results[0];
                let resp = responseCustom.v1(200, true, null, {skp, quiz})
                res.status(resp.status).send(resp)
            }).catch(error => {
                throw error
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Quiz-List :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.quizDetail = async (req, res, next) => {
    console.log(`├── ${log} :: Quiz-Detail`);

    try {
        let quiz_id      = req.params.quizid;

        QuizQuestions.findAll(
            {where: { quiz_id }},
            {raw: true}
        ).then(async data => {
            let arr = []
            for(let i = 0; i < data.length; i++) {
                arr.push(data[i].dataValues)
            }
            let questions   = await utils.randomArray(arr)
            let resp        = responseCustom.v1(200, true, null, questions)
            res.status(resp.status).send(resp)
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Quiz-Detail :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.submitOffline = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Offline`);

    try {
        let type        = `certificate`;
        let uid         = req.body.uid;
        let skp         = req.body.skp;
        let title       = req.body.title;
        let source      = req.body.source;
        let taken_on    = req.body.taken_on;
        
        UserSkpTmp
        .build({ uid, skp, title, source, taken_on })
        .save()
        .then(data => {
            let rowId       = data.id;
            const client    = new oss({
                region: 'oss-ap-southeast-5',
                accessKeyId: 'LTAINk4kOx5TLJT6',
                accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
                bucket: 'd2doss'
            })

            console.log(`start uploading...`);

            co(function* () {
                console.log(`mulai`);
                let arr = req.file.originalname.split('.');
                let ext = arr[(arr.length-1)];
                console.log(`${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);
                let result = yield client.put(`${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);

                fs.unlink(`../../fileupload/${req.file.filename}`, function(err) {
                    if(!err) {
                        UserSkpTmp.update(
                            {certificate: `${req.file.filename}.${ext}`},
                            {where: {id: rowId}}
                        ).then(() => {
                            let data =  {certificate: `https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/${uid}/${req.file.filename}.${ext}`}
                            let resp = responseCustom.v1(200, true, null, data)
                            res.status(resp.status).send(resp)
                        }).catch(error => {
                            throw error
                        })
                    } else {
                        throw error
                    }
                })
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Offline :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.resetSkp = async (req, res, next) => {
    console.log(`├── ${log} :: Reset-SKP`);

    try {
        let uid         = req.body.uid;
        
        UserSkpTmp.findAll(
            {where: {uid}}
        ).then(async data => {
            let arr = []
            await utils.asyncForEach(data, async (obj) => {
                delete obj.dataValues.id
                arr.push(obj.dataValues)
            })

            UserSkp.bulkCreate(arr).then(() => {
                UserSkpTmp.destroy(
                    {where: {uid}}
                ).then(() => {
                    let resp = responseCustom.v1(200, true, `SKP point successfully reset`, null)
                    res.status(resp.status).send(resp)
                }).catch(error => {
                    throw error
                })
            }).catch(error => {
                throw error
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Reset-SKP :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.emailCertificate = async (req, res, next) => {
    console.log(`├── ${log} :: Email-Certificate`);

    try {
        let uid             = req.headers.uid;
        let toEmail         = req.body.email;
        let subject         = `Your CME Certificate is here!`
        let name            = req.body.displayName;
        let certificate     = req.body.certificate;
        let title           = req.body.title;
        let emailContent    = `<p>Dear ${name},<br/><br/>Here is your certificate for quiz ${title}.<br/><br/>Best wishes,<br/>D2D<br/><br/></p><center><a href="${certificate}" target="_blank"><button class="activation">DOWNLOAD CERTIFICATE</button></a></center>`;
        let body            = {
                                toEmail,
                                subject,
                                emailContent,
                                apps: 'd2d'
                            }
        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/email/send`, body)
        request.post(options, function(error, response, body) {
            if(error) {
                throw error
            } else {
                let resp = responseCustom.v1(200, true, `Certificate has been sent to your registered email`, null)
                res.status(resp.status).send(resp)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Email-Certificate :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.submitQuiz = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Quiz`);

    try {
        let auth            = req.headers.authorization
        let plat            = req.headers.platform

        // asyncrequest        = asyncrequest.defaults({headers: {auth, plat}});
        let quizid          = req.body.quizid; //quiz id
        let uid             = req.body.uid; //uid
        let skp             = req.body.skp; //skp
        let source          = req.body.source; //source
        let answers         = JSON.parse(req.body.answers); //answers
        // let score           = await scoring(quizid, answers); //score
        // let result          = await (score >= 60) ? `Y` : `N`; //result

        // async function scoring(quizid, answers) {
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/cme/quiz-detail/${quizid}`, null)
            request.get(options, async function(error, response, body) {
                let sortedAnswers   = await utils.sort(answers);
                let getDb           = body;
                let parsedDb        = JSON.parse(getDb);
                let dbAnswers       = await utils.sort(parsedDb.result.data);
                let score           = await utils.compare(sortedAnswers, dbAnswers);
                let result          = await (score >= 60) ? `Y` : `N`; //result

                // return await score;
                console.log({quiz_id: quizid, uid, answers, score, result})

                QuizTaken.build(
                    {quiz_id: quizid, uid, answers: `${JSON.stringify(answers)}`, result}
                ).save().then(() => {
                    if(result == 'Y') {
                        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
                        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 60});
                        let form        = {uid, quiz: quizid}
                        let options     = requestOptions(req.headers, uid, `${CONFIG.URL_CMS_D2D}/api/v1/certificate/create`, form)
                        request.post(options, async function(error, response, body) {
                            if(!error) {
                                let result      = JSON.parse(response.body);
                                let certificate = result.data.split('/');
                                let dt          = datetime.create();
                                let formatted   = dt.format('Y-m-d H:M:S');

                                UserSkpTmp.build(
                                    {uid, skp, source, taken_on: formatted, certificate: certificate[1], quiz_id: quizid, status: 'valid'}
                                ).save().then(() => {
                                    let resp = responseCustom.v1(200, true, `You have successfully taken this quiz and get ${skp} SKP`, {certificate: `https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/${uid}/${certificate}`})
                                    res.status(resp.status).send(resp)
                                }).catch(error => {
                                    throw error
                                })
                            } else {
                                throw error
                            }
                        })
                    } else {
                        let resp = responseCustom.v1(200, false, 'You may try again next time', null)
                        res.status(resp.status).send(resp)
                    }
                }).catch(error => {
                    throw error
                })
            })
        // }
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Quiz :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = CmeController;