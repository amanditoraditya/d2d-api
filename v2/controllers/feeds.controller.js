const parseMessageCode = require('../../utils/helpers/parse-messagecode');
const requestOptions = require('../../utils/request-options');
const responseCustom = require('../../utils/response-custom');
const request = require('request');

const FeedsController        = {}
const log                   = 'Feeds controller';

FeedsController.feeds  = async (req, res, next) => {
    console.log(`├── ${log} :: feeds`);

    try {
        let bookmark        = (req.query.bookmark == null || req.query.bookmark == ``) ? `` : req.query.bookmark;
        let download        = (req.query.download == null || req.query.download == ``) ? `` : req.query.download;
        let month           = (req.query.month == null || req.query.month == ``) ? `` : req.query.month;
        let reserve         = (req.query.reserve == null || req.query.reserve == ``) ? `` : req.query.reserve;
        let page            = (req.query.page == null) ? '1' : req.query.page;
        let search          = (req.query.search == null || req.query.search == ``) ? `` : req.query.search;
        let competence      = (req.query.competence == null || req.query.competence == ``) ? `` : req.query.competence;
        let video_learning  = (req.query.video_learning == 'true') ? `true` : `false`;
        let video_materi    = (req.query.video_materi == 'true') ? `true` : `false`;
        let pdf_journal     = (req.query.pdf_journal == 'true') ? `true` : `false`;
        let pdf_materi      = (req.query.pdf_materi == 'true') ? `true` : `false`;
        let pdf_guideline   = (req.query.pdf_guideline == 'true') ? `true` : `false`;
        let event           = (req.query.event == 'true') ? `true` : `false`;
        let webinar         = (req.query.webinar == '' || req.query.webinar == null) ? `false` : req.query.webinar;
        let uid             = req.query.uid;

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/feeds?bookmark=${bookmark}&download=${download}&reserve=${reserve}&search=${search}&month=${month}&page=${page}&competence=${competence}&pdf_journal=${pdf_journal}&pdf_materi=${pdf_materi}&event=${event}&pdf_guideline=${pdf_guideline}&video_learning=${video_learning}&video_materi=${video_materi}&uid=${uid}&webinar=${webinar}`, null)
        request.get(options, function(error, response, body) {
            if(!error) {
                let resp = JSON.parse(body)
                res.status(resp.status).send(resp)
            } else {
                throw error
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = FeedsController;