const parseMessageCode = require('../../utils/helpers/parse-messagecode');
const responseCustom = require('../../utils/response-custom');
const Settings = require('../../models/Settings');

const SettingsController        = {}
const log                   = 'Settings controller';

SettingsController.getByCode  = async (req, res, next) => {
    console.log(`├── ${log} :: getByCode`);

    try {
        let code = req.params.code;

        Settings.findAll({
            where: {
                code
            }
        }).then(data => {
            const resp = responseCustom.v1(200, true, null, data);
            res.status(200).send(resp)
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: getByCode :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = SettingsController;