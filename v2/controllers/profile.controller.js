const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const User              = require('../../models/User');
const VProfileList      = require('../../models/VProfileList');
const request           = require('request');
const co = require('co');
const fs = require('fs');
const oss = require('ali-oss');

const ProfileController = {}
const log               = 'Profile controller';

ProfileController.getProfile  = async (req, res, next) => {
    console.log(`├── ${log} :: getProfile`);

    try {
        let npaoremailoruid  = req.params.npaoremailoruid;
        let uid         = req.headers.uid;
        let where       = null
        let cekNum      = await utils.cekNumber(npaoremailoruid)

        if(npaoremailoruid.includes('@')) {
            where = {where: {email: npaoremailoruid}}
        } else if(cekNum) {
            where = {where: {npa_idi: npaoremailoruid}}
        } else {
            where = {where: {uid: npaoremailoruid}}
        }

        VProfileList.findOne(where).then(async profile => {
            if(profile != '' || profile != null ) {
                let sb_id           = (profile.subscription_id == null) ?  '' : profile.subscription_id.split(',');
                let sb_name         = (profile.subscription_name == null) ?  '' : profile.subscription_name.split(',');
                let sb_description  = (profile.subscription_description == null) ?  '' : profile.subscription_description.split(',');
                let sp_id           = (profile.spesialization_id == null) ?  '' : profile.spesialization_id.split(',');
                let sp_name         = (profile.spesialization_name == null) ?  '' : profile.spesialization_name.split(',');
                let sp_description  = (profile.spesialization_description == null) ?  '' : profile.spesialization_description.split(',');
                let spp_id          = (profile.subspesialization_id == null) ?  '' : profile.subspesialization_id.split(',');
                let spp_name        = (profile.subspesialization_name == null) ?  '' : profile.subspesialization_name.split(',');
                let spp_description = (profile.subspesialization_description == null) ?  '' : profile.subspesialization_description.split(',');
                let spp_parent_id   = (profile.subspesialization_parent_id == null) ?  '' : profile.subspesialization_parent_id.split(',');
                let ea              = (profile.event_attendee == null) ?  [] : profile.event_attendee.split(',');
                let sb              = [];
                let sp              = [];
    
                for(let i = 0; i < sb_id.length; i++) {
                    sb.push({
                        id: sb_id[i],
                        title: sb_name[i],
                        description: sb_description[i]
                    });
                }

                for(let i = 0; i < sp_id.length; i++) {
                    let sublist = [];
                    for(let j = 0; j < spp_id.length; j++) {
                        if(spp_parent_id[j] == sp_id[i]) {
                            sublist.push({
                                id: spp_id[j],
                                title: spp_name[j],
                                description: spp_description[j]
                            })
                        }
                    }
                    sp.push({
                        id: sp_id[i],
                        title: sp_name[i],
                        description: sp_description[i],
                        subSpecialistList: sublist
                    });
                }

                let pns = (profile.pns == 'true') ? true : (profile.pns == 'false') ? false : null;
    
                let hasil = {
                    id: profile.id,
                    uid: profile.uid,
                    name: profile.name,
                    email: profile.email,
                    npa_idi: profile.npa_idi,
                    phone: profile.phone,
                    born_date: profile.born_date,
                    education_1: profile.education_1,
                    education_2: profile.education_2,
                    education_3: profile.education_3,
                    pns,
                    home_location: profile.home_location,
                    clinic_location_1: profile.clinic_location_1,
                    clinic_location_2: profile.clinic_location_2,
                    clinic_location_3: profile.clinic_location_3,
                    photo: profile.photo,
                    spesialization: sp,
                    subscription: sb,
                    event_attendee: ea,
                    counting: null
                };
    
                let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity?get=bookmark,download`, null)
                request.get(options, async function(error, response, body) {
                    if(!error) {
                        let result      = JSON.parse(body)
                        hasil.counting  = result.result.data[0]
                        let resp        = responseCustom.v1(200, true, null, hasil);
                        res.status(resp.status).send(resp)
                    } else {
                        throw error
                    }
                })
            } else if(profile == null) {
                let resp = await responseCustom.v1(404, false, 'User ID Not Found', null);
                res.status(resp.status).send(resp)
            }
        }).catch(error => {
            throw error
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: getProfile :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

ProfileController.updateProfile  = async (req, res, next) => {
    console.log(`├── ${log} :: updateProfile`);

    try {
        let uid                 = req.body.uid;
        let name                = req.body.name;
        let npa_idi             = req.body.npa_idi;
        let email               = req.body.email;
        let phone               = req.body.phone;
        let born_date           = req.body.born_date;
        let education_1         = req.body.education_1;
        let education_2         = req.body.education_2;
        let education_3         = req.body.education_3;
        let pns                 = (req.body.pns == '' || req.body.pns == null) ? '' : req.body.pns;
        let home_location       = req.body.home_location;
        let clinic_location_1   = req.body.clinic_location_1;
        let clinic_location_2   = req.body.clinic_location_2;
        let clinic_location_3   = req.body.clinic_location_3;
        let obj                 = null;

        if(pns == '') {
            if(npa_idi == null || npa_idi == 'null') {
                obj = {name, email, phone, born_date, education_1, education_2, education_3, home_location, clinic_location_1, clinic_location_2, clinic_location_3}
                updateProfile(obj)
            } else {
                obj = {name, npa_idi, email, phone, born_date, education_1, education_2, education_3, home_location, clinic_location_1, clinic_location_2, clinic_location_3}
                cekNpa(obj)
            }
        } else {
            if(npa_idi == null || npa_idi == 'null') {
                obj = {name, email, phone, born_date, education_1, education_2, education_3, pns, home_location, clinic_location_1, clinic_location_2, clinic_location_3}
                updateProfile(obj)
            } else {
                obj = {name, npa_idi, email, phone, born_date, education_1, education_2, education_3, pns, home_location, clinic_location_1, clinic_location_2, clinic_location_3}
                cekNpa(obj)
            }
        }

        async function cekNpa(obj) {
            User.findAll(
                {where:{npa_idi}}
            ).then(data => {
                if(data.length > 0) {
                    const resp  = responseCustom.v1(200, false, `NPA IDI anda [${npa_idi}] sudah terdaftar di aplikasi ini`, data);
                    res.status(resp.status).send(resp)
                } else {
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/doctor/idi/${name}/${npa_idi}`, null);
                    request.get(options, async function(error, response, body) {
                        if(!error) {
                            try {
                                let hasil = JSON.parse(body);

                                if(hasil.result.length > 0) {
                                    let nama = hasil.result[0].name
                                    obj.name = nama
                                    updateProfile(obj)
                                } else {
                                    let data    = {nama_lengkap: name, npa_idi}
                                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/scrape/idi`, data);
                                    request.post(options, function(error, response, body) {
                                        if(!error) {
                                            let hasil = JSON.parse(body);
                                            if(hasil.result.data != null) {
                                                let nama = hasil.result.data[0].name
                                                obj.name = nama
                                                updateProfile(obj)
                                            } else {
                                                const resp  = responseCustom.v1(200, false, `Nama atau NPA IDI tidak valid`, null);
                                                res.status(resp.status).send(resp)
                                            }
                                        } else {
                                            throw error
                                        }
                                    })
                                }
                            } catch (error) {
                                throw error
                            }
                        } else {
                            throw error
                        }
                    });
                }
            }).catch(error => {
                throw error
            })
        }

        async function updateProfile(obj) {
            User.update(
                obj,
                {where: {uid}}
            ).then(data => {
                let resp = responseCustom.v1(200, true, `Profile berhasil diupdate`, data)
                res.status(resp.status).send(resp)
            }).catch(error => {
                throw error
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: updateProfile :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

ProfileController.picture  = async (req, res, next) => {
    console.log(`├── ${log} :: picture`);

    try {
        let type = `image`;
        let content = `profile`;
        let uid = req.body.uid;

        const client = new oss({
            region: 'oss-ap-southeast-5',
            accessKeyId: 'LTAINk4kOx5TLJT6',
            accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
            bucket: 'd2doss'
        })
        
        co(function* () {
            let arr = req.file.originalname.split('.');
            let ext = arr[(arr.length-1)];
            let result = yield client.put(`${type}/${content}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);
            fs.unlink(`../../fileupload/${req.file.filename}`, function(err) {
                if(err) {
                    throw err;
                } else {
                    User.update(
                        {photo: `${req.file.filename}.${ext}`},
                        {where: {uid}}
                    ).then(() => {
                        res.json(result)
                    }).catch(error => {
                        throw error
                    })
                }
            });
        }).catch(function (err) {
            res.json(err);
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: picture :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ProfileController;