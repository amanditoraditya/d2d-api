const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const UserNotification                  = require('../../models/UserNotification');
const request               = require('request');

const NotificationController   = {}
const log               = 'Notification controller';

NotificationController.notification = async (req, res, next) => {
    console.log(`├── ${log} :: notification`);

    try {
        let uid         = req.params.uid;

        UserNotification.findAll(
            {where: {uid}},
            {order: ['create_date', 'DESC']}
        ).then(async data => {
            let hasil = await sort(data);
            
            async function sort(result) {
                let obj = [];

                for(let i = 0; i < result.length; i++) {
                    if(result[i].type == `cme_certificate`) {
                        result[i].slug = `https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/${result[i].uid}/${result[i].slug}`;
                    }

                    await obj.push(result[i]);
                }

                return await obj;
            }

            let resp = responseCustom.v1(200, true, null, hasil)
            res.status(resp.status).send(resp)
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: notification :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

NotificationController.read = async (req, res, next) => {
    console.log(`├── ${log} :: read`);

    try {
        let uid     = req.body.uid;
        let title   = req.body.title;
        let slug    = req.body.slug.split('/');

        let newSlug = (slug.length > 1) ? slug[(slug.length-1)] : slug[0];

        UserNotification.update(
            {status: 'read'},
            {where: {uid, title, slug: newSlug}}
        ).then(data => {
            let resp = responseCustom.v1(200, true, null, data)
            res.status(resp.status).send(resp)
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: notification :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = NotificationController;