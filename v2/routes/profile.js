const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { ProfileController } = require('../controllers')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/:npaoremailoruid', AuthMiddleware.authentication, ProfileController.getProfile)
    .put('/', AuthMiddleware.authentication, ProfileController.updateProfile)
    .post('/picture', AuthMiddleware.authentication, upload.single('filename'), ProfileController.picture)

module.exports = router;