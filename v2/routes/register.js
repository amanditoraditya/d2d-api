const router                    = require('express').Router()
const { RegisterController }    = require('../controllers')

router
    .post('/', RegisterController.register)
    .post('/resend_verification', RegisterController.resendVerification)

module.exports = router;