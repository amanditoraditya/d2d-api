const router                    = require('express').Router()
const { header, body }          = require('express-validator/check')
const { AuthMiddleware }        = require('../middlewares')
const { EventController }  = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/', AuthMiddleware.authentication, EventController.event)
    .get('/:slug/:slug_hash/:uid', AuthMiddleware.authentication, EventController.slug)
    .post('/reserve/:type', AuthMiddleware.authentication, EventController.reserve)
    .post('/signin/:uid', AuthMiddleware.authentication, EventController.signin)

module.exports = router;