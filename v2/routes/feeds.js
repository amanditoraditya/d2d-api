const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { FeedsController }   = require('../controllers')

router
    .get('/', AuthMiddleware.authentication, FeedsController.feeds)

module.exports = router;