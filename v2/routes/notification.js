const router = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { NotificationController } = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/:uid', AuthMiddleware.authentication, NotificationController.notification)
    .post('/read', AuthMiddleware.authentication, NotificationController.read)

module.exports = router;