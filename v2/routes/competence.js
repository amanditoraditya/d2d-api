const router                    = require('express').Router()
const { header, body }          = require('express-validator/check')
const { AuthMiddleware }        = require('../middlewares')
const { CompetenceController }  = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/', AuthMiddleware.authentication, CompetenceController.getCompetence)
    .get('/noauth', CompetenceController.getCompetence)
    .get('/count', AuthMiddleware.authentication, CompetenceController.count)
    .post('/subscribe/:type', AuthMiddleware.authentication, CompetenceController.subscribe)

module.exports = router;