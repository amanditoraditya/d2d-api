const router                    = require('express').Router()
const { header, body }          = require('express-validator/check')
const { SettingsController }    = require('../controllers')

// const Validate = {
//     tokenValidation: [
//         header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
//         header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
//     ]
// }

router
    .get('/:code', SettingsController.getByCode)

module.exports = router;