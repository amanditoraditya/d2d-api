const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { CmeController }     = require('../controllers')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/quiz-list/:type/:uid', AuthMiddleware.authentication, CmeController.quizList)
    .get('/quiz-detail/:quizid', AuthMiddleware.authentication, CmeController.quizDetail)
    .post('/submit-offline', AuthMiddleware.authentication, upload.single('filename'), CmeController.submitOffline)
    .post('/reset-skp', AuthMiddleware.authentication, CmeController.resetSkp)
    .post('/email-certificate', AuthMiddleware.authentication, CmeController.emailCertificate)
    .post('/submit-quiz', AuthMiddleware.authentication, CmeController.submitQuiz)

module.exports = router;