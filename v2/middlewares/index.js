const fs            = require('fs')
const path          = require('path')
const parseName     = require('../../utils/helpers/parse-basename')
const middleware    = {}
const currDir       = process.cwd()

fs.readdirSync(currDir + '/v2/middlewares').forEach(file => {
    let extname   = path.extname(file)
    let basename  = path.basename(file, extname)

    if (~file.indexOf('.js') && basename !== 'index') {
        middleware[parseName(basename)] = require(currDir + '/v2/middlewares/' + basename)
    }
})

module.exports = {
    ...middleware
}
