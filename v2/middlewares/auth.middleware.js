const admin = require('firebase-admin');
const request = require('request');
const { validationResult } = require('express-validator/check');
const parseValidator = require('../../utils/helpers/parse-validator');
const AuthMiddleware = {}
const log = 'Auth middleware';

AuthMiddleware.validation = (req, res, next) => {
    let errors  = validationResult(req);

    if (!errors.isEmpty())
    {
        console.log(`├── ${log} :: error validation`);
        
        let validator   = parseValidator(errors.array());
        let message     = typeof validator.messages == 'string' ? validator.messages : 'Something went wrong';
        let result      = typeof validator.result != 'undefined' ? validator.result : 'Something went wrong';
        
        res.status(200).send({
            message: message,
            acknowledge: false,
            result: {},
            errors: {
                validation: result
            }
        });
    }
    else {
        next();
    }
}

AuthMiddleware.authentication = (req, res, next) => {
    console.log('goto: ' + req.originalUrl)
    let fbtok = req.headers.authorization
    console.log('authorization: ' + req.headers.authorization)
    console.log('key: ' + req.headers.key)

    admin.auth().verifyIdToken(fbtok)
    .then(function(decodedToken) {
        req.headers.uid = decodedToken.uid;

        next();
    }).catch(function(error) {
        console.log(error)
        let err         = new Error(parseMessageCode(403))
        err.code        = 403
        
        next(err);
    });
}

module.exports = AuthMiddleware;