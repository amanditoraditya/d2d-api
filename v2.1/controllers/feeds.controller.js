const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const utils                 = require('../../utils/utils');
const request               = require('request');
const User                  = require('../../models/User');
const UserSVC               = require('../../utils/user-service');
const moment                = require('moment');
const _                     = require('lodash');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const FeedsController       = {}
const log                   = 'Feeds controller';
const datetime              = require('node-datetime');
const PaymentModel          = require('../../payment/models/payment.model');
const EventAttendeeModel    = require('../models/event_attendee.model');

FeedsController.feeds  = async (req, res, next) => {
    console.log(`├── ${log} :: feeds`);

    try {
        let bookmark        = (req.query.bookmark == null || req.query.bookmark == ``) ? `` : req.query.bookmark;
        let download        = (req.query.download == null || req.query.download == ``) ? `` : req.query.download;
        let month           = (req.query.month == null || req.query.month == ``) ? `` : req.query.month;
        let reserve         = (req.query.reserve == null || req.query.reserve == ``) ? `` : req.query.reserve;
        let join            = (req.query.join == null || req.query.join == ``) ? `` : req.query.join;
        let page            = (req.query.page == null) ? '1' : req.query.page;
        let search          = (req.query.search == null || req.query.search == ``) ? `` : req.query.search;
        let competence      = (req.query.competence == null || req.query.competence == ``) ? `` : req.query.competence;
        let video_learning  = (req.query.video_learning == 'true') ? `true` : `false`;
        let video_materi    = (req.query.video_materi == 'true') ? `true` : `false`;
        let pdf_journal     = (req.query.pdf_journal == 'true') ? `true` : `false`;
        let pdf_materi      = (req.query.pdf_materi == 'true') ? `true` : `false`;
        let pdf_guideline   = (req.query.pdf_guideline == 'true') ? `true` : `false`;
        let event           = (req.query.event == 'true') ? `true` : `false`;
        let webinar         = (req.query.webinar == '' || req.query.webinar == null) ? `false` : req.query.webinar;
        let uid             = req.query.uid;
        // console.log('headers', req.headers);
        let {countrycode}   = req.headers;
             countrycode    = _.isEmpty(countrycode) ? COUNTRY_CODES.Indonesia : countrycode;
        let cc              = _.isEmpty(countrycode) ? lANGUAGE_TYPE.Indonesia : 
                             (_.toUpper(countrycode) == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English);

        /* double kutip untuk search */
        search = search.replace(/'/g,"''")
        
        /* save to user services */
        // let usersProfile        = await User.findOne({ where: { uid: uid }});
        // let doctorFullName = usersProfile.name;
        // let userFirstname   = (usersProfile.name).split(' ').slice(0, -1).join(' ');
        // let userLastname   = (usersProfile.name).split(' ').slice(-1).join(' ');
        // userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
        // userLastname = (userFirstname ===  userLastname) ? '' : userLastname;
        // let born_date = (_.isEmpty(usersProfile.born_date)) ? "1000-01-01" : usersProfile.born_date;

        // let clinic          = [];
        // let education       = [];

        // if (!_.isEmpty(usersProfile.clinic_location_1)) {
        //     clinic.push(usersProfile.clinic_location_1);
        // }
        // if (!_.isEmpty(usersProfile.clinic_location_2)) {
        //     clinic.push(usersProfile.clinic_location_2);
        // }
        // if (!_.isEmpty(usersProfile.clinic_location_3)) {
        //     clinic.push(usersProfile.clinic_location_3);
        // }

        // if (!_.isEmpty(usersProfile.education_1)) {
        //     education.push(usersProfile.education_1);
        // }
        // if (!_.isEmpty(usersProfile.education_2)) {
        //     education.push(usersProfile.education_2);
        // }
        // if (!_.isEmpty(usersProfile.education_3)) {
        //     education.push(usersProfile.education_3);
        // }

        // let userAddress     = { 
        //     home        : usersProfile.home_location, 
        //     clinic      : clinic
        // }

        // let specialist = await utils.getUserSpecialist(uid);
        // let userOtherInfo   = {
        //     pns         : usersProfile.pns,
        //     npa_idi     : usersProfile.npa_idi,
        //     str_no      : '',
        //     education   : education,
        //     specialist  : specialist
        // }

        // let userData      = {
        //     uid             : uid,
        //     // nik              : '',
        //     firstname       : userFirstname,
        //     lastname        : userLastname,
        //     display_name    : doctorFullName,
        //     email           : usersProfile.email,
        //     phone_number    : usersProfile.phone,
        //     photo           : !_.isEmpty(usersProfile.photo) ? `https://static.d2d.co.id/image/profile/${usersProfile.photo}` : '',
        //     type            : 'doctor',
        //     birthday        : born_date,
        //     address         : JSON.stringify(userAddress),
        //     other_info      : JSON.stringify(userOtherInfo), 
        // }
            
        // console.log(`Profile : Save userInfo Before Hit getprofile`);
        // let userInfo = await UserSVC.saveUser(usersProfile.uid, userData);
        // console.log(`Profile : Save userInfo => datetime ( ${moment().format('DD MM YYYY h:mm:ss SSSSSSS')} ) ${userInfo.acknowledge}`);
        /** end save to user services */

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/feeds?bookmark=${bookmark}&download=${download}&reserve=${reserve}&join=${join}&search=${search}&month=${month}&page=${page}&competence=${competence}&pdf_journal=${pdf_journal}&pdf_materi=${pdf_materi}&event=${event}&pdf_guideline=${pdf_guideline}&video_learning=${video_learning}&video_materi=${video_materi}&uid=${uid}&webinar=${webinar}&country_code=${countrycode}`, null)
        request.get(options, async function(error, response, body) {
            if(!error) {
                let resp            = JSON.parse(body)
                let dt              = datetime.create();
                let formatted       = dt.format('Y-m-d H:M:S');
                resp.server_date    = formatted;
                let live            = []
                let paid            = []
                let attended        = []

                let { acknowledge: ackUser, result: userService } = await UserSVC.getUser(uid);
                
                //check priority yang bayar
                if (!_.isEmpty(resp.result.announcement)) {
                    live = resp.result.announcement.webinar.now
                }
                
                if (!_.isEmpty(live)) {
                    for(row of live) {
                        let condPay = [
                            { jointer: 'AND', key: 'uid', op: '=', value: uid },
                            { jointer: 'AND', key: 'ref_id', op: '=', value: `${row.event_id}` }
                        ]
                        let condAttend = [
                            { jointer: 'AND', key: 'event_slug_hash', op: '=', value: row.event_slug_hash },
                            { jointer: 'AND', key: 'email', op: '=', value: `${userService.email}` }
                        ]
                        let trx_db          = await PaymentModel.getBy(condPay, [], [], null, 1)
                        let attend_db       = await EventAttendeeModel.getBy(['id'],condAttend, [], [], null, 1)
                        if (!_.isEmpty(trx_db)) {
                            paid.push(row)
                        }
                        if (!_.isEmpty(attend_db)) {
                            attended.push(row)
                        }
                    }

                    if (!_.isEmpty(paid)) {
                        resp.result.announcement.webinar.now = paid
                    } else if (!_.isEmpty(attended)) {
                        resp.result.announcement.webinar.now = attended
                    }
                } 

                res.status(resp.status).send(resp)
            } else {
                console.log(error)
                let err   = new Error(error)
                err.code  = 503
                next(err)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = FeedsController;