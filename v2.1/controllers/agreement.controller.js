const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const User              = require('../../models/User');
const Agreement         = require('../../models/Agreement');
const UserAgreement     = require('../../models/UserAgreement');
const _                 = require('lodash');
const sequelize         = SEQUELIZE_INIT;
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const AgreementController     = {}
const log               = 'Agreement controller';

AgreementController.getAgreement = async (req, res, next) => {
    console.log(`├── ${log} :: Agreement`);

    try {
        let { uid, countrycode }     = req.headers;
        let query   = '';
        let agreement = null;
        let resp = null;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        let agreementsQuery = '';
        if(countrycode == COUNTRY_CODES.Indonesia){
            agreementsQuery = `SELECT A.*, A.id, UA.uid FROM agreements AS A
                LEFT JOIN user_agreements AS UA ON UA.agreement_id = A.id AND UA.uid = '${uid}'
                WHERE 
                    A.status = 'A' AND 
                    UA.uid IS NULL AND A.type = 'popup_tnc'`;
        }else{
            //connect to locale to get dib agreement
            agreementsQuery = `SELECT A.company, A.type, A.total_show, A.status, UA.uid, AL.*, AL.id as locale_id, AL.agreement_id as id
                FROM agreements AS A
                JOIN agreements_locale AS AL on AL.agreement_id = A.id
                LEFT JOIN user_agreements AS UA ON UA.agreement_id = A.id AND UA.uid = '${uid}'
                WHERE 
                    A.status = 'A' AND AL.country_code = '${countrycode}' AND
                    UA.uid IS NULL AND A.type = 'popup_tnc'`;
        }

        User.findOne(
            {where: {uid}}
        ).then(async data => {
            let userExist = (data == "" || data == null) ? false : true
            if (userExist == true) {
                
                sequelize.query(agreementsQuery).spread((rAgreements, metadata) => {
                    if (rAgreements.length > 0) {
                        let aggreements_logs = `INSERT INTO user_agreement_logs (uid, agreement_id) VALUES ('${uid}','${rAgreements[0].id}') `;
                        let aggreements_logs_select = `SELECT * FROM  user_agreement_logs WHERE uid='${uid}'`;
                        agreement   = _.omit(rAgreements[0], ['status', 'created', 'updated', 'uid']);
                        
                        sequelize.query(aggreements_logs_select).spread((rALogs, metadata) => {
                            console.log("select lenght : ", rALogs.length);
                            if(rALogs.length == 0) {
                                sequelize.query(aggreements_logs) 
                                resp    = responseCustom.v1(200, true, null, agreement)
                                res.status(resp.status).send(resp)
                            } else {
                                if(_.has(rALogs[0], 'show')){
                                    if (rALogs[0].show ==agreement.total_show) {
                                        resp    = responseCustom.v1(200, true, LANG[cc].AGREEMENTS.DATA_NOT_SHOW, null)
                                        res.status(resp.status).send(resp)
                                    } else if(rALogs[0].show <=agreement.total_show) {
                                        resp    = responseCustom.v1(200, true, null, agreement)
                                        res.status(resp.status).send(resp)
                                    } else {
                                        resp    = responseCustom.v1(200, true, LANG[cc].AGREEMENTS.DATA_NOT_SHOW, null)
                                        res.status(resp.status).send(resp)
                                    }
                                console.log("ini ralogs : ", rALogs[0]);
                                }
                            }
                        }).catch(error => {
                            console.log(error)
                            let err   = new Error(error)
                            err.code  = 488
                            next(err)
                        })
                    } else {
                        let resp = responseCustom.v1(200, true, LANG[cc].GENERAL.DATA_NOT_FOUND, null)
                        res.status(resp.status).send(resp)
                    } 
                }).catch(error => {
                    console.log(error)
                    let err   = new Error(error)
                    err.code  = 488
                    next(err)
                })   
            } else {
                let resp = responseCustom.v1(200, true, LANG[cc].GENERAL.DATA_NOT_FOUND, null)
                res.status(resp.status).send(resp)
            } 
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Agreement :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

AgreementController.submitAgreement = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Agreement`);

    try {
        let { uid, countrycode }     = req.headers;
        let agreement_id    = req.body.id
        let status          = req.body.status

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 

        User.findOne(
            {where: {uid}}
        ).then(user => {
            let userExist = (user == "" || user == null) ? false : true
            if(userExist == false || uid == ''){
                const resp = responseCustom.v1(200, true, LANG[cc].GENERAL.DATA_NOT_FOUND, null);
                res.status(resp.status).send(resp)
            } else {
                UserAgreement.findOne(
                    {where : {uid : uid, agreement_id : agreement_id}}
                ).then(ua => {
                    let uaExist = (ua == null ) ? true : false; 
                    if(uaExist){
                        UserAgreement
                            .build({ uid, agreement_id, status})
                            .save()
                            .then(data => {
                                const resp = responseCustom.v1(200, true, LANG[cc].AGREEMENTS.SUCCESS_SUBMIT, data);
                                res.status(resp.status).send(resp)
                            })
                            .catch(error => {
                                console.log(error)
                                let err   = new Error(error)
                                err.code  = 488
                                next(err)
                            })
                    }
                    else {
                        const resp = responseCustom.v1(200, true, LANG[cc].AGREEMENTS.ACCEPT_AGREEMENT, null);
                        res.status(resp.status).send(resp)
                    }
                })
                .catch(error => {
                    console.log(error)
                    let err   = new Error(error)
                    err.code  = 488
                    next(err)
                })
            }
        })
        .catch(error => {
            console.log(error)
            let err   = new Error(error)
            err.code  = 488
            next(err)
        })  
    } catch (error) {
        console.log(`│  ├── ${log} :: Submit-Agreement :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

AgreementController.cancelAgreement = async (req, res, next) => {
    console.log(`├── ${log} :: MinusTotalShow-Agreement`);

    try {
        let { uid, countrycode }     = req.headers;

        countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
        let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
        
        User.findOne(
            {where: {uid}}
        ).then(user => {
            let userExist = (user == "" || user == null) ? false : true
            if(userExist == false || uid == ''){
                const resp = responseCustom.v1(200, true, LANG[cc].GENERAL.DATA_NOT_FOUND, null);
                res.status(resp.status).send(resp)
            } else {
                    let selectLogs = `SELECT * FROM user_agreement_logs AS UAL WHERE UAL.uid = '${uid}'`;
                    
                    sequelize.query(selectLogs).spread((rselogs, metadata) => {
                        console.log(rselogs);
                        if(rselogs.length > 0 ) {
                            console.log("ini Show :", rselogs[0].show)
                            updateLogs = `UPDATE user_agreement_logs AS UALs SET UALs.show = UALs.show + 1 WHERE UALs.uid = '${uid}'`;
                            sequelize.query(updateLogs).spread((rlogs, metadata) => {
                                resp    = responseCustom.v1(200, true, `Added Show popup`, null)
                                res.status(resp.status).send(resp)
                            }).catch(error => {
                                console.log(error)
                                let err   = new Error(error)
                                err.code  = 488
                                next(err)
                            })  
                        } 
                    }).catch(error => {
                        console.log(error)
                        let err   = new Error(error)
                        err.code  = 488
                        next(err)
                    })
            }
        }).catch(error => {
            console.log(error)
            let err   = new Error(error)
            err.code  = 488
            next(err)
        })  
    } catch (error) {
        console.log(`│  ├── ${log} :: MinusTotalShow-Agreement :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = AgreementController;