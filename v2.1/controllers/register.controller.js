const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const User              = require('../../models/User');
const UserCompetence    = require('../../models/UserCompetence');
const request           = require('request');
const _                 = require('lodash');

const RegisterController    = {}
const log                   = 'Register controller';

RegisterController.doInsertUser  = async (req, res, next) => {
    console.log(`├── ${log} :: doRegister`);

    try {
        let name            = req.body.name;
        let npa_idi         = req.body.npa_idi;
        let email           = req.body.email;
        let password        = req.body.password;
        let kota            = req.body.kota
        let alamat_praktek  = req.body.alamat_praktek;
        let competence      = req.body.competence;
        let uid             = req.body.uid; 

        if(uid == null) {
            let form = {
                email,
                displayName: name,
                password,
                apps: `d2d`,
                subject: `D2D Account Activation`,
                emailContent: `<p>Dear ${name},<br/><br/>Congratulations!<br/>Thank you for registering to D2D.<br/><br/>Your account has been created. Please click the button below to activate your account and enjoy D2D’s features.<br/><br/>Best wishes,<br/>
                D2D</p>`
            }

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/create`, form);
            request.post(options, async function(error, response, body) {
                if(!error) {
                    console.log(body)
                    try{
                        let hasil   = await JSON.parse(body)
                            if(_.isEmpty(hasil.result) == false){
                                let uid     = hasil.result.data[0].uid
                                let obj     = (npa_idi == 'null' || npa_idi == null) ? { uid, name, email } : { uid, name, email, npa_idi }
                                
                                User
                                .build(obj)
                                .save()
                                .then(data => {
                                    UserCompetence
                                    .bulkCreate([
                                        {uid, type: 'subscription'},
                                        {uid, type: 'spesialization'}
                                    ]).then(() => {
                                        const resp = responseCustom.v1(200, true, 'Akun berhasil didaftarkan', [data]);
                                        res.status(resp.status).send(resp);
                                    }).catch(error => {
                                        console.log(error);
                                        let err = new Error(error)
                                        err.code = 488;
                                        next(err);
                                    })
                                })
                                .catch(error => {
                                    console.log(error);
                                    let err = new Error(error)
                                    err.code = 488;
                                    next(err);
                                })
                            }else{
                                const resp = responseCustom.v1(200, false, hasil.message, []);
                                res.status(resp.status).send(resp);
                            }
                    }catch(error){
                        console.log(error);
                        const resp = responseCustom.v1(200, false, 'Gagal pada saat didaftarkan', []);
                        res.status(resp.status).send(resp);
                    }
                } else{
                    console.log(error);
                    let err = new Error(error)
                    err.code = 503;
                    next(err);
                }    
            });
        } else {
            let form = {
                email,
                uid,
                displayName: name
            }

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/create/gue_user`, form);
            request.post(options, async function(error, response, body) {
                if(!error) {
                    console.log(body)
                    try{
                        let hasil   = await JSON.parse(body)
                        if(_.isEmpty(hasil.result) == false){
                            let uid     = hasil.result.data[0].uid
                            let obj     = (npa_idi == 'null' || npa_idi == null) ? { uid, name, email } : { uid, name, email, npa_idi }

                            User
                            .build(obj)
                            .save()
                            .then(data => {
                                UserCompetence
                                .bulkCreate([
                                    {uid, type: 'subscription'},
                                    {uid, type: 'spesialization'}
                                ]).then(() => {
                                    const resp = responseCustom.v1(200, true, 'Akun berhasil didaftarkan', [data]);
                                    res.status(resp.status).send(resp)
                                })
                            })
                            .catch(error => {
                                console.log(error);
                                let err = new Error(error)
                                err.code = 488;
                                next(err);
                            })
                        }else{
                            const resp = responseCustom.v1(200, false, hasil.message, []);
                            res.status(resp.status).send(resp);
                        }
                    }catch(error){
                        console.log(error);
                        const resp = responseCustom.v1(200, false, 'Gagal pada saat didaftarkan', []);
                        res.status(resp.status).send(resp);
                    }
                } else {
                    console.log(error);
                    let err = new Error(error)
                    err.code = 503;
                    next(err);
                }
            });
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: doRegister :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

RegisterController.doRegister  = async (req, res, next) => {
    console.log(`├── ${log} :: doRegister`);

    try {
        let name            = req.body.name;
        let npa_idi         = req.body.npa_idi;
        let email           = req.body.email;
        let password        = req.body.password;
        let kota            = req.body.kota
        let alamat_praktek  = req.body.alamat_praktek;
        let competence      = req.body.competence;
        let uid             = req.body.uid;
        
        let data = await User.findAll({where:{npa_idi}});

        if(data.length > 0) {
            const resp  = responseCustom.v1(200, false, `NPA IDI anda [${npa_idi}] sudah terdaftar di aplikasi ini`, data);
            res.status(resp.status).send(resp)
        } else {
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/doctor/idi/${name}/${npa_idi}`, null);
            request.get(options, async function(error, response, body) {
                if(!error) {
                    let hasil = JSON.parse(body);
                    if(hasil.result.length > 0) {
                        req.body.name = hasil.result[0].name
                        RegisterController.doInsertUser(req, res, next)
                    } else {
                        let data    = {nama_lengkap: name, npa_idi}
                        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/scrape/idi`, data);
                        request.post(options, function(error, response, body) {
                            if(!error) {
                                console.log(body)
                                let hasil = JSON.parse(body);
                                if(hasil.result.acknowledge == true) {
                                    if(hasil.result.data.length > 0) {
                                        req.body.name = hasil.result.data[0].name
                                        RegisterController.doInsertUser(req, res, next)
                                    } else {
                                        const resp  = responseCustom.v1(200, false, `Nama atau NPA IDI tidak valid`, null);
                                        res.status(resp.status).send(resp)
                                    }
                                } else {
                                    const resp  = responseCustom.v1(200, false, `Nama atau NPA IDI tidak valid`, null);
                                    res.status(resp.status).send(resp)
                                }
                            } else {
                                console.log(error);
                                let err = new Error(error)
                                err.code = 503;
                                next(err);
                            }
                        })
                    }
                } else {
                    console.log(error);
                    let err = new Error(error)
                    err.code = 503;
                    next(err);
                }
            });
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: doRegister :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

RegisterController.register  = async (req, res, next) => {
    console.log(`├── ${log} :: register`);

    try {
        let name        = req.body.name.replace("’", "'");
        let npa_idi     = req.body.npa_idi;
        let email       = req.body.email;
        let password    = req.body.password;
        let uid         = req.body.uid;

        if(await utils.cekNama(name)) {
            if(name.length >= 3) {
                if(uid == null) {
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/detail/email/${email}`, null);
                    request.get(options, function(error, response, body) {
                        if(!error) {
                            let parsedBody = JSON.parse(body);
            
                            if(parsedBody.status == 200) {
                                let resp = responseCustom.v1(200, false, `Email sudah terdaftar di akun GueSehat`, null)
                                res.status(resp.status).send(resp);
                            } else {
                                if(npa_idi == null || npa_idi == '' || npa_idi == 'null') {
                                    RegisterController.doInsertUser(req, res, next)
                                } else {
                                    RegisterController.doRegister(req, res, next)
                                }
                            }
                        } else {
                            console.log(error);
                            let err = new Error(error)
                            err.code = 503;
                            next(err);
                        }
                    });
                } else {
                    if(npa_idi == null || npa_idi == '' || npa_idi == 'null') {
                        RegisterController.doInsertUser(req, res, next)
                    } else {
                        RegisterController.doRegister(req, res, next)
                    }
                }
            } else {
                let resp = responseCustom.v1(200, false, `Masukkan Fullname minimal 3 karakter`, null)
                res.status(resp.status).send(resp)
            }
        } else {
            let resp = responseCustom.v1(200, false, `Format fullname yang Anda masukkan tidak sesuai`, null)
            res.status(resp.status).send(resp)
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: doRegister :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

RegisterController.resendVerification  = async (req, res, next) => {
    console.log(`├── ${log} :: resendVerification`);

    try {
        let email       = req.body.email;
        let name        = req.body.name;
        let uid         = req.body.uid;
        let data        =   {
                                email,
                                displayName: name,
                                uid,
                                apps: `d2d`,
                                subject: `D2D Account Activation`,
                                emailContent: `<p>Dear ${name},<br/><br/>Congratulations!<br/>Thank you for registering to D2D.<br/><br/>Your account has been created. Please click the button below to activate your account and enjoy D2D’s features.<br/><br/>Best wishes,<br/>
                                D2D</p>`
                            }

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/create/resend_verification`, data);
        request.post(options, function(error, response, body) {
            console.log(`${CONFIG.URL_GUE_SERVICE}/user/create/resend_verification`)
            if(!error) {
                let resp = responseCustom.v1(200, true, null, null)
                res.status(resp.status).send(resp);
            } else {
                console.log(error);
                let err = new Error(error)
                err.code = 503;
                next(err);
            }
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: doRegister :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = RegisterController;