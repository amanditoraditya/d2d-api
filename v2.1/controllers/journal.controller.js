const _                 = require('lodash');
const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const JournalRequest    = require('../../models/JournalRequest');
const request           = require('request');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const JournalController        = {}
const log                   = 'Journal controller';

JournalController.request  = async (req, res, next) => {
    console.log(`├── ${log} :: journal`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let uid = req.body.uid;
        let fullname = req.body.fullname;
        let competence_id = req.body.comid;
        let competence_name = req.body.comna;
        let phone = req.body.phone;
        let email = req.body.email;
        let city = req.body.city;
        let journal_title = req.body.journal_title;
        let address = req.body.address;

        JournalRequest.build(
            {uid, fullname, competence_id, competence_name, phone, email, city, address, journal_title, countrycode}
        ).save().then(data => {
            let idRequest   = data.id
            let options     = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${uid}`)
            request.get(options, function(error, response, body) {
                let result = JSON.parse(body)
                let subjectDoctor = LANG[cc].JOURNAL.FULL_JOURNAL_REQUEST;
                let contentDoctor = `<p>Dear ${result.result.data.name}<br><br>

                            ${LANG[cc].JOURNAL.THANKS_FOR_INTEREST}<br>
                            Anda baru saja mengirimkan permintaan untuk mengunduh full version jurnal ${journal_title}.<br>
                            Permintaan Anda akan kami proses terlebih dahulu, untuk informasi lebih lanjut terkait proses ini akan
                            kami informasikan melalui telepon atau email yang terdaftar. Mohon kesediaannya untuk menunggu
                            proses permintaan.<br>
                            Demikian informasi ini kami sampaikan, jika ada pertanyaan lebih lanjut, Anda dapat mengirimkan
                            email ke <a href="mailto:info@d2d.co.id" target="_top">info@d2d.co.id</a>.<br><br>
                        
                            Terima kasih telah menggunakan aplikasi D2D sebagai pilihan sarana informasi dan pembelajaran untuk
                            anda.<br><br>
                        
                            Salam,<br>
                            Tim Admin D2D</p>`;

                let subjectCs = `REQJ${idRequest}`;
                let contentCs = `<p>${LANG[cc].JOURNAL.JOURNAL_DEAR},<br><br>

                                ${LANG[cc].JOURNAL.HAVE_REQUEST} ${journal_title} untuk User dengan data sebagai berikut di
                                bawah:<br><br>

                                Nama: ${fullname}<br>
                                Doctor Practice: ${address}<br>
                                City: ${city}<br>
                                Country Code: ${countrycode}<br>
                                Specialist: ${competence_name}<br>
                                Phone Number: ${phone}<br>
                                Email: <a href="mailto:${email}" target="_top">${email}</a><br><br>
                        
                                Mohon untuk dapat diproses lebih lanjut.<br><br>
                                Terima Kasih.</p>`;

                sendEmail(email, subjectDoctor, contentDoctor)
                sendEmail(`info@d2d.co.id`, subjectCs, contentCs)

                async function sendEmail(toEmail, subject, emailContent) {
                    let form = {
                        toEmail,
                        subject,
                        emailContent,
                        apps: 'd2d'
                    }
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/email/send`, form)
                    request.post(options, function(error, response, body) {
                        if(!error) {
                            let result = JSON.parse(body)
                            console.log(result)
                        } else {
                            console.log(error);
                            let err = new Error(error)
                            err.code = 503;
                            next(err);
                        }
                    })
                }
                message = LANG[cc].JOURNAL.SUCCESS_SUBMITED;
                let resp = responseCustom.v1(200, true, 
                    // `Your request was successfully submitted for preocessing. We will send the status update through your email.\nThank You`
                    message
                , data)
                res.status(resp.status).send(resp)
            })
        }).catch(error => {
            console.log(error);
            let err = new Error(error)
            err.code = 488;
            next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = JournalController;