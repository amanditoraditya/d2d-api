const _                     = require('lodash');
const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const responseCustom        = require('../../utils/response-custom');
const Settings              = require('../../models/Settings');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const SettingsController        = {}
const log                   = 'Settings controller';

SettingsController.getByCode  = async (req, res, next) => {
    console.log(`├── ${log} :: getByCode`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode)) ? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let code = req.params.code;
        
        let content = (countrycode == COUNTRY_CODES.Indonesia ? 'content' : 'content_en')
        Settings.findAll({
            where: {
                code
            },
            attributes: ['id', 'code', 'type', [content, 'content']]
        }).then(data => {
            const resp = responseCustom.v1(200, true, message, data);
            res.status(200).send(resp)
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: getByCode :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = SettingsController;