const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const _                     = require('lodash');
const request               = require('request');
const Banner                = require('../../models/Banner');
const sequelize             = SEQUELIZE_INIT;

const ObatatozController    = {}
const log                   = 'Obatatoz controller';
/**
 * Get All Obat A to Z with Keyword limit and page
 */
ObatatozController.read = async (req, res, next) => {
    console.log(`├── ${log} :: read`);

    try {
        let uid     = req.headers.uid;
        let page    = (_.isEmpty(req.query.page)) ? '1' : req.query.page;
        let keyword = (_.isEmpty(req.query.keyword)) ? '' : req.query.keyword;
        let limit  = (_.isEmpty(req.query.limit)) ? '' : req.query.limit;
        let prefix  = (_.isEmpty(req.query.prefix)) ? '' : req.query.prefix;

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_GS}/obat/filter?page=${page}&keyword=${keyword}&limit=${limit}&prefix=${prefix}&apps=d2d`, null);

        request.get(options, function(error, response, body) {
            if(!error) {
                const hasil = JSON.parse(body)
                if(hasil.data != null) {
                    let resp = responseCustom.v21(200, true, hasil.data)
                    res.status(resp.status).send(resp)
                } else if(hasil.status == 'error') {
                    let resp = responseCustom.v21(400, false, hasil.data)
                    res.status(resp.status).send(resp)
                } else if(hasil.data == null) {
                    let resp = responseCustom.v21(204, false, hasil.data)
                    res.status(resp.status).send(resp)
                }
            } else {
                console.log(error)
                let err   = new Error(error)
                err.code  = 503
                next(err)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}
/**
 * Detail Obat A to Z
 */
ObatatozController.detail = async (req, res, next) => {
    console.log(`├── ${log} :: detail`);

    try {
        let uid     = req.headers.uid;
        let obat_id = req.query.obat_id;

        let options = requestOptions(req.headers, uid, `${CONFIG.URL_GS}/obat/detail?obat_id=${obat_id}&apps=d2d`, null);

        request.get(options, function(error, response, body) {
            if(!error) {
                const hasil = JSON.parse(body)
                if(hasil.data != null) {
                    let resp = responseCustom.v21(200, true, hasil.data)
                    res.status(resp.status).send(resp)
                } else if(hasil.data == null) {
                    let resp = responseCustom.v21(204, false, hasil.data)
                    res.status(resp.status).send(resp) 
                }
            } else {
                console.log(error)
                let err   = new Error(error)
                err.code  = 503
                next(err)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}
/**
 * get Banner on Banner Mangement
 */
ObatatozController.banner = async (req, res, next) => {
    console.log(`├── ${log} :: Banner`);

    try {
        // Banner.findAll({
        //     where:{
        //       publish:'1',
        //       apps:'D2D',

        //     }
        // }).then(function(banners){
        //     if (banners != null){
        //         let hasil = responseCustom.addUrlimages(banners);
        //         let resp = responseCustom.v21(200, true, hasil)
        //         res.status(resp.status).send(resp)  
                          
        //     } else {
        //         let resp = responseCustom.v21(204, false, banners)
        //         res.status(resp.status).send(resp)
        //     }
            
        // }).catch(function(error){
        //     console.log(error)
        //     let err   = new Error(error)
        //     err.code  = 488
        //     next(err)
        // });
        let banners = []
        let resp = {}
        let query = `select b.* from banner b left join sizes s on b.size_id = s.id where apps = 'D2D' and publish = 1 and s.name = 'banner_atoz'`;
        banners = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

        if (banners != null) {
            banners = responseCustom.addUrlimages(banners);
            resp = responseCustom.v21(200, true, banners)
        } else {
            resp = responseCustom.v21(204, false, [])
        }
        res.status(resp.status).send(resp)
    } catch(error) {
        console.log(`│  ├── ${log} :: kota :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ObatatozController;