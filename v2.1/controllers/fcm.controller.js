const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const responseCustom    = require('../../utils/response-custom');
const requestOptions    = require('../../utils/request-options');
const UserDevice        = require('../../models/UserDevice');
const request           = require('request');
const sequelize         = SEQUELIZE_INIT;

const FcmController        = {}
const log                   = 'Fcm controller';

FcmController.fcm  = async (req, res, next) => {
    console.log(`├── ${log} :: fcm`);

    try {
        let type        = req.params.type; //save / remove
        let uid         = req.body.uid;
        let deviceToken = req.body.deviceToken;
        
        UserDevice.findAll(
            {where: {uid, device_token: deviceToken}}
        ).then(data => {
            if(data == '' || data == null) {
                if(type == 'save') {
                    UserDevice.build(
                        {uid, device_token: deviceToken}
                    ).save().then(data => {
                        let resp = responseCustom.v1(200, true, 'Device token tracked', {uid, deviceToken})
                        res.status(resp.status).send(resp)
                    }).catch(error => {
                        console.log(error)
                        let err   = new Error(error)
                        err.code  = 488
                        next(err)
                    })
                } else if(type == 'remove') {
                    UserDevice.destroy(
                        {where: {uid, device_token: deviceToken}}
                    ).then(() => {
                        let resp = responseCustom.v1(200, true, 'Device token removed', null)
                        res.status(resp.status).send(resp)
                    }).catch(error => {
                        console.log(error)
                        let err   = new Error(error)
                        err.code  = 488
                        next(err)
                    })
                }
            } else {
                if(type == 'save') {
                    let resp = responseCustom.v1(200, true, 'Device token exist', {uid, deviceToken})
                    res.status(resp.status).send(resp)
                } else if(type == 'remove') {
                    UserDevice.destroy(
                        {where: {uid, device_token: deviceToken}}
                    ).then(() => {
                        let resp = responseCustom.v1(200, true, 'Device token removed', null)
                        res.status(resp.status).send(resp)
                    }).catch(error => {
                        console.log(error)
                        let err   = new Error(error)
                        err.code  = 488
                        next(err)
                    })
                }
            }
        }).catch(error => {
            console.log(error)
            let err   = new Error(error)
            err.code  = 488
            next(err)
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: fcm :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = FcmController;