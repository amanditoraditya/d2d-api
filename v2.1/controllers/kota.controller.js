const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const User                  = require('../../models/User');
const UserCompetence        = require('../../models/UserCompetence');
const request               = require('request');

const KotaController        = {}
const log                   = 'Kota controller';

KotaController.kota = async (req, res, next) => {
    console.log(`├── ${log} :: kota`);

    try {
        let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_CONTENT}/kota`)
        request.get(options, function(error, response, body)  {
            let hasil = JSON.parse(body)
            res.status(hasil.status).send(hasil)
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: kota :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = KotaController;