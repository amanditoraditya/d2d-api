const _                 = require('lodash');
const jwt               = require('jsonwebtoken');
const UserSVC           = require('../../utils/user-service');
const responseCustom    = require('../../utils/response-custom');
const PatientController = {}
const log               = 'Patient controller';

PatientController.getAll = async (req, res, next) => {
    console.log(`├── ${log} :: getAll`);
    try {
        let decode  = jwt.decode(req.headers.authorization);
        let {
            page, limit, sort, keyword
        } = req.query;
        
        let params = [
            { key: 'page', value: page },
            { key: 'limit', value: limit },
            { key: 'sort', value: sort },
            { key: 'keyword', value: keyword },
            { key: 'uid', value: decode.user_id },
        ];
        let result      = await UserSVC.getPatients(decode.user_id, params);

        console.log(`├── ${log} :: getAll :: success`);
        
        let response    = responseCustom.v1(result.status, true, result.message, result.result.docs);
        res.status(response.status).send(response)
    } catch (error) {
        console.log(`├── ${log} :: getAll :: error`);
        console.log(error);

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

PatientController.get = async (req, res, next) => {
    console.log(`├── ${log} :: get`);
    try {
        let { uid }     = req.params;
        let decode      = jwt.decode(req.headers.authorization);

        let { status, message, result, acknowledge }      = await UserSVC.getUser(uid);
        console.log("result usersvc getuser: ", result);
        
        if (acknowledge == true && !_.isEmpty(result)) {
            console.log(`├── ${log} :: get :: success`);
            // remove unused object
            delete result.password_code;
            delete result.confirmation_hash;
            delete result.verified;
            delete result.disabled;
            delete result.deleted;
            
            let pmm         = await UserSVC.getPatientConnected(decode.user_id, uid);
            if (pmm.acknowledge == true && pmm.result.length > 0) {
                let record = pmm.result;
                let status = null;
            
                record.forEach((row, i) => {
                    status = row.status;
                    if (row.status == 'U' && decode.user_id == row.uid) {
                        let lastData        = JSON.parse(row.last_data);
                        
                        result.birthday         = lastData.birthday;
                        result.gender           = (lastData.gender === 'F') ? 'Perempuan' : 'Laki-laki';
                        result.other_info       = JSON.stringify(lastData.other_info);
                        result.updated          = row.updated;
                        result.created          = row.created;
                    }
                })
                result.gender  = (result.gender === 'F') ? 'Perempuan' : 'Laki-laki';
                _.assign(result, {status: status});           
            }
        }

        let response    = responseCustom.v1(status, true, message, result);
        res.status(response.status).send(response)
    } catch (error) {
        console.log(`├── ${log} :: get :: error`);
        console.log(error);

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = PatientController;