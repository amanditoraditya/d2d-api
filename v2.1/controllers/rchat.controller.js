const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const request               = require('request');
const RchatSvc              = require('../../utils/RchatSvc');

const ChatsvcController    = {}
const log                   = 'Activity controller';

ChatsvcController.testing = async (req, res, next) => {
    try {
        let chatReq = await RchatSvc.messagesTesting(req); 
    } catch (error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Login to chat
 * hit API post => rchat/auth/login
 */
ChatsvcController.login = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.login(req);
        let resp        = responseCustom.v1(200, true, null, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Send Message To Room OR DM user
 * hit API post => rchat/messages/send/:email
 */
ChatsvcController.sendMessage = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.sendMessage(req);
        let resp        = responseCustom.v1(200, true, null, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Uplaod Avatar User
 * hit API post => rchat/profile/avatar
 */
ChatsvcController.uploadAvatar = async (req, res, next) => {

}

/**
 * Get Avatar User
 * hit API get => rchat/profile/avatar/:username
 */
ChatsvcController.getAvatar = async (req, res, next) => {
    try {
        await RchatSvc.getAvatar(req, res);
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

/**
 * Create Group Chat
 * hit API post => rchat/groups/create
 */
ChatsvcController.createGroup = async (req, res, next) => {
    try {
        let chatReq     = await RchatSvc.createGroup(req);
        let resp        = responseCustom.v1(200, true, null, chatReq.data.result);
            res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: Login Chat Services :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ChatsvcController;