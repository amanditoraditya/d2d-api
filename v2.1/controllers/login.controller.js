const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const utils                 = require('../../utils/utils');
const User                  = require('../../models/User');
const UserSVC               = require('../../utils/user-service');
const UserCompetence        = require('../../models/UserCompetence');
const request               = require('request');
const _                     = require('lodash');
const moment                = require('moment');

const LoginController   = {}
const log               = 'Login controller';

LoginController.doLogin = async (req, res, next) => {
    console.log(`├── ${log} :: Login`, req.body);

    try {
        let uid         = req.body.uid;
        let email       = req.body.email;
        let name        = req.body.name;

        User.findOne(
            {where: {email}}
        ).then(async data => {
            let userExist = (data == "" || data == null) ? false : true

            if(userExist) {
                if(data.uid == '' || data.uid == null) {
                    User.update(
                        {uid},
                        {where: {email}}
                    ).then(() => {
                        UserCompetence.bulkCreate([
                            {uid, type: 'subscription'},
                            {uid, type: 'spesialization'}
                        ]).then(() => {
                            getProfile(uid)
                        }).catch(error => {
                            console.log(error);
                            let err = new Error(error)
                            err.code = 488;
                            next(err);
                        })
                    }).catch(error => {
                        console.log(error);
                        let err = new Error(error)
                        err.code = 488;
                        next(err);
                    })
                } else {
                    getProfile(data.uid)
                }
            } else {
                if(name == null || name == 'null' || name == '') {
                    let resp = responseCustom.v1(200, true, `Silahkan register dahulu`, {npa_idi: null})
                    res.status(resp.status).send(resp)
                } else {
                    let uid         = req.body.uid;
                    let email       = req.body.email;
                    let name        = req.body.name;
                    
                    User.build(
                        {uid, email, name}
                    )
                    .save()
                    .then(async () => {
                        let form = {
                            email,
                            uid,
                            displayName: name
                        }

                        let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/create/gue_user`, form);
                        request.post(options, function(error, response, body) {
                            if(!error) {
                                UserCompetence
                                .bulkCreate([
                                    {uid, type: 'subscription'},
                                    {uid, type: 'spesialization'}
                                ]).then(() => {
                                    return UserCompetence.findAll(
                                        {where: {uid}}
                                    )
                                }).then(data => {
                                    getProfile(uid)
                                })
                                .catch(error => {
                                    console.log(error);
                                    let err = new Error(error)
                                    err.code = 488;
                                    next(err);;
                                })
                            } else {
                                console.log(error);
                                let err = new Error(error)
                                err.code = 503;
                                next(err);
                            }
                        });
                    }).catch(error => {
                        console.log(error);
                        let err = new Error(error)
                        err.code = 488;
                        next(err);
                    })
                }
            }
        }).catch(error => {
            console.log(error);
            let err = new Error(error)
            err.code = 488;
            next(err);
        })

        async function getProfile(uid) {
            // let usersLoginCheck        = await User.findOne({ where: { uid }});
            // let doctorFullName = usersLoginCheck.name;
            // let userFirstname   = (usersLoginCheck.name).split(' ').slice(0, -1).join(' ');
            // let userLastname   = (usersLoginCheck.name).split(' ').slice(-1).join(' ');
            // userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
            // userLastname = (userFirstname ===  userLastname) ? '' : userLastname;
            // let born_date = (_.isEmpty(usersLoginCheck.born_date)) ? "1000-01-01" : usersLoginCheck.born_date;

            // let clinic          = [];
            // let education       = [];

            // if (!_.isEmpty(usersLoginCheck.clinic_location_1)) {
            //     clinic.push(usersLoginCheck.clinic_location_1);
            // }
            // if (!_.isEmpty(usersLoginCheck.clinic_location_2)) {
            //     clinic.push(usersLoginCheck.clinic_location_2);
            // }
            // if (!_.isEmpty(usersLoginCheck.clinic_location_3)) {
            //     clinic.push(usersLoginCheck.clinic_location_3);
            // }

            // if (!_.isEmpty(usersLoginCheck.education_1)) {
            //     education.push(usersLoginCheck.education_1);
            // }
            // if (!_.isEmpty(usersLoginCheck.education_2)) {
            //     education.push(usersLoginCheck.education_2);
            // }
            // if (!_.isEmpty(usersLoginCheck.education_3)) {
            //     education.push(usersLoginCheck.education_3);
            // }

            // let userAddress     = { 
            //     home        : usersLoginCheck.home_location, 
            //     clinic      : clinic
            // }

            // let specialist = await utils.getUserSpecialist(uid);
            // let userOtherInfo   = {
            //     pns         : usersLoginCheck.pns,
            //     npa_idi     : usersLoginCheck.npa_idi,
            //     str_no      : '',
            //     education   : education,
            //     specialist  : specialist
            // }

            // let userData      = {
            //     uid             : uid,
            //     // nik              : '',
            //     firstname       : userFirstname,
            //     lastname        : userLastname,
            //     display_name    : doctorFullName,
            //     email           : usersLoginCheck.email,
            //     phone_number    : usersLoginCheck.phone,
            //     photo           : !_.isEmpty(usersLoginCheck.photo) ? `https://static.d2d.co.id/image/profile/${usersLoginCheck.photo}` : '',
            //     type            : 'doctor',
            //     birthday        : born_date,
            //     address         : JSON.stringify(userAddress),
            //     other_info      : JSON.stringify(userOtherInfo), 
            // }
            
            // console.log(`Profile : Save userInfo Before Hit`);
            // let userInfo = await UserSVC.saveUser(usersLoginCheck.uid, userData);
            // console.log(`Profile : Save userInfo => datetime ( ${moment().format('DD MM YYYY h:mm:ss SSSSSSS')} ) ${userInfo.acknowledge}`);

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${uid}`, null)
            request.get(options, function(error, response, body) {
                if(!error) {
                    console.log(`body getProfile `, body)
                    let resp = JSON.parse(body);
                    res.status(resp.status).send(resp)
                } else {
                    console.log(error);
                    let err = new Error(error)
                    err.code = 503;
                    next(err);
                }
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: doLogin :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

LoginController.forgotPassword = async (req, res, next) => {
    console.log(`├── ${log} :: forgotPassword`);

    try {
        let email = req.body.email;
        let code = (req.body.code == null || req.body.code == '') ? null : req.body.code;
        let new_password = (req.body.new_password == null || req.body.new_password == '') ? null : req.body.new_password;
        let stage = (code == null && new_password == null) ? false : true;
        let name = ``;

        if(!stage) {
            let options = requestOptions(req.headers, null, `${CONFIG.URL_GUE_SERVICE}/user/detail/email/${email}`)
            request.get(options, function(error, response, body) {
                let result = JSON.parse(body);

                if(result.status == 500) {
                    let resp = responseCustom.v1(200, false, `Email tidak terdaftar`, [])
                    res.status(resp.status).send(resp)
                } else {
                    name = result.result[0].displayName;
                    let uid = result.result[0].uid;
                    form = {
                        email,
                        emailContent: `<p>Dear ${name},<br/><br/>Please enter this code in order to completely reset your password<br/><br/>Best wishes,<br/>D2D</p>`,
                        subject: `D2D Password Change Confirmation`,
                        apps: 'd2d'
                    }
                    let options = requestOptions(req.headers, uid, `${CONFIG.URL_GUE_SERVICE}/user/forgot_password/`, form)
                    request.post(options, function(error, response, body) {
                        let result = JSON.parse(body);

                        res.json(result);
                    })
                }
            })
        } else {
            form = {
                email,
                code,
                password: new_password
            }
            let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_GUE_SERVICE}/user/forgot_password/confirm`, form)
            request.post(options, function(error, response, body) {
                let result = JSON.parse(body);
                res.json(result);
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: forgotPassword :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = LoginController;