const _                 = require('lodash');
const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const Quiz              = require('../../models/Quiz');
const QuizQuestions     = require('../../models/QuizQuestions');
const UserSkpTmp        = require('../../models/UserSkpTmp');
const UserSkp           = require('../../models/UserSkp');
const QuizTaken           = require('../../models/QuizTaken');
const asyncrequest      = require('async-request');
const co                = require('co');
const fs                = require('fs');
const oss               = require('ali-oss');
const jwt               = require('jsonwebtoken');
const datetime          = require('node-datetime');
const request           = require('request');
const NotifService      = require('../../utils/notif-service');
const sequelize         = SEQUELIZE_INIT;
const CmeController     = {}
const log               = 'Cme controller';
const path              = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');

const UserSkpTmpModel    = require('../models/user_skp_tmp.model');
const D2DCMSSVC          = require('../../utils/d2d-cms-service');
const querystring        = require("querystring");

CmeController.quizListBackup = async (req, res, next) => {
    console.log(`├── ${log} :: Quiz-List`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let uid         = req.params.uid;
        let page        = (req.query.page == null) ? await utils.paging(1) : await utils.paging(req.query.page);
        let type        = req.params.type //all / done / available

        let done = (type == 'done') ? 'true' : (type == 'available') ? 'false' : '';
        let whereQ = '';
        let query = '';

        if(type == 'offline') {
            query = `SELECT
                    user_skp_tmp.uid,
                    user_skp_tmp.skp,
                    user_skp_tmp.title,
                    user_skp_tmp.source,
                    user_skp_tmp.taken_on,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                    user_skp_tmp.status
                    FROM
                    user_skp_tmp
                    WHERE
                    title IS NOT NULL AND
                    uid = '${uid}'
                    
                    UNION
                    
                    SELECT
                    user_skp.uid,
                    user_skp.skp,
                    user_skp.title,
                    user_skp.source,
                    user_skp.taken_on,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp.certificate) as certificate,
                    user_skp.status
                    FROM
                    user_skp
                    WHERE
                    title IS NOT NULL AND
                    uid = '${uid}'

                    ORDER BY
                    taken_on DESC
                    
                    LIMIT ${page},10`;
        } else if(type == 'all' || type == 'done' || type == 'available') {
            query = `SELECT X.*
                    
                    FROM

                    (SELECT
                    A.*,
                    B.certificate,
                    IF(B.uid IS NOT NULL, 'true', 'false') done
                    
                    FROM
                    
                    (SELECT
                    quiz.id,
                    quiz.source_id,
                    quiz.title,
                    quiz.description,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/pdf/quiz/', quiz.filename) as filename,
                    quiz.skp,
                    quiz.available_start,
                    quiz.available_end,
                    source_cme.source,
                    source_cme.description as source_description,
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/image/logo/', source_cme.logo) as logo
                    FROM
                    quiz
                    INNER JOIN source_cme ON quiz.source_id = source_cme.id) as A
                    
                    LEFT JOIN
                    
                    (SELECT
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                    user_skp_tmp.uid,
                    user_skp_tmp.status,
                    user_skp_tmp.skp,
                    user_skp_tmp.title,
                    user_skp_tmp.quiz_id
                    FROM
                    user_skp_tmp
                    WHERE
                    title IS NULL AND
                    status = 'valid' AND
                    uid = '${uid}'
                    
                    UNION
                    
                    SELECT
                    CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/','${uid}/', user_skp.certificate) as certificate,
                    user_skp.uid,
                    user_skp.status,
                    user_skp.skp,
                    user_skp.title,
                    user_skp.quiz_id
                    FROM
                    user_skp
                    WHERE
                    title IS NULL AND
                    status = 'valid' AND
                    uid = '${uid}') as B
                    
                    ON A.id = B.quiz_id
                    
                    WHERE
                    DATE_FORMAT(NOW(), '%Y-%m-%d') >= A.available_start AND
                    DATE_FORMAT(NOW(), '%Y-%m-%d') <= A.available_end) as X

                    ${(type == "done") ? "WHERE X.done = 'true'" : (type == "available") ? "WHERE X.done = 'false'" : ""}
                    
                    ORDER BY
                    X.done,
                    X.available_end

                    LIMIT ${page},10`;
        }

        sequelize.query(query).spread((results, metadata) => {
            let quiz = results

            let query = `SELECT
                uid,
                SUM(skp) as skp
                FROM
                user_skp_tmp
                WHERE
                uid = '${uid}' AND
                status = 'valid'
                GROUP BY
                uid`;

            sequelize.query(query).spread((results, metadata) => {
                let skp = results[0];
                let resp = responseCustom.v1(200, true, message, {skp, quiz})
                res.status(resp.status).send(resp)
            }).catch(error => {
                throw error
            })
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Quiz-List :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.quizList = async (req, res, next) => {
    console.log(`├── ${log} :: Quiz-List`);
    
    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {

        let uid         = req.params.uid;
        let page        = (req.query.page == null) ? await utils.paging(1) : await utils.paging(req.query.page);
        let type        = req.params.type //all / done / available

        let query       = '';
        let condition   = '';

        if(type == 'offline') {
            query = `SELECT
                        user_skp_tmp.uid, user_skp_tmp.skp, user_skp_tmp.title, user_skp_tmp.source, user_skp_tmp.taken_on,
                        CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                        user_skp_tmp.status
                    FROM user_skp_tmp
                    WHERE 1 
                        AND quiz_id IS NULL AND title IS NOT NULL AND uid = '${uid}'
                    
                    UNION
    
                    SELECT
                        user_skp.uid, user_skp.skp, user_skp.title, user_skp.source, user_skp.taken_on,
                        CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp.certificate) as certificate,
                        user_skp.status
                    FROM user_skp
                    WHERE 1 
                        AND quiz_id IS NULL AND title IS NOT NULL AND uid = '${uid}'

                    ORDER BY taken_on DESC
                    LIMIT ${page}, 10`;
        } else if(type == 'all' || type == 'done' || type == 'available') {

            if (type == 'all') {
                condition = `AND (DATE_FORMAT(NOW(), '%Y-%m-%d') >= quiz.available_start AND DATE_FORMAT(NOW(), '%Y-%m-%d') <= quiz.available_end)`;
            } else if (type == 'available') {
                condition = 'AND cert.uid IS NULL';
            } else if (type == 'done') {
                condition = 'AND cert.uid IS NOT NULL';
            }

            query = `SELECT
                        quiz.id,
                        quiz.source_id,
                        quiz.title,
                        quiz.description,
                        CONCAT('https://static.d2d.co.id/${path}pdf/quiz/', quiz.filename) as filename,
                        quiz.skp,
                        quiz.available_start,
                        quiz.available_end,
                        source_cme.source,
                        source_cme.description as source_description,
                        CONCAT('https://static.d2d.co.id/${path}image/logo/', source_cme.logo) as logo,
                        cert.certificate,
                        IF(cert.uid IS NOT NULL, 'true', 'false') done
                    FROM quiz
                    INNER JOIN quiz_locale ON quiz.id = quiz_locale.quiz_id
                    INNER JOIN source_cme ON quiz.source_id = source_cme.id
                    LEFT JOIN
                        (
                            SELECT
                                CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                                user_skp_tmp.uid,
                                user_skp_tmp.status,
                                user_skp_tmp.skp,
                                user_skp_tmp.title,
                                user_skp_tmp.quiz_id
                            FROM user_skp_tmp
                            WHERE 1 
                                AND title IS NULL
                                AND status = 'valid'
                                AND uid = '${uid}'
                                
                            UNION
                    
                            SELECT
                                CONCAT('https://static.d2d.co.id/${path}certificate/','${uid}/', user_skp.certificate) as certificate,
                                user_skp.uid,
                                user_skp.status,
                                user_skp.skp,
                                user_skp.title,
                                user_skp.quiz_id
                            FROM user_skp
                            WHERE 1 
                                AND title IS NULL
                                AND status = 'valid'
                                AND uid = '${uid}'
                        ) as cert ON cert.quiz_id = quiz.id
                    WHERE 1 AND quiz_locale.country_code = '${countrycode}' ${condition}
                    ORDER BY quiz.available_end, cert.uid IS NOT NULL
                    LIMIT ${page}, 10`;
        }

        sequelize.query(query).spread((results, metadata) => {
            let quiz = results

            let query = `SELECT
                uid,
                SUM(skp) as skp
                FROM
                user_skp_tmp
                WHERE
                uid = '${uid}' AND
                status = 'valid'
                GROUP BY
                uid`;

            sequelize.query(query).spread((results, metadata) => {
                let skp = results[0];
                let resp = responseCustom.v1(200, true, message, {skp, quiz})
                res.status(resp.status).send(resp)
            }).catch(error => {
                console.log(error);
                let err   = new Error(error)
                    err.code = 488;
                next(err);
            })
        }).catch(error => {
            console.log(error);
            let err   = new Error(error);
                err.code = 488;
            next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Quiz-List :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.quizDetail = async (req, res, next) => {
    console.log(`├── ${log} :: Quiz-Detail`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let quiz_id      = req.params.quizid;

        QuizQuestions.findAll(
            {where: { quiz_id }},
            {raw: true}
        ).then(async data => {
            let arr = []
            for(let i = 0; i < data.length; i++) {
                data[i].dataValues.answer   = unescape(data[i].dataValues.answer)
                let choices                 = JSON.parse(data[i].dataValues.choices)
                let pilihan                 = choices.map(unescape)
                data[i].dataValues.choices  = JSON.stringify(pilihan)
                //data[i].dataValues.choices  = choices.map(unescape)

                arr.push(data[i].dataValues)
            }
            
            let questions   = await utils.randomArray(arr)
            let resp        = responseCustom.v1(200, true, message, questions)
            res.status(resp.status).send(resp)
        }).catch(error => {
            console.log(error);
            let err   = new Error(error)
                err.code = 488;
            next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Quiz-Detail :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.submitOffline = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Offline`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let type        = `certificate`;
        let uid         = req.body.uid;
        let skp         = req.body.skp;
        let title       = req.body.title;
        let source      = req.body.source;
        let taken_on    = req.body.taken_on;
        
        UserSkpTmp
        .build({ uid, skp, title, source, taken_on, type: 'offline' })
        .save()
        .then(data => {
            let rowId       = data.id;
            const client    = new oss({
                region: 'oss-ap-southeast-5',
                accessKeyId: 'LTAINk4kOx5TLJT6',
                accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
                bucket: 'd2doss'
            })

            console.log(`start uploading...`);

            co(function* () {
                console.log(`mulai`);
                let arr = req.file.originalname.split('.');
                let ext = arr[(arr.length-1)];
                console.log(`${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);
                let result = yield client.put(`${path}${type}/${uid}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);

                fs.unlink(`../../fileupload/${req.file.filename}`, function(err) {
                    if(!err) {
                        UserSkpTmp.update(
                            {certificate: `${req.file.filename}.${ext}`},
                            {where: {id: rowId}}
                        ).then(() => {
                            let data =  {certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${req.file.filename}.${ext}`}
                            let resp = responseCustom.v1(200, true, message, data)
                            res.status(resp.status).send(resp)
                        }).catch(error => {
                            console.log(error);
                            let err = new Error(error)
                                err.code = 488;
                            next(err);
                        })
                    } else {
                        console.log(error);
                        let err = new Error(error)
                            err.code = 500;
                        next(err);
                    }
                })
            })
        }).catch(error => {
            console.log(error);
            let err = new Error(error)
                err.code = 500;
            next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Offline :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.resetSkp = async (req, res, next) => {
    console.log(`├── ${log} :: Reset-SKP`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let uid         = req.body.uid;
        
        UserSkpTmp.findAll(
            {where: {uid}}
        ).then(async data => {
            let arr = []
            await utils.asyncForEach(data, async (obj) => {
                delete obj.dataValues.id
                arr.push(obj.dataValues)
            })

            UserSkp.bulkCreate(arr).then(() => {
                UserSkpTmp.destroy(
                    {where: {uid}}
                ).then(() => {
                    message  = LANG[cc].CME.SKP.SUCCESS_RESET;
                    let resp = responseCustom.v1(200, true, message, null)
                    res.status(resp.status).send(resp)
                }).catch(error => {
                    console.log(error);
                    let err = new Error(error)
                        err.code = 488;
                    next(err);
                })
            }).catch(error => {
                console.log(error);
                let err = new Error(error)
                    err.code = 488;
                next(err);
            })
        }).catch(error => {
            console.log(error);
            let err = new Error(error)
                err.code = 488;
            next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Reset-SKP :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.emailCertificate = async (req, res, next) => {
    console.log(`├── ${log} :: Email-Certificate`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let uid             = req.headers.uid;

        let toEmail         = req.body.email;
        let subject         = LANG[cc].CME.CERTIFICATE.GET_CERTIFICATE
        let name            = req.body.displayName;
        let certificate     = req.body.certificate;
        let title           = req.body.title;
        let emailContent    = `<p>Dear ${name},<br/><br/>
        ${utils.stringReplace(LANG[cc].CME.CERTIFICATE.GET_CERTIFICATE_DETAIL,[title])}
        <br/><br/>Best wishes,<br/>D2D<br/><br/></p><center><a href="${certificate}" target="_blank"><button class="activation">DOWNLOAD CERTIFICATE</button></a></center>`;
        let body            = {
                                toEmail,
                                subject,
                                emailContent,
                                apps: 'd2d'
                            }
        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/email/send`, body)
        request.post(options, function(error, response, body) {
            if(error) {
                console.log(error);
                let err = new Error(error)
                    err.code = 503;
                next(err);
            } else {
                message  = LANG[cc].CME.CERTIFICATE.SEND_CERTIFICATE;
                let resp = responseCustom.v1(200, true, message, null)
                res.status(resp.status).send(resp)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Email-Certificate :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.submitQuiz = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Quiz`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let auth            = req.headers.authorization
        let plat            = req.headers.platform

        // asyncrequest        = asyncrequest.defaults({headers: {auth, plat}});
        let quizid          = req.body.quizid; //quiz id
        let uid             = req.body.uid; //uid
        let skp             = req.body.skp; //skp
        let source          = req.body.source; //source
        let ansTos          = req.body.answers.toString();
        let answer          = ansTos.replace(/\"\[/g, '[').replace(/\]\"/g, ']');
            answer          = answer.replace(/\\"/g, '"');

        // let answer          = ansTos.replace(/"\[/g, '[').replace(/\]"/g, ']');
        // answer              = answer.replace(/'\[/g, '[').replace(/\]'/g, ']');
        // answer              = answer.replace(/\\"/g, '"');
        let answers         = JSON.parse(answer); //answers
        // let score           = await scoring(quizid, answers); //score
        // let result          = await (score >= 60) ? `Y` : `N`; //result

        // async function scoring(quizid, answers) {

            // get detail quiz
            let quiz = await Quiz.findOne({ where: { id: quizid }, raw: true });

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/cme/quiz-detail/${quizid}`, null)
            request.get(options, async function(error, response, body) {
                let sortedAnswers   = await utils.sort(answers);
                let getDb           = body;
                let parsedDb        = JSON.parse(getDb);
                let dbAnswers       = await utils.sort(parsedDb.result.data);
                let score           = await utils.compare(sortedAnswers, dbAnswers);
                let result          = await (score >= 60) ? `Y` : `N`; //result

                // return await score;
                // console.log({quiz_id: quizid, uid, answers, score, result})
                let jawaban = answers.map(obj => {
                    let jawab = {
                                    id: obj.id,
                                    question: obj.question,
                                    answer: obj.answer,
                                    choices: obj.choices
                                }

                    jawab.answer = escape(obj.answer)
                    let pilihan = obj.choices
                    jawab.choices = JSON.stringify(pilihan.map(escape))

                    return jawab
                })

                // console.log({quiz_id: quizid, uid, answers: jawaban, score, result, detail: JSON.stringify(parsedDb)})

                QuizTaken.build(
                    {quiz_id: quizid, uid, answers: `${JSON.stringify(jawaban)}`, result}
                ).save().then( async () => {
                    if(result == 'Y') {
                        let conditions = [
                            { jointer: 'AND', key: `quiz_id`, value: quizid, op: '=' },
                            { jointer: 'AND', key: `uid`, value: uid, op: '=' }
                        ]
                        let certifExist = await UserSkpTmpModel.getBy(conditions,[],[],2,[])
                        //update data if certificate exist, and insert if not exist.
                        let isInsert = _.isEmpty(certifExist);

                        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
                        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 360});
                        let form        = {uid, quiz: quizid}

                        let createCertResult = await D2DCMSSVC.certificate.create(uid, querystring.stringify(form), token)
                        // console.log("certif create",createCertResult,uid, querystring.stringify(form),token);
                        // let result   = JSON.parse( await response.body);
                        let certificate = await createCertResult.data.split('/');
                        let dt          = datetime.create();
                        let formatted   = dt.format('Y-m-d H:M:S');

                        let dataSkpTmp = [
                            { key: 'uid', value: uid },
                            { key: 'skp', value: skp },
                            { key: 'source', value: source },
                            { key: 'taken_on', value: formatted },
                            { key: 'certificate', value: certificate[1] },
                            { key: 'quiz_id', value: quizid },
                            { key: 'type', value: 'cme' },
                            { key: 'status', value: 'valid' },
                        ];
                        if(isInsert == true) conditions = [];
                        let createUserSkpTmpRes = await UserSkpTmpModel.save(dataSkpTmp,conditions)

                        if(isInsert == true){
                            let notif   = {
                                title           : quiz.skp <= 0 ? 
                                    LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE : 
                                    utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE_DETAIL,[quiz.skp]),
                                body            : LANG[cc].CME.EMAIL_CERTIFICATE.ACCESS_NOTIFY,
                                type            : 'certificate_cme',
                                content_id      : `${quizid}`
                            };
                            
                            let data    = {
                                'type'          : notif.type,
                                'content_id'    : notif.content_id,
                                'certificate'   : ''
                            };

                            // sending notification
                            let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);
                            console.log(`├─ ${log} :: ecertificate :: send notification`);
                            message  = utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.SUCCESS_TAKE_QUIZ,[skp]);
                        }
                        
                        let resp = responseCustom.v1(200, true, message, {certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${certificate[1]}`})
                        res.status(resp.status).send(resp);
                    } else {
                        message  = LANG[cc].CME.EMAIL_CERTIFICATE.TRY_AGAIN;
                        let resp = responseCustom.v1(200, false, message, null)
                        res.status(resp.status).send(resp)
                    }
                }).catch(error => {
                    console.log(error);
                    let err = new Error(error)
                        err.code = 488;
                    next(err);
                })
            })
        // }
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Quiz :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

CmeController.submitQuizBackup = async (req, res, next) => {
    console.log(`├── ${log} :: Submit-Quiz`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;
    try {
        let auth            = req.headers.authorization
        let plat            = req.headers.platform

        // asyncrequest        = asyncrequest.defaults({headers: {auth, plat}});
        let quizid          = req.body.quizid; //quiz id
        let uid             = req.body.uid; //uid
        let skp             = req.body.skp; //skp
        let source          = req.body.source; //source
        let ansTos          = req.body.answers.toString();
        let answer          = ansTos.replace(/\"\[/g, '[').replace(/\]\"/g, ']');
            answer          = answer.replace(/\\"/g, '"');

        // let answer          = ansTos.replace(/"\[/g, '[').replace(/\]"/g, ']');
        // answer              = answer.replace(/'\[/g, '[').replace(/\]'/g, ']');
        // answer              = answer.replace(/\\"/g, '"');
        let answers         = JSON.parse(answer); //answers
        // let score           = await scoring(quizid, answers); //score
        // let result          = await (score >= 60) ? `Y` : `N`; //result

        // async function scoring(quizid, answers) {

            // get detail quiz
            let quiz = await Quiz.findOne({ where: { id: quizid }, raw: true });

            let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/cme/quiz-detail/${quizid}`, null)
            request.get(options, async function(error, response, body) {
                let sortedAnswers   = await utils.sort(answers);
                let getDb           = body;
                let parsedDb        = JSON.parse(getDb);
                let dbAnswers       = await utils.sort(parsedDb.result.data);
                let score           = await utils.compare(sortedAnswers, dbAnswers);
                let result          = await (score >= 60) ? `Y` : `N`; //result

                // return await score;
                // console.log({quiz_id: quizid, uid, answers, score, result})
                let jawaban = answers.map(obj => {
                    let jawab = {
                                    id: obj.id,
                                    question: obj.question,
                                    answer: obj.answer,
                                    choices: obj.choices
                                }

                    jawab.answer = escape(obj.answer)
                    let pilihan = obj.choices
                    jawab.choices = JSON.stringify(pilihan.map(escape))

                    return jawab
                })

                // console.log({quiz_id: quizid, uid, answers: jawaban, score, result, detail: JSON.stringify(parsedDb)})

                QuizTaken.build(
                    {quiz_id: quizid, uid, answers: `${JSON.stringify(jawaban)}`, result}
                ).save().then(() => {
                    if(result == 'Y') {
                        const payload   = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
                        const token     = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', {expiresIn: 360});
                        let form        = {uid, quiz: quizid}
                        let options     = requestOptions(req.headers, uid, `${CONFIG.URL_CMS_D2D}/api/v1/certificate/create`, form)
                        request.post(options, async function(error, response, body) {
                            if(!error) {
                                let result      = JSON.parse(response.body);
                                console.log(result)
                                if (result.data != null) {
                                    let certificate = result.data.split('/');
                                    let dt          = datetime.create();
                                    let formatted   = dt.format('Y-m-d H:M:S');

                                    UserSkpTmp.build(
                                        {uid, skp, source, taken_on: formatted, certificate: certificate[1], quiz_id: quizid, type: 'cme', status: 'valid'}
                                    ).save().then(async () => {
                                        // sending notification
                                        let notif   = {
                                            title           : quiz.skp <= 0 ? 
                                                LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE : 
                                                utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.GET_CERTIFICATE_DETAIL,[quiz.skp]),
                                            body            : LANG[cc].CME.EMAIL_CERTIFICATE.ACCESS_NOTIFY,
                                            type            : 'certificate_cme',
                                            content_id      : `${quizid}`
                                        };
                                        
                                        let data    = {
                                            'type'          : notif.type,
                                            'content_id'    : notif.content_id,
                                            'certificate'   : ''
                                        };

                                        // sending notification
                                        let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);
                                        console.log(`├─ ${log} :: ecertificate :: send notification`);
                                        
                                        message  = utils.stringReplace(LANG[cc].CME.EMAIL_CERTIFICATE.SUCCESS_TAKE_QUIZ,[skp]);
                                        let resp = responseCustom.v1(200, true, message, {certificate: `https://static.d2d.co.id/${path}certificate/${uid}/${certificate[1]}`})
                                        res.status(resp.status).send(resp)
                                    }).catch(error => {
                                        throw error
                                    })
                                } else {
                                    message  = LANG[cc].CME.EMAIL_CERTIFICATE.FAILED_CREATED;
                                    let resp = responseCustom.v1(200, false, message, null);
                                    res.status(resp.status).send(resp)
                                }
                            } else {
                                throw error
                            }
                        })
                    } else {
                        message  = LANG[cc].CME.EMAIL_CERTIFICATE.TRY_AGAIN;
                        let resp = responseCustom.v1(200, false, message, null)
                        res.status(resp.status).send(resp)
                    }
                }).catch(error => {
                    throw error
                })
            })
        // }
    } catch(error) {
        console.log(`│  ├── ${log} :: Submit-Quiz :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = CmeController;