const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const generateToken         = require('../../utils/helpers/generate-token');
const request               = require('request');
const _                     = require('lodash');
const API_REQUEST           = require('../../utils/api-request');

const ActivityController    = {}
const log                   = 'Activity controller';

ActivityController.insert = async (req, res, next) => {
    console.log(`├── ${log} :: insert`);

    try {
        let uid = req.headers.uid;
        if(_.isEmpty(uid)){
            uid = req.body.uid;
        }
        // let form = {
        //     act: req.body.act,
        //     content: req.body.content,
        //     contentId: req.body.contentId
        // }
        let payload = {
            activity: req.body.act,
            content: req.body.content,
            content_id: req.body.contentId,
            uid,
            app:1
        }
        let BASE_URL_V2 = `${process.env.CONTENTSVC_URL}/${process.env.CONTENTSVC_VERSION}`
        // let options = requestOptions(req.headers, uid, `${url}/useractivity`, form);
        let headers = { platform: 'android' }
        let TOKEN = generateToken(uid, 'content', headers.platform);
        let URL   = `useractivity`;
        
        let record = await API_REQUEST(BASE_URL_V2, TOKEN, []).post(URL, payload);

        if (record.data.acknowledge == false) {
            console.log('Error: ', record);
        }
        if (record.data.message == 'No data found!') {
            console.log('Error: ', record);
        }
        obj = {}
        obj.status = record.status
        obj.result = {}
        obj.result.acknowledge = record.status == 200 ? true : false
        obj.result.message = record.data.result == [] ? 'No data found' : 'Success' 
        obj.result.data = record.data.message == 'No data found!' ? [] : record.data.result

        res.status(record.status).send(obj)
        // request.post(options, function(error, response, body) {
        //     if(!error) {
        //         const hasil = JSON.parse(body)
        //         res.status(hasil.status).send(hasil)
        //     } else {
        //         console.log(error)
        //         let err   = new Error(error)
        //         err.code  = 503
        //         next(err)
        //     }
        // })
    } catch(error) {
        console.log(`│  ├── ${log} :: insert :: error`);
        // console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ActivityController;