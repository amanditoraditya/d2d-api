const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const requestOptions        = require('../../utils/request-options');
const responseCustom        = require('../../utils/response-custom');
const EventAttendee         = require('../../models/EventAttendee');
const request               = require('request');
const sequelize             = SEQUELIZE_INIT;
const path              = CONFIG.API_ENVIRONMENT == 'production' ? '' : 'dev/';

const RedirectController    = {}
const log                   = 'Redirect controller';

RedirectController.redirect = async (req, res, next) => {
    console.log(`├── ${log} :: redirect`);

    try {
        let type        = req.params.type; // = bisa signin
        let slug        = req.params.slug; // = event kalau signin
        let slug_hash   = req.params.slug_hash;
        let uid         = req.params.uid;
        let npaoremail  = req.params.npaoremail;
        
        if(type == 'cme') {
            let query = `SELECT X.*

                            FROM
                            
                            (SELECT
                            A.*,
                            B.certificate,
                            IF(B.uid IS NOT NULL, 'true', 'false') done
                            
                            FROM
                            
                            (SELECT
                            quiz.id,
                            quiz.source_id,
                            quiz.title,
                            quiz.description,
                            CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/${path}pdf/quiz/', quiz.filename) as filename,
                            quiz.skp,
                            quiz.available_start,
                            quiz.available_end,
                            source_cme.source,
                            source_cme.description as source_description,
                            CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/${path}image/logo/', source_cme.logo) as logo
                            FROM
                            quiz
                            INNER JOIN source_cme ON quiz.source_id = source_cme.id) as A
                            
                            LEFT JOIN
                            
                            (SELECT
                            CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/${path}certificate/','${uid}/', user_skp_tmp.certificate) as certificate,
                            user_skp_tmp.uid,
                            user_skp_tmp.status,
                            user_skp_tmp.skp,
                            user_skp_tmp.title,
                            user_skp_tmp.quiz_id
                            FROM
                            user_skp_tmp
                            WHERE
                            title IS NULL AND
                            status = 'valid' AND
                            uid = '${uid}'
                            
                            UNION
                            
                            SELECT
                            CONCAT('https://d2doss.oss-ap-southeast-5.aliyuncs.com/${path}certificate/','${uid}/', user_skp.certificate) as certificate,
                            user_skp.uid,
                            user_skp.status,
                            user_skp.skp,
                            user_skp.title,
                            user_skp.quiz_id
                            FROM
                            user_skp
                            WHERE
                            title IS NULL AND
                            status = 'valid' AND
                            uid = '${uid}') as B
                            
                            ON A.id = B.quiz_id
                            
                            WHERE
                            DATE_FORMAT(NOW(), '%Y-%m-%d') >= A.available_start AND
                            DATE_FORMAT(NOW(), '%Y-%m-%d') <= A.available_end) as X
                            
                            WHERE 1
                            AND X.id = ${slug_hash}
                            
                            ORDER BY
                            X.done,
                            X.available_end`;
            console.log(query);
            sequelize.query(query).spread((results, metadata) => {
                let resp = responseCustom.v1(200, true, null, results[0])
                res.status(resp.status).send(resp)
            }).catch(error => {
                console.log(error)
                let err   = new Error(error)
                err.code  = 488
                next(err)
            })
        } else {
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/redirect/${type}/${slug}/${slug_hash}/${uid}`)
            request.get(options, function(error, response, body) {
                if (!error) {
                    let result = JSON.parse(body)
                    if(type == 'event' && npaoremail != null) {

                        let kunci = (npaoremail.includes('@')) ? {event_slug_hash: slug_hash, email: npaoremail} : {event_slug_hash: slug_hash, npa_idi: npaoremail} 

                        EventAttendee.findAll(
                            {where: kunci}
                        ).then(data => {
                            if(data == '' || data == null) {
                                result.result.data[0].attendee = false
                            } else {
                                result.result.data[0].attendee = true
                            }

                            res.status(result.status).send(result)
                        }).catch(error => {
                            console.log(error)
                            let err   = new Error(error)
                            err.code  = 488
                            next(err)
                        })
                    } else {
                        res.status(result.status).send(result)
                    }
                } else {
                    console.log(error)
                    let err   = new Error(error)
                    err.code  = 503
                    next(err)
                }
            })
        }
    } catch(error) {
        console.log(`│  ├── ${log} :: redirect :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

RedirectController.temandiabetesGraph = async (req, res, next) => {
    console.log(`├── ${log} :: temandiabetesGraph`);

    try {
        let patient_uid = req.params.patient_uid;
        let doctor_uid  = req.params.doctor_uid;
        
        let options = requestOptions(req.headers, doctor_uid, `${CONFIG.URL_TD}/get-link-d2dQR/${patient_uid}/${doctor_uid}`, null)
        request.get(options, function(error, response, body)  {
            if(!error) {
                if(body == null || body == "") {
                    console.log(error)
                    let err   = new Error(error)
                    err.code  = 404
                    next(err)
                } else {
                    let hasil = JSON.parse(body)
                    let resp = responseCustom.v1(hasil.status, hasil.acknowledge, hasil.message, hasil.link)

                    let form = {
                        act: 'view',
                        content: 'td_graph',
                        contentId: patient_uid
                    }
            
                    let options = requestOptions(req.headers, doctor_uid, `${CONFIG.URL_CONTENT}/user-activity`, form);
            
                    request.post(options, function(error, response, body) {
                        if(!error) {
                            console.log(body)
                            res.status(resp.status).send(resp)
                        } else {
                            console.log(error)
                            let err   = new Error(error)
                            err.code  = 503
                            next(err)
                        }
                    })
                }
            } else {
                console.log(error)
                let err   = new Error(error)
                err.code  = 503
                next(err)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: temandiabetesGraph :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = RedirectController;