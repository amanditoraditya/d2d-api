const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const responseCustom    = require('../../utils/response-custom');
const requestOptions    = require('../../utils/request-options');
const EventReservation  = require('../../models/EventReservation');
const EventAttendee     = require('../../models/EventAttendee');
const User              = require('../../models/User');
const request           = require('request');
const _                 = require('lodash');
const dateTime          = require('node-datetime');
const Sequelize         = require('sequelize');
const { Op }            = require('sequelize');
const sequelize         = SEQUELIZE_INIT;
const contentAccess     = require('../../utils/content-access');
const NotifService      = require('../../utils/notif-service');
const axios             = require('axios');
const {COUNTRY_CODES, LANG, lANGUAGE_TYPE} = require('../../utils/helpers/localization');
const Utils = require('../../utils/utils');

const EventController        = {}
const log                   = 'Event controller';

EventController.event  = async (req, res, next) => {
    console.log(`├── ${log} :: event`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let bookmark    = (req.query.bookmark == null || req.query.bookmark == ``) ? `` : req.query.bookmark;
        let reserve     = (req.query.reserve == null || req.query.reserve == ``) ? `` : req.query.reserve;
        let page        = (req.query.page == null) ? '1' : req.query.page;
        let search      = (req.query.search == null || req.query.search == ``) ? `` : req.query.search;

        let options = requestOptions(req.headers, req.headers.uid, 
            `${CONFIG.URL_CONTENT}/event?page=${page}&bookmark=${bookmark}&reserve=${reserve}&search=${search}`, null)
            
        request.get(options, function(error, response, body) {
            if(!error) {
                let hasil = JSON.parse(body)
                res.status(hasil.status).send(hasil)
            } else {
                console.log(error);
                let err = new Error(error)
                err.code = 503;
                next(err);
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: event :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.slug  = async (req, res, next) => {
    console.log(`├── ${log} :: slug`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let slug        = req.params.slug;
        let slug_hash   = req.params.slug_hash;
        let uid         = req.params.uid;

        let options = requestOptions(req.headers, req.headers.uid, `${CONFIG.URL_CONTENT}/event/${slug}/${slug_hash}/${uid}`, null)
        request.get(options, function(error, response, body) {
            if (!error) {
                let eventDetail = JSON.parse(body);
                eventDetail     = eventDetail.result.data[0];
                
                if (eventDetail.id != null) {
                    // get materi event
                    let eventMateriOptions  = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/materi/${eventDetail.id}`, null)
                    request.get(eventMateriOptions, async function(error, response, body) {
                        //materi default
                        eventDetail.materi  = [];

                        // shange contact string to array and remove null 
                        eventDetail.cp_name = _.isEmpty(eventDetail.cp_name) ? [] : _.without(eventDetail.cp_name.split('"'), '[', ']', ',', '');
                        eventDetail.cp_number = _.isEmpty(eventDetail.cp_number) ? [] : _.without(eventDetail.cp_number.split('"'), '[', ']', ',', '');
                        eventDetail.cp_email = _.isEmpty(eventDetail.cp_email) ? [] : _.without(eventDetail.cp_email.split('"'), '[', ']', ',', '');
                        
                        
                        let query = `SELECT * FROM user_certificate WHERE uid = '${uid}' AND event_id = '${eventDetail.id}' AND status = 'A' ORDER BY created DESC LIMIT 1`;
                        let certificate = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
                        if (certificate) {
                            eventDetail.certificate = certificate[0];
                        }

                        let queryVerified = `SELECT * FROM event_attendee as ae INNER JOIN user as us ON us.email = ae.email AND ae.type = 'materi' AND ae.event_slug_hash = '${slug_hash}' AND ae.verified = '1' AND us.uid = '${uid}' LIMIT 1`;
                        let isVerified = await sequelize.query(queryVerified, { type: sequelize.QueryTypes.SELECT });

                        eventDetail.isVerified = 0;
                        
                        if (isVerified.length > 0) {
                            eventDetail.isVerified = isVerified[0].verified;
                        }

                        let showSertif = `SELECT * FROM event_attendee as ae INNER JOIN user as us ON us.email = ae.email AND ae.type = 'materi' AND ae.event_slug_hash = '${slug_hash}' AND us.uid = '${uid}' LIMIT 1`;
                        let showisSertif = await sequelize.query(showSertif, { type: sequelize.QueryTypes.SELECT });
                        
                        console.log("show certif => ",showisSertif.length);
                        if (showisSertif.length <= 0) {
                            eventDetail.showCertificate = 0;
                        } else {
                                let eventMateri = JSON.parse(body);
                                eventMateri = eventMateri.result.data;
                                
                                eventDetail.materi  = eventMateri;
                            
                        }

                        console.log(`│  ├── ${log} :: slug :: success`);
                        console.log(eventDetail);
                        let resp = responseCustom.v1(200, true, message, eventDetail)
                        res.status(resp.status).send(resp)
                    })
                } else {
                    console.log(`│  ├── ${log} :: slug :: success :: event not found`);
                    message  = LANG[cc].EVENT.NOT_FOUND;
                    let resp = responseCustom.v1(200, false, message, null)
                    res.status(resp.status).send(resp)
                }
            } else {
                console.log(error);
                let err = new Error(error)
                err.code = 503;
                next(err);
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: slug :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.reserve  = async (req, res, next) => {
    console.log(`├── ${log} :: reserve`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let uid         = req.body.uid;
        let event_id    = req.body.event_id;
        let event_name  = req.body.event_name;
        let type        = req.params.type;

        if(type == 'add') {
            EventReservation.build(
                {uid, event_id, event_name}
            ).save().then(data => {
                aktifitas(data)
            }).catch(error => {
                console.log(error);
                let err = new Error(error)
                err.code = 488;
                next(err);
            })
        } else if(type == 'delete') {
            EventReservation.destory(
                {where: {uid}}
            ).then(data => {
                aktifitas(data)
            }).catch(error => {
                console.log(error);
                let err = new Error(error)
                err.code = 488;
                next(err);
            })
        }

        async function aktifitas(data) {
            let act = (type == `add`) ? `reserve`: `unreserve`;
            let form    = {act, content: 'event', contentId: event_id}
            let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form)
            request.post(options, function(error, response, body) {
                let mess    = (type == `add`) ? `Reservasi berhasil` : `Reservasi telah dihapus`;
                let resp    = responseCustom.v1(200, true, mess, data)
                res.status(resp.status).send(resp)
            })
        }        
    } catch(error) {
        console.log(`│  ├── ${log} :: reserve :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.backupsignin  = async (req, res, next) => {
    console.log(`├── ${log} :: signin`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let slug_hash   = req.body.slug_hash;
        let uid         = req.params.uid;
        let email       = req.body.email;

        EventAttendee.findAll(
            {where: {event_slug_hash: slug_hash, email, type: 'materi'}}
        ).then(data => {
            if(data != null || data != '') {
                let event_name  = ``;
                let name        = ``;
                let options     = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/event/${slug_hash}/${uid}`, null)
                request.get(options, function(error, response, body){
                    console.log(body)
                    let result              = JSON.parse(body)
                    let event_start_date    = result.result.data[0].start_date;
                    let event_end_date      = result.result.data[0].end_date;
                    let tgl                 = datetime.create().format('Y-m-d');
                    let available           = (tgl >= event_start_date && tgl <= event_end_date) ? true : false;
                    let errMsg              = (tgl > event_end_date) ? `${LANG[cc].EVENT.SIGN_IN.HAS_END},toast` : (tgl < event_start_date) ? `${LANG[cc].EVENT.SIGN_IN.NOT_START},toast` : ``;
                    
                    event_name = result.result.data[0].title;

                    if(available) {
                        let options = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${npaoremail}`, null)
                        request.get(options, function(error, response, body) {
                            let hasil   = JSON.parse(body)
                            let email   = hasil.result[0].email
                            name        = hasil.result[0].name
                            
                            EventAttendee.build(
                                {event_slug_hash, event_name, name, email, type: 'absent'}
                            ).then(data => {
                                let form    = {act: 'bookmark', content: 'event', contentId: result.result.data[0].id}
                                let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form)
                                request.post(options, function(error, response, body) {
                                    if(!error) {
                                        message  = LANG[cc].EVENT.SIGN_IN.SUCCESS;
                                        let resp = responseCustom.v1(200, true,message, result.result.data)
                                        res.status(resp.status).send(resp)
                                    } else  {
                                        throw error
                                    }
                                })
                            }).catch(error => {
                                console.log(error);
                let err = new Error(error)
                err.code = 488;
                next(err);
                            })
                        })
                    } else {
                        let resp = responseCustom.v1(200, true, errMsg, result.result.data)
                        res.status(resp.status).send(resp)
                    }
                })
            } else {
                message  = LANG[cc].EVENT.SIGN_IN.NOT_REGISTERED;
                let resp = responseCustom.v1(200, false, `${message},popup`, null)
                res.status(resp.status).send(resp)
            }
        }).catch(error => {
            throw error
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: signin :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.signin  = async (req, res, next) => {
    console.log(`├── ${log} :: signin`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let slug_hash   = req.body.slug_hash;
        let email       = req.body.email;
        let uid         = req.params.uid;

        // check event
        let eventOptions     = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/event/get-data/${slug_hash}`, null)
        request.get(eventOptions, function(error, response, body) {
            if (error) {
                let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                next(err);
            } else {
                let event   = JSON.parse(body);
                event       = event.result.data[0];
                
                if (!_.isEmpty(event))
                {
                    let event_start_date    = event.start_date;
                        event_start_date    = dateTime.create(event_start_date).format('Y-m-d 00:00:00');
                    let event_end_date      = event.end_date;
                        event_end_date      = dateTime.create(event_end_date).format('Y-m-d 23:59:59');
                    let today               = dateTime.create().format('Y-m-d H:M:S');
                    let available           = (today >= event_start_date && today <= event_end_date) ? true : false;

                    console.log(`today: ${today}`)
                    console.log(`start: ${event_start_date}`)
                    console.log(`end: ${event_end_date}`)
                    console.log(`available: ${available}`)

                    if (available == true) 
                    {
                        // get event detail
                        let eventDetailOptions  = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/event/${event.slug}/${slug_hash}/${uid}`, null)
                        request.get(eventDetailOptions, function(error, response, body) {
                            if (!error) 
                            {
                                let eventDetail = JSON.parse(body);
                                eventDetail     = eventDetail.result.data[0];

                                // get materi event
                                let eventMateriOptions  = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/materi/${event.id}`, null)
                                request.get(eventMateriOptions, function(error, response, body) {
                                    if (error) {
                                        eventDetail.materi  = [];
                                    } else {
                                        let eventMateri = JSON.parse(body);
                                        eventMateri = eventMateri.result.data;
                                        
                                        eventDetail.materi  = eventMateri;
                                    }
                                    
                                    // get profile
                                    let profileOptions = requestOptions(req.headers, uid, `${CONFIG.URL_D2D}/profile/${uid}`, null)
                                    request.get(profileOptions, async (error, response, body) => {
                                        if (error) {
                                            let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                                            next(err);
                                        } 
                                        else 
                                        {
                                            let profile = JSON.parse(body);
                                            profile     = profile.result.data;
                                            
                                            if (!_.isEmpty(profile)) {
                                                // checking event attendee
                                                // EventAttendee
                                                //     .findAll({ where: { event_slug_hash: slug_hash, email, type: 'materi' } })
                                                //     .then(attendee => {
                                                        
                                                        // checking user is reistered on event
                                                        // if (!_.isEmpty(attendee)) {

                                                            // checking has absent today?
                                                            let hasAbsent       = `SELECT * FROM event_attendee WHERE DATE_FORMAT(createdAt, '%Y-%m-%d') = '${today}' AND event_slug_hash = '${slug_hash}' AND type = 'absent' AND email = '${email}' LIMIT 1`;
                                                            let existWhitelist  = `SELECT * FROM event_attendee WHERE 1 AND event_slug_hash = '${slug_hash}' AND type = 'materi' AND email = '${email}' LIMIT 1`;
                                                            let whitelist       = await sequelize.query(existWhitelist, { type: Sequelize.QueryTypes.SELECT });
                                                            
                                                            sequelize.query(hasAbsent, { type: Sequelize.QueryTypes.SELECT })
                                                                .then((record) => {
                                                                    if (_.isEmpty(record)) {
                                                                        // absent on event

                                                                        let absentData  = [
                                                                            {event_slug_hash: slug_hash, event_name: eventDetail.title, name: profile.name, email, type: 'absent', need_verified: event.is_close}
                                                                        ];

                                                                        if (_.isEmpty(whitelist)) {
                                                                            absentData.push({event_slug_hash: slug_hash, event_name: eventDetail.title, name: profile.name, email, type: 'materi', need_verified: event.is_close})
                                                                        }
                                                                        
                                                                        EventAttendee
                                                                            .bulkCreate(absentData)
                                                                            .then(data => {

                                                                                // save activity join
                                                                                let form    = {act: 'join', content: 'event', contentId: event.id}
                                                                                let activityOptions = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity`, form)
                                                                                request.post(activityOptions, function(error, response, body) {
                                                                                    if(!error) {
                                                                                        console.log(`│  ├── ${log} :: signin :: success`);
                                                                                        message = LANG[cc].EVENT.SIGN_IN.SUCCESS;
                                                                                        let resp = responseCustom.v1(200, true, message, eventDetail)
                                                                                        res.status(resp.status).send(resp)
                                                                                    } else  {
                                                                                        console.log(error);
                                                                                        let err = new Error(error)
                                                                                        err.code = 503;
                                                                                        next(err);
                                                                                    }
                                                                                })
                                                                            })
                                                                            .catch(error => {
                                                                                console.log(error)
                                                                                let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                                                                                next(err);
                                                                            })
                                                                    } else {
                                                                        // has absent on event
                                                                        message = LANG[cc].EVENT.SIGN_IN.ALREADY_IN
                                                                        let resp = responseCustom.v1(200, true, message, eventDetail)
                                                                        console.log(`│  ├── ${log} :: signin :: success :: have absent on event`);
                                                                        res.status(resp.status).send(resp)
                                                                    }
                                                                })
                                                                .catch(error => {
                                                                    let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                                                                    next(err);
                                                                })
                                                        // }
                                                        // else
                                                        // {
                                                        //     // not registered on event
                                                        //     let resp = responseCustom.v1(200, true, `Anda tidak terdaftar dalam event ini!,toast`, null)
                                                        //     console.log(`│  ├── ${log} :: signin :: success :: not registered on event`);
                                                        //     res.status(resp.status).send(resp)
                                                        // }
                                                    // })
                                                    // .catch(error => {
                                                    //     let err     = new Error('Somthing went wrong when get the data.')
                                                    //     next(err);
                                                    // })
                                            }
                                            else 
                                            {
                                                // not registered on d2d
                                                message = `${LANG[cc].EVENT.SIGN_IN.NOT_REGISTERED},toast`;
                                                let resp = responseCustom.v1(200, true, message, null)
                                                console.log(`│  ├── ${log} :: signin :: success :: not registered on d2d`);
                                                res.status(resp.status).send(resp)
                                            } // endif profile exist
                                        } // endif get profile
                                    })
                                })
                            }
                            else
                            {
                                let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                                next(err);
                            } // endif get event detail
                        });
                    } // endif avialable 
                    else 
                    {
                        let resp = {};
                        if (today > event_end_date) {
                            resp = responseCustom.v1(200, true, LANG[cc].EVENT.SIGN_IN.HAS_END, null);
                        } else if  (today < event_start_date) {
                            resp = responseCustom.v1(200, true, LANG[cc].EVENT.SIGN_IN.NOT_START, null);
                        }
                        res.status(resp.status).send(resp)
                    }
                } // endif event is exist
                else
                {
                    // not registered on event
                    message = `${LANG[cc].EVENT.NOT_FOUND},toast`;
                    let resp = responseCustom.v1(200, true, message, null)
                    console.log(`│  ├── ${log} :: signin :: failed :: event not found`);
                    res.status(resp.status).send(resp)
                } 
            }
        })
        
    } catch(error) {
        console.log(`│  ├── ${log} :: signin :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.registerEvent  = async (req, res, next) => {
    console.log(`├── ${log} :: registrasiEvent`);

    let {countrycode}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let email       = req.body.email;

        User.findOne(
            {where: {email}}
        ).then(async data => {
            if(data == null || data == '' || data == 'null') {
                res.redirect('https://www.d2d.co.id/internalpractice?status=false');
            } else {
                let event_name  = `EMERGENCY CASES IN INTERNAL MEDICINE PRACTICE 2019`;
                let event_slug_hash = 'NjQw';

                EventAttendee
                .bulkCreate([
                    {event_slug_hash: 'NjQw', event_name: 'EMERGENCY CASES IN INTERNAL MEDICINE PRACTICE 2019', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'NjY5', event_name: 'Diagnostic  Approach in Patients with Dyspnea - Practice', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'Njcw', event_name: 'Convenient Way to Update Medical Knowledge with Digital Application - Practice', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'Njcx', event_name: 'WS Endocrine : Diabetes Care & Insulin - Practice', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'Njcy', event_name: 'WS Gastroenterohepatology - Practice', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'Njcz', event_name: 'WS Cardiovascular - Practice', name: data.name, email, type: 'materi'},
                    {event_slug_hash: 'Njc0', event_name: 'WS Emergency in Hematology - Oncology - Practice', name: data.name, email, type: 'materi'},
                ])
                .then(data => {
                    res.redirect('https://www.d2d.co.id/internalpractice?status=true');
                })
                .catch(error => {
                    console.log(error)
                    let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG)
                    next(err);
                })
            }
        }).catch(error => {
            console.log(error);
                let err = new Error(error)
                err.code = 488;
                next(err);
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: registrasiEvent :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.ecertificate  = async (req, res, next) => {
    console.log(`├── ${log} :: ecertificate`);

    let {countrycode}  = req.headers;
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message     = LANG[cc].GENERAL.SUCCESS;

    try {
        let { event_id, fullname }       = req.body;
        let uid     = req.body.uid;
        let user    = await User.findOne({ where: { uid } });
        let url     = contentAccess.CmsContentRequest(req, 'event/certificate')
        
        fullname = _.replace(fullname, new RegExp(/\'/gm), `\\'`);
        
        if (user) {
            const headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
            
            request.post(url, {headers: headers, form: req.body}, async function(error, response, body) {
                if (error) {
                    let err     = new Error(LANG[cc].GENERAL.SOMETHING_WENT_WRONG);
                    next(err);
                } else {
                    let result  = JSON.parse(body);
                    console.log(result.data);

                    if (!_.isEmpty(result.data)) {
                        // event detail
                        let event = {
                            skp         : result.data.skp == null ? 0 : result.data.skp,
                            title       : _.replace(result.data.event_title, `'`, `''`),
                            certificate : result.data.filename,
                        }
                        
                        // insert user certificate
                        let queryCertificate = `INSERT INTO user_certificate 
                                (uid, event_id, event_title, fullname, filename, type, status, created) 
                                    VALUES 
                                ('${uid}', '${event_id}', '${event.title}', '${fullname}', '${event.certificate}', 'event','A', NOW())`;
                        await sequelize.query(queryCertificate);
                        
                        // check existing skp
                        let queryCekSkp = `SELECT * FROM user_skp_tmp WHERE uid = '${uid}' AND quiz_id = '${event_id}' AND type = 'event'`;
                        let existingSkp = await sequelize.query(queryCekSkp, { type: sequelize.QueryTypes.SELECT });
                        
                        if (!_.isEmpty(existingSkp)) {
                            // update user skp
                            let querySkp = `UPDATE user_skp_tmp 
                                SET title = '${event.title}', skp = ${event.skp}, certificate = '${event.certificate}' 
                                WHERE uid = '${uid}' AND quiz_id = '${event_id}' AND type = 'event'`;
                            await sequelize.query(querySkp);
                        } else {
                            // insert user skp
                            let querySkp = `INSERT INTO user_skp_tmp  
                                (uid, skp, title, source, taken_on, certificate, quiz_id, type, status, create_date)
                                    VALUES 
                                ('${uid}', '${event.skp}', '${event.title}', 'D2D', NOW(), '${event.certificate}', ${event_id}, 'event', 'valid', NOW())`;
                            await sequelize.query(querySkp);
                        }
                        
                        let notifTitle = event.skp <= 0 ? LANG[cc].EVENT.CERTIFICATE.GET_CERTIFICATE : Utils.stringReplace(LANG[cc].EVENT.CERTIFICATE.GET_SKP_CERTIFICATE,[event.skp, event.title])
                        notifTitle = notifTitle.length > 100 ? notifTitle.substring(0,96) + '...' : notifTitle;

                        // sending notification
                        let notif   = {
                            title           : notifTitle,
                            body            : LANG[cc].EVENT.CERTIFICATE.ACCESS_NOTIFY,
                            type            : 'certificate_event',
                            content_id      : `${event_id}`
                        };
                        console.log(notif);
                        let data    = {
                            'type'          : notif.type,
                            'content_id'    : notif.content_id,
                            'certificate'   : ''
                        };

                        // sending notification
                        let notification = await NotifService.addNotification(uid, notif.type, notif.title, notif.body, data);
                        console.log(`├─ ${log} :: ecertificate :: send notification`);
                    }
                    let resp = responseCustom.v1(200, result.data != null, `${result.message},toast`, result.data)
                    res.status(resp.status).send(resp)
                }
            })
        } else {
            let resp = responseCustom.v1(200, false, `${LANG[cc].EVENT.CERTIFICATE.ACCESS_DENIED},toast`, null)
            res.status(resp.status).send(resp)
        }

    } catch(error) {
        console.log(`│  ├── ${log} :: ecertificate :: error`);
        console.log(error);

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

EventController.emailCertificate = async (req, res, next) => {
    console.log(`├── ${log} :: Email-Certificate`);

    let {countrycode, uid}  = req.headers;
    //make dafult id if empty
    countrycode = (_.isEmpty(countrycode))? COUNTRY_CODES.Indonesia : _.toUpper(countrycode);
    let cc =  (countrycode == COUNTRY_CODES.Indonesia ? lANGUAGE_TYPE.Indonesia : lANGUAGE_TYPE.English); 
    let message = LANG[cc].GENERAL.SUCCESS;

    try {
        let toEmail         = req.body.email;
        let name            = req.body.displayName;
        let certificate     = req.body.certificate;
        let title           = req.body.title;
        let subject         = Utils.stringReplace(LANG[cc].EVENT.EMAIL_CERTIFICATE.GET_CERTIFICATE,[title]);
        let emailContent    = 
        `<p>Dear ${name},
        <br/>
        <br/>${Utils.stringReplace(LANG[cc].EVENT.EMAIL_CERTIFICATE.THANKS_FOR_PARTICIPATE,[title])}
        <br/>Here is your e-certificate.<br/>
        <br/>Best wishes,<br/>D2D<br/><br/>
        </p>
        <center><a href="${certificate}" target="_blank"><button class="activation">DOWNLOAD CERTIFICATE</button></a></center>`;
        let body            = { toEmail, subject, emailContent, apps: 'd2d' };
        
        let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/email/send`, body)
        request.post(options, function(error, response, body) {
            if(error) {
                console.log(error);
                let err = new Error(error)
                err.code = 503;
                next(err);
            } else {
                let resp = responseCustom.v1(200, true, LANG[cc].EVENT.EMAIL_CERTIFICATE.SEND_CERTIFICATE, null)
                res.status(resp.status).send(resp)
            }
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: Email-Certificate :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        message         = LANG[cc].GENERAL.SOMETHING_WENT_WRONG;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : message)
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = EventController;
