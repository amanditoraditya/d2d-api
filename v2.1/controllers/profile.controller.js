const parseMessageCode  = require('../../utils/helpers/parse-messagecode');
const requestOptions    = require('../../utils/request-options');
const responseCustom    = require('../../utils/response-custom');
const utils             = require('../../utils/utils');
const UserSVC           = require('../../utils/user-service');
const ContentSVC        = require('../../utils/content-service');
const User              = require('../../models/User');
const UserSkpTmp        = require('../../models/UserSkpTmp');
const VProfileList      = require('../../models/VProfileList');
const sequelize         = SEQUELIZE_INIT
const request           = require('request');
const _                 = require('lodash');
const axios             = require('axios');
const RchatSvc              = require('../../utils/RchatSvc');
const co = require('co');
const fs = require('fs');
const oss = require('ali-oss');
const jwt               = require('jsonwebtoken');
const moment = require('moment');

const ProfileController = {}
const log               = 'Profile controller';

ProfileController.getProfile  = async (req, res, next) => {
    console.log(`├── ${log} :: getProfile`);

    // console.log(req.useragent);

    try {
        let npaoremailoruid  = req.params.npaoremailoruid;
        let uid         = req.headers.uid;
        let where       = null
        let cekNum      = await utils.cekNumber(npaoremailoruid)

        if(npaoremailoruid.includes('@')) {
            where = {where: {email: npaoremailoruid}}
        } else if(cekNum) {
            where = {where: {npa_idi: npaoremailoruid}}
        } else {
            where = {where: {uid: npaoremailoruid}}
        }

        // /** save to user services */
        // let usersProfile        = await User.findOne({ where: { uid: npaoremailoruid }});
        // let doctorFullName = usersProfile.name;
        // let userFirstname   = (usersProfile.name).split(' ').slice(0, -1).join(' ');
        // let userLastname   = (usersProfile.name).split(' ').slice(-1).join(' ');
        // userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
        // userLastname = (userFirstname ===  userLastname) ? '' : userLastname;
        // let born_date = (_.isEmpty(usersProfile.born_date)) ? "1000-01-01" : usersProfile.born_date;

        // let clinic          = [];
        // let education       = [];

        // if (!_.isEmpty(usersProfile.clinic_location_1)) {
        //     clinic.push(usersProfile.clinic_location_1);
        // }
        // if (!_.isEmpty(usersProfile.clinic_location_2)) {
        //     clinic.push(usersProfile.clinic_location_2);
        // }
        // if (!_.isEmpty(usersProfile.clinic_location_3)) {
        //     clinic.push(usersProfile.clinic_location_3);
        // }

        // if (!_.isEmpty(usersProfile.education_1)) {
        //     education.push(usersProfile.education_1);
        // }
        // if (!_.isEmpty(usersProfile.education_2)) {
        //     education.push(usersProfile.education_2);
        // }
        // if (!_.isEmpty(usersProfile.education_3)) {
        //     education.push(usersProfile.education_3);
        // }

        // let userAddress     = { 
        //     home        : usersProfile.home_location, 
        //     clinic      : clinic
        // }

        // let specialist = await utils.getUserSpecialist(uid);
        // let userOtherInfo   = {
        //     pns         : usersProfile.pns,
        //     npa_idi     : usersProfile.npa_idi,
        //     str_no      : '',
        //     education   : education,
        //     specialist  : specialist
        // }

        // let userData      = {
        //     uid             : uid,
        //     // nik              : '',
        //     firstname       : userFirstname,
        //     lastname        : userLastname,
        //     display_name    : doctorFullName,
        //     email           : usersProfile.email,
        //     phone_number    : usersProfile.phone,
        //     photo           : !_.isEmpty(usersProfile.photo) ? `https://static.d2d.co.id/image/profile/${usersProfile.photo}` : '',
        //     type            : 'doctor',
        //     birthday        : born_date,
        //     address         : JSON.stringify(userAddress),
        //     other_info      : JSON.stringify(userOtherInfo), 
        // }
            
        // console.log(`Profile : Save userInfo Before Hit getprofile`);
        // let userInfo = await UserSVC.saveUser(usersProfile.uid, userData);
        // console.log(`Profile : Save userInfo => datetime ( ${moment().format('DD MM YYYY h:mm:ss SSSSSSS')} ) ${userInfo.acknowledge}`);
        // /** end save to user services */

        let query = `
                SELECT 
                    u.uid, u.name, u.email, u.npa_idi, u.phone, u.born_date, 
                    u.education_1, u.education_2, u.education_3, u.pns, u.home_location, u.photo, u.country_code,
                    u.clinic_location_1, u.clinic_location_2, u.clinic_location_3,
                    sbs.subscription_id, sbs.subscription_name, sbs.subscription_description,
                    sps.spesialization_id, sps.spesialization_name, sps.spesialization_description,
                    sbsps.subspesialization_id, sbsps.subspesialization_name, sbsps.subspesialization_description,
                    sbsps.subspesialization_parent_id, ea.event_attendee
                FROM user AS u
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(competence_id) AS subscription_id,
                            GROUP_CONCAT(competence_name) AS subscription_name,
                            GROUP_CONCAT(competence_description) AS subscription_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'subscription'
                    ) AS sbs ON sbs.uid = u.uid
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(competence_id) AS spesialization_id,
                            GROUP_CONCAT(competence_name) AS spesialization_name,
                            GROUP_CONCAT(competence_description) AS spesialization_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'spesialization'
                    ) AS sps ON sps.uid = u.uid
                    LEFT JOIN (
                        SELECT 
                            GROUP_CONCAT(parent_id) AS subspesialization_parent_id,
                            GROUP_CONCAT(competence_id) AS subspesialization_id,
                            GROUP_CONCAT(competence_name) AS subspesialization_name,
                            GROUP_CONCAT(competence_description) AS subspesialization_description,
                            uid
                        FROM user_competence
                        WHERE uid = '${npaoremailoruid}' AND type = 'subspesialization'
                    ) AS sbsps ON sbsps.uid = u.uid
                    LEFT JOIN (
                        SELECT GROUP_CONCAT(DISTINCT event_slug_hash) AS event_attendee, u.uid FROM event_attendee AS ea
                            LEFT JOIN user as u ON u.email = ea.email OR u.npa_idi = ea.npa_idi
                        WHERE u.uid = '${npaoremailoruid}' AND ea.type = 'materi'
                    ) AS ea ON ea.uid = u.uid
                WHERE u.uid = '${npaoremailoruid}'
        `;

        sequelize.query(query).spread(async (results, metadata) => {
            if (!_.isEmpty(results)) {
                let profile = results[0]
                
                let sb_id           = (profile.subscription_id == null || typeof profile.subscription_id == undefined) ?  [] : profile.subscription_id.split(',');
                let sb_name         = (profile.subscription_name == null || typeof profile.subscription_name == undefined) ?  [] : profile.subscription_name.split(',');
                let sb_description  = (profile.subscription_description == null || typeof profile.subscription_description == undefined) ?  [] : profile.subscription_description.split(',');
                let sp_id           = (profile.spesialization_id == null || typeof profile.spesialization_id == undefined) ?  [] : profile.spesialization_id.split(',');
                let sp_name         = (profile.spesialization_name == null || typeof profile.spesialization_name == undefined) ?  [] : profile.spesialization_name.split(',');
                let sp_description  = (profile.spesialization_description == null || typeof profile.spesialization_description == undefined) ?  [] : profile.spesialization_description.split(',');
                let spp_id          = (profile.subspesialization_id == null || typeof profile.subspesialization_id == undefined) ?  [] : profile.subspesialization_id.split(',');
                let spp_name        = (profile.subspesialization_name == null || typeof profile.subspesialization_name == undefined) ?  [] : profile.subspesialization_name.split(',');
                let spp_description = (profile.subspesialization_description == null || typeof profile.subspesialization_description == undefined) ?  [] : profile.subspesialization_description.split(',');
                let spp_parent_id   = (profile.subspesialization_parent_id == null || typeof profile.subspesialization_parent_id == undefined) ?  [] : profile.subspesialization_parent_id.split(',');
                let ea              = (profile.event_attendee == null || typeof profile.event_attendee == undefined) ?  [] : profile.event_attendee.split(',');
                let sb              = [];
                let sp              = [];
    
                for(let i = 0; i < sb_id.length; i++) {
                    sb.push({
                        id: sb_id[i],
                        title: sb_name[i],
                        description: sb_description[i]
                    });
                }

                for(let i = 0; i < sp_id.length; i++) {
                    let sublist = [];
                    for(let j = 0; j < spp_id.length; j++) {
                        if(spp_parent_id[j] == sp_id[i]) {
                            sublist.push({
                                id: spp_id[j],
                                title: spp_name[j],
                                description: spp_description[j]
                            })
                        }
                    }
                    sp.push({
                        id: sp_id[i],
                        title: sp_name[i],
                        description: sp_description[i],
                        subSpecialistList: sublist
                    });
                }

                let pns         = (profile.pns == 'true') ? true : (profile.pns == 'false') ? false : null;
                let npaIDI      = (_.isEmpty(profile.npa_idi) || profile.npa_idi == 'null') ? '' : profile.npa_idi;
                let born_date   = _.toLower(moment(new Date(profile.born_date)).format('YYYY-MM-DD'));
                    born_date   = born_date == "invalid date" || born_date == 'null' ? "" : born_date;

                let hasil = {
                    id: profile.id,
                    uid: profile.uid,
                    name: profile.name,
                    email: profile.email,
                    npa_idi: npaIDI,
                    phone: profile.phone,
                    born_date: born_date,
                    education_1: profile.education_1,
                    education_2: profile.education_2,
                    education_3: profile.education_3,
                    pns,
                    home_location: profile.home_location,
                    country_code: profile.country_code,
                    clinic_location_1: profile.clinic_location_1,
                    clinic_location_2: profile.clinic_location_2,
                    clinic_location_3: profile.clinic_location_3,
                    photo: profile.photo,
                    spesialization: sp,
                    subscription: sb,
                    event_attendee: ea,
                    webinar: [],
                    counting: null
                };

                let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/user-activity?get=bookmark,download`, null)
                request.get(options, async function(error, response, body) {
                    if(!error) {
                        // console.log('ini body dari user activity :', body);
                        let result      = JSON.parse(body)
                        hasil.counting  = result.result.data[0];
                        if (hasil.event_attendee.length > 0) {
                            let options = requestOptions(req.headers, uid, `${CONFIG.URL_CONTENT}/event/webinar`, {events: `'${hasil.event_attendee.join("','")}'` });
                            request.get(options, (error, response, body) => {
                                if (!error) {
                                    let webinar = JSON.parse(body);
                                    webinar     = webinar.result.data;
                                    
                                    hasil.webinar = _.isEmpty(webinar) ? [] : webinar.split(',');
                                }

                                let resp        = responseCustom.v1(200, true, null, hasil);
                                res.status(resp.status).send(resp)
                            })
                        }
                        else
                        {
                            let resp        = responseCustom.v1(200, true, null, hasil);
                            res.status(resp.status).send(resp)
                        }
                    } else {
                        console.log(error);
                        let err = new Error(error)
                        err.code = 503;
                        next(err);
                    }
                })
            } else {
                let resp = await responseCustom.v1(404, false, 'User ID Not Found', null);
                res.status(resp.status).send(resp)
            }
        }).catch(error => {
            console.log(error);
                let err = new Error(error)
                err.code = 488;
                next(err);
        })

        // VProfileList.findOne(where).then(async profile => {
            
        // }).catch(error => {
        //     throw error
        // });
    } catch(error) {
        console.log(`│  ├── ${log} :: getProfile :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

// updated by rahmat1929 06 march 2020
ProfileController.updateProfile  = async (req, res, next) => {
    try {
        let {
            uid, name, npa_idi, email, phone, 
            home_location, pns, born_date,
            education_1, education_2, education_3,
            clinic_location_1, clinic_location_2, clinic_location_3,
            // for device info
            app_version, device_unique_id, fcm_token, device_platform, 
            system_version, os_build_version, device_brand
        } = req.body;
        
        // check if invalid born date
        born_date   = _.toLower(born_date);
        born_date   = born_date == "invalid date" || born_date == 'null' || _.isEmpty(born_date) ? null : born_date;
        born_date   = moment(new Date(born_date)).format('YYYY-MM-DD');
        name        = name.replace("’", "'");

        let result      = {};
        let message     = '';
        let user        = await User.findAll({ where: { uid }});
            user        = user.length > 0 ? user[0] : {};
        
        console.log('Profile : Save');
        let emptyNpa    = (npa_idi == null || npa_idi == 'null' || _.isEmpty(npa_idi));
        let onlyNumber  = false;

        if (emptyNpa == false) {
            onlyNumber  = npa_idi.match(/\d/g).length == npa_idi.length;
        }
        
        if (!_.isEmpty(user) && user.npa_idi !== npa_idi && emptyNpa !== true && onlyNumber === true) {
            // checking npa idi is already used
            let existUser = await User.findAll({ where: { npa_idi }})
            
            if (existUser.length > 0) {
                result  = existUser[0];
                message = `NPA IDI anda [${npa_idi}] sudah digunakan di aplikasi ini.`;
                npa_idi = null;
            } else {
                // checking to content -> data doctor
                let options = { uid, apps_id: 1 };
                let doctor  = await ContentSVC.checkIdi(name, npa_idi, options);
                
                if (_.isEmpty(doctor)) {
                    message = 'NPA IDI dan nama yang anda masukkan tidak ditemukan';
                    npa_idi = null;
                } else {
                    name = doctor.name
                }
            }
        }

        let data = { uid, name, npa_idi, email, phone, 
            home_location, pns, born_date,
            education_1, education_2, education_3,
            clinic_location_1, clinic_location_2, clinic_location_3
        };
        
        // checking pns is not set
        if (_.isEmpty(pns) || pns === 'null') {
            delete data.pns;
        }
        // checking npa idi not set
        if (_.isEmpty(npa_idi) || onlyNumber === false) {
            delete data.npa_idi;
        }

        // update profile
        let update  = await User.update(data, { where: { uid }});
        update  = update.length > 0 & update[0] === 1;
        
        // get profile
        user        = await User.findAll({ where: { uid }});
        user        = user.length > 0 ? user[0] : {};

        if (!_.isEmpty(user)) {
            // get user from user service
            let { result: dataUserInfo, acknowledge }      = await UserSVC.getUser(uid);

            user.npa_idi = _.isEmpty(user.npa_idi) ? '' : user.npa_idi;
            result  = user;
            
            if (onlyNumber === false) {
                if (emptyNpa === false) {
                    message     = onlyNumber == false ? `NPA IDI yang anda masukkan tidak sesuai.` : '';
                }
            }

            if (!_.isEmpty(data.npa_idi)) {
                message = update == true ? 'Profile berhasil diupdate' : message;
            }
            
            message = _.isEmpty(message) ? 'Profile berhasil diupdate' : message;

            // save device info
            let clinic          = [];
            let education       = [];

            if (!_.isEmpty(clinic_location_1)) {
                clinic.push(clinic_location_1);
            }
            if (!_.isEmpty(clinic_location_2)) {
                clinic.push(clinic_location_2);
            }
            if (!_.isEmpty(clinic_location_3)) {
                clinic.push(clinic_location_3);
            }

            if (!_.isEmpty(education_1)) {
                education.push(education_1);
            }
            if (!_.isEmpty(education_2)) {
                education.push(education_2);
            }
            if (!_.isEmpty(education_3)) {
                education.push(education_3);
            }

            let specialist      = await utils.getUserSpecialist(uid);

            let userAddress     = {};
            let userOtherInfo   = {};
            
            if (acknowledge == true && !_.isEmpty(dataUserInfo)) {
                if (!_.isEmpty(dataUserInfo.other_info)) {
                    userOtherInfo   = JSON.parse(dataUserInfo.other_info);
                    userOtherInfo['pns']         = pns ? pns : user.pns;
                    userOtherInfo['npa_idi']     = npa_idi ? npa_idi : user.npa_idi;
                    userOtherInfo['str_no']      = '';
                    userOtherInfo['education']   = education;
                    userOtherInfo['specialist']  = specialist;
                }

                if (!_.isEmpty(dataUserInfo.address)) {
                    userAddress     = JSON.parse(dataUserInfo.address);
                    userAddress['home']          = home_location;
                    userAddress['clinic']        = clinic;
                }
            }

            let doctorFullName = user.name;
            let userFirstname   = (user.name).split(' ').slice(0, -1).join(' ');
            let userLastname   = (user.name).split(' ').slice(-1).join(' ');
            userFirstname = (_.isEmpty(userFirstname) || userFirstname == '') ? userLastname : userFirstname;
            userLastname = (userFirstname ===  userLastname) ? '' : userLastname;
            console.log(`Profile : Save userInfo Before Define Data`);
            let userDevice      = {
                uid             : user.uid,
                // nik              : '',
                firstname       : userFirstname,
                lastname        : userLastname,
                display_name    : doctorFullName,
                email           : user.email,
                phone_number    : user.phone,
                photo           : !_.isEmpty(user.photo) ? `https://static.d2d.co.id/image/profile/${user.photo}` : '',
                birthday        : born_date,
                // gender           : 'M',
                // religion         : 'Islam',
                address         : JSON.stringify(userAddress),
                other_info      : JSON.stringify(userOtherInfo), 
                type            : 'doctor',
                //---------------------------------------------- for device -----------------------------------------------
                app_version     : app_version,
                device_unique_id: device_unique_id,
                fcm_token       : fcm_token,
                device_platform : device_platform,
                system_version  : system_version,
                os_build_version: os_build_version,
                device_brand    : device_brand
            }
            
            console.log(`Profile : Save userInfo Before Hit`);
            let userInfo = await UserSVC.saveUser(user.uid, userDevice);
            console.log(`Profile : Save userInfo => datetime ( ${moment().format('DD MM YYYY h:mm:ss SSSSSSS')} ) ${userInfo.acknowledge}`);

            // Update Chat services
            // await RchatSvc.updateName(req);
        }
        
        resp = responseCustom.v1(200, true, message, result);
        res.status(resp.status).send(resp)
    } catch (error) {
        console.log(`│  ├── ${log} :: updateProfile :: error`);
        console.log('Profile : error', error)
        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

ProfileController.picture  = async (req, res, next) => {
    console.log(`├── ${log} :: picture`);

    try {
        let type = `image`;
        let content = `profile`;
        let uid = req.body.uid;

        const client = new oss({
            region: 'oss-ap-southeast-5',
            accessKeyId: 'LTAINk4kOx5TLJT6',
            accessKeySecret: 'tvERdMC2mGQhHpcas0bcmmJJvZqu0O',
            bucket: 'd2doss'
        })
        
        co(function* () {
            let arr = req.file.originalname.split('.');
            let ext = arr[(arr.length-1)];
            let result = yield client.put(`${type}/${content}/${req.file.filename}.${ext}`, `../../fileupload/${req.file.filename}`);
            fs.unlink(`../../fileupload/${req.file.filename}`, function(err) {
                if(err) {
                    throw err;
                } else {
                    User.update(
                        {photo: `${req.file.filename}.${ext}`},
                        {where: {uid}}
                    ).then(() => {
                        res.json(result)
                    }).catch(error => {
                        console.log(error);
                        let err = new Error(error)
                        err.code = 500;
                        next(err);
                    })
                }
            });
        }).catch(function (err) {
            res.json(err);
        });
    } catch(error) {
        console.log(`│  ├── ${log} :: picture :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

module.exports = ProfileController;