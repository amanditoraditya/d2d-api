const _                     = require('lodash');
const jwt                   = require('jsonwebtoken');
const moment                = require('moment');
const parseMessageCode      = require('../../utils/helpers/parse-messagecode');
const responseCustom        = require('../../utils/response-custom');
const UserNotification      = require('../../models/UserNotification');
const UserDevice            = require('../../models/UserDevice');
const NotifService          = require('../../utils/notif-service');
const {TYPES_NOTIFICATION}  = require('../../utils/helpers/enum');
const sequelize             = require('sequelize');

const NotificationController   = {}
const log               = 'Notification controller';

NotificationController.notification = async (req, res, next) => {
    console.log(`├── ${log} :: notification`);

    try {
        let uid         = req.params.uid;

        UserNotification.findAll(
            {where: {uid, type: 'cme_certificate'}},
            {order: ['created', 'DESC']}
        ).then(async data => {
            let hasil = await sort(data);
            
            async function sort(result) {
                let obj = [];

                for(let i = 0; i < result.length; i++) {
                    if(result[i].type == `cme_certificate`) {
                        result[i].slug = `https://d2doss.oss-ap-southeast-5.aliyuncs.com/certificate/${result[i].uid}/${result[i].slug}`;
                    }

                    await obj.push(result[i]);
                }

                return await obj;
            }

            let resp = responseCustom.v1(200, true, null, hasil)
            res.status(resp.status).send(resp)
        }).catch(error => {
            console.log(error)
            let err   = new Error(error)
            err.code  = 488
            next(err)
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: notification :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

NotificationController.read = async (req, res, next) => {
    console.log(`├── ${log} :: read`);

    try {
        let uid     = req.body.uid;
        let title   = req.body.title;
        let slug    = req.body.slug.split('/');

        let newSlug = (slug.length > 1) ? slug[(slug.length-1)] : slug[0];

        UserNotification.update(
            {status: 'read'},
            {where: {uid, title, slug: newSlug}}
        ).then(data => {
            let resp = responseCustom.v1(200, true, null, data)
            res.status(resp.status).send(resp)
        }).catch(error => {
            console.log(error)
            let err   = new Error(error)
            err.code  = 488
            next(err)
        })
    } catch(error) {
        console.log(`│  ├── ${log} :: notification :: error`);
        console.log(error)

        let code        = typeof error.sqlMessage != 'undefined' ? 488 : 400;
        let msg         = parseMessageCode(code, code == 488 ? error.sqlMessage : 'Something went wrong')
        let err         = new Error(msg)

        err.code        = code
        err.database    = code == 488 ? { message: msg } : null;
        next(err);
    }
}

NotificationController.getAll = async (req, res, next) => {
    let result          = [];
    let statusCode      = 200;
    let acknowledge     = true;
    let message         = 'Success';

    try {
        let { page, limit, type } = req.query;
        let { user_id }     = jwt.decode(req.headers.authorization);
        let offset          = ((page == 1 ? 0 : page - 1) * limit);
        // user_id         = '3vILTa37aPfMa7cDKPJQpJNewaZ2'; // rahmat1929@gmail.com
        let condition       = { uid: user_id };

        if (!_.isEmpty(type) && !_.isUndefined(type)) {
            _.assign(condition, { type });
        } else {
            _.assign(condition, { 
                type: {
                    [sequelize.Op.not]: 'pmm_patient'
                }
            })
        }

        let userNotif   = await UserNotification.findAll({
            where: condition,
            order: [['created', 'DESC']],
            limit: _.toInteger(limit),
            offset: _.toInteger(offset),
        });

        _.assign(condition, { status: 'unread' });
        let userNotifCounter   = await UserNotification.count({
            col: 'id',
            where: condition,
            distinct: true,
        });
        
        console.log(`│  ├── ${log} :: getAll :: success`);
            result  = {
                docs: userNotif,
                unread: userNotifCounter,
            };
    } catch (error) {
        console.log(`│  ├── ${log} :: getAll :: error`);
        console.log(error);
        
        statusCode  = 400;
        acknowledge = false;
        message     = 'Something went wrong, went get notifications.';
    }
    
    let resp = responseCustom.v1(statusCode, acknowledge, message, result)
    res.status(resp.status).send(resp)
}


NotificationController.save = async (req, res, next) => {
    let result          = [];
    let statusCode      = 200;
    let acknowledge     = true;
    let message         = 'Success';

    try {
        const EnumType = TYPES_NOTIFICATION;
        
        let messages    = [];
        let { user_id, uid } = jwt.decode(req.headers.authorization);
        let { id, title, body, type, data, status } = req.body;

        console.log("request Body From TD pairing PMM : ", req.body);
        console.log("request headers From TD pairing PMM : ", req.headers);

        user_id = _.isUndefined(user_id) ? uid : user_id;

        // user_id = '9KNqx8FG85bh1O7jfg9hvoWDemc2'; // rahmat1929@gmail.com

        if (_.isEmpty(title) || _.isUndefined(title)) {
            acknowledge = false;
            messages.push('title is required');
        }

        if (_.isEmpty(body) || _.isUndefined(body)) {
            acknowledge = false;
            messages.push('body is required');
        }

        if (_.isEmpty(data) || _.isUndefined(data)) {
            acknowledge = false;
            messages.push('data is required');
        }

        if (_.isEmpty(type) || _.isUndefined(type)) {
            acknowledge = false;
            messages.push('type is required');
        }

        if (!_.includes(EnumType, type)) {
            acknowledge = false;
            messages.push('type is not valid');
        }

        if (acknowledge == false) {
            console.log(`│  ├── ${log} :: save :: error validation`);

            statusCode  = 400;
            message     = messages.join(' and ');
        } else {
            data    = JSON.parse(data);
            status  = _.isEmpty(status) ? 'unread' : status;

            _.assign(data, { title, body, type });

            let userNotif   = { title, body, type, data: JSON.stringify(data), status, uid: user_id } 
            
            if (!_.isUndefined(`${id}`) && id) {
                console.log(`│  ├── ${log} :: save :: update notification`);

                // update data user notification
                _.assign(userNotif, { updated: moment().format('YYYY-MM-DD kk:mm:ss')} ); 

                let saveNotif   = await UserNotification.update(userNotif, { where: { id: id, uid: user_id }, raw: true });
                    result      = saveNotif.dataValues;
            } else {
                console.log(`│  ├── ${log} :: save :: save notification`);

                // save data user notification
                let saveNotif   = await UserNotification.build(userNotif).save();
                    result      = saveNotif.dataValues;

                // get FCM token
                let userDevices = await UserDevice.findAll({ where: { uid: user_id } });
                let devices     = [];
                userDevices.forEach((val, i) => {
                    if (!_.isEmpty(val.dataValues.device_token)) {
                        devices.push(val.dataValues.device_token);
                    }
                });

                if (!_.isEmpty(devices)) {
                    let target  = { type: "tokens", "value": devices };
                    let payload = {
                        priority    : 10,
                        title       : title,
                        body        : body,
                        data        : JSON.stringify(data),
                        target      : JSON.stringify(target)
                    };
                    console.log("Payload Notifikation : ", payload);
                    // sending notification
                    await NotifService.sendNotification(user_id, payload);
                    console.log(`│  ├── ${log} :: notification :: send notification`);
                }
            }
        }

    } catch (error) {
        console.log(`│  ├── ${log} :: save :: error`);
        console.log(error);

        statusCode  = 400;
        acknowledge = false;
        message     = 'Something went wrong, went create notifications.';
    }

    let resp = responseCustom.v1(200, acknowledge, message, result)
    res.status(resp.status).send(resp)
}

module.exports = NotificationController;