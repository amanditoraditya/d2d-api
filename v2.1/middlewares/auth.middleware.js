const jwt       = require('jsonwebtoken');
const admin     = require('firebase-admin');
const request   = require('request');
const { validationResult } = require('express-validator/check');
const utils             = require('../../utils/utils');
const parseValidator    = require('../../utils/helpers/parse-validator');
const AuthMiddleware    = {}
const log = 'Auth middleware';

AuthMiddleware.validation = (req, res, next) => {
    let errors  = validationResult(req);

    if (!errors.isEmpty())
    {
        console.log(`├── ${log} :: error validation`);
        
        let validator   = parseValidator(errors.array());
        let message     = typeof validator.messages == 'string' ? validator.messages : 'Something went wrong';
        let result      = typeof validator.result != 'undefined' ? validator.result : 'Something went wrong';
        
        res.status(200).send({
            message: message,
            acknowledge: false,
            result: {},
            errors: {
                validation: result
            }
        });
    }
    else {
        next();
    }
}

AuthMiddleware.authentication = async (req, res, next) => {
    try {
        let { authorization } = req.headers;
        console.log(`- url            : ${req.originalUrl}`)
        console.log(`- authorization  : ${authorization}`)

        let access = false
        let decode = jwt.decode(authorization);

        if (typeof decode.user_id != 'undefined') {
            req.headers.uid = decode.user_id;
            let token   = await admin.auth().verifyIdToken(authorization);
                access  = token ? true : false;
        } else {
            let user    = await utils.getUser(decode.user_id);
            console.log(user)
                access  = user ? true : false;
        }

        if (access === true) {
            next();
        }
    } catch (error) {
        let err         = new Error(error.message);
        err.code        = 403
        
        next(err);
    }
}

module.exports = AuthMiddleware;