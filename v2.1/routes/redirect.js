const router                    = require('express').Router()
const { header, body }          = require('express-validator/check')
const { AuthMiddleware }        = require('../middlewares')
const { RedirectController }    = require('../controllers')
const multer                    = require('multer');
const upload                    = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .all('*', AuthMiddleware.authentication)
    .get('/:type/:slug/:slug_hash/:uid/:npaoremail?', RedirectController.redirect)
    .get('/temandiabetes/:patient_uid/:doctor_uid', RedirectController.temandiabetesGraph)
module.exports = router;