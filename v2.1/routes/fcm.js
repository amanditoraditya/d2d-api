const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { FcmController }   = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .post('/:type', AuthMiddleware.authentication, FcmController.fcm)

module.exports = router;