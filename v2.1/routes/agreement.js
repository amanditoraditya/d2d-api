const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { AgreementController }     = require('../controllers')
const multer                = require('multer');

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/active', AuthMiddleware.authentication, AgreementController.getAgreement)
    .post('/', AuthMiddleware.authentication, AgreementController.submitAgreement)
    .post('/cancel', AuthMiddleware.authentication, AgreementController.cancelAgreement)

module.exports = router;