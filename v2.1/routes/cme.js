const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { CmeController }     = require('../controllers')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .all('*', AuthMiddleware.authentication)
    .get('/quiz-list/:type/:uid', CmeController.quizList)
    .get('/quiz-detail/:quizid', CmeController.quizDetail)
    .post('/submit-offline', upload.single('filename'), CmeController.submitOffline)
    .post('/reset-skp', CmeController.resetSkp)
    .post('/email-certificate', CmeController.emailCertificate)
    .post('/submit-quiz', CmeController.submitQuiz)

module.exports = router;