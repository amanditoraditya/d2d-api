const router                = require('express').Router();
const { AuthMiddleware }    = require('../middlewares');
const { PatientController } = require('../controllers');

router
    .all('*', AuthMiddleware.authentication)
    .get('/', PatientController.getAll)
    .get('/:uid', PatientController.get)

module.exports = router;