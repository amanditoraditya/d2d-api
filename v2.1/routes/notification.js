const router = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { NotificationController } = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .all('*', AuthMiddleware.authentication)
    .get('/', NotificationController.getAll)
    .post('/', NotificationController.save)
    .get('/:uid', NotificationController.notification)
    .post('/read', NotificationController.read)

module.exports = router;