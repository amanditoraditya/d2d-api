const router                = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { ProfileController } = require('../controllers')
const multer                = require('multer');
const upload                = multer({ dest: '../../fileupload/' });

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .all('*', AuthMiddleware.authentication)
    .get('/:npaoremailoruid', ProfileController.getProfile)
    .put('/', ProfileController.updateProfile)
    .post('/picture', upload.single('filename'), ProfileController.picture)

module.exports = router;