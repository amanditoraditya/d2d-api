const router = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { ObatatozController } = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/', AuthMiddleware.authentication, ObatatozController.read)
    .get('/detail', AuthMiddleware.authentication, ObatatozController.detail)
    .get('/banner', AuthMiddleware.authentication, ObatatozController.banner)

module.exports = router;