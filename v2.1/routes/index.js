const fs    = require('fs')
const path  = require('path')

module.exports = (app) => {
    const currDir = process.cwd()

    fs.readdirSync(currDir + '/v2.1/routes').forEach((file) => {
        let extname     = path.extname(file)
        let basename    = path.basename(file, extname)
        
        if (~file.indexOf('.js') && basename !== 'index') {
            app.use('/v2.1/' + basename, require(currDir + '/v2.1/routes/' + file))
        }
    })
}
