const router = require('express').Router()
const { header, body }      = require('express-validator/check')
const { AuthMiddleware }    = require('../middlewares')
const { RchatController } = require('../controllers')

const Validate = {
    tokenValidation: [
        header('authorization').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value'),
        header('platform').not().isEmpty().withMessage('undefined parameter').withMessage('invalid value')
    ]
}

router
    .get('/testRchat', AuthMiddleware.authentication, RchatController.testing)
    .post('/auth/login', AuthMiddleware.authentication, RchatController.login)
    .post('/messages/send/:email', AuthMiddleware.authentication, RchatController.sendMessage)
    .post('/profile/avatar', AuthMiddleware.authentication, RchatController.uploadAvatar)
    .get('/profile/avatar/:username', AuthMiddleware.authentication, RchatController.getAvatar)
    .post('/groups/create', AuthMiddleware.authentication, RchatController.createGroup)

module.exports = router;